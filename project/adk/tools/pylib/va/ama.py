from csr.dev.fw.firmware_component import FirmwareComponent
from csr.dev.model import interface


class Ama(FirmwareComponent):
    """
    Ama addon analysis class
    """

    def __init__(self, env, core, parent=None):
        FirmwareComponent.__init__(self, env, core, parent=parent)

        self._ama = None
        try:
            self._ama = env.econst.voice_ui_provider_ama
        except AttributeError:
            raise self.NotDetected("AMA doesnt't seem to be enabled in this build")

        self._env = env

    @property
    def enabled(self):
        return self._ama is not None

    @property
    def active(self):
        return self._env.vars['active_va'].deref.voice_assistant.deref.va_provider.value == self._env.econst.voice_ui_provider_ama

    def _generate_report_body_elements(self):

        content = []  # Construct a list of Renderables

        # Groups are a way of providing titles
        grp = interface.Group("Ama status")
        tbl = interface.Table(["Included in build", "Active"])
        tbl.add_row([
            "Y" if self.enabled else "N",
            "Y" if self.active else "N"
        ])
        grp.append(tbl)

        content.append(grp)

        return content
