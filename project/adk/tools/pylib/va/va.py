############################################################################
#
# Copyright (c) 2020 Qualcomm Technologies International, Ltd.
#
############################################################################

from csr.dev.fw.firmware_component import FirmwareComponent
from csr.dev.model import interface

from .gaa import Gaa
from .ama import Ama


class VA(FirmwareComponent):
    """
    Container for analysis classes representing generic parts of CAA applications
    """
    def __init__(self, env, core, parent=None):
        FirmwareComponent.__init__(self, env, core, parent=parent)

    @property
    def subcomponents(self):
        return {
            "gaa": "_gaa",
            "ama": "_ama",
        }

    @property
    def gaa(self):
        try:
            self._gaa
        except AttributeError:
            self._gaa = Gaa(self.env, self._core, self)
        return self._gaa

    @property
    def ama(self):
        try:
            self._ama
        except AttributeError:
            self._ama = Ama(self.env, self._core, self)
        return self._ama

    @property
    def current_state(self):
        return self.env.cus['kymera_va.c'].localvars['current_state']

    def _generate_report_body_elements(self):

        content = []  # Construct a list of Renderables

        # Groups are a way of providing titles
        grp = interface.Group("Va state")
        state = interface.Text("Current state: {}".format(self.current_state))
        tbl = interface.Table(["Previous state", "Event", "New State"])
        tbl.add_row([
            "Test prev", "Test transition", "Test new"
        ])
        grp.append(state)
        grp.append(tbl)

        content.append(grp)

        return content
