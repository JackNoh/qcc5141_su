/*!
\copyright  Copyright (c) 2020 Qualcomm Technologies International, Ltd.\n
            All Rights Reserved.\n
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\file
\brief      Handset service config
*/

#include "handset_service_config.h"
#include "handset_service.h"
#include "connection_manager.h"

const handset_service_config_t handset_service_multipoint_config = 
{
    /* Two connections supported */
    .max_bredr_connections = 2,
    /* Two ACL reconnection attempts per supported connection */
    .acl_connect_attempt_limit = 2,
    /* Page timeout 5 seconds*/
    .page_timeout = MS_TO_BT_SLOTS(5 * MS_PER_SEC)
};

const handset_service_config_t handset_service_singlepoint_config = 
{
    /* One connection supported */
    .max_bredr_connections = 1,
    /* Three ACL reconnection attempts per supported connection */
    .acl_connect_attempt_limit = 3,
    /* Page timeout 10 seconds*/
    .page_timeout = MS_TO_BT_SLOTS(10 * MS_PER_SEC)
};

handset_service_config_t handset_service_config;

void HandsetServiceConfig_Init(void)
{
#ifdef ENABLE_MULTIPOINT
    HandsetService_Configure(handset_service_multipoint_config);
#else
    HandsetService_Configure(handset_service_singlepoint_config);
#endif
}

bool HandsetService_Configure(handset_service_config_t config)
{
    if(config.max_bredr_connections > HANDSET_SERVICE_MAX_PERMITTED_BREDR_CONNECTIONS)
        return FALSE;
    
    if(config.max_bredr_connections < 1)
        return FALSE;
    
    handset_service_config = config;
    
    ConManager_SetPageTimeout(config.page_timeout);

    return TRUE;
}
