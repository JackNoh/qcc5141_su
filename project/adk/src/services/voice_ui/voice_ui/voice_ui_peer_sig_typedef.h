/*!
    \copyright Copyright (c) 2021 Qualcomm Technologies International, Ltd.
        All Rights Reserved.
        Qualcomm Technologies International, Ltd. Confidential and Proprietary.
    \version %%version
    \file 
    \brief The voice_ui_peer_sig c type definitions. This file is generated by C:\Users\gaudio\Documents\1_HBS-FP8_r253.1____20210517_Patch\adk\tools/packages/typegen/typegen.py.
*/

#ifndef _VOICE_UI_PEER_SIG_TYPEDEF_H__
#define _VOICE_UI_PEER_SIG_TYPEDEF_H__

#include <csrtypes.h>

/*! Voice Assistant Provider update sent from Primary to Secondary */
typedef struct 
{
    /*! Voice Assistant Provider */
    uint8 va_provider;
    /*! Reboot required */
    uint8 reboot;
} voice_ui_selected_va_provider_t;

/*! Voice Assistant device flags */
typedef struct 
{
    /*! Voice Assistant device flags */
    uint8 flags;
} voice_ui_device_flags_t;

/*! Voice Assistant locale */
typedef struct 
{
    /*! Voice Assistant locale */
    uint8 locale[4];
} voice_ui_locale_t;

#endif /* _VOICE_UI_PEER_SIG_TYPEDEF_H__ */

