/*!
\copyright  Copyright (c) 2021 Qualcomm Technologies International, Ltd.
            All Rights Reserved.
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\file
\brief      Voice UI Multipoint
*/

#include "voice_ui_multipoint.h"
#ifdef ENABLE_MULTIPOINT
#include "voice_ui_container.h"
#include "av.h"
#include <logging.h>

static audio_source_t previously_routed_source = audio_source_none;

static const bdaddr * voiceUi_GetVaSourceBdAddress(void)
{
    const voice_ui_handle_t * va = VoiceUi_GetActiveVa();

    if (va)
    {
        return va->voice_assistant->GetBtAddress();
    }

    return NULL;
}

static const bdaddr * voiceUi_GetA2dpSourceBdAddress(audio_source_t source)
{
    const avInstanceTaskData *av = Av_GetInstanceForHandsetSource(source);

    if (av)
    {
        return &(av->bd_addr);
    }

    return NULL;
}

static bool voiceUi_IsValidBtAddress(const bdaddr *addr)
{
    return addr && !BdaddrIsZero(addr);
}

static bool voiceUi_IsSameBtAdress(const bdaddr *addr_1, const bdaddr *addr_2)
{
    if (voiceUi_IsValidBtAddress(addr_1) && voiceUi_IsValidBtAddress(addr_2))
    {
        return BdaddrIsSame(addr_1, addr_2);
    }

    return FALSE;
}

static audio_source_t voiceUi_GetVaAudioSource(void)
{
    const bdaddr *va_addr = voiceUi_GetVaSourceBdAddress();
    audio_source_t source;
    for_all_a2dp_audio_sources(source)
    {
        if (voiceUi_IsSameBtAdress(va_addr, voiceUi_GetA2dpSourceBdAddress(source)))
        {
            return source;
        }
    }

    return audio_source_none;
}
#endif // ENABLE_MULTIPOINT

void VoiceUi_PauseNonVaSources(void)
{
#ifdef ENABLE_MULTIPOINT
    audio_source_t routed_source = AudioSources_GetRoutedSource();
    if ((routed_source != audio_source_none) && (voiceUi_GetVaAudioSource() != routed_source))
    {
        DEBUG_LOG_DEBUG("VoiceUi_PauseNonVaSources: routed_source enum:audio_source_t:%d", routed_source);
        AudioSources_PauseAll();
        previously_routed_source = routed_source;
    }
#endif
}

void VoiceUi_ResumeNonVaSource(void)
{
#ifdef ENABLE_MULTIPOINT
    if (previously_routed_source != audio_source_none)
    {
        DEBUG_LOG_DEBUG("VoiceUi_ResumeNonVaSource: resume enum:audio_source_t:%d", previously_routed_source);
        AudioSources_PauseAll();
        AudioSources_Play(previously_routed_source);
    }

    previously_routed_source = audio_source_none;
#endif
}
