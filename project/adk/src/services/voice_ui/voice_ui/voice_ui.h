/*!
\copyright  Copyright (c) 2019 - 2020 Qualcomm Technologies International, Ltd.
            All Rights Reserved.
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\file       voice_ui.h
\defgroup   voice_ui 
\ingroup    services
\brief      A component responsible for controlling voice assistants.

Responsibilities:
- Voice Ui control (like A way to start/stop voice capture as a result of some user action) 
  Notifications to be sent to the Application raised by the VA.

The Voice Ui uses  \ref audio_domain Audio domain and \ref bt_domain BT domain.

*/

#ifndef VOICE_UI_H_
#define VOICE_UI_H_

#include "domain_message.h"
#include <bt_device.h>
#include <message.h>
#include <operator.h>

/* Length of locale string, not including terminator */
#define VOICE_UI_LOCALE_LENGTH (5)

/*! \brief Specify whether to reboot the device following an update to the VA provider  */
#define NO_REBOOT_AFTER_VA_CHANGE          FALSE

/*! \brief Voice UI Provider contexts */
typedef enum
{
    context_voice_ui_default = 0
} voice_ui_context_t;


/*! \brief Messages sent by the voice ui service to interested clients. */
typedef enum
{
    VOICE_UI_IDLE   = VOICE_UI_SERVICE_MESSAGE_BASE,
    VOICE_UI_CONNECTED,
    VOICE_UI_ACTIVE,
    VOICE_UI_MIC_OPEN,
    VOICE_UI_CAPTURE_START,
    VOICE_UI_CAPTURE_END,
    VOICE_UI_MIC_CLOSE,
    VOICE_UI_DISCONNECTED,

    /*! This must be the final message */
    VOICE_UI_SERVICE_MESSAGE_END
} voice_ui_msg_id_t;

/*! \brief voice ui service message. */
typedef struct
{
    void *vui_handle;
} voice_ui_msg_t;

/*\{*/

/*! \brief Initialise the voice ui service

    \param init_task Unused
 */
bool VoiceUi_Init(Task init_task);

/*! \brief Notify clients of the Voice UI Service

    \param msg The message to notify the clients of the Voice UI with
 */
void VoiceUi_Notify(voice_ui_msg_id_t msg);

/*! \brief Gets the A2DP status of the currently active voice assistant

    \return TRUE if VA A2DP is in streaming state, FALSE otherwise
 */
bool VoiceUi_IsVoiceAssistantA2dpStreamActive(void);

/*! \brief Enables the wake word detection feature
 */
void VoiceUi_EnableWakeWordDetection(void);

/*! \brief Disables the wake word detection feature
 */
void VoiceUi_DisableWakeWordDetection(void);

/*! \brief Establishes if the wake word feature is enabled

    \return TRUE if the wake word feature is enabled, FALSE otherwise
 */
bool VoiceUi_WakeWordDetectionEnabled(void);

/*! \brief Gets a single voice assistant flag setting from the Device database
    \param flag The flag to get
    \return The value of the flag, TRUE or FALSE
 */
bool VoiceUi_GetDeviceFlag(device_va_flag_t flag);

/*! \brief Gets the voice assistant flags from the Device database
    \return The flags bitmap
 */
device_va_flag_t VoiceUi_GetDeviceFlags(void);

/*! \brief Stores a single voice assistant flag setting in the Device database
    \param flag The flag to set
    \param value The value for the flag, TRUE or FALSE
 */
void VoiceUi_SetDeviceFlag(device_va_flag_t flags, bool value);

/*! \brief Stores thee voice assistant flags in the Device database
    \param flags The flags bitmap
 */
void VoiceUi_SetDeviceFlags(device_va_flag_t flags);

/*! \brief Stores the wake word enabled setting in the Device database
 */
void VoiceUi_SetWuwEnable(bool enable);

/*! \brief Stores the voice assistant locale setting in the Device database.
    \param locale The locale name.  A locale name is made from an ISO-639
           Language Code and an ISO-3166 Country Code separated by a hyphen.
 */
void VoiceUi_SetLocale(const char *locale);

/*! \brief Gets the voice assistant locale setting from the Device database.
    \param locale Buffer to hold the locale name.  The buffer must have
           space for six characters to include the string and terminator.
 */
void VoiceUi_GetLocale(char *locale);

/*\}*/

#endif /* VOICE_UI_H_ */
