/*!
\copyright  Copyright (c) 2021 Qualcomm Technologies International, Ltd.
            All Rights Reserved.
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\file
\brief      Private APIs for Voice UI Multipoint
*/

#ifndef VOICE_UI_MULTIPOINT_H_
#define VOICE_UI_MULTIPOINT_H_

/*! \brief Pause all a2dp sources apart from the one with the active VA
*/
void VoiceUi_PauseNonVaSources(void);

/*! \brief Resume the a2dp source previously paused
*/
void VoiceUi_ResumeNonVaSource(void);

#endif // VOICE_UI_MULTIPOINT_H_
