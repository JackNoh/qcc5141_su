/****************************************************************************
Copyright (c) 2015 - 2020 Qualcomm Technologies International, Ltd.


FILE NAME
    dm_ble_advetising.h

DESCRIPTION
    This file contains the prototypes for BLE DM advertising msgs from Bluestack .

NOTES

*/

#ifndef DISABLE_BLE
#ifndef DM_BLE_ADVERTISING_H
#define DM_BLE_ADVERTISING_H

/****************************************************************************
NAME
    connectionHandleDmBleAdvParamUpdateInd

DESCRIPTION
    Handle the DM_ULP_ADV_PARAM_UPDATE_IND message from Bluestack and pass it
    on to the appliction that initialised the CL.

RETURNS
    void
*/
void connectionHandleDmBleAdvParamUpdateInd(
        const DM_ULP_ADV_PARAM_UPDATE_IND_T *ind
        );


/****************************************************************************
NAME
    connectionHandleDmBleSetAdvertiseEnableReq

DESCRIPTION
    This function will initiate an Advertising Enable request.

RETURNS
   void
*/
void connectionHandleDmBleSetAdvertiseEnableReq(
        connectionBleScanAdState *state,
        const CL_INTERNAL_DM_BLE_SET_ADVERTISE_ENABLE_REQ_T *req
        );


/****************************************************************************
NAME
    connectionHandleDmBleSetAdvertiseEnableCfm

DESCRIPTION
    Handle the DM_HCI_ULP_SET_ADVERTISE_ENABLE_CFM from Bluestack.

RETURNS
   void
*/
void connectionHandleDmBleSetAdvertiseEnableCfm(
        connectionBleScanAdState *state,
        const DM_HCI_ULP_SET_ADVERTISE_ENABLE_CFM_T *cfm
        );

/*****************************************************************************
 *                  Extended Advertising functions                           *
 *****************************************************************************/

/****************************************************************************
NAME
    connectionHandleDmBleRegisterAppAdvSetReq

DESCRIPTION
    This function will initiate an Extended Advertising Register App Adv Set request.

RETURNS
   void
*/
void connectionHandleDmBleRegisterAppAdvSetReq(
        connectionDmExtAdvState *state,
        const CL_INTERNAL_DM_BLE_EXT_ADV_REGISTER_APP_ADV_SET_REQ_T *req
        );

/****************************************************************************
NAME
    connectionHandleDmBleExtAdvRegisterAppAdvSetCfm

DESCRIPTION
    Handle the DM_ULP_EXT_ADV_REGISTER_APP_ADV_SET_CFM from Bluestack.

RETURNS
   void
*/
void connectionHandleDmBleExtAdvRegisterAppAdvSetCfm(connectionDmExtAdvState *state,
        const DM_ULP_EXT_ADV_REGISTER_APP_ADV_SET_CFM_T *cfm
        );

/****************************************************************************
NAME
    connectionHandleDmBleExtAdvertiseEnableCfm

DESCRIPTION
    Handle the DM_ULP_EXT_ADV_ENABLE_CFM from Bluestack.

RETURNS
   void
*/
void connectionHandleDmBleExtAdvertiseEnableCfm(
        connectionDmExtAdvState *state,
        const DM_ULP_EXT_ADV_ENABLE_CFM_T *cfm
        );

/****************************************************************************
NAME
    connectionHandleDmBleUnregisterAppAdvSetReq

DESCRIPTION
    This function will initiate an Extended Advertising Unregister App Adv Set request.

RETURNS
   void
*/
void connectionHandleDmBleUnregisterAppAdvSetReq(
        connectionDmExtAdvState *state,
        const CL_INTERNAL_DM_BLE_EXT_ADV_UNREGISTER_APP_ADV_SET_REQ_T *req
        );

/****************************************************************************
NAME
    connectionHandleDmBleExtAdvUnregisterAppAdvSetCfm

DESCRIPTION
    Handle the DM_ULP_EXT_ADV_UNREGISTER_APP_ADV_SET_CFM from Bluestack.

RETURNS
   void
*/
void connectionHandleDmBleExtAdvUnregisterAppAdvSetCfm(connectionDmExtAdvState *state,
        const DM_ULP_EXT_ADV_UNREGISTER_APP_ADV_SET_CFM_T *cfm
        );

/****************************************************************************
NAME
    connectionHandleDmBleExtAdvertiseEnableReq

DESCRIPTION
    This function will initiate an Extended Advertising Enable request.

RETURNS
   void
*/
void connectionHandleDmBleExtAdvertiseEnableReq(connectionDmExtAdvState *state,
        const CL_INTERNAL_DM_BLE_EXT_ADVERTISE_ENABLE_REQ_T *req
        );

/****************************************************************************
NAME
    connectionHandleDmBleExtAdvSetParamsReq

DESCRIPTION
    This function will initiate an Extended Advertising Set Parameters request.

RETURNS
   void
*/
void connectionHandleDmBleExtAdvSetParamsReq(
        connectionDmExtAdvState *state,
        const CL_INTERNAL_DM_BLE_EXT_ADV_SET_PARAMS_REQ_T *req
        );

/****************************************************************************
NAME
    connectionHandleDmBleSetExtAdvertisingParamsCfm

DESCRIPTION
    Handle the DM_ULP_EXT_ADV_SET_PARAMS_CFM from Bluestack.

RETURNS
   void
*/
void connectionHandleDmBleExtAdvSetParamsCfm(
        connectionDmExtAdvState *state,
        const DM_ULP_EXT_ADV_SET_PARAMS_CFM_T *cfm
        );

/****************************************************************************
NAME
    connectionHandleDmBleExtAdvSetDataReq

DESCRIPTION
    This function will initiate an Extended Advertising Set Data request.

RETURNS
   void
*/
void connectionHandleDmBleExtAdvSetDataReq(
        connectionDmExtAdvState *state,
        const CL_INTERNAL_DM_BLE_EXT_ADV_SET_DATA_REQ_T *req
        );

/****************************************************************************
NAME
    connectionHandleDmBleSetExtAdvertisingDataCfm

DESCRIPTION
    Handle the DM_HCI_ULP_EXT_ADV_SET_DATA_CFM from Bluestack.

RETURNS
   void
*/
void connectionHandleDmBleExtAdvSetDataCfm(
        connectionDmExtAdvState *state,
        const DM_HCI_ULP_EXT_ADV_SET_DATA_CFM_T *cfm
        );

/****************************************************************************
NAME
    connectionHandleDmBleExtAdvSetScanRespDataReq

DESCRIPTION
    This function will initiate an Extended Advertising Set Scan Response Data request.

RETURNS
   void
*/
void connectionHandleDmBleExtAdvSetScanRespDataReq(
        connectionDmExtAdvState *state,
        const CL_INTERNAL_DM_BLE_EXT_ADV_SET_SCAN_RESP_DATA_REQ_T *req
        );

/****************************************************************************
NAME
    connectionHandleDmBleSetExtAdvScanRespDataCfm

DESCRIPTION
    Handles status of Extended Advertising Scan Response Data Request

RETURNS
    void
*/


void connectionHandleDmBleExtAdvSetScanRespDataCfm(
        connectionDmExtAdvState *state,
        const DM_HCI_ULP_EXT_ADV_SET_SCAN_RESP_DATA_CFM_T *cfm
        );


/*****************************************************************************
 *                  Periodic Advertising functions                           *
 *****************************************************************************/

/****************************************************************************
NAME
    connectionHandleDmBlePerAdvSetParamsReq

DESCRIPTION
    This function will initiate an Periodic Advertising Set Parameters request.

RETURNS
   void
*/
void connectionHandleDmBlePerAdvSetParamsReq(
        connectionDmPerAdvState *state,
        const CL_INTERNAL_DM_BLE_PER_ADV_SET_PARAMS_REQ_T *req
        );

/****************************************************************************
NAME
    connectionHandleDmBlePerAdvSetParamsCfm

DESCRIPTION
    Handles status of Periodic Advertising Parameters Request

RETURNS
    void
*/

void connectionHandleDmBlePerAdvSetParamsCfm(connectionDmPerAdvState *state,
                            const DM_ULP_PERIODIC_ADV_SET_PARAMS_CFM_T *cfm);

/****************************************************************************
NAME
    connectionHandleDmBlePerAdvSetDataReq

DESCRIPTION
    This function will initiate an Periodic Advertising Set Data request.

RETURNS
   void
*/
void connectionHandleDmBlePerAdvSetDataReq(
        connectionDmPerAdvState *state,
        const CL_INTERNAL_DM_BLE_PER_ADV_SET_DATA_REQ_T *req
        );

/****************************************************************************
NAME
    connectionHandleDmBlePerAdvSetDataCfm

DESCRIPTION
    Handles status of Periodic Advertising Data Request

RETURNS
    void
*/
void connectionHandleDmBlePerAdvSetDataCfm(connectionDmPerAdvState *state,
        const DM_HCI_ULP_PERIODIC_ADV_SET_DATA_CFM_T *cfm
        );

/****************************************************************************
NAME
    connectionHandleDmBlePerAdvStartReq

DESCRIPTION
    This function will initiate an Periodic Advertising Start request.

RETURNS
   void
*/
void connectionHandleDmBlePerAdvStartReq(
        connectionDmPerAdvState *state,
        const CL_INTERNAL_DM_BLE_PER_ADV_START_REQ_T *req
        );

/****************************************************************************
NAME
    connectionHandleDmBlePerAdvStartCfm

DESCRIPTION
    Handles status of Periodic Advertising Start Request

RETURNS
    void
*/

void connectionHandleDmBlePerAdvStartCfm(connectionDmPerAdvState *state,
                            const DM_ULP_PERIODIC_ADV_START_CFM_T *cfm);

/****************************************************************************
NAME
    connectionHandleDmBlePerAdvStopReq

DESCRIPTION
    This function will initiate an Periodic Advertising Stop request.

RETURNS
   void
*/
void connectionHandleDmBlePerAdvStopReq(
        connectionDmPerAdvState *state,
        const CL_INTERNAL_DM_BLE_PER_ADV_STOP_REQ_T *req
        );

/****************************************************************************
NAME
    connectionHandleDmBlePerAdvStopCfm

DESCRIPTION
    Handles status of Periodic Advertising Stop Request

RETURNS
    void
*/

void connectionHandleDmBlePerAdvStopCfm(connectionDmPerAdvState *state,
                            const DM_ULP_PERIODIC_ADV_STOP_CFM_T *cfm);

/****************************************************************************
NAME
    connectionHandleDmBlePerAdvSetTransferReq

DESCRIPTION
    This function will initiate an Periodic Advertising Set Transfer request.

RETURNS
   void
*/
void connectionHandleDmBlePerAdvSetTransferReq(
        connectionDmPerAdvState *state,
        const CL_INTERNAL_DM_BLE_PER_ADV_SET_TRANSFER_REQ_T *req
        );

/****************************************************************************
NAME
    connectionHandleDmBlePerAdvSetTransferCfm

DESCRIPTION
    Handles status of Periodic Advertising Set Transfer Request

RETURNS
    void
*/

void connectionHandleDmBlePerAdvSetTransferCfm(connectionDmPerAdvState *state,
                            const DM_ULP_PERIODIC_ADV_SET_TRANSFER_CFM_T *cfm);

#endif /* DM_BLE_ADVERTISING_H */
#endif /* DISABLE_BLE */
