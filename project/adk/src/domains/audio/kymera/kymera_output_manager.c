/*!
\copyright  Copyright (c) 2020 Qualcomm Technologies International, Ltd.
            All Rights Reserved.
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\brief      Kymera manager of the output chain
*/

#include "kymera_output_if.h"
#include "kymera_output_private.h"
#include "kymera_private.h"
#include "kymera.h"

typedef const output_registry_entry_t* registry_entry_t;

typedef struct
{
    uint8 length;
    registry_entry_t *entries;
} registry_t;

static struct
{
    registry_t registry;
    output_users_t ready_users;
    output_users_t connected_users;
    output_users_t custom_chain_creator;
    kymera_output_chain_config current_chain_config;
    unsigned current_chain_is_voice:1;
} state =
{
    .registry = {0, NULL},
    .ready_users = output_user_none,
    .connected_users = output_user_none,
    .custom_chain_creator = output_user_none,
    .current_chain_config = {0},
    .current_chain_is_voice = FALSE,
};

static const registry_entry_t kymera_GetUserRegistryEntry(output_users_t user)
{
    for(unsigned i = 0; i < state.registry.length; i++)
    {
        if (state.registry.entries[i]->user == user)
            return state.registry.entries[i];
    }

    return NULL;
}

static const registry_entry_t kymera_AssertValidUserRegistryEntry(output_users_t user)
{
    registry_entry_t entry = kymera_GetUserRegistryEntry(user);
    PanicFalse(entry != NULL);
    return entry;
}

static const output_custom_chain_creator_t * kymera_GetCustomChainCreatorInfo(output_users_t user)
{
    const output_custom_chain_creator_t *creator = kymera_AssertValidUserRegistryEntry(user)->creator;
    PanicFalse(creator != NULL);
    return creator;
}

static output_connection_t kymera_GetUserConnectionType(output_users_t user)
{
    return kymera_AssertValidUserRegistryEntry(user)->connection;
}

static bool kymera_IsUserAssumedChainCompatible(output_users_t user)
{
    return kymera_AssertValidUserRegistryEntry(user)->assume_chain_compatibility;
}

static bool kymera_IsScoUser(output_users_t user)
{
    return kymera_AssertValidUserRegistryEntry(user)->is_voice;
}

static bool kymera_IsRegisteredUser(output_users_t user)
{
    return (kymera_GetUserRegistryEntry(user) != NULL);
}

static bool kymera_IsCustomChainUser(output_users_t user)
{
    registry_entry_t entry = kymera_GetUserRegistryEntry(user);
    if (entry && entry->creator)
        return TRUE;

    return FALSE;
}

static void kymera_RegisterUser(const output_registry_entry_t *user_info)
{
    state.registry.entries = PanicNull(realloc(state.registry.entries, (state.registry.length + 1) * sizeof(*state.registry.entries)));
    state.registry.entries[state.registry.length] = user_info;
    state.registry.length++;
}

static void kymera_CreateOutputChain(const kymera_output_chain_config *config, bool is_voice)
{
    DEBUG_LOG("kymera_CreateOutputChain");
    state.current_chain_config = *config;
    state.current_chain_is_voice = is_voice;
    KymeraGetTaskData()->output_rate = config->rate;
    KymeraOutput_CreateChainWithConfig(config);
    KymeraOutput_SetOperatorUcids(is_voice);
    appKymeraExternalAmpControl(TRUE);
}

static void kymera_DestroyOutputChain(void)
{
    DEBUG_LOG("kymera_DestroyOutputChain");
    memset(&state.current_chain_config, 0, sizeof(state.current_chain_config));
    state.current_chain_is_voice = FALSE;
    appKymeraExternalAmpControl(FALSE);
    KymeraOutput_DestroyChain();
    KymeraGetTaskData()->output_rate = 0;
}

static bool kymera_ConnectToOutputChain(output_users_t user, const output_source_t *sources)
{
    bool status = TRUE;
    output_connection_t connection = kymera_GetUserConnectionType(user);

    if (state.custom_chain_creator)
        status = kymera_GetCustomChainCreatorInfo(state.custom_chain_creator)->OutputConnect(connection, sources);
    else
    {
        switch(connection)
        {
            case output_connection_mono:
                PanicFalse(KymeraOutput_ConnectInput(sources->mono, EPR_SINK_MIXER_MAIN_IN));
                break;
            case output_connection_aux:
                PanicFalse(KymeraOutput_ConnectInput(sources->aux, EPR_VOLUME_AUX));
                break;
            case output_connection_stereo:
                PanicFalse(KymeraOutput_ConnectInput(sources->stereo.left, EPR_SINK_STEREO_MIXER_L));
                PanicFalse(KymeraOutput_ConnectInput(sources->stereo.right, EPR_SINK_STEREO_MIXER_R));
                break;
            default:
                Panic();
                break;
        }
    }

    return status;
}

static void kymera_DisconnectFromOutputChain(output_users_t user)
{
    output_connection_t connection = kymera_GetUserConnectionType(user);

    if (state.custom_chain_creator)
        kymera_GetCustomChainCreatorInfo(state.custom_chain_creator)->OutputDisconnect(connection);
    else
    {
        switch(connection)
        {
            case output_connection_mono:
                KymeraOutput_DisconnectInput(EPR_SINK_MIXER_MAIN_IN);
                break;
            case output_connection_aux:
                KymeraOutput_DisconnectInput(EPR_VOLUME_AUX);
                break;
            case output_connection_stereo:
                KymeraOutput_DisconnectInput(EPR_SINK_STEREO_MIXER_L);
                KymeraOutput_DisconnectInput(EPR_SINK_STEREO_MIXER_R);
                break;
            default:
                Panic();
                break;
        }
    }
}

static bool kymera_IsCurrentChainCompatible(const kymera_output_chain_config *config, bool is_voice)
{
    return (memcmp(config, &state.current_chain_config, sizeof(*config)) == 0) && (is_voice == state.current_chain_is_voice);
}

static bool kymera_IsConnectedUser(output_users_t user)
{
    return (state.connected_users & user) != output_user_none;
}

static bool kymera_IsCurrentCreator(output_users_t user)
{
    // Can only have one custom chain creator
    return state.custom_chain_creator == user;
}

static output_users_t kymera_GetCurrentUsers(void)
{
    return state.ready_users | state.connected_users | state.custom_chain_creator;
}

static bool kymera_IsCurrentUser(output_users_t user)
{
    return (kymera_GetCurrentUsers() & user) != output_user_none;
}

static bool kymera_NoCurrentUsers(void)
{
    return kymera_GetCurrentUsers() == output_user_none;
}

static bool kymera_DisconnectUsers(output_users_t users)
{
    const registry_entry_t *entries = state.registry.entries;
    bool status = TRUE;

    for(unsigned i = 0; i < state.registry.length; i++)
    {
        if ((entries[i]->user & users) && kymera_IsCurrentUser(entries[i]->user))
        {
            if (entries[i]->callbacks && entries[i]->callbacks->OutputDisconnectRequest &&
                entries[i]->callbacks->OutputDisconnectRequest())
            {
                DEBUG_LOG("kymera_DisconnectUsers: Disconnected enum:output_users_t:%d", entries[i]->user);
                // If the user has accepted the disconnection request he should no longer be current
                PanicFalse(kymera_IsCurrentUser(entries[i]->user) == FALSE);
            }
            else
                status = FALSE;
        }
    }

    return status;
}

static bool kymera_PrepareUser(output_users_t user, const kymera_output_chain_config *chain_config)
{
    bool status = TRUE;

    if (kymera_NoCurrentUsers())
        kymera_CreateOutputChain(chain_config, kymera_IsScoUser(user));

    if ((kymera_IsUserAssumedChainCompatible(user) == FALSE) &&
        (kymera_IsCurrentChainCompatible(chain_config, kymera_IsScoUser(user)) == FALSE))
    {
        if (kymera_DisconnectUsers(kymera_GetCurrentUsers()))
            kymera_CreateOutputChain(chain_config, kymera_IsScoUser(user));
        else
            status = FALSE;
    }

    return status;
}

void Kymera_OutputRegister(const output_registry_entry_t *user_info)
{
    DEBUG_LOG("Kymera_OutputRegister: enum:output_users_t:%d", user_info->user);
    PanicFalse(kymera_IsRegisteredUser(user_info->user) == FALSE);

    if (user_info->creator)
    {
        PanicFalse(user_info->creator->OutputConnect != NULL);
        PanicFalse(user_info->creator->OutputDisconnect != NULL);
    }

    kymera_RegisterUser(user_info);
}

bool Kymera_OutputPrepare(output_users_t user, const kymera_output_chain_config *chain_config)
{
    if ((kymera_IsRegisteredUser(user) == FALSE) || kymera_IsCurrentUser(user))
        return FALSE;

    if (kymera_PrepareUser(user, chain_config) == FALSE)
        return FALSE;

    DEBUG_LOG("Kymera_OutputPrepare: enum:output_users_t:%d", user);
    state.ready_users |= user;
    return TRUE;
}

bool Kymera_OutputConnect(output_users_t user, const output_source_t *sources)
{
    if ((kymera_IsRegisteredUser(user) == FALSE) || kymera_IsConnectedUser(user) || kymera_NoCurrentUsers() ||
        (kymera_GetUserConnectionType(user) == output_connection_none))
        return FALSE;

    if (kymera_ConnectToOutputChain(user, sources) == FALSE)
        return FALSE;

    DEBUG_LOG("Kymera_OutputConnect: enum:output_users_t:%d", user);
    state.connected_users |= user;
    return TRUE;
}

void Kymera_OutputDisconnect(output_users_t user)
{
    if (kymera_IsCurrentUser(user))
    {
        bool disconnect = kymera_IsConnectedUser(user);
        state.connected_users &= ~user;
        state.ready_users &= ~user;

        DEBUG_LOG("Kymera_OutputDisconnect: enum:output_users_t:%d", user);

        if (kymera_NoCurrentUsers())
            kymera_DestroyOutputChain();
        else if (disconnect)
            kymera_DisconnectFromOutputChain(user);
    }
}

bool Kymera_OutputCreatingCustomChain(output_users_t user)
{
    if ((kymera_IsCustomChainUser(user) == FALSE) || kymera_IsCurrentUser(user))
        return FALSE;

    if (kymera_DisconnectUsers(kymera_GetCurrentUsers()) == FALSE)
        return FALSE;

    DEBUG_LOG("Kymera_OutputCreatingCustomChain: enum:output_users_t:%d", user);
    state.custom_chain_creator = user;
    return TRUE;
}

void Kymera_OutputDestroyingCustomChain(output_users_t user)
{
    if (kymera_IsCurrentCreator(user))
    {
        DEBUG_LOG("Kymera_OutputDestroyingCustomChain: enum:output_users_t:%d", user);
        // All other users must disconnect first in such a scenario
        PanicFalse(kymera_DisconnectUsers(kymera_GetCurrentUsers() & ~user));

        state.custom_chain_creator = output_user_none;
        state.connected_users &= ~user;
        state.ready_users &= ~user;
    }
}
