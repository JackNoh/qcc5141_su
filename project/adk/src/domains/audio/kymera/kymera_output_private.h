/*!
\copyright  Copyright (c) 2020 Qualcomm Technologies International, Ltd.
            All Rights Reserved.
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\brief      Kymera module with private audio output chain definitions
*/

#ifndef KYMERA_OUTPUT_PRIVATE_H
#define KYMERA_OUTPUT_PRIVATE_H

#include "kymera_output_chain_config.h"
#include "chain.h"

/*! \brief Create and configure the audio output chain.
    \param kick_period The kymera kick period.
    \param buffer_size The PCM buffer size.
    \param volume_in_db The start volume.

    \note If buffer_size is zero, the buffer size is not configured.
    \note Default parameters will be set for other parameters in the kymera_output_chain_config.
*/
void KymeraOutput_CreateChain(unsigned kick_period, unsigned buffer_size, int16 volume_in_db);

/*! \brief Create and configure the audio output chain.
    \param config The output chain configuration.
*/
void KymeraOutput_CreateChainWithConfig(const kymera_output_chain_config *config);

/*! \brief Stop and destroy the audio output chain.
*/
void KymeraOutput_DestroyChain(void);

/*! \brief Connect to audio output chains input.
    \param source The source to connect to the chain.
    \param input_role The audio output chain role that corresponds to the input to connect to.
    \return TRUE if connection successful else FALSE.
 */
bool KymeraOutput_ConnectInput(Source source, unsigned input_role);

/*! \brief Disconnect input of audio output chain.
    \param input_role The audio output chain role that corresponds to the input to disconnect.
 */
void KymeraOutput_DisconnectInput(unsigned input_role);

#endif /* KYMERA_OUTPUT_PRIVATE_H */
