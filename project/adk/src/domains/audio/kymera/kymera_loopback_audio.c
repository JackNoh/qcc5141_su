/*!
\copyright  Copyright (c) 2020  Qualcomm Technologies International, Ltd.
            All Rights Reserved.
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\brief      loop back from Mic input to DAC chain
*/

#include <kymera.h>
#include <kymera_output_private.h>
#include <kymera_private.h>

void appKymeraCreateLoopBackAudioChain(microphone_number_t mic_number, uint32 sample_rate)
{
    kymeraTaskData *theKymera = KymeraGetTaskData();

    DEBUG_LOG_FN_ENTRY("appKymeraCreateLoopBackAudioChain, mic %u, sample rate %u ",
                        mic_number, sample_rate);

    if (Kymera_IsIdle())
    {
        Source mic = Kymera_GetMicrophoneSource(mic_number, NULL, sample_rate, high_priority_user);
        /* Need to set up audio output chain to play microphone source from scratch */
        theKymera->output_rate = sample_rate;
#if defined( FEATURE_BLUECOM )
		KymeraOutput_CreateChain(KICK_PERIOD_SLOW, 0, 10);	
#else
        KymeraOutput_CreateChain(KICK_PERIOD_SLOW, 0, 0);	
#endif
        appKymeraExternalAmpControl(TRUE);
        KymeraOutput_ChainStart();
        Operator op = KymeraOutput_ChainGetOperatorByRole(OPR_VOLUME_CONTROL);
        OperatorsVolumeMute(op, FALSE);

        appKymeraSetState(KYMERA_STATE_MIC_LOOPBACK);
        KymeraOutput_ConnectInput(mic,EPR_VOLUME_AUX);
        /* Set output chain volume */
        op = KymeraOutput_ChainGetOperatorByRole(OPR_VOLUME_CONTROL);
#if defined( FEATURE_BLUECOM )
		OperatorsVolumeSetAuxGain(op, 90);
		//appKymeraSetVolume
#else
        OperatorsVolumeSetAuxGain(op, -10);
#endif
    }
}

void appKymeraDestroyLoopbackAudioChain(microphone_number_t mic_number)
{
    kymeraTaskData *theKymera = KymeraGetTaskData();

    DEBUG_LOG_FN_ENTRY("appKymeraDestroyLoopbackAudioChain, mic %u", mic_number);

    if (appKymeraGetState() == KYMERA_STATE_MIC_LOOPBACK)
    {
        Operator op = KymeraOutput_ChainGetOperatorByRole(OPR_VOLUME_CONTROL);
        uint16 volume = Kymera_VolDbToGain(0);
        OperatorsVolumeSetMainGain(op, volume);
        OperatorsVolumeMute(op, TRUE);

        Kymera_CloseMicrophone(mic_number, high_priority_user);
        appKymeraExternalAmpControl(FALSE);
        KymeraOutput_DestroyChain();
        theKymera->output_rate = 0;

        appKymeraSetState(KYMERA_STATE_IDLE);
    }
}
