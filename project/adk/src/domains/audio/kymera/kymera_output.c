/*!
\copyright  Copyright (c) 2020 Qualcomm Technologies International, Ltd.
            All Rights Reserved.
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\file       kymera_output.c
\brief      Kymera module with audio output chain definitions

*/

#include <audio_output.h>
#include <cap_id_prim.h>
#include <vmal.h>

#include "kymera_output_private.h"
#include "kymera_private.h"
#include "kymera_aec.h"
#include "kymera_config.h"
#include "kymera_source_sync.h"

/*To identify if a splitter is needed after output chain to actiavte second DAC path, when ANC is enabled in earbud application */
#if defined(ENABLE_ENHANCED_ANC) && !defined(INCLUDE_KYMERA_AEC) && !defined(ENABLE_ADAPTIVE_ANC)
#define INCLUDE_OUTPUT_SPLITTER
#endif

/*!@} */
#define DEFAULT_AEC_REF_TERMINAL_BUFFER_SIZE 15

#define SPLITTER_TERMINAL_IN_0       0
#define SPLITTER_TERMINAL_OUT_0      0
#define SPLITTER_TERMINAL_OUT_1      1

/*! \brief The chain output channels */
typedef enum
{
    output_right_channel = 0,
    output_left_channel
}output_channel_t;


static chain_endpoint_role_t kymeraOutput_GetOuputRole(output_channel_t is_left);

#ifdef INCLUDE_OUTPUT_SPLITTER
static Operator output_splitter;

static void kymeraOutput_CreateSplitter(void)
{
    output_splitter = (Operator)(VmalOperatorCreate(CAP_ID_SPLITTER));
}

static void kymeraOutput_ConfigureSplitter(void)
{
    OperatorsSplitterSetWorkingMode(output_splitter, splitter_mode_clone_input);
    OperatorsSplitterEnableSecondOutput(output_splitter, FALSE);
    OperatorsSplitterSetDataFormat(output_splitter, operator_data_format_pcm);
}

static void kymeraOutput_ConnectSplitter(void)
{
    Source output_channel_left = ChainGetOutput(KymeraGetTaskData()->chainu.output_vol_handle, kymeraOutput_GetOuputRole(output_left_channel));
    PanicFalse(StreamConnect(output_channel_left,
                             StreamSinkFromOperatorTerminal(output_splitter, SPLITTER_TERMINAL_IN_0)));
}

static void kymeraOutput_ConnectOutputChainToSplitter(void)
{
    kymeraOutput_CreateSplitter();
    kymeraOutput_ConfigureSplitter();
    kymeraOutput_ConnectSplitter();
}

static void kymeraOutput_ConnectSplitterToAudioSink(void)
{
    Source splitter_out_0 = StreamSourceFromOperatorTerminal(output_splitter, SPLITTER_TERMINAL_OUT_0);
    Source splitter_out_1 = StreamSourceFromOperatorTerminal(output_splitter, SPLITTER_TERMINAL_OUT_1);

    Kymera_ConnectOutputSource(splitter_out_0, splitter_out_1, KymeraGetTaskData()->output_rate);
}

static void kymeraOutput_StartSplitter(void)
{
    PanicZero(output_splitter);
    OperatorsSplitterEnableSecondOutput(output_splitter, TRUE);
    PanicFalse(OperatorStartMultiple(1, &output_splitter, NULL));
}

static void KymeraOutput_StopAndDisconnectSplitterFromAudioSink(void)
{
    Source splitter_out_0 = StreamSourceFromOperatorTerminal(output_splitter, SPLITTER_TERMINAL_OUT_0);
    Source splitter_out_1 = StreamSourceFromOperatorTerminal(output_splitter, SPLITTER_TERMINAL_OUT_1);

    /* Stop the splitter operator */
    PanicFalse(OperatorStopMultiple(1, &output_splitter, NULL));

    Kymera_DisconnectIfValid(splitter_out_0, NULL);
    Kymera_DisconnectIfValid(splitter_out_1, NULL);
}

static void KymeraOutput_DestroySplitter(void)
{
    OperatorsDestroy(&output_splitter, 1);
}

static void KymeraOutput_DisconnectAndDestroySplitter(void)
{
    KymeraOutput_StopAndDisconnectSplitterFromAudioSink();
    KymeraOutput_DestroySplitter();
}
#endif  /* INCLUDE_OUTPUT_SPLITTER */

static void kymeraOutput_ConnectChainToAudioSink(const connect_audio_output_t *params)
{
#if defined(INCLUDE_KYMERA_AEC) || defined(ENABLE_ADAPTIVE_ANC)
    aec_connect_audio_output_t aec_connect_params = {0};
    aec_audio_config_t config = {0};
    aec_connect_params.input_1 = params->input_1;
    aec_connect_params.input_2 = params->input_2;

    config.spk_sample_rate = KymeraGetTaskData()->output_rate;
    config.ttp_delay = VA_MIC_TTP_LATENCY;

#ifdef ENABLE_AEC_LEAKTHROUGH
    config.is_source_clock_same = TRUE; //Same clock source for speaker and mic path for AEC-leakthrough.
                                        //Should have no implication on normal aec operation.
    config.buffer_size = DEFAULT_AEC_REF_TERMINAL_BUFFER_SIZE;
#endif

    Kymera_ConnectAudioOutputToAec(&aec_connect_params, &config);
#elif defined(INCLUDE_OUTPUT_SPLITTER)
    UNUSED(params);
    kymeraOutput_ConnectSplitterToAudioSink();
#else
    Kymera_ConnectOutputSource(params->input_1,params->input_2,KymeraGetTaskData()->output_rate);
#endif
}

static void kymeraOutput_DisconnectChainFromAudioSink(const connect_audio_output_t *params)
{
#if defined(INCLUDE_KYMERA_AEC) || defined(ENABLE_ADAPTIVE_ANC)
    UNUSED(params);
    Kymera_DisconnectAudioOutputFromAec();
#else
#ifdef INCLUDE_OUTPUT_SPLITTER
    KymeraOutput_DisconnectAndDestroySplitter();
#endif
    Kymera_DisconnectIfValid(params->input_1, NULL);
    Kymera_DisconnectIfValid(params->input_2, NULL);
    AudioOutputDisconnect();
#endif
}

static chain_endpoint_role_t kymeraOutput_GetOuputRole(output_channel_t is_left)
{
    chain_endpoint_role_t output_role;

    if(appConfigOutputIsStereo())
    {
        output_role = is_left ? EPR_SOURCE_STEREO_OUTPUT_L : EPR_SOURCE_STEREO_OUTPUT_R;
    }
    else
    {
        output_role = EPR_SOURCE_MIXER_OUT;
    }

    return output_role;
}

static void kymeraOutput_ConfigureOutputChainOperatorsWithConfig(kymera_chain_handle_t chain,
                                    const kymera_output_chain_config *config, bool is_stereo)
{
    Operator volume_op;
    bool input_buffer_set = FALSE;

    if (config->source_sync_input_buffer_size_samples)
    {
        /* Not all chains have a separate latency buffer operator but if present
           then set the buffer size. Source Sync version X.X allows its input
           buffer size to be set, so chains using that version of source sync
           typically do not have a seperate latency buffer and the source sync
           input buffer size is set instead in appKymeraConfigureSourceSync(). */
        Operator op;
        if (GET_OP_FROM_CHAIN(op, chain, OPR_LATENCY_BUFFER))
        {
            OperatorsStandardSetBufferSize(op, config->source_sync_input_buffer_size_samples);
            /* Mark buffer size as done */
            input_buffer_set = TRUE;
        }
    }
    appKymeraConfigureSourceSync(chain, config, !input_buffer_set, is_stereo);

    volume_op = ChainGetOperatorByRole(chain, OPR_VOLUME_CONTROL);
    OperatorsStandardSetSampleRate(volume_op, config->rate);
    appKymeraSetVolume(chain, config->volume_in_db);
}

/*! \brief Create only the output audio chain */
static void kymeraOutput_CreateOutputChainOnly(const kymera_output_chain_config *config)
{
    kymera_chain_handle_t chain;

    DEBUG_LOG("kymeraOutput_CreateOutputChainOnly");

    /* Setting leakthrough mic path sample rate same as speaker path sampling rate */
    Kymera_setLeakthroughMicSampleRate(config->rate);

    /* Create chain */
    if (config->chain_exclude_basic_passthrough)
    {
        chain = ChainCreate(Kymera_GetChainConfigs()->chain_output_volume_no_passthrough_config);
    }
    else
    {
        chain = ChainCreate(Kymera_GetChainConfigs()->chain_output_volume_config);
    }
    KymeraGetTaskData()->chainu.output_vol_handle = chain;

    bool is_stereo = appConfigOutputIsStereo();	
    kymeraOutput_ConfigureOutputChainOperatorsWithConfig(chain, config, is_stereo);

    PanicFalse(OperatorsFrameworkSetKickPeriod(config->kick_period));

    ChainConnect(chain);

#ifdef INCLUDE_OUTPUT_SPLITTER
    kymeraOutput_ConnectOutputChainToSplitter();
#endif
}

/*! \brief Create only the audio output - e.g. the DACs */
static void KymeraOutput_CreateOutput(void)
{
    connect_audio_output_t connect_params = {0};

    DEBUG_LOG_FN_ENTRY("KymeraOutput_CreateOutput");

    connect_params.input_1 = ChainGetOutput(KymeraGetTaskData()->chainu.output_vol_handle, kymeraOutput_GetOuputRole(output_left_channel));

    if(appConfigOutputIsStereo())
    {
        connect_params.input_2 = ChainGetOutput(KymeraGetTaskData()->chainu.output_vol_handle, kymeraOutput_GetOuputRole(output_right_channel));
    }

    kymeraOutput_ConnectChainToAudioSink(&connect_params);
}

static Sink kymeraOutput_GetInput(unsigned input_role)
{
    return ChainGetInput(KymeraGetTaskData()->chainu.output_vol_handle, input_role);
}

void KymeraOutput_SetDefaultOutputChainConfig(kymera_output_chain_config *config,
                                                 uint32 rate, unsigned kick_period,
                                                 unsigned buffer_size, int16 volume_in_db)
{
    memset(config, 0, sizeof(*config));
    config->rate = rate;
    config->kick_period = kick_period;
    config->volume_in_db = volume_in_db;
    config->source_sync_input_buffer_size_samples = buffer_size;
    /* By default or for source sync version <=3.3 the output buffer needs to
       be able to hold at least SS_MAX_PERIOD worth  of audio (default = 2 *
       Kp), but be less than SS_MAX_LATENCY (5 * Kp). The recommendation is 2 Kp
       more than SS_MAX_PERIOD, so 4 * Kp. */
    appKymeraSetSourceSyncConfigOutputBufferSize(config, 4, 0);
}

void KymeraOutput_ConfigureChainOperators(kymera_chain_handle_t chain,
                                            uint32 sample_rate, unsigned kick_period,
                                            unsigned buffer_size, int16 volume_in_db,
                                            bool is_stereo)
{
    kymera_output_chain_config config;
    KymeraOutput_SetDefaultOutputChainConfig(&config, sample_rate, kick_period, buffer_size, volume_in_db);
    kymeraOutput_ConfigureOutputChainOperatorsWithConfig(chain, &config, is_stereo);
}

void KymeraOutput_CreateChainWithConfig(const kymera_output_chain_config *config)
{
    kymeraOutput_CreateOutputChainOnly(config);
    KymeraOutput_CreateOutput();
}

void KymeraOutput_CreateChain(unsigned kick_period, unsigned buffer_size, int16 volume_in_db)
{
    kymera_output_chain_config config;
    KymeraOutput_SetDefaultOutputChainConfig(&config, KymeraGetTaskData()->output_rate, kick_period, buffer_size, volume_in_db);
    kymeraOutput_CreateOutputChainOnly(&config);
    KymeraOutput_CreateOutput();
}

void KymeraOutput_DestroyChain(void)
{
    kymera_chain_handle_t chain = KymeraGetTaskData()->chainu.output_vol_handle;
    Kymera_setLeakthroughMicSampleRate(DEFAULT_MIC_RATE);

    DEBUG_LOG("KymeraOutput_DestroyChain");

    if (chain)
    {
#ifdef INCLUDE_MIRRORING
        /* Destroying the output chain powers-off the DSP, if another
        prompt or activity is pending, the DSP has to start all over again
        which takes a long time. Therefore prospectively power on the DSP
        before destroying the output chain, which will avoid an unneccesary
        power-off/on */
        appKymeraProspectiveDspPowerOn();
#endif
        connect_audio_output_t disconnect_params = {0};
        disconnect_params.input_1 = ChainGetOutput(chain, kymeraOutput_GetOuputRole(output_left_channel));
        disconnect_params.input_2 = ChainGetOutput(chain, kymeraOutput_GetOuputRole(output_right_channel));

        ChainStop(chain);
        kymeraOutput_DisconnectChainFromAudioSink(&disconnect_params);

        ChainDestroy(chain);
        KymeraGetTaskData()->chainu.output_vol_handle = NULL;
    }
}

void KymeraOutput_SetOperatorUcids(bool is_voice)
{
    /* Operators that have UCID set either reside in sco_handle or output_vol_handle
       which are both in the chainu union. */
    kymera_chain_handle_t chain = KymeraGetTaskData()->chainu.output_vol_handle;

    if (is_voice)
    {
        /* SCO/MIC forwarding RX chains do not have CVC send or receive */
        Kymera_SetOperatorUcid(chain, OPR_CVC_SEND, UCID_CVC_SEND);
        #ifdef INCLUDE_SPEAKER_EQ
            Kymera_SetOperatorUcid(chain, OPR_CVC_RECEIVE, UCID_CVC_RECEIVE_EQ);
        #else
            Kymera_SetOperatorUcid(chain, OPR_CVC_RECEIVE, UCID_CVC_RECEIVE);
        #endif
    }

    /* Operators common to SCO/A2DP chains */
    PanicFalse(Kymera_SetOperatorUcid(chain, OPR_VOLUME_CONTROL, UCID_VOLUME_CONTROL));
    PanicFalse(Kymera_SetOperatorUcid(chain, OPR_SOURCE_SYNC, UCID_SOURCE_SYNC));
}

Operator KymeraOutput_ChainGetOperatorByRole(unsigned operator_role)
{
    return ChainGetOperatorByRole(KymeraGetTaskData()->chainu.output_vol_handle, operator_role);
}

void  KymeraOutput_ChainStart(void)
{
#ifdef INCLUDE_OUTPUT_SPLITTER
    kymeraOutput_StartSplitter();
#endif
    ChainStart(KymeraGetTaskData()->chainu.output_vol_handle);
}

kymera_chain_handle_t KymeraOutput_GetOutputHandle(void)
{
    return KymeraGetTaskData()->chainu.output_vol_handle;
}

void KymeraOutput_SetVolume(int16 volume_in_db)
{
    appKymeraSetVolume(KymeraGetTaskData()->chainu.output_vol_handle, volume_in_db);
}

void KymeraOutput_SetMainVolume(int16 volume_in_db)
{
    appKymeraSetMainVolume(KymeraGetTaskData()->chainu.output_vol_handle, volume_in_db);
}

bool KymeraOutput_ConnectInput(Source source, unsigned input_role)
{
    return (StreamConnect(source, kymeraOutput_GetInput(input_role)) != NULL);
}

void KymeraOutput_DisconnectInput(unsigned input_role)
{
    StreamDisconnect(NULL, kymeraOutput_GetInput(input_role));
}
