/*!
\copyright  Copyright (c) 2020 Qualcomm Technologies International, Ltd.
            All Rights Reserved.
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\file
\defgroup   audio_router single_entity
\ingroup    audio_domain
\brief

*/

#include "single_entity_data.h"
#include "focus_audio_source.h"
#include "focus_voice_source.h"
#include "audio_router.h"
#include "logging.h"
#include "panic.h"

static unsigned source_type_priority[source_type_max] = {0};

static bool singleEntityData_IsActive(audio_router_data_t* source_data)
{
    bool active = FALSE;

    switch(source_data->state)
    {
        case audio_router_state_connecting:
        case audio_router_state_connected:
        case audio_router_state_disconnecting:
        case audio_router_state_disconnected_pending:
        case audio_router_state_disconnecting_no_connect:
        case audio_router_state_connected_pending:
            active = TRUE;
            break;

        case audio_router_state_disconnected:
        case audio_router_state_new_source:
            break;

        default:
            /* invalid state */
            Panic();
    }
    return active;
}


bool SingleEntityData_AreSourcesSame(generic_source_t source1, generic_source_t source2)
{
    bool same = FALSE;

    DEBUG_LOG_V_VERBOSE("SingleEntityData_AreSourcesSame src1=(%d,%d) src2=(%d,%d)",
                        source1.type, source1.u.voice, source2.type, source2.u.voice);

    if(source1.type == source2.type)
    {
        switch(source1.type)
        {
            case source_type_voice:
                same = (source1.u.voice == source2.u.voice);
                break;

            case source_type_audio:
                same = (source1.u.audio == source2.u.audio);
                break;

            default:
                break;
        }
    }
    return same;
}

static audio_router_data_t* singleEntityData_FindSourceInData(generic_source_t source)
{
    DEBUG_LOG_V_VERBOSE("singleEntityData_FindSourceInData src=(%d,%d)",
                        source.type, source.u.voice);

    audio_router_data_t* data = NULL;
    audio_router_data_iterator_t *iterator = AudioRouter_CreateDataIterator();

    while(NULL != (data = AudioRouter_GetNextEntry(iterator)))
    {
        if(data->present || singleEntityData_IsActive(data))
        {
            if(SingleEntityData_AreSourcesSame(data->source, source))
            {
                break;
            }
        }
    }
    AudioRouter_DestroyDataIterator(iterator);

    return data;
}

static bool singleEntityData_AddSourceToList(generic_source_t source)
{
    bool success = FALSE;
    audio_router_data_t* data;
    audio_router_data_iterator_t *iterator = AudioRouter_CreateDataIterator();

    DEBUG_LOG_V_VERBOSE("singleEntityData_AddSourceToList src=(%d,%d)",
                        source.type, source.u.voice);

    while(NULL != (data = AudioRouter_GetNextEntry(iterator)))
    {
        if(!(data->present || singleEntityData_IsActive(data)))
        {
            data->source = source;
            data->present = TRUE;
            data->state = audio_router_state_new_source;
            success = TRUE;
            break;
        }
    }

    AudioRouter_DestroyDataIterator(iterator);

    return success;
}

static bool singleEntityData_SetSourceNotPresent(generic_source_t source)
{
    audio_router_data_t* source_entry = singleEntityData_FindSourceInData(source);

    DEBUG_LOG_V_VERBOSE("singleEntityData_SetSourceNotPresent src=(%d,%d)",
                        source.type, source.u.voice);

    if((source_entry) && source_entry->present)
    {
        source_entry->present = FALSE;
        return TRUE;
    }
    return FALSE;
}

bool SingleEntityData_AddSource(generic_source_t source)
{
    DEBUG_LOG_VERBOSE("SingleEntityData_AddSource src=(%d,%d)",
                      source.type, source.u.voice);

    if(!singleEntityData_FindSourceInData(source))
    {
        return singleEntityData_AddSourceToList(source);
    }
    /* Already present */
    return TRUE;
}

bool SingleEntityData_RemoveSource(generic_source_t source)
{
    DEBUG_LOG_FN_ENTRY("SingleEntityData_RemoveSource");

    return singleEntityData_SetSourceNotPresent(source);
}

bool SingleEntityData_GetActiveSource(generic_source_t* source)
{
    bool success = FALSE;;
    audio_router_data_t* data = NULL;
    audio_router_data_iterator_t *iterator = AudioRouter_CreateDataIterator();

    DEBUG_LOG_FN_ENTRY("SingleEntityData_GetActiveSource");

    while(NULL != (data = AudioRouter_GetNextEntry(iterator)))
    {
        if(singleEntityData_IsActive(data))
        {
            *source = data->source;
            success = TRUE;
            break;
        }
    }
    AudioRouter_DestroyDataIterator(iterator);
    return success;
}

bool SingleEntityData_FindTransientSource(generic_source_t* source)
{
    bool success = FALSE;
    audio_router_data_t* data = NULL;
    audio_router_data_iterator_t *iterator = AudioRouter_CreateDataIterator();

    DEBUG_LOG_FN_ENTRY("SingleEntityData_FindTransientSource");

    while(!success && (NULL != (data = AudioRouter_GetNextEntry(iterator))))
    {
        switch(data->state)
        {
            case audio_router_state_connecting:
            case audio_router_state_disconnecting:
            case audio_router_state_disconnected_pending:
            case audio_router_state_disconnecting_no_connect:
            case audio_router_state_connected_pending:
                *source = data->source;
                success = TRUE;
                break;
            default:
                break;
        }
    }
    AudioRouter_DestroyDataIterator(iterator);
    return success;
}

unsigned SingleEntityData_GetSourceTypePriority(source_type_t source_type)
{
    unsigned priority = 0;

    DEBUG_LOG_FN_ENTRY("SingleEntityData_GetSourceTypePriority");

    if(source_type < source_type_max)
    {
        priority = source_type_priority[source_type];
    }
    return priority;
}

bool SingleEntityData_SetSourceTypePriority(source_type_t source_type, unsigned priority)
{
    bool success = FALSE;

    DEBUG_LOG_FN_ENTRY("SingleEntityData_SetSourceTypePriority");

    if(source_type < source_type_max)
    {
        source_type_priority[source_type] = priority;
        success = TRUE;
    }
    return success;
}


static bool singleEntityData_DoesSourceHaveFocus(generic_source_t source)
{
    focus_t focus = focus_none;

    DEBUG_LOG_FN_ENTRY("singleEntityData_DoesSourceHaveFocus");

    switch(source.type)
    {
        case source_type_audio:
            focus = Focus_GetFocusForAudioSource(source.u.audio);
            break;

        case source_type_voice:
            focus = Focus_GetFocusForVoiceSource(source.u.voice);
            break;

        default:
            break;
    }
    return (focus == focus_foreground);
}

static bool singleEntityData_FirstSourceIsHigherPriority(generic_source_t source1, generic_source_t source2)
{
    unsigned source1_priority, source2_priority;

    DEBUG_LOG_FN_ENTRY("singleEntityData_FirstSourceIsHigherPriority");

    source1_priority = SingleEntityData_GetSourceTypePriority(source1.type);
    source2_priority = SingleEntityData_GetSourceTypePriority(source2.type);

    return (source1_priority > source2_priority);
}


bool SingleEntityData_GetSourceToRoute(generic_source_t* source)
{
    bool have_entry = FALSE;
    audio_router_data_t* data = NULL;
    audio_router_data_iterator_t *iterator = AudioRouter_CreateDataIterator();

    DEBUG_LOG_FN_ENTRY("SingleEntityData_GetSourceToRoute");

    while(NULL != (data = AudioRouter_GetNextEntry(iterator)))
    {
        if(data->present)
        {
            if(singleEntityData_DoesSourceHaveFocus(data->source))
            {
                if(have_entry)
                {
                    if(singleEntityData_FirstSourceIsHigherPriority(data->source, *source))
                    {
                        *source = data->source;
                    }
                }
                else
                {
                    *source = data->source;
                    have_entry = TRUE;
                }
            }
        }
    }
    AudioRouter_DestroyDataIterator(iterator);

    return have_entry;
}

bool SingleEntityData_IsSourcePresent(generic_source_t source)
{
    audio_router_data_t* source_entry = singleEntityData_FindSourceInData(source);
    if(source_entry)
    {
        return source_entry->present;
    }
    return FALSE;
}

bool SingleEntityData_SetSourceState(generic_source_t source, audio_router_state_t state)
{
    audio_router_data_t* source_entry = singleEntityData_FindSourceInData(source);

    if(source_entry)
    {
        DEBUG_LOG_INFO("SingleEntityData_SetSourceState setting enum:source_type_t:%d, source=%d to state enum:audio_router_state_t:%d",
                            source.type, source.u.audio, state);

        source_entry->state = state;
    }
    return (source_entry != NULL);
}

audio_router_state_t SingleEntityData_GetSourceState(generic_source_t source)
{
    audio_router_data_t* source_entry = singleEntityData_FindSourceInData(source);

    audio_router_state_t state = audio_router_state_invalid;

    if(source_entry)
    {
        state = source_entry->state;
    }
    return state;
}

bool SingleEntityData_IsSourceActive(generic_source_t source)
{
    audio_router_data_t* source_entry = singleEntityData_FindSourceInData(source);
    if(source_entry)
    {
        return singleEntityData_IsActive(source_entry);
    }
    return FALSE;
}

bool SingleEntityData_FindNewSource(generic_source_t* source)
{
    bool success = FALSE;
    audio_router_data_t* data = NULL;
    audio_router_data_iterator_t *iterator = AudioRouter_CreateDataIterator();

    DEBUG_LOG_FN_ENTRY("SingleEntityData_FindNewSource");

    while(NULL != (data = AudioRouter_GetNextEntry(iterator)))
    {
        if(data->present)
        {
            if(data->state == audio_router_state_new_source)
            {
                *source = data->source;
                success = TRUE;
                break;
            }
        }
    }

    AudioRouter_DestroyDataIterator(iterator);

    return success;
}
