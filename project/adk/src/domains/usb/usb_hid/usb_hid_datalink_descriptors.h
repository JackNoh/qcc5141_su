/*!
\copyright  Copyright (c) 2020 Qualcomm Technologies International, Ltd.\n
            All Rights Reserved.\n
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\file
\brief      Header file for using default descriptors of USB HID Consumer Transport
*/

#ifndef USB_HID_DATALINK_DESCRIPTORS_H
#define USB_HID_DATALINK_DESCRIPTORS_H

#include "usb_hid_class.h"

#define HID_REPORTID_DATA_TRANSFER          1   /* data from host for AHI */
#define HID_REPORTID_DATA_TRANSFER_SIZE     62

#define HID_REPORTID_RESPONSE               2   /* device response for AHI */
#define HID_REPORTID_RESPONSE_SIZE          16

#define HID_REPORTID_COMMAND                3   /* command channel */
#define HID_REPORTID_COMMAND_SIZE          62

#define HID_REPORTID_CONTROL                4   /* control channel dedicated to HID library */
#define HID_REPORTID_CONTROL_SIZE          62

#define HID_REPORTID_UPGRADE_DATA_TRANSFER  5   /* data from host for Upgrade */
#define HID_REPORTID_UPGRADE_DATA_TRANSFER_SIZE 512

#define HID_REPORTID_UPGRADE_RESPONSE       6   /* device response for Upgrade */
#define HID_REPORTID_UPGRADE_RESPONSE_SIZE  12

/*! Default USB HID datalink class descriptor */
extern const usb_hid_class_desc_t usb_hid_datalink_class_desc;
/*! Default USB HID datalink report descriptor */
extern const usb_hid_report_desc_t usb_hid_datalink_report_desc;
/*! Default USB HID datalink endpoint config */
extern const usb_hid_endpoint_desc_t usb_hid_datalink_endpoint;
/*! Default USB HID datalink configuration */
extern const usb_hid_config_params_t usb_hid_datalink_config;


#endif /* USB_HID_DATALINK_DESCRIPTORS_H */
