/*!
\copyright  Copyright (c) 2020 Qualcomm Technologies International, Ltd.\n
            All Rights Reserved.\n
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\file
\brief      USB HID: Datalink interface
*/

#include "usb_hid_datalink.h"
#include "usb_hid_class.h"
#include "usb_hid_common.h"

#include "logging.h"

#include <csrtypes.h>

#include <usb.h>
#include <message.h>

#include <panic.h>
#include <sink.h>
#include <source.h>
#include <string.h>
#include <stream.h>
#include <stdlib.h>

TaskData hid_datalink_task;

static usb_hid_class_data_t *hid_class_data;

typedef struct usb_hid_handler_list_t
{
    usb_hid_handler_t handler;
    struct usb_hid_handler_list_t *next;
} usb_hid_handler_list_t;

usb_hid_handler_list_t *hid_datalink_handlers;

typedef struct
{
    uint8 report_id;
    uint16 report_size;
} hid_datalink_report_info_t;

const hid_datalink_report_info_t hid_datalink_reports[] =
{
        {HID_REPORTID_DATA_TRANSFER,         HID_REPORTID_DATA_TRANSFER_SIZE},
        {HID_REPORTID_RESPONSE,              HID_REPORTID_RESPONSE_SIZE},
        {HID_REPORTID_COMMAND,               HID_REPORTID_COMMAND_SIZE},
        {HID_REPORTID_CONTROL,               HID_REPORTID_CONTROL_SIZE},
        {HID_REPORTID_UPGRADE_DATA_TRANSFER, HID_REPORTID_UPGRADE_DATA_TRANSFER_SIZE},
        {HID_REPORTID_UPGRADE_RESPONSE,      HID_REPORTID_UPGRADE_RESPONSE_SIZE}
};



static const hid_datalink_report_info_t *find_report_info(uint8 report_id)
{
    const hid_datalink_report_info_t *r = NULL;
    int i;

    for (i = 0; i < ARRAY_DIM(hid_datalink_reports); i++)
    {
        if (hid_datalink_reports[i].report_id == report_id)
        {
            r = &hid_datalink_reports[i];
            break;
        }
    }
    return r;
}

usb_result_t UsbHid_Datalink_SendReport(usb_class_context_t context,
                                        uint8 report_id,
                                        const uint8 * report_data, uint16 size)
{
    /* send data from client to USB */
    Sink sink;
    uint8 *out;
    const hid_datalink_report_info_t *r = find_report_info(report_id);
    uint16 report_data_size;
    usb_hid_class_data_t *data = hid_class_data;

    if (!r)
    {
        DEBUG_LOG_ERROR("UsbHid:DL SendReport - report %u not found",
                        report_id);
        Panic();
    }

    if (!data ||
            (usb_class_context_t)data != context)
    {
        return USB_RESULT_NOT_FOUND;
    }

    sink = data->class_sink;

    out = SinkMapClaim(sink, r->report_size);

    if (!out)
    {
        DEBUG_LOG_ERROR("UsbHid:DL SendReport - cannot claim sink space\n");
        return USB_RESULT_NO_SPACE;
    }

    report_data_size = r->report_size - 2;

    if (!report_data)
    {
        size = 0;
    }
    else
    {
        size = MIN(size, report_data_size);
    }

    out[0] = report_id;
    out[1] = size;

    memcpy(&out[2], report_data, size);
    memset(&out[2 + size], 0, report_data_size - size);

    if(!SinkFlush(sink, r->report_size))
    {
        DEBUG_LOG_ERROR("UsbHid:DL SendReport - failed to send data\n");
        Panic();
    }

    return USB_RESULT_OK;
}

static void usbHid_DatalinkHandler(Task task, MessageId id, Message message)
{
    Source source;
    Sink sink;
    uint16 packet_size;
    usb_hid_class_data_t *data = hid_class_data;

    UNUSED(task);

    if (id != MESSAGE_MORE_DATA)
    {
        return;
    }

    source = ((MessageMoreData *)message)->source;

    if (!data || data->class_source != source)
    {
        return;
    }

    sink = data->class_sink;

    while ((packet_size = SourceBoundary(source)) != 0)
    {
        UsbResponse resp;
        bool response_sent = FALSE;
        /* Build the response. It must contain the original request, so copy
           from the source header. */
        memcpy(&resp.original_request, SourceMapHeader(source), sizeof(UsbRequest));

        /* Set the response fields to default values to make the code below simpler */
        resp.success = FALSE;
        resp.data_length = 0;

        switch (resp.original_request.bRequest)
        {
            case HID_GET_REPORT:
                 DEBUG_LOG_INFO("UsbHid:DL Get_Report wValue=0x%X wIndex=0x%X wLength=0x%X",
                                resp.original_request.wValue,
                                resp.original_request.wIndex,
                                resp.original_request.wLength);
                break;

            case HID_GET_IDLE:
            {
                uint8 *out;
                if ((out = SinkMapClaim(sink, 1)) != 0)
                {
                     DEBUG_LOG_INFO("UsbHid:DL Get_Idle wValue=0x%X wIndex=0x%X",
                                    resp.original_request.wValue,
                                    resp.original_request.wIndex);
                    out[0] = data->idle_rate;
                    resp.success = TRUE;
                    resp.data_length = 1;
                }
                break;
            }

            case HID_SET_REPORT:
            {
                uint16 size_data = resp.original_request.wLength;
                uint8 report_id = resp.original_request.wValue & 0xff;
                DEBUG_LOG_INFO("UsbHid:DL Set_Report wValue=0x%X wIndex=0x%X wLength=0x%X",
                               resp.original_request.wValue,
                               resp.original_request.wIndex,
                               resp.original_request.wLength);

                resp.success = TRUE;

#if 0
                if (size_data)
                {
                    const uint8 *report_data;
                    usb_hid_handler_list_t *handler_list = hid_datalink_handlers;

                    /* That's a special case as we want to complete
                     * control transfer as soon as possible.
                     * Careful: need to make sure Source buffer is big enough
                     * for 2x largest requests. */
                    (void) SinkClaim(sink, 1);
                    (void) SinkFlushHeader(sink, 1,(uint16 *) &resp, sizeof(UsbResponse));
                    response_sent = TRUE;

                    report_data = SourceMap(source);
                    size_data = MIN(packet_size, size_data);

                    while (handler_list)
                    {
                        handler_list->handler(report_id, report_data, size_data);
                        handler_list = handler_list->next;
                    }
                }
#endif
                break;
            }

            case HID_SET_IDLE:
                 DEBUG_LOG_INFO("UsbHid:DL Set_Idle wValue=0x%X wIndex=0x%X",
                                resp.original_request.wValue,
                                resp.original_request.wIndex);
                data->idle_rate = (resp.original_request.wValue >> 8) & 0xff;
                resp.success = TRUE;
                break;

            default:
            {
                 DEBUG_LOG_ERROR("UsbHid:DL req=0x%X wValue=0x%X HID wIndex=0x%X wLength=0x%X\n",
                        resp.original_request.bRequest,
                        resp.original_request.wValue,
                        resp.original_request.wIndex,
                        resp.original_request.wLength);
                break;
            }
        }

        if (!response_sent)
        {
            /* Send response */
            if (resp.data_length)
            {
                (void)SinkFlushHeader(sink, resp.data_length, (uint16 *)&resp, sizeof(UsbResponse));
            }
            else
            {
                   /* Sink packets can never be zero-length, so flush a dummy byte */
                (void) SinkClaim(sink, 1);
                (void) SinkFlushHeader(sink, 1, (uint16 *) &resp, sizeof(UsbResponse));
            }
        }
        /* Discard the original request */
        SourceDrop(source, packet_size);
    }
}

static usb_class_context_t usbHid_Datalink_Create(usb_device_index_t dev_index,
                                  usb_class_interface_config_data_t config_data)
{
    UsbCodes codes;
    UsbInterface intf;
    EndPointInfo ep_info;
    uint8 endpoint;
    usb_hid_class_data_t *data;
    const usb_hid_config_params_t *config = (const usb_hid_config_params_t *)config_data;

    DEBUG_LOG_INFO("UsbHid:DL Datalink");

    if (hid_class_data)
    {
        DEBUG_LOG_ERROR("UsbHid:DL ERROR - class already present");
        Panic();
    }

    if (!config)
    {
        DEBUG_LOG_ERROR("UsbHid:DL ERROR - configuration not provided");
        Panic();
    }

    /* HID no boot codes */
    codes.bInterfaceClass    = B_INTERFACE_CLASS_HID;
    codes.bInterfaceSubClass = B_INTERFACE_SUB_CLASS_HID_NO_BOOT;
    codes.bInterfaceProtocol = B_INTERFACE_PROTOCOL_HID_NO_BOOT;
    codes.iInterface         = 0;

    intf = UsbAddInterface(&codes, B_DESCRIPTOR_TYPE_HID,
                           config->class_desc->descriptor,
                           config->class_desc->size_descriptor);

    if (intf == usb_interface_error)
    {
        DEBUG_LOG_ERROR("UsbHid:DL UsbAddInterface ERROR");
        Panic();
    }

    /* Register HID Datalink report descriptor with the interface */
    if (!UsbAddDescriptor(intf, B_DESCRIPTOR_TYPE_HID_REPORT,
                          config->report_desc->descriptor,
                          config->report_desc->size_descriptor))
    {
        DEBUG_LOG_ERROR("UsbHid:DL UsbAddDescriptor ERROR");
        Panic();
    }

    /* USB HID endpoint information */
    endpoint = UsbDevice_AllocateEndpointAddress(dev_index,
                                                 config->endpoint->is_to_host);
    if (!endpoint)
    {
        DEBUG_LOG_ERROR("UsbHid:DL UsbDevice_AllocateEndpointAddress ERROR");
        Panic();
    }

    ep_info.bEndpointAddress = endpoint;
    ep_info.bmAttributes = end_point_attr_int;
    ep_info.wMaxPacketSize = config->endpoint->wMaxPacketSize;
    ep_info.bInterval = config->endpoint->bInterval;
    ep_info.extended = NULL;
    ep_info.extended_length = 0;

    /* Add required endpoints to the interface */
    if (!UsbAddEndPoints(intf, 1, &ep_info))
    {
        DEBUG_LOG_ERROR("UsbHid:DL UsbAddEndPoints ERROR");
        Panic();
    }

    hid_datalink_task.handler = usbHid_DatalinkHandler;

    data = (usb_hid_class_data_t *)
            PanicUnlessMalloc(sizeof(usb_hid_class_data_t));
    memset(data, 0, sizeof(usb_hid_class_data_t));

    data->dev_index = dev_index;
    data->intf = intf;

    data->class_sink = StreamUsbClassSink(intf);
    data->class_source = StreamSourceFromSink(data->class_sink);
    MessageStreamTaskFromSink(data->class_sink, &hid_datalink_task);

    data->ep_sink = StreamUsbEndPointSink(endpoint);
    hid_class_data = data;

    return (usb_class_context_t)hid_class_data;
}

static usb_result_t usbHid_Datalink_Destroy(usb_class_context_t context)
{
    if (!hid_class_data ||
            (usb_class_context_t)hid_class_data != context)
    {
        return USB_RESULT_NOT_FOUND;
    }

    free(hid_class_data);
    hid_class_data = NULL;

    DEBUG_LOG_INFO("UsbHid:DL closed");

    return USB_RESULT_OK;
}


const usb_class_interface_cb_t UsbHid_Datalink_Callbacks =
{
        .Create = usbHid_Datalink_Create,
        .Destroy = usbHid_Datalink_Destroy,
        .SetInterface = NULL
};

void UsbHid_Datalink_RegisterSetReportHandler(usb_hid_handler_t handler)
{
    UNUSED(handler);
    /* Sink app had 2x clients:
     * AhiUsbHostHandleMessage -> AhiTransportProcessData((TaskData*)&ahiTask, (uint8*)msg->report);
     * HidUpgradeSetReportHandler(msg->report_id, msg->size_report, msg->report);
     */
}

void UsbHid_Datalink_UnregisterSetReportHandler(usb_hid_handler_t handler)
{
    UNUSED(handler);
    /* todo */
}
