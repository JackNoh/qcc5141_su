#include "usb_hid_consumer_transport_descriptors.h"

/*!
\copyright  Copyright (c) 2020 Qualcomm Technologies International, Ltd.\n
            All Rights Reserved.\n
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\file
\brief      USB HID Consumer Transport default descriptors
*/


/* HID Report Descriptor - Consumer Transport Control Device */
static const uint8 report_descriptor_hid_consumer_transport[] =
{
    0x05, 0x0C,                  /* USAGE_PAGE (Consumer Devices) */
    0x09, 0x01,                  /* USAGE (Consumer Control) */
    0xa1, 0x01,                  /* COLLECTION (Application) */

    0x85, USB_HID_CONSUMER_TRANSPORT_REPORT_ID, /*   REPORT_ID (1) */

    0x15, 0x00,                  /*   LOGICAL_MINIMUM (0) */
    0x25, 0x01,                  /*   LOGICAL_MAXIMUM (1) */
    0x09, 0xcd,                  /*   USAGE (Play/Pause - OSC) */
    0x09, 0xb5,                  /*   USAGE (Next Track - OSC) */
    0x09, 0xb6,                  /*   USAGE (Previous Track - OSC) */
    0x09, 0xb7,                  /*   USAGE (Stop - OSC) */
    0x75, 0x01,                  /*   REPORT_SIZE (1) */
    0x95, 0x04,                  /*   REPORT_COUNT (4) */
    0x81, 0x02,                  /*   INPUT (Data,Var,Abs) */

    0x15, 0x00,                  /*   LOGICAL_MINIMUM (0) */
    0x25, 0x01,                  /*   LOGICAL_MAXIMUM (1) */
    0x09, 0xb0,                  /*   USAGE (Play - OOC) */
    0x09, 0xb1,                  /*   USAGE (Pause - OOC) */
    0x09, 0xb3,                  /*   USAGE (Fast Forward -OOC) */
    0x09, 0xb4,                  /*   USAGE (Rewind - OOC) */
    0x75, 0x01,                  /*   REPORT_SIZE (1) */
    0x95, 0x04,                  /*   REPORT_COUNT (4) */
    0x81, 0x22,                  /*   INPUT (Data,Var,Abs,NoPref) */

    0x15, 0x00,                  /*   LOGICAL_MINIMUM (0) */
    0x25, 0x01,                  /*   LOGICAL_MAXIMUM (1) */
    0x09, 0xe9,                  /*   USAGE (Volume Increment - RTC) */
    0x09, 0xea,                  /*   USAGE (Volume Decrement - RTC) */
    0x75, 0x01,                  /*   REPORT_SIZE (1) */
    0x95, 0x02,                  /*   REPORT_COUNT (2) */
    0x81, 0x02,                  /*   INPUT (Data,Var,Abs,Bit Field) */
    0x09, 0xe2,                  /*   USAGE (Mute - OOC) */

    0x95, 0x01,                  /*   REPORT_COUNT (1) */
    0x81, 0x06,                  /*   INPUT (Data,Var,Rel,Bit Field) */

    0x95, 0x05,                  /*   REPORT_COUNT (5) */
    0x81, 0x01,                  /*   INPUT (Const) */

    0xc0                        /* END_COLLECTION */
};

/* USB HID class descriptor - Consumer Transport Control Device*/
static const uint8 interface_descriptor_hid_consumer_transport[] =
{
    HID_DESCRIPTOR_LENGTH,              /* bLength */
    B_DESCRIPTOR_TYPE_HID,              /* bDescriptorType */
    0x11, 0x01,                         /* bcdHID */
    0,                                  /* bCountryCode */
    1,                                  /* bNumDescriptors */
    B_DESCRIPTOR_TYPE_HID_REPORT,       /* bDescriptorType */
    sizeof(report_descriptor_hid_consumer_transport),   /* wDescriptorLength */
    0                                   /* wDescriptorLength */
};

const usb_hid_class_desc_t usb_hid_consumer_transport_class_desc = {
        .descriptor = interface_descriptor_hid_consumer_transport,
        .size_descriptor = sizeof(interface_descriptor_hid_consumer_transport)
};

const usb_hid_report_desc_t usb_hid_consumer_transport_report_desc = {
        .descriptor = report_descriptor_hid_consumer_transport,
        .size_descriptor = sizeof(report_descriptor_hid_consumer_transport)
};

const usb_hid_endpoint_desc_t usb_hid_consumer_transport_endpoint = {
        .is_to_host = TRUE,
        .wMaxPacketSize = 64,
        .bInterval = 8
};

const usb_hid_config_params_t usb_hid_consumer_transport_config = {
        .class_desc = &usb_hid_consumer_transport_class_desc,
        .report_desc = &usb_hid_consumer_transport_report_desc,
        .endpoint = &usb_hid_consumer_transport_endpoint
};
