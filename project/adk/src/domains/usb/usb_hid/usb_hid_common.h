/*!
\copyright  Copyright (c) 2020 Qualcomm Technologies International, Ltd.\n
            All Rights Reserved.\n
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\file
\brief      Header file for USB HID common code
*/

#ifndef USB_HID_COMMON_H_
#define USB_HID_COMMON_H_

#include <usb_device.h>
#include <usb_device_utils.h>
#include <usb.h>
#include <csrtypes.h>
#include <sink.h>
#include <source.h>

typedef struct usb_hid_class_data_t
{
    usb_device_index_t dev_index;
    UsbInterface intf;

    Sink class_sink;
    Source class_source;
    Sink ep_sink;

    uint8 idle_rate;
} usb_hid_class_data_t;

#endif /* USB_HID_COMMON_H_ */
