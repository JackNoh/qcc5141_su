/*!
\copyright  Copyright (c) 2018 - 2019 Qualcomm Technologies International, Ltd.\n
            All Rights Reserved.\n
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\file
\brief      Management of Bluetooth Low Energy extended specific advertising
*/

#ifdef INCLUDE_ADVERTISING_EXTENSIONS

#include "le_advertising_manager_select_extended.h"

#include "le_advertising_manager_data_common.h"
#include "le_advertising_manager_data_extended.h"
#include "le_advertising_manager_private.h"
#include "le_advertising_manager_select_common.h"
#include "le_advertising_manager_sm.h"
#include "le_advertising_manager_utils.h"

#include "local_addr.h"

#include <bdaddr.h>

#include <panic.h>


#define ADV_EVENT_PROPERTIES_CONNECTABLE   1
#define ADV_FILTER_POLICY                  0
#define ADV_PRIMARY_PHY                    1
#define ADV_SECONDARY_PHY                  1
#define ADV_SECONDARY_MAX_SKIP             0
#define ADV_SID                            0


/* Data type for the state of extended advertising */
typedef enum{
    le_adv_mgr_extended_state_idle,
    le_adv_mgr_extended_state_starting,
    le_adv_mgr_extended_state_started,
    le_adv_mgr_extended_state_suspending,
    le_adv_mgr_extended_state_suspended
}le_adv_mgr_extended_state_t;

le_adv_mgr_extended_state_t le_adv_mgr_extended_state = le_adv_mgr_extended_state_idle;


static void leAdvertisingManager_ResumeExtendedAdvertising(void)
{
    ConnectionDmBleExtAdvertiseEnableReq(AdvManagerGetTask(), TRUE, ADV_HANDLE_APP_SET_1);
}

static void leAdvertisingManager_SuspendExtendedAdvertising(void)
{
    ConnectionDmBleExtAdvertiseEnableReq(AdvManagerGetTask(), FALSE, ADV_HANDLE_APP_SET_1);
}

static bool leAdvertisingManager_GetExtendedDataUpdateRequired(void)
{
    adv_mgr_task_data_t *adv_task_data = AdvManagerGetTaskData();
    
    return adv_task_data->is_extended_data_update_required;
}

static void leAdvertisingManager_EnterExtendedIdleState(void)
{
    le_adv_mgr_extended_state = le_adv_mgr_extended_state_idle;
}

static void leAdvertisingManager_EnterExtendedSuspendingState(void)
{
    le_adv_mgr_extended_state = le_adv_mgr_extended_state_suspending;
    leAdvertisingManager_SuspendExtendedAdvertising();
}

static void leAdvertisingManager_EnterExtendedSuspendedState(void)
{
    le_adv_mgr_extended_state = le_adv_mgr_extended_state_suspended;
}

static void leAdvertisingManager_EnterExtendedStartingState(void)
{
    le_adv_mgr_extended_state = le_adv_mgr_extended_state_starting;
    
}

static void leAdvertisingManager_EnterExtendedStartedState(void)
{
    le_adv_mgr_extended_state = le_adv_mgr_extended_state_started;
}

static bool leAdvertisingManager_IsExtendedStateSuspending(void)
{
    return (le_adv_mgr_extended_state_suspending == le_adv_mgr_extended_state);
}

static bool leAdvertisingManager_IsExtendedStateSuspended(void)
{
    return (le_adv_mgr_extended_state_suspended == le_adv_mgr_extended_state);
}

static bool leAdvertisingManager_IsExtendedStateStarting(void)
{
    return (le_adv_mgr_extended_state_starting == le_adv_mgr_extended_state);
}

static bool leAdvertisingManager_IsExtendedStateStarted(void)
{
    return (le_adv_mgr_extended_state_started == le_adv_mgr_extended_state);
}


static void leAdvertisingManager_SetExtendedState(le_adv_mgr_extended_state_t state)
{
    le_adv_mgr_extended_state_t old_state = le_adv_mgr_extended_state;
    
    switch(state)
    {
        case le_adv_mgr_extended_state_idle:
        leAdvertisingManager_EnterExtendedIdleState();
        break;        
        
        case le_adv_mgr_extended_state_starting:
        if(le_adv_mgr_extended_state_idle != old_state)
            leAdvertisingManager_ResumeExtendedAdvertising();
        leAdvertisingManager_EnterExtendedStartingState();
        break;        
        
        case le_adv_mgr_extended_state_suspending:
        leAdvertisingManager_EnterExtendedSuspendingState();     
        break;
        
        case le_adv_mgr_extended_state_started:
        leAdvertisingManager_EnterExtendedStartedState();
        break;
                
        case le_adv_mgr_extended_state_suspended:
        leAdvertisingManager_EnterExtendedSuspendedState();
        break;
        
        default:
        break;
    }
}

static void leAdvertisingManager_SetAdvertisingParamsReq(void)
{
    typed_bdaddr taddr;
    /*ble_adv_type type = leAdvertisingManager_GetAdvertType(leAdvertisingManager_GetDataSetEventType());*/
    
    ble_adv_params_t interval_params = leAdvertisingManager_GetAdvertisingIntervalParams();

    BdaddrTypedSetEmpty(&taddr);

    /* todo Decide correct parameters to use */
    ConnectionDmBleExtAdvSetParamsReq(AdvManagerGetTask(), 
                                        ADV_HANDLE_APP_SET_1, 
                                        ADV_EVENT_PROPERTIES_CONNECTABLE /*todo connectable for now*/,
                                        interval_params.undirect_adv.adv_interval_min,
                                        interval_params.undirect_adv.adv_interval_max, 
                                        BLE_ADV_CHANNEL_ALL, 
                                        LocalAddr_GetBleType(),
                                        taddr, 
                                        ADV_FILTER_POLICY,
                                        ADV_PRIMARY_PHY,
                                        ADV_SECONDARY_MAX_SKIP,
                                        ADV_SECONDARY_PHY,
                                        ADV_SID);
}

/* Local Function to Set Up Advertising Parameters */
static void leAdvertisingManager_SetupAdvertParams(void)
{           
    DEBUG_LOG_LEVEL_1("leAdvertisingManager_SetupAdvertParams");
    
    adv_mgr_task_data_t *adv_task_data = AdvManagerGetTaskData();
    adv_task_data->blockingCondition = ADV_SETUP_BLOCK_ADV_PARAMS_CFM;

    DEBUG_LOG_LEVEL_2("leAdvertisingManager_SetupAdvertParams Info, Request advertising parameters set, blocking condition is %x", adv_task_data->blockingCondition);
    leAdvertisingManager_SetAdvertisingParamsReq();
}

/* Local Function to Start Extended Advertising */
static bool leAdvertisingManager_StartExtendedAdvertising(void)
{    
    DEBUG_LOG_LEVEL_1("leAdvertisingManager_StartExtendedAdvertising");
    
    if (!LeAdvertisingManager_CanAdvertisingBeStarted())
    {
        DEBUG_LOG_LEVEL_1("leAdvertisingManager_StartExtendedAdvertising Failure");
        return FALSE;
    }
    
    leAdvertisingManager_SetupAdvertParams();
    
    leAdvertisingManager_SetExtendedState(le_adv_mgr_extended_state_starting);
    return TRUE;
}

/* Local Function to Handle Internal Advertising Start Request */
static void leAdvertisingManager_HandleExtendedInternalStartRequest(const LE_ADV_MGR_INTERNAL_START_T * msg)
{
    adv_mgr_task_data_t *adv_task_data = AdvManagerGetTaskData();
    
    DEBUG_LOG_LEVEL_1("leAdvertisingManager_HandleExtendedInternalStartRequest");
    
    if(leAdvertisingManager_IsExtendedStateStarted())
    {
        DEBUG_LOG_LEVEL_2("leAdvertisingManager_HandleExtendedInternalStartRequest Info, Advertising already started, suspending and rescheduling");
        
        leAdvertisingManager_SetExtendedState(le_adv_mgr_extended_state_suspending);
        adv_task_data->blockingCondition = ADV_SETUP_BLOCK_ADV_ENABLE_CFM;

        leAdvertisingManager_SetDataUpdateRequired(LE_ADV_MGR_ADVERTISING_SET_EXTENDED, TRUE);
        
        LeAdvertisingManager_ScheduleAdvertisingStart(msg->set);
        return;
    }
    
    leAdvertisingManager_SetExtendedState(le_adv_mgr_extended_state_idle);
    
    leAdvertisingManager_SetDataSetEventType(le_adv_event_type_connectable_general);
    
    leAdvertisingManager_StartExtendedAdvertising();
    
    LeAdvertisingManager_SendSelectConfirmMessage();
}

static void leAdvertisingManager_handleExtendedAdvertMessage(Task task, MessageId id, Message message)
{
    UNUSED(task);

    switch (id)
    {
        case LE_ADV_MGR_INTERNAL_START:
            DEBUG_LOG_LEVEL_1("LE_ADV_MGR_INTERNAL_START. Extended");
            leAdvertisingManager_HandleExtendedInternalStartRequest((const LE_ADV_MGR_INTERNAL_START_T *)message);
            break;
            
        default:
            break;
    }
    
}

static void leAdvertisingManager_RegisterExtendedApplicationSet(uint8 adv_handle)
{   
    adv_mgr_task_data_t *adv_task_data = AdvManagerGetTaskData();

    PanicFalse(adv_task_data->blockingCondition == ADV_SETUP_BLOCK_NONE);
    
    adv_task_data->blockingCondition = ADV_SETUP_BLOCK_ADV_REGISTER_CFM;

    ConnectionDmBleExtAdvRegisterAppAdvSetReq(AdvManagerGetTask(), adv_handle);
}


void leAdvertisingManager_SelectExtendedAdvertisingInit(void)
{
    adv_mgr_task_data_t *adv_task_data = AdvManagerGetTaskData();
    
    adv_task_data->extended_task.handler = leAdvertisingManager_handleExtendedAdvertMessage;
    
    le_adv_mgr_extended_state = le_adv_mgr_extended_state_idle;
    
    leAdvertisingManager_RegisterExtendedDataIf();
    
    leAdvertisingManager_RegisterExtendedApplicationSet(ADV_HANDLE_APP_SET_1);
}

bool leAdvertisingManager_EnableExtendedAdvertising(bool enable)
{
    bool extended_enabled = FALSE;
    adv_mgr_task_data_t *adv_task_data = AdvManagerGetTaskData();
    
    DEBUG_LOG_LEVEL_1("leAdvertisingManager_EnableExtendedAdvertising enable:%d dataset:%d state enum:le_adv_mgr_extended_state_t:%d",
                      enable, leAdvertisingManager_GetDataSetSelected(), le_adv_mgr_extended_state);

    if (leAdvertisingManager_SelectOnlyExtendedSet((leAdvertisingManager_GetDataSetSelected()))
        || leAdvertisingManager_IsExtendedStateSuspending())
    {
        if(enable)
        {
            if(leAdvertisingManager_IsExtendedStateSuspended())
            {
                leAdvertisingManager_SetExtendedState(le_adv_mgr_extended_state_starting);
                adv_task_data->blockingCondition = ADV_SETUP_BLOCK_ADV_ENABLE_CFM;
                extended_enabled = TRUE;
            }
            else if(leAdvertisingManager_IsExtendedStateStarting())
            {
                extended_enabled = TRUE;
            }
        }
        else
        {
            if(leAdvertisingManager_IsExtendedStateStarted())
            {
                leAdvertisingManager_SetExtendedState(le_adv_mgr_extended_state_suspending);
                adv_task_data->blockingCondition = ADV_SETUP_BLOCK_ADV_ENABLE_CFM;
                extended_enabled = TRUE;
            }
            else if(leAdvertisingManager_IsExtendedStateSuspending())
            {
                extended_enabled = TRUE;
            }
        }
    }
    
    DEBUG_LOG_LEVEL_1("leAdvertisingManager_EnableExtendedAdvertising enabled:%d", extended_enabled);
    
    return extended_enabled;
}

/* Function to handle CL_DM_BLE_SET_EXT_ADV_DATA_CFM message */
void leAdvertisingManager_HandleExtendedSetAdvertisingDataCfm(const CL_DM_BLE_SET_EXT_ADV_DATA_CFM_T* cfm)
{
    adv_mgr_task_data_t *adv_task_data = AdvManagerGetTaskData();
    le_adv_data_set_t set;

    DEBUG_LOG_LEVEL_1("leAdvertisingManager_HandleExtendedSetAdvertisingDataCfm");
    
    if(adv_task_data->blockingCondition == ADV_SETUP_BLOCK_ADV_DATA_CFM)
    {
        if (success == cfm->status)
        {
            DEBUG_LOG_LEVEL_2("leAdvertisingManager_HandleExtendedSetAdvertisingDataCfm Info, CL_DM_BLE_SET_ADVERTISING_DATA_CFM received with success");
            set = leAdvertisingManager_SelectOnlyExtendedSet((leAdvertisingManager_GetDataSetSelected()));
            leAdvertisingManager_SetupScanResponseData(set);
            leAdvertisingManager_ClearData(set);
            adv_task_data->blockingCondition = ADV_SETUP_BLOCK_ADV_SCAN_RESPONSE_DATA_CFM;
        }
        else
        {
            DEBUG_LOG_LEVEL_1("leAdvertisingManager_HandleExtendedSetAdvertisingDataCfm Failure, CL_DM_BLE_SET_ADVERTISING_DATA_CFM received with failure");
            Panic();
        }
    }
    else
    {
        DEBUG_LOG_LEVEL_1("leAdvertisingManager_HandleExtendedSetAdvertisingDataCfm Failure, Message Received in Unexpected Blocking Condition %x", adv_task_data->blockingCondition);
        Panic();
    }
}

/* Function to handle CL_DM_BLE_EXT_ADV_SET_SCAN_RESPONSE_DATA_CFM message */
void leAdvertisingManager_HandleExtendedSetScanResponseDataCfm(const CL_DM_BLE_EXT_ADV_SET_SCAN_RESPONSE_DATA_CFM_T * cfm)
{    
    DEBUG_LOG_LEVEL_1("leAdvertisingManager_HandleExtendedSetScanResponseDataCfm");    
    
    adv_mgr_task_data_t *adv_task_data = AdvManagerGetTaskData();
    
    if (adv_task_data->blockingCondition == ADV_SETUP_BLOCK_ADV_SCAN_RESPONSE_DATA_CFM)
    {        
        DEBUG_LOG_LEVEL_2("leAdvertisingManager_HandleExtendedSetScanResponseDataCfm Info, adv_task_data->blockingCondition is %x cfm->status is %x", adv_task_data->blockingCondition, cfm->status);    

        if (success == cfm->status)
        {            
            DEBUG_LOG_LEVEL_2("leAdvertisingManager_HandleExtendedSetScanResponseDataCfm Info, CL_DM_BLE_SET_SCAN_RESPONSE_DATA_CFM received with success");    
            
            leAdvertisingManager_SetExtendedState(le_adv_mgr_extended_state_starting);
            adv_task_data->blockingCondition = ADV_SETUP_BLOCK_ADV_ENABLE_CFM;
        }
        else
        {
            DEBUG_LOG_LEVEL_1("leAdvertisingManager_HandleExtendedSetScanResponseDataCfm Failure, CL_DM_BLE_SET_SCAN_RESPONSE_DATA_CFM received with failure");
            Panic();
        }
    }
    else
    {
        DEBUG_LOG_LEVEL_1("leAdvertisingManager_HandleExtendedSetScanResponseDataCfm Failure, Message Received in Unexpected Blocking Condition %x", adv_task_data->blockingCondition);    
        Panic();
    }
}

/* Function to handle CL_DM_BLE_SET_EXT_ADV_PARAMS_CFM message */
void leAdvertisingManager_HandleExtendedSetAdvertisingParamCfm(const CL_DM_BLE_SET_EXT_ADV_PARAMS_CFM_T *cfm)
{
    DEBUG_LOG_LEVEL_1("leAdvertisingManager_HandleExtendedSetAdvertisingParamCfm");    
    
    adv_mgr_task_data_t *adv_task_data = AdvManagerGetTaskData();
    
    if (adv_task_data->blockingCondition == ADV_SETUP_BLOCK_ADV_PARAMS_CFM)
    {            
        DEBUG_LOG_LEVEL_2("leAdvertisingManager_HandleExtendedSetAdvertisingParamCfm Info, adv_task_data->blockingCondition is %x cfm->status is %x", adv_task_data->blockingCondition, cfm->status);    
            
        if (success == cfm->status)
        {            
            DEBUG_LOG_LEVEL_2("leAdvertisingManager_HandleExtendedSetAdvertisingParamCfm Info, CL_DM_BLE_SET_ADVERTISING_PARAMS_CFM received with success");    
            
            if(leAdvertisingManager_GetExtendedDataUpdateRequired())
            {
                DEBUG_LOG_LEVEL_2("leAdvertisingManager_HandleExtendedSetAdvertisingParamCfm Info, Data update is needed");        
                
                leAdvertisingManager_SetDataUpdateRequired(LE_ADV_MGR_ADVERTISING_SET_EXTENDED, FALSE);
                
                if(leAdvertisingManager_BuildData(leAdvertisingManager_SelectOnlyExtendedSet((leAdvertisingManager_GetDataSetSelected()))))
                {
                    leAdvertisingManager_SetupAdvertData(leAdvertisingManager_SelectOnlyExtendedSet(leAdvertisingManager_GetDataSetSelected()));
                    adv_task_data->blockingCondition = ADV_SETUP_BLOCK_ADV_DATA_CFM;
                }
                else
                {
                    DEBUG_LOG_LEVEL_2("leAdvertisingManager_HandleExtendedSetAdvertisingParamCfm Info, There is no data to advertise");
                    leAdvertisingManager_ClearData(leAdvertisingManager_SelectOnlyExtendedSet((leAdvertisingManager_GetDataSetSelected())));
                }
            }
            else
            {
                leAdvertisingManager_SetExtendedState(le_adv_mgr_extended_state_starting);
                adv_task_data->blockingCondition = ADV_SETUP_BLOCK_ADV_ENABLE_CFM;
            }
        }
        else
        {            
            DEBUG_LOG_LEVEL_1("leAdvertisingManager_HandleExtendedSetAdvertisingParamCfm Failure, CL_DM_BLE_SET_ADVERTISING_PARAMS_CFM received with failure");

            Panic();
        }
    }
    else
    {      
        DEBUG_LOG_LEVEL_1("leAdvertisingManager_HandleExtendedSetAdvertisingParamCfm Failure, Message Received in Unexpected Blocking Condition %x", adv_task_data->blockingCondition);    
                                    
        Panic();
    }
}

void leAdvertisingManager_HandleExtendedSetAdvertisingEnableCfm(const CL_DM_BLE_EXT_ADVERTISE_ENABLE_CFM_T *cfm)
{
    adv_mgr_task_data_t *adv_task_data = AdvManagerGetTaskData();
    DEBUG_LOG_LEVEL_1("leAdvertisingManager_HandleExtendedSetAdvertisingEnableCfm");
    
    if (adv_task_data->blockingCondition == ADV_SETUP_BLOCK_ADV_ENABLE_CFM)
    {
        if(hci_success == cfm->status)
        {
            if(leAdvertisingManager_IsExtendedStateSuspending())
            {
                DEBUG_LOG_LEVEL_2("leAdvertisingManager_HandleExtendedSetAdvertisingEnableCfm Info, State machine is in suspending state");
                
                leAdvertisingManager_SetExtendedState(le_adv_mgr_extended_state_suspended);
            }
            else if(leAdvertisingManager_IsExtendedStateStarting())
            {
                DEBUG_LOG_LEVEL_2("leAdvertisingManager_HandleExtendedSetAdvertisingEnableCfm Info, State machine is in starting state");

                leAdvertisingManager_SetExtendedState(le_adv_mgr_extended_state_started);
            }
        }
        else if( (hci_error_command_disallowed == cfm->status) && leAdvertisingManager_IsExtendedStateSuspending() )
        {
            DEBUG_LOG_LEVEL_2("leAdvertisingManager_HandleExtendedSetAdvertisingEnableCfm Info, State machine is in suspending state, encountered an expected command disallowed error, treated as success, HCI status is %x", cfm->status);

            leAdvertisingManager_SetExtendedState(le_adv_mgr_extended_state_suspended);
        }
        else
        {
            DEBUG_LOG_LEVEL_1("leAdvertisingManager_HandleExtendedSetAdvertisingEnableCfm Failure, CL_DM_BLE_SET_ADVERTISE_ENABLE_CFM received with failure, HCI status is %x", cfm->status);

            leAdvertisingManager_SetExtendedState(le_adv_mgr_extended_state_suspended);
        }
        
        adv_task_data->blockingCondition = ADV_SETUP_BLOCK_NONE;
    }
    else
    {
        DEBUG_LOG_LEVEL_1("leAdvertisingManager_HandleExtendedSetAdvertisingEnableCfm Failure, Message Received in Unexpected Blocking Condition %x", adv_task_data->blockingCondition);    
        
        Panic();
    }
}

void leAdvertisingManager_HandleExtendedAdvertisingRegisterCfm(const CL_DM_BLE_EXT_ADV_REGISTER_APP_ADV_SET_CFM_T *cfm)
{
    adv_mgr_task_data_t *adv_task_data = AdvManagerGetTaskData();
    DEBUG_LOG_LEVEL_1("leAdvertisingManager_HandleExtendedAdvertisingRegisterCfm");
    
    if (adv_task_data->blockingCondition == ADV_SETUP_BLOCK_ADV_REGISTER_CFM)
    {
        adv_task_data->blockingCondition = ADV_SETUP_BLOCK_NONE;
        
        if(hci_success != cfm->status)
        {
            Panic();
        }
    }
}

bool LeAdvertisingManager_IsExtendedAdvertisingActive(void)
{
    return (le_adv_mgr_extended_state_started == le_adv_mgr_extended_state);
}

#endif /* INCLUDE_ADVERTISING_EXTENSIONS*/
