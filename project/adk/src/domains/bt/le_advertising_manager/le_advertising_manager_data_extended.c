/*!
\copyright  Copyright (c) 2018 - 2019 Qualcomm Technologies International, Ltd.\n
            All Rights Reserved.\n
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\file
\brief      Manage execution of callbacks to construct adverts and scan response
*/

#ifdef INCLUDE_ADVERTISING_EXTENSIONS

#include "le_advertising_manager_data_extended.h"

#include "le_advertising_manager_data_common.h"
#include "le_advertising_manager_clients.h"
#include "le_advertising_manager_uuid.h"
#include "le_advertising_manager_local_name.h"

#include <stdlib.h>
#include <panic.h>

/*! Maximum data length of an advert if advertising length extensions are not used */
#define MAX_AD_DATA_SIZE_IN_OCTETS      (0x1F)


typedef struct 
{
    uint8  data[MAX_AD_DATA_SIZE_IN_OCTETS];
    uint8* head;
    uint8  space;
} le_adv_data_packet_t;

static le_adv_data_packet_t* le_adv_data_packet[le_adv_manager_data_packet_max];


static bool leAdvertisingManager_AddDataItemToExtendedPacket(le_adv_data_packet_t* packet, const le_adv_data_item_t* item)
{
    PanicNull(packet);
    PanicNull((le_adv_data_item_t*)item);
    
    if(item->size > packet->space)
    {
        return FALSE;
    }
    
    if(item->size)
    {
        memcpy(packet->head, item->data, item->size);
        packet->head  += item->size;
        packet->space -= item->size;
    }
    
    return TRUE;
}

static bool leAdvertisingManager_createNewExtendedDataPacket(le_adv_manager_data_packet_type_t type)
{
    le_adv_data_packet_t* new_packet = PanicUnlessMalloc(sizeof(le_adv_data_packet_t));
    
    new_packet->head = new_packet->data;
    new_packet->space = MAX_AD_DATA_SIZE_IN_OCTETS;
    
    le_adv_data_packet[type] = new_packet;
    
    return TRUE;
}

static bool leAdvertisingManager_destroyExtendedDataPacket(le_adv_manager_data_packet_type_t type)
{
    DEBUG_LOG_VERBOSE("leAdvertisingManager_destroyExtendedDataPacket type:%d ptr:%p", type, le_adv_data_packet[type]);
    le_adv_data_packet[type] = NULL;
    
    return TRUE;
}

static unsigned leAdvertisingManager_getSizeExtendedDataPacket(le_adv_manager_data_packet_type_t type)
{
    return (le_adv_data_packet[type]->head - le_adv_data_packet[type]->data);
}

static bool leAdvertisingManager_addItemToExtendedDataPacket(le_adv_manager_data_packet_type_t type, const le_adv_data_item_t* item)
{
    return leAdvertisingManager_AddDataItemToExtendedPacket(le_adv_data_packet[type], item);
}

static void leAdvertisingManager_setupExtendedAdvertData(void)
{
    uint8 size_advert = leAdvertisingManager_getSizeExtendedDataPacket(le_adv_manager_data_packet_advert);
    uint8* advert_start[8];
    advert_start[0] = size_advert ? le_adv_data_packet[le_adv_manager_data_packet_advert]->data : NULL;
    /* todo Need to add support for more data to be set - for now set all but first block to NULL */
    for (int i = 1; i < 8; i++)
    {
        advert_start[i] = NULL;
    }
    
    DEBUG_LOG_VERBOSE("leAdvertisingManager_SetupAdvertData, Size is %d data:%p", size_advert, advert_start[0]);

    leAdvertisingManager_DebugDataItems(size_advert, advert_start[0]);

    ConnectionDmBleExtAdvSetDataReq(AdvManagerGetTask(), ADV_HANDLE_APP_SET_1, complete_data, size_advert, advert_start);
}

static void leAdvertisingManager_setupExtendedScanResponseData(void)
{
    uint8 size_scan_rsp = leAdvertisingManager_getSizeExtendedDataPacket(le_adv_manager_data_packet_scan_response);
    uint8* scan_rsp_start[8];
    scan_rsp_start[0] = size_scan_rsp ? le_adv_data_packet[le_adv_manager_data_packet_scan_response]->data : NULL;
    /* todo Need to add support for more data to be set - for now set all but first block to NULL */
    for (int i = 1; i < 8; i++)
    {
        scan_rsp_start[i] = NULL;
    }
    
    DEBUG_LOG("leAdvertisingManager_SetupScanResponseData, Size is %d", size_scan_rsp);

    leAdvertisingManager_DebugDataItems(size_scan_rsp, scan_rsp_start[0]);

    ConnectionDmBleExtAdvSetScanRespDataReq(AdvManagerGetTask(), ADV_HANDLE_APP_SET_1, complete_data, size_scan_rsp, scan_rsp_start);
}

static const le_advertising_manager_data_packet_if_t le_advertising_manager_extended_data_fns = 
{
    .createNewDataPacket = leAdvertisingManager_createNewExtendedDataPacket,
    .destroyDataPacket = leAdvertisingManager_destroyExtendedDataPacket,
    .getSizeDataPacket = leAdvertisingManager_getSizeExtendedDataPacket,
    .addItemToDataPacket = leAdvertisingManager_addItemToExtendedDataPacket,
    .setupAdvertData = leAdvertisingManager_setupExtendedAdvertData,
    .setupScanResponseData = leAdvertisingManager_setupExtendedScanResponseData
};


void leAdvertisingManager_RegisterExtendedDataIf(void)
{
    leAdvertisingManager_RegisterDataClient(LE_ADV_MGR_ADVERTISING_SET_EXTENDED,
                                            &le_advertising_manager_extended_data_fns);
                                            
    for (unsigned index = 0; index < le_adv_manager_data_packet_max; index++)
    {
        le_adv_data_packet[index] = NULL;
    }
}

#endif /* INCLUDE_ADVERTISING_EXTENSIONS*/
