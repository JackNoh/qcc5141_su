/*!
\copyright  Copyright (c) 2020 Qualcomm Technologies International, Ltd.\n
            All Rights Reserved.\n
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\file
\defgroup   case_comms Case Communications Domain
\ingroup    domains
\brief      
@{
*/

#ifndef CC_WITH_EARBUDS_H
#define CC_WITH_EARBUDS_H

#if defined(INCLUDE_CASE_COMMS) && defined (HAVE_CC_MODE_CASE)

#include <cc_protocol.h>

#include <task_list.h>

#include <message.h>

/*! Peer pairing state of an Earbud. */
typedef enum
{
    PP_STATE_UNKNOWN,
    PP_STATE_NOT_PAIRED,
    PP_STATE_PAIRED
} pp_state_t;

/*! Initialise the case comms with earbuds component. */
bool CcWithEarbuds_Init(Task init_task);

/*! \brief Register client task to receive Earbud state messages.
*/
void CcWithEarbuds_RegisterClient(Task client_task);

/*! \brief Unregister client task to stop receiving Earbud state messages.
*/
void CcWithEarbuds_UnregisterClient(Task client_task);

/*! \brief Handle receipt of non-info Earbud Status message. */
void CcWithEarbuds_EarbudStatusRx(cc_dev_t source, uint8 battery_state, bool peer_paired);

/*! brief Handle receipt of BT address message. */
void CcWithEarbuds_EarbudBtAddress(const bdaddr* addr, cc_dev_t source);

#else /* defined(INCLUDE_CASE_COMMS) && defined (HAVE_CC_MODE_CASE) */

#define CcWithEarbuds_RegisterClient(_client_task)      (UNUSED(_client_task))
#define CcWithEarbuds_UnregisterClient(_client_task)    (UNUSED(_client_task))
#define CcWithEarbuds_EarbudStatusRx(_source, _battery_state, _peer_paired) (UNUSED(_source), UNUSED(_battery_state), UNUSED(_peer_paired))
#define CcWithEarbuds_EarbudBtAddress(_addr, _source)   (UNUSED(_addr), UNUSED(_source))

#endif /* defined(INCLUDE_CASE_COMMS) && defined (HAVE_CC_MODE_CASE) */
#endif /* CC_WITH_EARBUDS_H */
/*! @} End of group documentation */
