/*!
\copyright  Copyright (c) 2020 Qualcomm Technologies International, Ltd.\n
            All Rights Reserved.\n
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\file
\ingroup    case_comms
\brief      Case channel interface.
*/
/*! \addtogroup case_comms
@{
*/

#ifdef INCLUDE_CASE_COMMS

#include "cc_protocol.h"

#define BATTERY_STATE_CHARGING_BIT      (0x80)
#define BATTERY_STATE_SET_CHARGING(x)   ((x) |= BATTERY_STATE_CHARGING_BIT)
#define BATTERY_STATE_CLEAR_CHARGING(x) ((x) &= ~BATTERY_STATE_CHARGING_BIT)
#define BATTERY_STATE_IS_CHARGING(x)    (((x) & BATTERY_STATE_CHARGING_BIT) == BATTERY_STATE_CHARGING_BIT)

#define BATTERY_STATE_PERCENTAGE(x)     ((x) & 0x7F)

/*! Configuration of a CASE_CHANNEL_MID_CASE_STATUS message. */
typedef struct
{
    /*! Set to TRUE to only send the first byte and omit the
        battery state bytes. */
    bool short_form:1;

    /*! @{ lid and charger connectivity - included in short form. */
    bool lid_open:1;
    bool charger_connected:1;
    /* @} */

    /*! @{ Battery levels - only included if short_form is FALSE. */
    uint8 case_battery_state;
    uint8 left_earbud_battery_state;
    uint8 right_earbud_battery_state;
    /* @} */
} case_status_config_t;

/*! \brief Initialise the Case Channel.
 
    Registers case channel with CcProtocol as the handler for
    CASECOMMS_CID_CASE.
*/
void CcCaseChannel_Init(void);

/*! \brief Send a CASE_CHANNEL_MID_EARBUD_STATUS_REQ to a device.
    \param dest Earbud destination.
    \return bool TRUE if packet was accepted for TX to the Earbud, FALSE otherwise. 

    \note Valid destinations are CASECOMMS_DEVICE_LEFT_EB or CASECOMMS_DEVICE_RIGHT_EB.
*/ 
bool CcCaseChannel_EarbudStatusReqTx(cc_dev_t dest);

/*! \brief Send a CASE_CHANNEL_MID_CASE_STATUS to device(s).
    \param dest single Earbud or broadcast to both Earbuds.
    \return bool TRUE if packet was accepted for TX to the Earbud, FALSE otherwise. 

    \note Valid destinations are CASECOMMS_DEVICE_LEFT_EB, CASECOMMS_DEVICE_RIGHT_EB
          or CASECOMMS_DEVICE_BROADCAST
*/
bool CcCaseChannel_CaseStatusTx(cc_dev_t dest, case_status_config_t* config);

/*! \brief Command an Earbud to reboot.
    \param factory_reset TRUE perform factory reset and reboot, otherwise just a reboot.
    \return bool TRUE if packet was accepted for TX to the Earbud, FALSE otherwise. 

    \note Factory reset of Earbuds is not currently supported.
          The message is sent but Earbuds will log a warning only.
*/
bool CcCaseChannel_EarbudResetTx(cc_dev_t dest, bool factory_reset);

/*! \brief Get the BT address of an Earbud.
    \param dest Left or right earbud destinations only are valid.
    \return bool TRUE if packet was accepted for TX to the Earbud, FALSE otherwise. 
*/
bool CcCaseChannel_EarbudBtAddressInfoReqTx(cc_dev_t dest);

#endif /* INCLUDE_CASE_COMMS */
/*! @} End of group documentation */
