/*!
\copyright  Copyright (c) 2020 Qualcomm Technologies International, Ltd.\n
            All Rights Reserved.\n
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\file
\ingroup    case_comms
\brief      Interface to communication with the case using charger comms traps.
*/
/*! \addtogroup case_comms
@{
*/

#ifndef CC_PROTOCOL_H
#define CC_PROTOCOL_H

#ifdef INCLUDE_CASE_COMMS

/*! \brief Types of transport over which to operate case comms. */
typedef enum
{
    /*! Original low speed transport. */
    CASECOMMS_TRANS_SCHEME_A,

    /*! High speed single wire UART transport. */
    CASECOMMS_TRANS_SCHEME_B,

    /*! Plain UART for testing. */
    CASECOMMS_TRANS_TEST_UART
} cc_trans_t;

/* Enforce during compilation that a transport must be defined, and define a commmon symbol
   for the selected transport, used in CcProtocol_Init calls.
*/
#if defined(HAVE_CC_TRANS_SCHEME_A)
#define CC_TRANSPORT CASECOMMS_TRANS_SCHEME_A
#elif defined(HAVE_CC_TRANS_SCHEME_B)
#define CC_TRANSPORT CASECOMMS_TRANS_SCHEME_B
#elif defined(HAVE_CC_TRANS_TEST_UART)
#define CC_TRANSPORT CASECOMMS_TRANS_TEST_UART
#else
#error "No case comms transport defined, must define one of HAVE_CC_TRANS_SCHEME_A, HAVE_CC_TRANS_SCHEME_B or HAVE_CC_TRANS_TEST_UART"
#endif

/*! \brief Channel IDs used by components to communicate over Case Comms. 
    \note These values are used in the protocol with the case
          and must remain in sync with case software.
*/
typedef enum
{
    /*! Status information from the case. */
    CASECOMMS_CID_CASE = 0x0,

    CASECOMMS_CID_MAX,

    CASECOMMS_CID_INVALID = 0xF
} cc_cid_t;

/*! Result of call to #Case_CommsTransmit().
*/
typedef enum
{
    /*! Message successfully received by destination. */
    CASECOMMS_TX_SUCCESS,

    /*! Message transmit failed, destination did not receive. */
    CASECOMMS_TX_FAIL
} cc_tx_status_t;

/*! \brief Devices participating in the Case Comms network.
    \note These values are used in the protocol with the case
          and must remain in sync with case software.
 */
typedef enum
{
    /*! Case. */
    CASECOMMS_DEVICE_CASE      = 0x0,

    /*! Right Earbud. */
    CASECOMMS_DEVICE_RIGHT_EB  = 0x1,

    /*! Left Earbud. */
    CASECOMMS_DEVICE_LEFT_EB   = 0x2,

    /*! Broadcast to both Left and Right Earbud. */
    CASECOMMS_DEVICE_BROADCAST = 0x3,
} cc_dev_t;

/*! \brief Types of mode in which the case comms protocol may be operating.
*/
typedef enum
{
    /*! Earbud mode for use on Earbud devices. */
    CASECOMMS_MODE_EARBUD,

    /*! Case mode for use on Case devices and communicating with 2 devices
        in Earbud mode.
        \note This mode enables additional handling:-
            - transmit ACKs when responses are pending from the Earbuds
            - reset the link with a broadcast message when 3xNACK received */
    CASECOMMS_MODE_CASE
} cc_mode_t;

/*! Callback declaration to be provided by case comms clients for receiving TX status. */
typedef void (*CcProtocol_TxStatus_fn)(cc_tx_status_t status);

/*! Callback declaration to be provided by case comms clients for receiving incoming messages. */
typedef void (*CcProtocol_RxInd_fn)(unsigned mid, const uint8* msg, unsigned len, cc_dev_t source_dev);

/*! \brief Channel configuration to be supplied by case commms channel clients. */
typedef struct
{
    /*! Case comms channel ID being registered. */
    cc_cid_t          cid;

    /*! TX status callback. */
    CcProtocol_TxStatus_fn    tx_sts;

    /*! RX indication callback. */
    CcProtocol_RxInd_fn       rx_ind;
} cc_chan_config_t;

/*! \brief Initialise comms with the case.
*/
void CcProtocol_Init(cc_mode_t mode, cc_trans_t trans);

/*! \brief Register client handler for a case comms channel.
 
    \param config Pointer to channel ID and callback configuration.
*/
void CcProtocol_RegisterChannel(const cc_chan_config_t* config);

/*! \brief Transmit a message over case comms.

    \param dest Destination device.
    \param cid Case comms channel on which to transmit.
    \param mid Case comms message ID of the packet.
    \param data Pointer to the packet payload.
    \param len Length in bytes of the payload.

    \return bool TRUE packet will be transmitted.
                 FALSE packet can not be transmitted.

    \note Return value of TRUE only indicates the packet has been accepted
          for transmission. Client must await CcProtocol_TxStatus_fn callback
          for indication of success/fail receipt by destination.
*/
bool CcProtocol_Transmit(cc_dev_t dest, cc_cid_t cid, unsigned mid, 
                         uint8* data, uint16 len);

#endif /* INCLUDE_CASE_COMMS */
#endif /* CC_PROTOCOL_H */
/*! @} End of group documentation */
