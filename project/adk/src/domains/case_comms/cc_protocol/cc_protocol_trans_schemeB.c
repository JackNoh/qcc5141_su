/*!
\copyright  Copyright (c) 2020 Qualcomm Technologies International, Ltd.\n
            All Rights Reserved.\n
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\file
\ingroup    case_comms
\brief      Transmit and receive handling for Scheme B transport.
*/
/*! \addtogroup case_comms
@{
*/

#ifdef INCLUDE_CASE_COMMS
#ifdef HAVE_CC_TRANS_SCHEME_B

#include "cc_protocol.h"
#include "cc_protocol_private.h"
#include "cc_protocol_trans_schemeB.h"
#include "cc_protocol_trans_schemeB_hw.h"
#include "cc_protocol_config.h"

#include <logging.h>
#include <multidevice.h>
#include <pio_common.h>

#include <message.h>
#include <chargercomms.h>
#include <stdlib.h>
#include <stream.h>
#include <source.h>
#include <sink.h>
#include <panic.h>

bool ccProtocol_TransSchemeBTransmit(cc_dev_t dest, cc_cid_t cid, unsigned mid, 
                                       uint8* data, uint16 len)
{
    cc_protocol_t* td = CcProtocol_GetTaskData();
    uint8* snk = NULL;
    uint8 hdr = 0;
    unsigned total_len = len + CASECOMMS_HEADER_LEN;
    bool sent = FALSE;
    uint8 cc_header = 0;

    /* if there is space in the stream for the packet
     * claim the space
     * get pointer to the correct place in the stream to write */
    if (SinkSlack(td->scheme_data.cc_sink) >= total_len)
    {
        uint16 offset = SinkClaim(td->scheme_data.cc_sink, total_len);
        if (offset != 0xffff)
        {
            snk = (uint8*)PanicNull(SinkMap(td->scheme_data.cc_sink)) + offset;

            /* compose the packet
                - write chargercomms dest into the header
                - write case comms header
                - write case comms payload
             */
            hdr = dest;
            ccProtocol_CaseCommsSetCID(&cc_header, cid);
            ccProtocol_CaseCommsSetMID(&cc_header, mid);
            snk[SCHEME_B_CASECOMMS_HEADER_OFFSET] = cc_header;
            memcpy(&snk[SCHEME_B_CASECOMMS_PAYLOAD_OFFSET], data, len);

            DEBUG_LOG_VERBOSE("ccProtocol_TransSchemeBTransmit enum:cc_dev_t:%d enum:cc_cid_t:%d mid:%d len:%d", dest, cid, mid, total_len);

            /* flush the payload and the header */
            SinkFlushHeader(td->scheme_data.cc_sink, total_len, &hdr, SCHEME_B_CHARGERCOMMS_HEADER_LEN);

            sent = TRUE;
        }
    }

    return sent;
}

void ccProtocol_TransSchemeBReceive(Source src)
{
    unsigned pkt_len = 0;

    PanicFalse(SourceIsValid(src));

    while (SourceSizeHeader(src) == SCHEME_B_CHARGERCOMMS_HEADER_LEN)
    {
        const uint8* pkt = NULL;
        cc_dev_t source_dev;
        cc_cid_t cid = CASECOMMS_CID_INVALID;
        unsigned mid = 0;
        const uint8* hdr = (uint8*)PanicNull((void*)SourceMapHeader(src));
        
        /* get source dev from the header */
        source_dev = *hdr;

        /* access the packet and extract case comms header fields */
        pkt_len = SourceBoundary(src);
        pkt = SourceMap(src);
        if (!pkt_len || !pkt)
        {
            DEBUG_LOG_ERROR("ccProtocol_TransSchemeBReceive len %d pkt %p", pkt_len, pkt);
            Panic();
        }
        cid = ccProtocol_CaseCommsGetCID(pkt[SCHEME_B_CASECOMMS_HEADER_OFFSET]);
        mid = ccProtocol_CaseCommsGetMID(pkt[SCHEME_B_CASECOMMS_HEADER_OFFSET]);

        DEBUG_LOG_VERBOSE("ccProtocol_TransSchemeBReceive enum:cc_dev_t:%d enum:cc_cid_t:%d mid:%d len:%d", source_dev, cid, mid, pkt_len);

        /* pass packet to client, strip case comms header */
        ccProtocol_SendRXPacketToClient(pkt + CASECOMMS_HEADER_LEN,
                                        pkt_len - CASECOMMS_HEADER_LEN,
                                        cid, mid, source_dev);
        SourceDrop(src, pkt_len);
    }
}

/*! single-wire UART stream setup */
void ccProtocol_TransSchemeBSetup(void)
{
    cc_protocol_t* td = CcProtocol_GetTaskData();
    uint32 device_id = CHARGER_COMMS_UART_DEVICE_ID_CASE;

    /* setup PIO as the single wire for TX/RX */
    PioSetMapPins32Bank(PioCommonPioBank(CcProtocol_ConfigSchemeBTxRxPio()), 
                        PioCommonPioMask(CcProtocol_ConfigSchemeBTxRxPio()), 0);
    PioSetFunction(CcProtocol_ConfigSchemeBTxRxPio(), UART_TX);
    PioSetFunction(CcProtocol_ConfigSchemeBTxRxPio(), UART_RX);

    /* complete any hardware setup related to Scheme B */
    ccProtocol_TransSchemeBHwSetup();

    /* determine device ID if we're an earbud */
    if (td->mode == CASECOMMS_MODE_EARBUD)
    {
        device_id = Multidevice_IsLeft() ? CHARGER_COMMS_UART_DEVICE_ID_EB_L :
                                           CHARGER_COMMS_UART_DEVICE_ID_EB_R;
    }

    /* Configure chargercomms over the UART */
    PanicFalse(ChargerCommsUartConfigure(CHARGER_COMMS_UART_CFG_KEY_RX_ENABLE, 1));
    PanicFalse(ChargerCommsUartConfigure(CHARGER_COMMS_UART_CFG_KEY_DEVICE_ID, device_id));
    PanicFalse(ChargerCommsUartConfigure(CHARGER_COMMS_UART_CFG_KEY_BAUD_RATE, 4096));
    PanicFalse(ChargerCommsUartConfigure(CHARGER_COMMS_UART_CFG_KEY_PARITY, 1));

    /* Get the charger comms UART stream handle */
    td->scheme_data.cc_sink = StreamChargerCommsUartSink();

    /* ensure we get messages to the cc_protocol task */
    MessageStreamTaskFromSink(td->scheme_data.cc_sink, CcProtocol_GetTask());
    SourceConfigure(StreamSourceFromSink(td->scheme_data.cc_sink), VM_SOURCE_MESSAGES, VM_MESSAGES_ALL);
    SinkConfigure(td->scheme_data.cc_sink, VM_SINK_MESSAGES, VM_MESSAGES_ALL);
    
    /* check for data already in the stream source before we registered as the Task to
     * be informed */
    ccProtocol_ProcessStreamSource(StreamSourceFromSink(td->scheme_data.cc_sink));;
}

#endif /* HAVE_CC_TRANS_SCHEME_B */
#endif /* INCLUDE_CASE_COMMS */
/*! @} End of group documentation */

