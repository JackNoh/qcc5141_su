/*!
\copyright  Copyright (c) 2020 Qualcomm Technologies International, Ltd.\n
            All Rights Reserved.\n
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\file
\ingroup    case_comms
\brief      Communication with the case using charger comms traps.
*/
/*! \addtogroup case_comms
@{
*/

#include "cc_protocol.h"
#include "cc_protocol_private.h"
#include "cc_protocol_trans_schemeA.h"
#include "cc_protocol_trans_schemeB.h"
#include "cc_protocol_trans_test_uart.h"

#include <multidevice.h>

#include <logging.h>

#include <message.h>
#include <stdlib.h>
#include <panic.h>
#include <chargercomms.h>
#include <stream.h>
#include <Source.h>
#include <sink.h>

#ifdef INCLUDE_CASE_COMMS

/*! Case comms protocol task state. */
cc_protocol_t cc_protocol;

/***********************************************
 * Case Comms Protocol message utility functions
 ***********************************************/
/*! \brief Utility function to read Channel ID from a Case Comms header. */
cc_cid_t ccProtocol_CaseCommsGetCID(uint8 ccomms_header)
{
    return ((ccomms_header & CASECOMMS_CID_MASK) >> CASECOMMS_CID_BIT_OFFSET);
}

/*! \brief Utility function to set Channel ID in a Case Comms header. */
void ccProtocol_CaseCommsSetCID(uint8* ccomms_header, cc_cid_t cid)
{
    *ccomms_header = (*ccomms_header & ~CASECOMMS_CID_MASK) | ((cid << CASECOMMS_CID_BIT_OFFSET) & CASECOMMS_CID_MASK);
}

/*! \brief Utility function to read Message ID from a Case Comms header. */
unsigned ccProtocol_CaseCommsGetMID(uint8 ccomms_header)
{
    return ((ccomms_header & CASECOMMS_MID_MASK) >> CASECOMMS_MID_BIT_OFFSET);
}

/*! \brief Utility function to set Message ID in a Case Comms header. */
void ccProtocol_CaseCommsSetMID(uint8* ccomms_header, unsigned mid)
{
    *ccomms_header = (*ccomms_header & ~CASECOMMS_MID_MASK) | ((mid << CASECOMMS_MID_BIT_OFFSET) & CASECOMMS_MID_MASK);
}

/****************************************
 * Case Comms component utility functions
 ****************************************/
static cc_chan_config_t* ccProtocol_GetChannelConfig(cc_cid_t cid)
{
    cc_protocol_t* td = CcProtocol_GetTaskData();
    PanicFalse(cid < CASECOMMS_CID_MAX);
    return &td->channel_cfg[cid];
}

/*! \brief Convert charger comms status to case comms status. */
static cc_tx_status_t ccProtocol_CommsGetStatus(charger_comms_msg_status status)
{
    cc_tx_status_t sts = CASECOMMS_TX_SUCCESS;

    if (status != CHARGER_COMMS_MSG_SUCCESS)
    {
        sts = CASECOMMS_TX_FAIL;
    }

    return sts;
}

/***********************************
 * Common transport handling
 ***********************************/
static void ccProtocol_HandleMessageChargercommsStatus(const MessageChargerCommsStatus* status)
{
    cc_protocol_t* td = CcProtocol_GetTaskData();

    DEBUG_LOG_INFO("ccProtocol_HandleMessageChargercommsStatus sts:%d", status->status);

    /* notify client status of message transmission */
    switch (td->cc_cid_in_transit)
    {
        case CASECOMMS_CID_CASE:
            /* if registered pass status to client */
            if (ccProtocol_GetChannelConfig(CASECOMMS_CID_CASE)->tx_sts)
            {
                ccProtocol_GetChannelConfig(CASECOMMS_CID_CASE)->tx_sts(ccProtocol_CommsGetStatus(status->status));
            }
            break;
        case CASECOMMS_CID_INVALID:
            DEBUG_LOG_WARN("ccProtocol_HandleMessageChargercommsStatus recv unsolicited status %d", status->status);
            break;
        default:
            DEBUG_LOG_WARN("ccProtocol_HandleMessageChargercommsStatus recv status %d for unsupported cid %d", status->status, td->cc_cid_in_transit);
            break;
    }

    td->cc_cid_in_transit = CASECOMMS_CID_INVALID;
}

void ccProtocol_SendRXPacketToClient(const uint8* pkt, unsigned len, cc_cid_t cid, unsigned mid, cc_dev_t source_dev)
{
    DEBUG_LOG_VERBOSE("ccProtocol_SendRXPacketToClient len:%d enum:cc_cid_t:%d mid:%d enum:cc_dev_t:%d", len, cid, mid, source_dev);

    switch (cid)
    {
        case CASECOMMS_CID_CASE:
            /* if registered, pass incoming message payload to case channel handler */
            if (ccProtocol_GetChannelConfig(cid)->rx_ind)
            {
                ccProtocol_GetChannelConfig(cid)->rx_ind(mid, pkt, len, source_dev);
            }
            break;
        default:
            DEBUG_LOG_WARN("ccProtocol_SendRXPacketToClient unsupported cid enum:cc_cid_t:%d", cid);
            break;
    }
}

void ccProtocol_ProcessStreamSource(Source src)
{
    cc_protocol_t* td = CcProtocol_GetTaskData();

    switch (td->trans)
    {
        case CASECOMMS_TRANS_SCHEME_B:
            ccProtocol_TransSchemeBReceive(src);
            break;
        case CASECOMMS_TRANS_TEST_UART:
            ccProtocol_TransTestUartReceive(src);
            break;
        case CASECOMMS_TRANS_SCHEME_A:
            /* fall-thru */
        default:
            DEBUG_LOG_ERROR("ccProtocol_ProcessStreamSource unsupported transport enum:cc_trans_t:%d for MMD from source 0x%x", td->trans, src);
            Panic();
            break;
    }
}

static void CcProtocol_HandleMessage(Task task, MessageId id, Message message)
{
    UNUSED(task);

    switch (id)
    {
        /* stream based transport messages */
        case MESSAGE_MORE_DATA:
            ccProtocol_ProcessStreamSource(((const MessageMoreData*)message)->source);
            break;
        case MESSAGE_MORE_SPACE:
            /* not used */
            break;

        /* trap based transport messages */
        case MESSAGE_CHARGERCOMMS_IND:
            ccProtocol_TransSchemeAReceive((const MessageChargerCommsInd*)message);
            break;

        /* Commone messages */
        case MESSAGE_CHARGERCOMMS_STATUS:
            ccProtocol_HandleMessageChargercommsStatus((const MessageChargerCommsStatus*)message);
            break;

        default:
            DEBUG_LOG_WARN("CcProtocol_HandleMessage. Unhandled message MESSAGE:0x%x",id);
            break;
    }
}

/*****************************
 * Public API
 *****************************/
void CcProtocol_Init(cc_mode_t mode, cc_trans_t trans)
{
    cc_protocol_t* td = CcProtocol_GetTaskData();

    memset(td, 0, sizeof(cc_protocol_t));
    td->task.handler = CcProtocol_HandleMessage;
    td->mode = mode;
    td->trans = trans;
    td->cc_cid_in_transit = CASECOMMS_CID_INVALID;

    /* Register to receive charger comms messages from P0 */
    MessageChargerCommsTask(CcProtocol_GetTask());

    /* Setup the transports */
    /* scheme A transport has no setup requirements */
    ccProtocol_TransSchemeBSetup();
    ccProtocol_TransTestUartSetup();
}

bool CcProtocol_Transmit(cc_dev_t dest, cc_cid_t cid, unsigned mid, 
                        uint8* data, uint16 len)
{
    cc_protocol_t* td = CcProtocol_GetTaskData();

    /* only support a single message at a time */
    if (td->cc_cid_in_transit != CASECOMMS_CID_INVALID)
    {
        DEBUG_LOG_WARN("CcProtocol_Transmit message already in transit enum:cc_cid_t:%d", td->cc_cid_in_transit);
        return FALSE;
    }

    /* call the transport specific transmit function, resolved by the linker to the
       specific transport compiled in. */
    if (!ccProtocol_TransTransmit(dest, cid, mid, data, len))
    {
        DEBUG_LOG_WARN("CcProtocol_Transmit transport failed to send message enum:cc_dev_t:%d enum:cc_cid_t:%d mid:%d", dest, cid, mid);
        return FALSE;
    }
    else
    {
        /* record the CID currently being transmitted
         * not for the test uart as ACKs are not supported, so would not get
         * cleared and would block any further transmissions */
        if (td->trans != CASECOMMS_TRANS_TEST_UART)
        {
            td->cc_cid_in_transit = cid;
        }
        return TRUE;
    }
}

void CcProtocol_RegisterChannel(const cc_chan_config_t* config)
{
    cc_chan_config_t* cfg = ccProtocol_GetChannelConfig(config->cid);

    cfg->cid = config->cid;
    cfg->tx_sts = config->tx_sts;
    cfg->rx_ind = config->rx_ind;
}

#endif /* INCLUDE_CASE_COMMS */
/*! @} End of group documentation */
