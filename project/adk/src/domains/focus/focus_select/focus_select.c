/*!
\copyright  Copyright (c) 2020 Qualcomm Technologies International, Ltd.\n
            All Rights Reserved.\n
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\file
\defgroup   select_focus_domains Focus Select
\ingroup    focus_domains
\brief      This module is an implementation of the focus interface which supports
            selecting the active, focussed device during multipoint use cases.
*/

#include "focus_select.h"

#include "focus_audio_source.h"
#include "focus_device.h"
#include "focus_voice_source.h"

#include <audio_sources.h>
#include <sources_iterator.h>
#include <bt_device.h>
#include <connection_manager.h>
#include <device.h>
#include <device_list.h>
#include <device_properties.h>
#include <handset_service_config.h>
#include <logging.h>
#include <panic.h>
#include <ui.h>
#include <ui_inputs.h>
#include <stdlib.h>
#include <audio_router.h>

#define CONVERT_AUDIO_SOURCE_TO_ARRAY_INDEX(x) ((x)-1)

#define SOURCE_HAS_AUDIO 0x40

#ifndef ENABLE_A2DP_BARGE_IN
#define ENABLE_A2DP_BARGE_IN FALSE
#endif

/* Used to collect context information from the audio sources available in the
   framework, in a standard form. This data can then be processed to decide what
   should be assigned foreground focus. */
typedef struct
{
    audio_source_provider_context_t context_by_source_array[CONVERT_AUDIO_SOURCE_TO_ARRAY_INDEX(max_audio_sources)];
    audio_source_t highest_priority_audio_source;
    audio_source_provider_context_t highest_priority_context;
    uint8 num_highest_priority_sources;

} audio_source_focus_status_t;

static focus_select_audio_priority_t * audio_source_priorities = NULL;
static bool a2dp_barge_in_enabled;

static audio_source_provider_context_t focusSelect_GetContext(audio_source_focus_status_t * focus_status, audio_source_t source)
{
    PanicFalse(source != audio_source_none);
    return focus_status->context_by_source_array[CONVERT_AUDIO_SOURCE_TO_ARRAY_INDEX(source)];
}

static bool focusSelect_IsSourceContextConnected(audio_source_focus_status_t * focus_status, audio_source_t source)
{
    return focusSelect_GetContext(focus_status, source) == context_audio_connected;
}

static bool focusSelect_IsSourceContextHighestPriority(audio_source_focus_status_t * focus_status, audio_source_t source)
{
    return focusSelect_GetContext(focus_status, source) == focus_status->highest_priority_context;
}

static bool focusSelect_CompileAudioSourceFocusStatus(audio_source_focus_status_t * focus_status)
{
    audio_source_t curr_source = audio_source_none;
    sources_iterator_t iter = NULL;
    bool source_found = FALSE;

    iter = SourcesIterator_Create(source_type_audio);
    curr_source = SourcesIterator_NextAudioSource(iter);

    while (curr_source != audio_source_none)
    {
        audio_source_provider_context_t context = AudioSources_GetSourceContext(curr_source);

        PanicFalse(context != BAD_CONTEXT);

        if (!source_found || context > focus_status->highest_priority_context)
        {
            focus_status->highest_priority_audio_source = curr_source;
            focus_status->highest_priority_context = context;
            focus_status->num_highest_priority_sources = 1;
            source_found = TRUE;

        }
        else if (context == focus_status->highest_priority_context)
        {
            focus_status->num_highest_priority_sources += 1;
        }

        focus_status->context_by_source_array[CONVERT_AUDIO_SOURCE_TO_ARRAY_INDEX(curr_source)] = context;

        curr_source = SourcesIterator_NextAudioSource(iter);
    }

    SourcesIterator_Destroy(iter);

    return source_found;
}

static audio_source_t focusSelect_GetMruAudioSource(void)
{
    uint8 is_mru_handset = TRUE;
    device_t device = DeviceList_GetFirstDeviceWithPropertyValue(device_property_mru, &is_mru_handset, sizeof(uint8));
    
    if (device == NULL)
    {
        deviceType type = DEVICE_TYPE_HANDSET;
        device = DeviceList_GetFirstDeviceWithPropertyValue(device_property_type, &type, sizeof(deviceType));
    }
    
    if (device != NULL)
    {
        return DeviceProperties_GetAudioSource(device);
    }
    
    return audio_source_none;
}


static audio_source_t focusSelect_ConvertAudioPrioToSource(focus_select_audio_priority_t prio)
{
    audio_source_t source = audio_source_none;
    switch(prio)
    {
    case FOCUS_SELECT_AUDIO_LINE_IN:
        source = audio_source_line_in;
        break;
    case FOCUS_SELECT_AUDIO_USB:
        source = audio_source_usb;
        break;
    case FOCUS_SELECT_AUDIO_A2DP:
        {
            source = AudioRouter_GetLastRoutedAudio();
            
            if(a2dp_barge_in_enabled || source == audio_source_none)
            {
                source = focusSelect_GetMruAudioSource();
            }
        }
        break;
    default:
        break;
    }
    return source;
}

static void focusSelect_HandleTieBreak(audio_source_focus_status_t * focus_status)
{
    audio_source_t last_routed_audio;

    /* Nothing to be done if all audio sources are disconnected or there is no need to tie break */
    if (focus_status->highest_priority_context == context_audio_disconnected ||
        focus_status->num_highest_priority_sources == 1)
    {
        return;
    }

    last_routed_audio = AudioRouter_GetLastRoutedAudio();

    /* A tie break is needed. Firstly, use the last routed audio source, if one exists */
    if (last_routed_audio != audio_source_none && focusSelect_IsSourceContextConnected(focus_status, last_routed_audio) )
    {
        focus_status->highest_priority_audio_source = last_routed_audio;
    }
    /* Otherwise, run through the prioritisation of audio sources and select the highest */
    else
    {
        audio_source_t curr_source = audio_source_none;
        if (audio_source_priorities != NULL)
        {
            /* Tie break using the Application specified priority. */
            for (int i=0; i<FOCUS_SELECT_AUDIO_MAX_SOURCES; i++)
            {
                curr_source = focusSelect_ConvertAudioPrioToSource(audio_source_priorities[i]);
                if (focusSelect_IsSourceContextHighestPriority(focus_status, curr_source))
                {
                    focus_status->highest_priority_audio_source = curr_source;
                    break;
                }
            }
        }
        else
        {
            /* Tie break using implicit ordering of audio source. */
            for (curr_source++ ; curr_source < max_audio_sources; curr_source++)
            {
                if (focusSelect_IsSourceContextHighestPriority(focus_status, curr_source))
                {
                    focus_status->highest_priority_audio_source = curr_source;
                    break;
                }
            }
        }
    }

    DEBUG_LOG("focusSelect_HandleTieBreak enum:audio_source_t:%d enum:audio_source_provider_context_t:%d",
               focus_status->highest_priority_audio_source,
               focus_status->highest_priority_context);
}

static bool focusSelect_GetAudioSourceForContext(audio_source_t * audio_source)
{
    bool source_found = FALSE;
    audio_source_focus_status_t focus_status = {0};

    *audio_source = audio_source_none;

    source_found = focusSelect_CompileAudioSourceFocusStatus(&focus_status);
    if (source_found)
    {
        focusSelect_HandleTieBreak(&focus_status);

        /* Assign selected audio source */
        *audio_source = focus_status.highest_priority_audio_source;
    }

    DEBUG_LOG_DEBUG("focusSelect_GetAudioSourceForContext enum:audio_source_t:%d found=%d",
                    *audio_source, source_found);

    return source_found;
}

static bool focusSelect_GetAudioSourceForUiInput(ui_input_t ui_input, audio_source_t * audio_source)
{
    bool source_found = FALSE;
    audio_source_focus_status_t focus_status = {0};

    /* For audio sources, we don't need to consider the UI Input type. This is because it
       is effectively prescreened by the UI component, which responds to the context returned
       by this module in the API focusSelect_GetAudioSourceForContext().

       A concrete example being we should only receive ui_input_stop if
       focusSelect_GetAudioSourceForContext() previously provided context_audio_is_streaming.
       In that case there can only be a single streaming source and it shall consume the
       UI Input. All other contentions are handled by focusSelect_HandleTieBreak. */

    *audio_source = audio_source_none;

    source_found = focusSelect_CompileAudioSourceFocusStatus(&focus_status);
    if (source_found)
    {
        focusSelect_HandleTieBreak(&focus_status);

        /* Assign selected audio source */
        *audio_source = focus_status.highest_priority_audio_source;
    }

    DEBUG_LOG_DEBUG("focusSelect_GetAudioSourceForUiInput enum:ui_input_t:%d enum:audio_source_t:%d found=%d",
                    ui_input, *audio_source, source_found);

    return source_found;
}

static focus_t focusSelect_GetFocusForAudioSource(const audio_source_t audio_source)
{
    focus_t focus = focus_none;
    bool source_found = FALSE;
    audio_source_focus_status_t focus_status = {0};

    source_found = focusSelect_CompileAudioSourceFocusStatus(&focus_status);
    if (source_found)
    {
        if (focusSelect_GetContext(&focus_status, audio_source) != context_audio_disconnected)
        {
            focusSelect_HandleTieBreak(&focus_status);

            if (focus_status.highest_priority_audio_source == ((audio_source_t)audio_source))
            {
                focus = focus_foreground;
            }
            else
            {
                focus = focus_background;
            }
        }
    }

    DEBUG_LOG_DEBUG("focusSelect_GetFocusForAudioSource enum:audio_source_t:%d enum:focus_t:%d",
                    audio_source, focus);

    return focus;
}

#define CONVERT_VOICE_SOURCE_TO_ARRAY_INDEX(x) ((x)-1)

typedef struct
{
    voice_source_provider_context_t context;
    bool has_audio;
} voice_source_cache_data_t;

/* Used to collect context information from the voice sources available in the
   framework, in a standard form. This data can then be processed to decide what
   should be assigned foreground focus. */
typedef struct
{
    voice_source_cache_data_t cache_data_by_source_array[CONVERT_VOICE_SOURCE_TO_ARRAY_INDEX(max_voice_sources)];
    voice_source_t highest_priority_voice_source;
    bool highest_priority_voice_source_has_audio;
    voice_source_provider_context_t highest_priority_context;
    uint8 num_highest_priority_sources;

} voice_source_focus_status_t;

static focus_select_voice_priority_t * voice_source_priorities = NULL;

static voice_source_provider_context_t focusSelect_VoiceGetContext(voice_source_focus_status_t * focus_status, voice_source_t source)
{
    PanicFalse(source != voice_source_none);
    return focus_status->cache_data_by_source_array[CONVERT_VOICE_SOURCE_TO_ARRAY_INDEX(source)].context;
}

static bool focusSelect_GetAvailabilityFromStatus(voice_source_focus_status_t * focus_status, voice_source_t source)
{
    PanicFalse(source != voice_source_none);
    return focus_status->cache_data_by_source_array[CONVERT_VOICE_SOURCE_TO_ARRAY_INDEX(source)].has_audio;
}

static void focusSelect_SetCacheDataForSource(voice_source_focus_status_t * focus_status, voice_source_t source, voice_source_provider_context_t context, bool has_audio)
{
    PanicFalse(source != voice_source_none);
    focus_status->cache_data_by_source_array[CONVERT_VOICE_SOURCE_TO_ARRAY_INDEX(source)].has_audio = has_audio;
    focus_status->cache_data_by_source_array[CONVERT_VOICE_SOURCE_TO_ARRAY_INDEX(source)].context = context;
}

static bool focusSelect_VoiceIsSourceContextHighestPriority(voice_source_focus_status_t * focus_status, voice_source_t source)
{
    return focusSelect_VoiceGetContext(focus_status, source) == focus_status->highest_priority_context;
}

/* Look-up table mapping the voice_context symbol to the relative priority of
   that context in determining focus. This table considers priorities for audio
   routing purposes. 0 is the lowest priority. */
static int8 voice_context_to_audio_prio_mapping[] = {
    0, // context_voice_disconnected
    1, // context_voice_connected,
    4, // context_voice_ringing_outgoing,
    2, // context_voice_ringing_incoming,
    3, // context_voice_in_call,
};
COMPILE_TIME_ASSERT(ARRAY_DIM(voice_context_to_audio_prio_mapping) == max_voice_contexts, FOCUS_SELECT_invalid_size_audio_prio_mapping_table);

/* Look-up table mapping the voice_context symbol to the relative priority of
   that context in determining focus. This table considers priorities for UI
   interactions. 0 is the lowest priority. */
static int8 voice_context_to_ui_prio_mapping[] = {
    0, // context_voice_disconnected
    1, // context_voice_connected,
    3, // context_voice_ringing_outgoing,
    4, // context_voice_ringing_incoming,
    2, // context_voice_in_call,
};
COMPILE_TIME_ASSERT(ARRAY_DIM(voice_context_to_ui_prio_mapping) == max_voice_contexts, FOCUS_SELECT_invalid_size_ui_prio_mapping_table);

/* Functions of this type shall return an integer value indicating whether the current source has higher,
   equal, or lower priority than the existing highest priority source stored in the focus_status.

   A positive integer indicates higher priority, 0 indicates equal priority and a negative value indicates
   lower priority. */
typedef int8 (*priority_evaluator_fn_t)(voice_source_focus_status_t * focus_status, unsigned context, bool has_audio);

static int8 focusSelect_PriorityEvaluatorForAudio(voice_source_focus_status_t * focus_status, unsigned context, bool has_audio)
{
    // Basic prioritisation
    int8 existing_priority = voice_context_to_audio_prio_mapping[focus_status->highest_priority_context];
    int8 this_source_priority = voice_context_to_audio_prio_mapping[context];

    // Factor in for if there is audio available for the sources, we use a bit mask for this
    this_source_priority |= (has_audio ? SOURCE_HAS_AUDIO : 0x0);
    existing_priority |= (focus_status->highest_priority_voice_source_has_audio ? SOURCE_HAS_AUDIO : 0x0);

    return (this_source_priority - existing_priority);
}

static int8 focusSelect_PriorityEvaluatorForUi(voice_source_focus_status_t * focus_status, unsigned context, bool has_audio)
{
    UNUSED(has_audio);

    int8 existing_priority = voice_context_to_ui_prio_mapping[focus_status->highest_priority_context];
    int8 this_source_priority = voice_context_to_ui_prio_mapping[context];

    return (this_source_priority - existing_priority);
}

static bool focusSelect_CompileVoiceSourceFocusStatus(voice_source_focus_status_t * focus_status, priority_evaluator_fn_t evaluator)
{
    voice_source_t curr_source = voice_source_none;
    sources_iterator_t iter = NULL;
    bool source_found = FALSE;

    iter = SourcesIterator_Create(source_type_voice);
    curr_source = SourcesIterator_NextVoiceSource(iter);

    while (curr_source != voice_source_none)
    {
        bool source_has_audio = VoiceSources_IsVoiceChannelAvailable(curr_source);
        voice_source_provider_context_t source_context = VoiceSources_GetSourceContext(curr_source);

        PanicFalse(source_context != BAD_CONTEXT);

        int16 priority_evalution = evaluator(focus_status, source_context, source_has_audio);
        if (!source_found || priority_evalution > 0)
        {
            /* New highest priority source found */
            focus_status->highest_priority_voice_source = curr_source;
            focus_status->highest_priority_context = source_context;
            focus_status->highest_priority_voice_source_has_audio = source_has_audio;
            focus_status->num_highest_priority_sources = 1;
            source_found = TRUE;
        }
        else if (priority_evalution == 0)
        {
            /* The sources have equal priority, this may cause a tie break to occur later */
            focus_status->num_highest_priority_sources += 1;
        }
        else
        {
            /* priority_evalution < 0, indicates the source is lower priority than
               existing, do nothing */
        }

        focusSelect_SetCacheDataForSource(focus_status, curr_source, source_context, source_has_audio);

        curr_source = SourcesIterator_NextVoiceSource(iter);
    }

    SourcesIterator_Destroy(iter);

    return source_found;
}


static voice_source_t focusSelect_ConvertVoicePrioToSource(focus_select_voice_priority_t prio)
{
    voice_source_t source = voice_source_none;
    switch(prio)
    {
    case FOCUS_SELECT_VOICE_USB:
        source = voice_source_usb;
        break;
    case FOCUS_SELECT_VOICE_HFP:
        {
            uint8 is_mru_handset = TRUE;
            device_t device = DeviceList_GetFirstDeviceWithPropertyValue(device_property_mru, &is_mru_handset, sizeof(uint8));
            if (device == NULL)
            {
                deviceType type = DEVICE_TYPE_HANDSET;
                device = DeviceList_GetFirstDeviceWithPropertyValue(device_property_type, &type, sizeof(deviceType));
                DEBUG_LOG_DEBUG("focusSelect_HandleVoiceTieBreak using first handset in device db voice source");
            }
            else
            {
                DEBUG_LOG_DEBUG("focusSelect_HandleVoiceTieBreak using MRU device voice source");
            }
            if (device != NULL)
            {
                source = DeviceProperties_GetVoiceSource(device);
            }
        }
        break;
    default:
        break;
    }
    return source;
}

static void focusSelect_HandleVoiceTieBreak(voice_source_focus_status_t * focus_status)
{
    /* Nothing to be done if all voice sources are disconnected or there is no need to tie break */
    if (focus_status->highest_priority_context == context_voice_disconnected ||
        focus_status->num_highest_priority_sources == 1)
    {
        return;
    }

    /* Run through the prioritisation of voice sources and select the highest */
    voice_source_t curr_source = voice_source_none;
    if (voice_source_priorities != NULL)
    {
        /* Tie break using the Application specified priority. */
        for (int i=0; i<FOCUS_SELECT_VOICE_MAX_SOURCES; i++)
        {
            curr_source = focusSelect_ConvertVoicePrioToSource(voice_source_priorities[i]);
            if (focusSelect_VoiceIsSourceContextHighestPriority(focus_status, curr_source))
            {
                focus_status->highest_priority_voice_source = curr_source;
                break;
            }
        }
    }
    else
    {
        /* Tie break using implicit ordering of voice source. */
        for (curr_source++ ; curr_source < max_voice_sources; curr_source++)
        {
            if (focusSelect_VoiceIsSourceContextHighestPriority(focus_status, curr_source))
            {
                focus_status->highest_priority_voice_source = curr_source;
                break;
            }
        }
    }

    DEBUG_LOG_VERBOSE("focusSelect_HandleVoiceTieBreak selected enum:voice_source_t:%d  enum:voice_source_provider_context_t:%d",
                      focus_status->highest_priority_voice_source,
                      focus_status->highest_priority_context);
}

static bool focusSelect_GetVoiceSourceForUiInteraction(voice_source_t * voice_source)
{
    bool source_found = FALSE;
    voice_source_focus_status_t focus_status = {0};

    *voice_source = voice_source_none;

    source_found = focusSelect_CompileVoiceSourceFocusStatus(&focus_status, focusSelect_PriorityEvaluatorForUi);
    if (source_found)
    {
        focusSelect_HandleVoiceTieBreak(&focus_status);

        /* Assign selected voice source */
        *voice_source = focus_status.highest_priority_voice_source;
    }

    return source_found;
}

static bool focusSelect_GetVoiceSourceForContext(ui_providers_t provider, voice_source_t * voice_source)
{
    bool source_found = focusSelect_GetVoiceSourceForUiInteraction(voice_source);

    DEBUG_LOG_DEBUG("focusSelect_GetVoiceSourceForContext enum:ui_providers_t:%d enum:voice_source_t:%d found=%d",
                    provider, *voice_source, source_found);

    return source_found;
}

static bool focusSelect_GetVoiceSourceForUiInput(ui_input_t ui_input, voice_source_t * voice_source)
{
    bool source_found = focusSelect_GetVoiceSourceForUiInteraction(voice_source);

    DEBUG_LOG_DEBUG("focusSelect_GetVoiceSourceForUiInput enum:ui_input_t:%d enum:voice_source_t:%d found=%d",
                    ui_input, *voice_source, source_found);

    return source_found;
}

static focus_t focusSelect_GetFocusForVoiceSource(const voice_source_t voice_source)
{
    focus_t focus = focus_none;
    bool source_found = FALSE;
    voice_source_focus_status_t focus_status = {0};

    source_found = focusSelect_CompileVoiceSourceFocusStatus(&focus_status, focusSelect_PriorityEvaluatorForAudio);
    if (source_found)
    {
        if ((focusSelect_VoiceGetContext(&focus_status, voice_source) != context_voice_disconnected) ||
            focusSelect_GetAvailabilityFromStatus(&focus_status, voice_source))
        {
            focusSelect_HandleVoiceTieBreak(&focus_status);

            if (focus_status.highest_priority_voice_source == ((voice_source_t)voice_source))
            {
                focus = focus_foreground;
            }
            else
            {
                focus = focus_background;
            }
        }
    }

    DEBUG_LOG_DEBUG("focusSelect_GetFocusForVoiceSource enum:voice_source_t:%d enum:focus_t:%d",
                    voice_source, focus);

    return focus;
}

static bool focusSelect_GetHandsetDevice(device_t *device)
{
    DEBUG_LOG_FN_ENTRY("focusSelect_GetHandsetDevice");
    bool device_found = FALSE;

    PanicNull(device);

    for (uint8 pdl_index = 0; pdl_index < DeviceList_GetMaxTrustedDevices(); pdl_index++)
    {
        if (BtDevice_GetIndexedDevice(pdl_index, device))
        {
            if (BtDevice_GetDeviceType(*device) == DEVICE_TYPE_HANDSET)
            {

                uint8 is_excluded = FALSE;
                Device_GetPropertyU8(*device, device_property_excludelist, &is_excluded);

                if (!is_excluded)
                {
                    device_found = TRUE;
                    break;
                }
            }
        }
    }

    return device_found;
}

static bool focusSelect_GetDeviceForUiInput(ui_input_t ui_input, device_t * device)
{
    bool device_found = FALSE;

    switch (ui_input)
    {
        case ui_input_connect_handset:
            device_found = focusSelect_GetHandsetDevice(device);
            break;
        default:
            DEBUG_LOG_WARN("focusSelect_GetDeviceForUiInput enum:ui_input_t:%d not supported", ui_input);
            break;
    }

    return device_found;
}

static bool focusSelect_GetDeviceForContext(ui_providers_t provider, device_t* device)
{
    UNUSED(provider);
    UNUSED(device);
    return FALSE;
}

static focus_t focusSelect_GetFocusForDevice(const device_t device)
{
    UNUSED(device);
    focus_t focus = focus_none;

    return focus;
}


static bool focusSelect_ExcludeDevice(device_t device)
{
    DEBUG_LOG_FN_ENTRY("focusSelect_ExcludeDevice device 0x%p", device);
    if (device)
    {
        return Device_SetPropertyU8(device, device_property_excludelist, TRUE);
    }
    return FALSE;
}

static bool focusSelect_IncludeDevice(device_t device)
{
    DEBUG_LOG_FN_ENTRY("focusSelect_IncludeDevice device 0x%p", device);

    if (device)
    {
        return Device_SetPropertyU8(device, device_property_excludelist, FALSE);
    }
    return FALSE;
}

static void focusSelect_ResetExcludedDevices(void)
{
    DEBUG_LOG_FN_ENTRY("focusSelect_ResetExcludedDevices");

    device_t* devices = NULL;
    unsigned num_devices = 0;
    uint8 excluded = TRUE;

    DeviceList_GetAllDevicesWithPropertyValue(device_property_excludelist, 
                                        (void*)&excluded, sizeof(excluded), 
                                        &devices, &num_devices);

    if (devices && num_devices)
    {
        for (unsigned i = 0; i < num_devices; i++)
        {
            bdaddr handset_addr = DeviceProperties_GetBdAddr(devices[i]);
            bool is_acl_connected = ConManagerIsConnected(&handset_addr);

            /* Only remove the device from exclude list if ACL is not connected. */
            if(!is_acl_connected)
            {
                focusSelect_IncludeDevice(devices[i]);
            }
        }
    }
    free(devices);
    devices = NULL;
}

static const focus_device_t interface_fns_for_device =
{
    .for_context = focusSelect_GetDeviceForContext,
    .for_ui_input = focusSelect_GetDeviceForUiInput,
    .focus = focusSelect_GetFocusForDevice,
    .add_to_excludelist = focusSelect_ExcludeDevice,
    .remove_from_excludelist = focusSelect_IncludeDevice,
    .reset_excludelist = focusSelect_ResetExcludedDevices
};

static const focus_get_audio_source_t interface_fns =
{
    .for_context = focusSelect_GetAudioSourceForContext,
    .for_ui_input = focusSelect_GetAudioSourceForUiInput,
    .focus = focusSelect_GetFocusForAudioSource
};

static const focus_get_voice_source_t voice_interface_fns =
{
    .for_context = focusSelect_GetVoiceSourceForContext,
    .for_ui_input = focusSelect_GetVoiceSourceForUiInput,
    .focus = focusSelect_GetFocusForVoiceSource
};

bool FocusSelect_Init(Task init_task)
{
    UNUSED(init_task);

    DEBUG_LOG_FN_ENTRY("FocusSelect_Init");

    Focus_ConfigureDevice(&interface_fns_for_device);
    Focus_ConfigureAudioSource(&interface_fns);
    Focus_ConfigureVoiceSource(&voice_interface_fns);

    audio_source_priorities = NULL;
    a2dp_barge_in_enabled = ENABLE_A2DP_BARGE_IN;

    return TRUE;
}

void FocusSelect_ConfigureAudioSourcePriorities(const focus_select_audio_priority_t *prioritisation)
{
    audio_source_priorities = (focus_select_audio_priority_t *)prioritisation;
}

void FocusSelect_ConfigureVoiceSourcePriorities(const focus_select_voice_priority_t *prioritisation)
{
    voice_source_priorities = (focus_select_voice_priority_t *)prioritisation;
}

void FocusSelect_EnableA2dpBargeIn(bool barge_in_enable)
{
    a2dp_barge_in_enabled = barge_in_enable;
}
