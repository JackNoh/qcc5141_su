/*!
\copyright  Copyright (c) 2020 Qualcomm Technologies International, Ltd.\n
            All Rights Reserved.\n
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\file
\brief      Charger Detection
*/
#include "logging.h"

#include "charger_detect.h"
#include "charger_detect_config.h"

#include "charger_monitor.h"

#include "usb_device.h"

#include "panic.h"

#ifdef INCLUDE_CHARGER_DETECT

/*! Charger Detection data */
typedef struct
{
    /*! Task data */
    TaskData task;

    const charger_config_t *charger_config;
} charger_detection_data_t;

charger_detection_data_t charger_detect_data;

unsigned ChargerDetect_Current(void)
{
    return charger_detect_data.charger_config ?
            charger_detect_data.charger_config->current:
            0;
}

void ChargerDetect_Detected(MessageChargerDetected *msg)
{
    DEBUG_LOG("ChargerDetect: MSG detected %d", msg->attached_status);

    const charger_config_t *charger_config = ChargerDetect_GetConfig(msg);

    if (!charger_config ||
            charger_detect_data.charger_config == charger_config)
    {
        return;
    }

    DEBUG_LOG_INFO("ChargerDetect: current %d", charger_config->current);

    charger_detect_data.charger_config = charger_config;

    Charger_UpdateConnected(msg->attached_status != DETACHED);
    Charger_UpdateCurrent();
}

void ChargerDetect_Changed(MessageChargerChanged *msg)
{
    DEBUG_LOG("ChargerDetect: MSG connected %d", msg->charger_connected);

    const charger_config_t *charger_config = ChargerDetect_GetConnectedConfig(msg->charger_connected);

    if (!charger_config ||
            charger_detect_data.charger_config == charger_config)
    {
        return;
    }

    DEBUG_LOG_INFO("ChargerDetect: connected %d", msg->charger_connected);

    charger_detect_data.charger_config = charger_config;

    Charger_UpdateConnected(msg->charger_connected);
    Charger_UpdateCurrent();
}

static void chargerDetect_HandleMessage(Task task, MessageId id, Message message)
{
    UNUSED(task);
    UNUSED(message);

    switch (id)
    {
    case USB_DEVICE_ENUMERATED:
    case USB_DEVICE_DECONFIGURED:
    case USB_DEVICE_SUSPEND:
    case USB_DEVICE_RESUME:
        /* TBD: handle USB device events */
        break;

    default:
        DEBUG_LOG_ALWAYS("ChargerDetect: unhandled message %d", id);
        break;
    }
}

void ChargerDetect_Init(void)
{
    charger_detect_data.task.handler = chargerDetect_HandleMessage;

    UsbDevice_ClientRegister(&charger_detect_data.task);

    MessageChargerChanged msg_changed;
    msg_changed.charger_connected = Charger_Status() != NO_POWER;
    msg_changed.vreg_en_high = 0;

    ChargerDetect_Changed(&msg_changed);

    MessageChargerDetected msg_detected;
    msg_detected.attached_status = Charger_AttachedStatus();
    msg_detected.cc_status = CC_CURRENT_DEFAULT;
    msg_detected.charger_dm_millivolts = 0;
    msg_detected.charger_dp_millivolts = 0;

    ChargerDetect_Detected(&msg_detected);
}

#endif /* INCLUDE_CHARGER_DETECT */
