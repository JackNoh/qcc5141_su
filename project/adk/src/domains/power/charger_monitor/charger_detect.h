/*!
\copyright  Copyright (c) 2020 Qualcomm Technologies International, Ltd.\n
            All Rights Reserved.\n
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\file
\brief      Header file for Charger Detection module
*/

#ifndef CHARGER_DETECT_H_
#define CHARGER_DETECT_H_

#include <message.h>

/*! Init Charger Detection */
void ChargerDetect_Init(void);

/*! Return maximum current that current charger can provide */
unsigned ChargerDetect_Current(void);

/*! Handle charger detected message */
void ChargerDetect_Detected(MessageChargerDetected *msg);
/*! Handle charger changed message */
void ChargerDetect_Changed(MessageChargerChanged *msg);

#endif /* CHARGER_DETECT_H_ */
