/*!
\copyright  Copyright (c) 2020-2021 Qualcomm Technologies International, Ltd.
            All Rights Reserved.
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\file
\brief      Implementation of common LE Audio specifc testing functions.
*/

#include <logging.h>

#include "le_advertising_manager_select_extended.h"

#include "le_audio_test.h"


#ifdef GC_SECTIONS
/* Move all functions in KEEP_PM section to ensure they are not removed during
 * garbage collection */
#pragma unitcodesection KEEP_PM
#endif


bool leAudioTest_IsExtendedAdvertisingActive(void)
{
    return LeAdvertisingManager_IsExtendedAdvertisingActive();
}

bool leAudioTest_IsAnyBroadcastSourcePaSynced(void)
{
    return FALSE;
}
