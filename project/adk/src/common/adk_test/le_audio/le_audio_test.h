/*!
\copyright  Copyright (c) 2020-2021 Qualcomm Technologies International, Ltd.\n
            All Rights Reserved.\n
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\file
\defgroup   adk_test_common le_audio
\ingroup    common
\brief      Interface for common LE Audio specifc testing functions.
*/
#ifndef LE_AUDIO_TEST_H
#define LE_AUDIO_TEST_H


/*! \brief Check if LE extended advertsing is enabled.

    Check the state of the le_advertising_manager to see if extended
    advertising is currently active.

    \return TRUE if extended advertising is active, FALSE othrwise.
*/
bool leAudioTest_IsExtendedAdvertisingActive(void);

/*! \brief Check if any LE Broadcast source is pa synced.

    \return TRUE if any source is pa synced; FALSE otherwise.
*/
bool leAudioTest_IsAnyBroadcastSourcePaSynced(void);

#endif // LE_AUDIO_TEST_H
