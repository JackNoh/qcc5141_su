/*!
\copyright  Copyright (c) 2020 Qualcomm Technologies International, Ltd.
            All Rights Reserved.
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\version    
\file       adk_test_va.h
\brief      Interface for specifc application testing functions
*/

#ifndef ADK_TEST_VA_H
#define ADK_TEST_VA_H

#include <va_audio_types.h>

void appTestVaTap(void);
void appTestVaDoubleTap(void);
void appTestVaPressAndHold(void);
void appTestVaRelease(void);
void appTestVaHeldRelease(void);
void appTestSetActiveVa2GAA(void);
void appTestSetActiveVa2AMA(void);
bool appTestStartVaCapture(va_audio_codec_t encoder, unsigned num_of_mics);
bool appTestStopVaCapture(void);
bool appTestStartVaWakeUpWordDetection(va_wuw_engine_t wuw_engine, unsigned num_of_mics, bool, va_audio_codec_t encoder);
bool appTestStopVaWakeUpWordDetection(void);
unsigned appTest_VaGetSelectedAssistant(void);
void appTest_PrintVaLocale(void);

#endif /* ADK_TEST_VA_H */

