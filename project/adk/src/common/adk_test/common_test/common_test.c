/*!
\copyright  Copyright (c) 2020 Qualcomm Technologies International, Ltd.
            All Rights Reserved.
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\file
\brief      Implementation of common testing functions.
*/

#include "common_test.h"

#include <logging.h>
#include <connection_manager.h>
#include "hfp_profile.h"
#include "hfp_profile_instance.h"
#include "hfp_profile_typedef.h"

#ifdef GC_SECTIONS
/* Move all functions in KEEP_PM section to ensure they are not removed during
 * garbage collection */
#pragma unitcodesection KEEP_PM
#endif

bool appTestIsHandsetQhsConnectedAddr(const bdaddr* handset_bd_addr)
{
    bool qhs_connected_status = FALSE;

    if (handset_bd_addr != NULL)
    {
        qhs_connected_status = ConManagerGetQhsConnectStatus(handset_bd_addr);

        DEBUG_LOG_ALWAYS("appTestIsHandsetQhsConnectedAddr addr [%04x,%02x,%06lx] qhs_connected:%d", 
                         handset_bd_addr->nap,
                         handset_bd_addr->uap,
                         handset_bd_addr->lap,
                         qhs_connected_status);
    }
    else
    {
        DEBUG_LOG_WARN("appTestIsHandsetQhsConnectedAddr BT adrress is NULL");
    }

    return qhs_connected_status;
}

bool appTestIsHandsetHfpScoActiveAddr(const bdaddr* handset_bd_addr)
{
    bool is_sco_active = FALSE;

    if (handset_bd_addr != NULL)
    {
        hfpInstanceTaskData* instance = HfpProfileInstance_GetInstanceForBdaddr(handset_bd_addr);

        is_sco_active = HfpProfile_IsScoActiveForInstance(instance);
        DEBUG_LOG_ALWAYS("appTestIsHandsetHfpScoActiveAddr addr [%04x,%02x,%06lx] is_sco_active:%d", 
                         handset_bd_addr->nap,
                         handset_bd_addr->uap,
                         handset_bd_addr->lap,
                         is_sco_active);
    }
    else
    {
        DEBUG_LOG_WARN("appTestIsHandsetHfpScoActiveAddr BT adrress is NULL");
    }

    return is_sco_active;
}
