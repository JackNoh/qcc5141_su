/* Copyright (c) 2020 Qualcomm Technologies International, Ltd. */
/*   %%version */
/****************************************************************************
FILE
    charger_comms_if.h

CONTAINS
    Definitions for the charger communication subsystem.

DESCRIPTION
    This file is seen by the stack, and VM applications, and
    contains things that are common between them.
*/

#ifndef __APP_CHARGER_COMMS_IF_H__
#define __APP_CHARGER_COMMS_IF_H__

#include "app/uart/uart_if.h"

/*! @brief Status of the previous charger comms message sent by the ChargerCommsTransmit() trap.*/
typedef enum
{
    /*! The message was sent and acknowledged by the recipient successfully.*/
    CHARGER_COMMS_MSG_SUCCESS,

    /*! Charger was removed during message transmission. */
    CHARGER_COMMS_MSG_INTERRUPTED,

    /*! The message was not acknowledged by the recipient. */
    CHARGER_COMMS_MSG_FAILED,

    /*! The message request was rejected as the transmit queue was full. */
    CHARGER_COMMS_MSG_QUEUE_FULL,

    /*! An unexpected error occurred. */
    CHARGER_COMMS_MSG_UNKNOWN_ERROR
} charger_comms_msg_status;

/*! @brief Device id for ChargerCommsUart used by the ChargerCommsUartConfigure() trap.
 *
 * The supported device ID includes case, earbud right and earbud left.
 */
typedef enum
{
    /*! case */
    CHARGER_COMMS_UART_DEVICE_ID_CASE,
     /*! earbud right */
    CHARGER_COMMS_UART_DEVICE_ID_EB_R,
     /*! earbud left */
    CHARGER_COMMS_UART_DEVICE_ID_EB_L,
     /*! unsupported device */
    CHARGER_COMMS_UART_DEVICE_ID_UNUSED
}charger_comms_uart_device_id_t;

/*! @brief Destination address for ChargerCommsUart stream header. */
typedef enum
{
    /*! Case */
    CHARGER_COMMS_UART_ADDRESS_CASE      = 0,
    /*! Right Earbud */
    CHARGER_COMMS_UART_ADDRESS_EB_R      = 1,
    /*! Left Earbud */
    CHARGER_COMMS_UART_ADDRESS_EB_L      = 2,
    /*! Broadcast (right and left earbuds) */
    CHARGER_COMMS_UART_ADDRESS_BROADCAST = 3
} charger_comms_uart_address;

/*! @brief Configuration key used by the ChargerCommsUartConfigure() trap. */
typedef enum
{
    /*! This key is used to enable/disable UART Rx.
     * 0: Disable, 1: Enable. */
    CHARGER_COMMS_UART_CFG_KEY_RX_ENABLE,
     /*! This key is used to set the device id.
      * The valid configuration value refers to charger_comms_uart_device_id_t. */
    CHARGER_COMMS_UART_CFG_KEY_DEVICE_ID,
     /*! This key is used to set baud rate of UART.
      * The configuration value refers to vm_uart_rate. */
    CHARGER_COMMS_UART_CFG_KEY_BAUD_RATE,
     /*! This key is used to set the parity bit of UART.
      * The configuration value refers to vm_uart_parity. */
    CHARGER_COMMS_UART_CFG_KEY_PARITY,
     /*! This key is used to set the stop bits of UART.
      * The configuration value refers to vm_uart_stop. */
    CHARGER_COMMS_UART_CFG_KEY_STOP_BITS,
     /*! This key is used to set the tx time out of UART. 
      * @@TODO: the details about tx timeout is not determined yet. */
    CHARGER_COMMS_UART_CFG_KEY_TIME_OUT
}charger_comms_uart_config_key_t;

#endif /* __APP_CHARGER_COMMS_IF_H__  */
