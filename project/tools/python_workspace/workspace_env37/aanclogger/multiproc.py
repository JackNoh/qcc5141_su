############################################################################
# CONFIDENTIAL
#
# Copyright (c) 2020 Qualcomm Technologies, Inc. and/or its subsidiaries.
# All rights reserved.
#
############################################################################
"""Module to provide support for running ACAT in multiprocessing mode.

This allows a stereo class to be created that can access fields from both
devices.

"""

from multiprocessing.managers import BaseManager, NamespaceProxy
import operator

class MyManager(BaseManager):
    """Custom manager to multi-process ACAT instances."""


class AANCProxy(NamespaceProxy):
    """Proxy for AANC."""
    _exposed_ = ('__getattribute__', 'read', 'do_analysis', 'refresh')

    def read(self, attr):
        """read values bypassing the need to pickle intermediate objects."""
        callmethod = object.__getattribute__(self, '_callmethod')
        return callmethod('read', (attr,))

    def do_analysis(self):
        """Run an ACAT analysis."""
        callmethod = object.__getattribute__(self, '_callmethod')
        return callmethod('do_analysis')

    def refresh(self):
        """Refresh object data."""
        callmethod = object.__getattribute__(self, '_callmethod')
        return callmethod('refresh')

    def __getattribute__(self, attr):
        if '.' in attr:
            return self.read(attr)

        return super(AANCProxy, self).__getattribute__(attr)


class AANCLauncher():
    """Launch an AANC class.

    Args:
        params (list): List of parameters to launch ACAT with.

    """
    def __init__(self, params):
        self._params = params

        # ACAT import has to be within the class to be encapsulated inside
        # the subprocess.
        import ACAT # pylint: disable=import-outside-toplevel
        ACAT.parse_args(self._params)
        self._session = ACAT.load_session()

        aanc_analysis = operator.attrgetter("p0.aanc")(self._session)
        try:
            self._operator = aanc_analysis.find_operators()[0]
        except IndexError as err:
            raise IndexError("No AANC operators found in the graph. Check that "
                             "the graph is running with an AANC operator and "
                             "that the correct download_aanc.elf file has been "
                             "passed to the logging tool.") from err

    def read(self, attr):
        """Read a value from the operator.

        Args:
            attr (str): Attribute to read.
        """
        attrs = attr.split('.')[::-1]
        handle = self._operator

        while len(attrs) > 1:
            cur_attr = attrs.pop()
            try:
                handle = handle.__getattr__(cur_attr)
            except AttributeError:
                handle = handle.__getattribute__(cur_attr)

        cur_attr = attrs.pop()
        try:
            return handle.__getattr__(cur_attr)
        except AttributeError:
            return handle.__getattribute__(cur_attr)

    def do_analysis(self):
        """Do the ACAT analysis."""
        # ACAT import has to be within the class to be encapsulated inside
        # the subprocess.
        import ACAT # pylint: disable=import-outside-toplevel
        ACAT.do_analysis(self._session)

    def refresh(self):
        """Refresh the data."""
        return self._operator.refresh()

MyManager.register('AANC', AANCLauncher, AANCProxy)


class StereoConnection(): # pylint: disable=too-few-public-methods
    """Represent a stereo connection to two AANC devices.

    Args:
        a0i (AANCProxy): Proxy to the left connection
        a1i (AANCProxy, optional): Proxy to the right connection. Defaults to
            None.
    """
    def __init__(self, a0i, a1i):
        self.a0i = a0i
        self.a1i = a1i

    def refresh(self):
        """Refresh the object data."""
        rf0 = self.a0i.refresh()
        rf1 = True
        if self.a1i is not None:
            self.a1i.refresh()

        return rf0 and rf1

class SingleConnection(): # pylint: disable=too-few-public-methods
    """Represent a single connection to an AANC device.

    The principle here is to expose the same `a0i` member as a
    `StereoConnection` to allow graphs connecting to a single device to work
    with either mode.

    Args:
        params (list): List of parameters to launch ACAT with.

    """
    def __init__(self, params):
        self.a0i = AANCLauncher(params)

    def refresh(self):
        """Refresh the object data."""
        return self.a0i.refresh()
