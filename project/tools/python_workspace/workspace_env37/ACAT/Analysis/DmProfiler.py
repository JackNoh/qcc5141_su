############################################################################
# CONFIDENTIAL
#
# Copyright (c) 2020 Qualcomm Technologies, Inc. and/or its subsidiaries.
# All rights reserved.
#
############################################################################
"""DM Profiler Analysis.

Module used for DM profiling.
"""
try:
    from future_builtins import hex  # pylint: disable=redefined-builtin
except ImportError:
    pass

from ACAT.Analysis import Analysis
from ACAT.Core import Arch
from ACAT.Core.exceptions import BundleMissingError
from ACAT.Core.CoreTypes import ChipVarHelper as ch
from ACAT.Core.exceptions import (
    OutdatedFwAnalysisError, InvalidDebuginfoEnumError, DebugInfoNoVariableError)

DM_PROFILING_PATCH_MAGIC = 0xfab01005


class DmProfiler(Analysis.Analysis):
    """
    Encapsulates an analysis for DM memory: find the owner of individual
    blocks, in heap and pools. Then for each owner report the sum of owned
    blocks, and a list of allocated blocks for (a list of) owner(s).

    Args:
        **kwargs: Arbitrary keyword arguments.
    """

    def __init__(self, **kwargs):
        # Call the base class constructor.
        Analysis.Analysis.__init__(self, **kwargs)
        
        self.heap = self.interpreter.get_analysis(
            "heapmem", self.chipdata.processor
        )
        self.pools = self.interpreter.get_analysis(
            "poolinfo", self.chipdata.processor
        )
        self._check_kymera_version()

    def run_all(self):
        """Analyse DM memory used by operator tasks.
        """
        self.analyse_dm_memory(False, False)

    def profile_dm_memory(self, every=False):
        """Analyse DM memory used by tasks and return a list
           with profile information. Each list entry is a list
           itself with N entries:

           0. Task ID,
           1. Task description,
           2. Sum size of allocations (in octets),
           3. Sum size of all heap allocations (in octets),
           4. Sum size of all pool allocations (in octets),
           5. List of the allocated blocks for this entry's task ID, each
              of which is a list by itself, e.g.:

            ::

              [[address1, length1, name1],
               [address2, length2, name2],
               ...
              ]

        Args:
            every (bool): If True, analyse only operator tasks,
                 if False, analyse all tasks.
        """
        task_generator = DMProfilerTaskIDs(self)
        t_list, tstr_list = task_generator.get_tasks(every)
        hsz_list, hblk_list, hsz_unknown = self.heap.get_dmprofiling_heaps_blocks(t_list)
        psz_list, pblk_list, psz_unknown = self.pools.get_dmprofiling_pool_blocks(t_list)

        sz_list = [
            [
                item,
                tstr_list[i],
                hsz_list[i] + psz_list[i],
                hsz_list[i],
                psz_list[i],
                hblk_list[i] + pblk_list[i]
            ]
            for i, item in enumerate(t_list)
        ]

        entry = [
            256,
            "Unknown (not in task list)",
            hsz_unknown + psz_unknown,
            hsz_unknown,
            psz_unknown
        ]
        sz_list.append(entry)

        if not self.heap.dmprofiling_enabled:
            self.formatter.output(
                "\nDM Memory Profiling appears not to be supported or "
                "enabled in the firmware.\nInformation displayed may be "
                "incorrect."
            )

        return sz_list

    def analyse_dm_memory(self, every=False, verbose=False):
        """Analyse DM memory used by tasks and display it.

        Args:
            every (bool): If True, analyse only operator tasks, if False,
                analyse all tasks.
            verbose (bool): if True, show all heap and pool blocks used by
                a task/bgint if False, don't show any heap and pool block
                information
        """
        sz_list = self.profile_dm_memory(every)

        self._display_dm_memory(sz_list, every)
        if verbose:
            self._display_verbose_dm_memory(sz_list, every)

    def _check_patch_level_dm_profiling(self):
        """Examine the release patch version number for AuraPlus.
           This tells whether DM Profiling is enabled in the patch.

        Returns:
            bool: Whether DM Profiling is enabled in the patch or not.
        """
        try:
            patch_analysis = self.interpreter.get_analysis(
                "patches",
                self.chipdata.processor
            )
            # AuraPlus 1.1: introduced in Audio Package ID 556 of 21/05/2020
            if self.chipdata.get_firmware_id() == 7120:
                if patch_analysis.get_patch_level() >= 10340:
                    return True
                else:
                    return False
            # AuraPlus 1.2: has DM Profiler support in rom
            elif self.chipdata.get_firmware_id() == 11639:
                return True
            # AuraPlus ?.?: ?
            else:
                return False
        except KeyError:
            return False
        return False

    def _check_patch_dm_profiling(self):
        """Queries the global patch variables for the presence of a patch
           global variable initialised to DM_PROFILING_PATCH_MAGIC. This
           indicates that DM Profiling is enabled in the patch.

        Returns:
            bool: Whether DM Profiling is enabled in the patch or not.
        """

        # If not Hydra or not Kalimba 4, no DM Profiling
        if (Arch.kal_arch != 4) or (Arch.chip_arch != "Hydra"):
            return False
        # Aura and earlier chips have no DM Profiling
        if Arch.chip_id <= 0x4A:
            return False
        # Mora and later chips have DM Profiling
        if Arch.chip_id > 0x4C:
            return True
        # AuraPlus 1.2: has DM Profiler support in rom
        if self.chipdata.get_firmware_id() == 11639:
            return True

        # Examine the release patch version number for AuraPlus
        if self._check_patch_level_dm_profiling():
            return True

        # Look through patch global data if dm profiling magic number is set (debug patch)
        try:
            patch_start = self.debuginfo.get_var_strict("$PATCH_RESERVED_DM_START").address
        except DebugInfoNoVariableError:
            return False
        try:
            patch_end   = self.debuginfo.get_var_strict("$PATCH_RESERVED_DM_END").address
        except DebugInfoNoVariableError:
            return False

        buffer_content = self.chipdata.get_data(
            patch_start, patch_end - patch_start
        )

        for offset, value in enumerate(buffer_content):
            if value == DM_PROFILING_PATCH_MAGIC:
                return True

        return False

    def _check_kymera_version(self):
        """Checks if the Kymera version is compatible with this analysis.

        Raises:
            OutdatedFwAnalysisError: For an outdated Kymera.
        """
        try:
            self.debuginfo.get_enum("DMPROFILING_OWNER")
        except InvalidDebuginfoEnumError:
            if not self._check_patch_dm_profiling():
                # fallback to the old implementation
                raise OutdatedFwAnalysisError()
        
    def _display_dm_memory(self, sz_list, every):
        """Display DM memory used by tasks as specified in 'sz_list'.

        Args:
            sz_list: list as produced by 'profile_dm_memory'
            every:   if True, only operator tasks in 'sz_list',
                     if False, all tasks in 'sz_list'.
        """

        if every:
            descr = "Analyse task/bgint dm memory ({0} tasks/bgints)".format(
                len(sz_list) - 1
            )

        else:
            descr = "Analyse operator dm memory ({0} operators)".format(
                len(sz_list) - 1
            )

        self.formatter.section_start(descr)
        for i in range(len(sz_list) - 1):
            self.formatter.output(
                "ID %3d size %6d (heap %6d, pools %4d) %s" % (
                    sz_list[i][0],
                    sz_list[i][2],
                    sz_list[i][3],
                    sz_list[i][4],
                    sz_list[i][1]
                )
            )

        i = len(sz_list) - 1
        self.formatter.output(
            "Other  size %6d (heap %6d, pools %4d) %s" % (
                sz_list[i][2],
                sz_list[i][3],
                sz_list[i][4],
                sz_list[i][1]
            )
        )
        self.formatter.section_end()

    def _display_heap_block(self, block_list):
        """Display the entries of a list of allocated DM memory blocks
           used by a particular task ID as specified in 'block_list'.

        Args:
            block_list (lst): List of allocated memory blocks, each
                allocated memory block is a list itself, e.g.:

                        [[address1, length1, name1],
                         [address2, length2, name2],
                         ...
                        ]
        """
        for block in block_list:
            if len(block) > 2:
                self.formatter.output(
                    "   address 0x%08x size %6d:  %s" % (
                        block[0],
                        block[1],
                        block[2]
                    )
                )

    def _display_verbose_dm_memory(self, sz_list, every):
        """Display DM memory used by tasks as specified in 'sz_list',
           and also the heap and pool blocks.

        Args:
            sz_list (list): List as produced by 'profile_dm_memory'
            every (bool): If True, only operator tasks in 'sz_list', if
                False, all tasks in 'sz_list'.
        """

        if every:
            descr = (
                "Heap/pool blocks for task/bgint dm memory "
                "(%d tasks/bgints)"
            ) % (len(sz_list) - 1)

        else:
            descr = (
                "Heap/pool blocks for operator dm memory "
                "(%d operators)"
            ) % (len(sz_list) - 1)

        self.formatter.section_start(descr)

        for i in range(len(sz_list) - 1):
            self.formatter.output(
                "ID %3d size %6d (heap %6d, pools %4d) %s" % (
                    sz_list[i][0],
                    sz_list[i][2],
                    sz_list[i][3],
                    sz_list[i][4],
                    sz_list[i][1]
                )
            )
            self._display_heap_block(sz_list[i][5])

        i = len(sz_list) - 1
        self.formatter.output(
            "Other  size %6d (heap %6d, pools %4d) %s" % (
                sz_list[i][2],
                sz_list[i][3],
                sz_list[i][4],
                sz_list[i][1]
            )
        )

        self.formatter.section_end()


class DMProfilerTaskIDs:
    """An object to generate tasks and their info.

    Args:
        dmprofiler_analysis: An instance of DmProfiler.
    """
    def __init__(self, dmprofiler_analysis):
        self._analysis = dmprofiler_analysis

        self._tasks_list = []
        self._tasks_info = []

        self._all_tasks_list = []
        self._all_tasks_info = []

        self._populate_task_qeues()

        self._allbgints_list = []
        self._allbgints_info = []

        self._populate_bigints_in_priority()

    def _populate_task_qeues(self):
        # Get the list of task queues.
        task_queues = self._analysis.chipdata.get_var_strict(
            '$_tasks_in_priority'
        )

        for queue in task_queues:
            if queue['first'].value == 0:
                continue
            first_task = queue['first'].deref
            all_tasks_in_q = [
                t for t in ch.parse_linked_list(first_task, 'next')
            ]

            for task in all_tasks_in_q:
                self._all_tasks_list.append(task['id'].value & 0xFF)
                handler = task['handler'].value
                if handler != 0:
                    try:
                        module_name = self._analysis.debuginfo.get_source_info(
                            handler
                        ).module_name
                    except BundleMissingError:
                        module_name = (
                            "No source information." + "Bundle is missing."
                        )
                    self._all_tasks_info.append(module_name)
                else:
                    self._all_tasks_info.append("")

    def _populate_bigints_in_priority(self):
        bg_ints_in_priority = self._analysis.chipdata.get_var_strict(
            '$_bg_ints_in_priority'
        )
        for bg_int_g in bg_ints_in_priority:
            if bg_int_g['first'].value == 0:
                continue

            first_bg_int = self._analysis.chipdata.cast(
                bg_int_g['first'].value, 'BGINT'
            )

            bg_int_queue = ch.parse_linked_list(
                first_bg_int, 'next'
            )
            for bg_int in bg_int_queue:
                self._allbgints_list.append(
                    bg_int['id'].value & 0xFF
                )
                handler = bg_int['handler'].value
                if handler != 0:
                    try:
                        module_name = self._analysis.debuginfo.get_source_info(
                            handler
                        ).module_name

                    except BundleMissingError:
                        module_name = (
                            "No source information." + "Bundle is missing."
                        )

                    self._allbgints_info.append(module_name)

                    if module_name == "opmgr_operator_bgint_handler":
                        try:
                            # Get the operator id
                            opmgr_analysis = self._analysis.interpreter.get_analysis(
                                "opmgr", self._analysis.chipdata.processor
                            )
                            oppointer = self._analysis.chipdata.get_data(
                                bg_int['ppriv'].value
                            )
                            opdata = self._analysis.chipdata.cast(
                                oppointer, "OPERATOR_DATA"
                            )
                            opid = opdata['id'].value
                            operator = opmgr_analysis.get_operator(opid)
                            desc_str = (
                                ' Operator {} {}'.format(
                                    hex(operator.op_ep_id),
                                    operator.cap_data.name
                                )
                            )
                        except BaseException as error:
                            desc_str = 'Operator not found %s' % str(error)

                        self._tasks_list.append(
                            bg_int['id'].value & 0xFF
                        )
                        self._tasks_info.append(desc_str)

                        self._allbgints_info.pop()
                        self._allbgints_info.append(desc_str)
                else:
                    self._allbgints_info.append("")

    def _list_all(self):
        # If we have a full list, e.g. both tasks and bgints, we need to
        # process the list to throw out duplicates.  Description strings
        # for the same owner id are concatenated.  The actual task ids or
        # bgint ids are larger than 1 octet, as used in the pool and heap
        # block descriptors, so there are going to be multiple occurances
        # of owner ids.  When only looking for an operator task id list,
        # this is not necessary, as every operator id is unique and the
        # task id and bgint id are the same (for the ls octet).
        self._tasks_list = []
        self._tasks_info = []

        # Read the alltasks list and add those entries that
        # are not already present in it to tasks list
        for index, task in enumerate(self._all_tasks_list):
            tid = task & 0xFF
            descr = self._all_tasks_info[index]
            try:
                idx = self._tasks_list.index(tid)
                self._tasks_info[idx] = ', '.join(
                    (self._tasks_info[idx], descr)
                )
            except ValueError:
                self._tasks_list.append(tid)
                self._tasks_info.append(descr)

        # Read the allbgints list and add those entries that
        # are not already present in it to tasks list
        for index, bigint in enumerate(self._allbgints_list):
            tid = bigint & 0xFF
            descr = self._allbgints_info[index]
            try:
                idx = self._tasks_list.index(tid)
                self._tasks_info[idx] = ', '.join(
                    (self._tasks_info[idx], descr)
                )
            except ValueError:
                self._tasks_list.append(tid)
                self._tasks_info.append(descr)

        # Finally, add the 'no task' (0xFF). Heap/pool blocks with
        # such a tags are typically allocated before 'sched()' init.
        tid = 0xFF
        descr = "No task"
        try:
            idx = self._tasks_list.index(tid)
            self._tasks_info[idx] = self._tasks_info[idx] + ", " + descr
        except ValueError:
            self._tasks_list.append(tid)
            self._tasks_info.append(descr)

    def get_tasks(self, every=False):
        """Returns a tuple, list of tasks and their info.

        Args:
            every (bool): If `True` it lists all tasks and big-ints, it
                lists operators otherwise.

        Returns:
            tuple: (tasks list, tasks info)
        """
        self._tasks_list = []
        self._tasks_info = []

        self._all_tasks_list = []
        self._all_tasks_info = []
        self._populate_task_qeues()

        self._allbgints_list = []
        self._allbgints_info = []
        self._populate_bigints_in_priority()

        if every:
            self._list_all()

        return self._tasks_list, self._tasks_info
