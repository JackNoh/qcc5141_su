#! /usr/bin/env python
# %%fullcopyright
# %%version
# Utils for XUV/ELF related low-level manipulations

from csr.wheels.global_streams import iprint
def loadable_to_bytes(loadable, verbose=False):
    '''
    Read loadable sections (e.g. from a filesystem or an elf) into a 
    bytearray padding as necessary.
    Note that this needs the sections to be in address order otherwise
    the padding will go very wrong.
    '''
    last_paddr = 0
    output_bytes = bytearray()
    for section in loadable:
        addr = section.paddr
        data = section.data
        if addr != last_paddr:
            pad_len = addr - last_paddr
            if verbose:
                iprint("  Pad %d bytes" % pad_len) 
            output_bytes += bytearray([0xff] * pad_len)
        if verbose:
            iprint("  %s block starting at 0x%08x, %d bytes" %
                                        (section.name, addr, len(data)))
        output_bytes += bytearray(data)
        last_paddr = addr + len(data)
    return output_bytes
