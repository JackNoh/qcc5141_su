############################################################################
# CONFIDENTIAL
#
# Copyright (c) 2015 - 2016 Qualcomm Technologies International, Ltd.
#   %%version
#
############################################################################

from csr.dev.model.interface import Group, Table, Code
from csr.dev.fw.firmware_component import FirmwareComponent
from csr.dev.env.env_helpers import _Variable
from csr.dwarf.read_dwarf import DwarfNoSymbol
from csr.wheels.bitsandbobs import bytes_to_dwords
from .structs import IAppsStructHandler

class IPC(FirmwareComponent):
    """
    Provide debugging access to the IPC mechanism
    """

    @staticmethod
    def create(fw_env, core):
        """
        Create an IPC object if possible.  If we're running against old firmware
        that doesn't support it, then it won't be.  We detect this by the
        failure to find key symbols in the firmware.
        """
        try:
            return IPC(fw_env, core)
        except DwarfNoSymbol:
            return None

    def buf_msg_decoder(self, msg_buf, ring_entry, start_index, msg):
        """
        Decoder to register for IPC's BUFFER_MSGs with the BufferMsg struct
        handler class
        """
        return self._decode(msg, join=False)
    
    def __init__(self, fw_env, core):
        
        FirmwareComponent.__init__(self, fw_env, core)

        self._ipc_msg_names_to_ids = fw_env.enums["IPC_SIGNAL_ID"]
        self._ipc_msg_ids_to_names = dict((id, name) for (name, id) in 
                                          self._ipc_msg_names_to_ids.items())
        self._ipc_struct = self.env.gbl.ipc_data
        self._send_buf = IAppsStructHandler.handler_factory("BUFFER_MSG")(
                                                  core, 
                                                  self._ipc_struct.send.deref)
        self._recv_buf = IAppsStructHandler.handler_factory("BUFFER_MSG")(
                                                  core,
                                                  self._ipc_struct.recv.deref)
        self._send_buf.set_msg_decoder(self.buf_msg_decoder)
        self._recv_buf.set_msg_decoder(self.buf_msg_decoder)
        
        # We need to look up trap definitions info in P1's DWARF
        try:
            self._p1_dwarf = self._core.subsystem.p1.fw.env.dwarf
        except AttributeError:
            self._p1_dwarf = None
            
        
    @property
    def title(self):
        return "IPC"
        
    def show_recv(self):
        """
        Decode recent entries in the IPC receive buffer
        """
        self._recv_buf.display()

    def show_send(self):
        """
        Decode recent entries in the IPC send buffer
        """
        self._send_buf.display()
        
    def _generate_report_body_elements(self):
        """
        Decode all the recent IPC messages that are still visible
        """
        reports = []
        # 1. Grab messages
        for dir in ("send", "recv"):
            ipc_dir = Group(dir)
            
            grp_cleared = Group("cleared")
            ipc_dir.append(grp_cleared)
            buf = getattr(self, "_%s_buf" % dir)
            for msg in buf.still_mapped_msgs[2]:
                grp_cleared.append(Code(self._decode(msg, join=True)))

            grp_read = Group("read")
            ipc_dir.append(grp_read)
            for msg in buf.read_msgs[2]:
                grp_read.append(Code(self._decode(msg, join=True)))

            grp_unread = Group("unread")
            ipc_dir.append(grp_unread)
            for msg in buf.unread_msgs[2]:
                grp_unread.append(Code(self._decode(msg, join=True)))
                
            reports.append(ipc_dir)
        return reports

    def _decode(self, msg, join=False):
        """
        Interpret the supplied list of bytes as an IPC primitive
        """
        msg_id = self._core.info.layout_info.deserialise(msg[0:4])
        try:
            message_name = self._ipc_msg_ids_to_names[msg_id]
            struct_name = message_name.replace("_SIGNAL_ID", "")
            if struct_name in ("IPC_TEST_TUNNEL_PRIM",):
                struct_name = "IPC_TUNNELLED_PRIM_OUTBAND"
            elif struct_name in ("IPC_PFREE", "IPC_SFREE"):
                struct_name = "IPC_FREE"
            elif struct_name in ("IPC_BLUESTACK_PRIM_RECEIVED",):
                struct_name = "IPC_BLUESTACK_PRIM"
            elif struct_name in ("IPC_APP_SINK_MSG", 
                                 "IPC_APP_SINK_SOURCE_MSG",
                                 "IPC_APP_MSG"):
                struct_name += "_PRIM"
            elif struct_name in ("IPC_XIO_RSP"):
                struct_name = "IPC_BOOL_RSP"
                
            try:
                type = self.env.types[struct_name]
            except DwarfNoSymbol:
                # Probably indicates a response type that is derived from the
                # trap's return value rather than its name
                
                if self._p1_dwarf is None:
                    # We *could* look up the info via the XML but if p1's firmware
                    # isn't available we're probably not bothered about IPC so
                    # there seems little point
                    raise
                
                # Construct the trap function's name
                func_name_cmpts = (struct_name.replace("IPC_","")
                                              .replace("_RSP","")
                                              .split("_"))
                name = ""
                for cmpt in func_name_cmpts:
                    name += (cmpt[0] + cmpt[1:].lower())
                try:
                    func = self._p1_dwarf.get_function(name)
                    if func.return_type is not None:
                        struct_name = "IPC_%s_RSP" % (func.return_type.struct_dict[
                                                         "type_name"].upper())
                    else:
                        struct_name = "IPC_VOID_RSP"
                    type = self.env.types[struct_name]
                except DwarfNoSymbol:
                    type = None

            if type:            
                struct = _Variable.create_from_type(
                            type,
                            0, msg, self._core.info.layout_info,
                            ptd_to_space=False)
                display_lines = struct.display("IPC msg", "", [], [])
            else:
                display_lines = ["%s %s" % (message_name, msg[4:])]
        except (KeyError, DwarfNoSymbol):
            # The very first two messages in the receive buffer on P1 aren't IPC
            # primitives - they're raw pointers to the send and recv buffers
            display_lines = ["%s <Not decoded>" % msg] 

        if join:
            return "\n".join(display_lines)
        return display_lines
    
    def mmu_handles(self):
        """
        Returns the MMU handles owned by this module
        """
        results = []
        handles = [self.env.gv["ipc_data"]["send"].deref["buf"]["handle"].value & 0xff]
        text = "ipc_data (send)"
        ss_handles = [text, handles]
        results.append(ss_handles)
        
        handles = [self.env.gv["ipc_data"]["recv"].deref["buf"]["handle"].value & 0xff]
        text = "ipc_data (recv)"
        ss_handles = [text, handles]
        results.append(ss_handles)
        
        return results
        
        
