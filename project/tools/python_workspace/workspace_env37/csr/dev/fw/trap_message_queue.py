############################################################################
# CONFIDENTIAL
#
# Copyright (c) 2015 - 2016 Qualcomm Technologies International, Ltd.
#   %%version
#
############################################################################
"""
@file
Trap Message Queue Firmware Component file.

@section Description
Implements decoding of the vm message queue entries

@section Usage
Use apps1.fw.trap_message_queue.report()
"""
from csr.dev.model.base_component import BaseComponent
from csr.dev.model import interface
from csr.dev.fw.debug_log import TrapLogDecoder

class TrapMessageQueue(BaseComponent):
    def __init__(self, fw):
        self._fw = fw
        
    def _generate_report_body_elements(self):
        output = interface.Group("message queue")
        ptr = self._fw.gbl.vm_message_queue
        decoder = TrapLogDecoder(self._fw.env, self._fw._core)
        queue_desc = []
        prev_due = None
        while ptr.value:
            entry = ptr.deref
            entry_desc = []
            try:
                element_task = entry.t.task
            except AttributeError:
                element_task = entry.task
            element_str = "task %s" % element_task
            try:
                element_str += " (%s)" % decoder.get_task_name(element_task.value)
            except TypeError:
                pass
            entry_desc.append(element_str)
            entry_desc.append("id: %s (%s)" % (entry.id, 
                       decoder.get_id_name(element_task.value, entry.id.value)))
            if entry.message.value:
                element_str = "message: %s" % entry.message
                msg_mem = self._fw.pmalloc.memory_block_mem(entry.message.value)
                if msg_mem:
                    element_str += " = " + " ".join(["%02x" % a for a in msg_mem])
                entry_desc.append(element_str)
            if entry.condition_addr.value:
                entry_desc.append("condition_addr: %s" % entry.condition_addr)
                entry_desc.append("c_width: %s" % entry.c_width)
            element_str = "due: %s" % entry.due
            if prev_due: 
                element_str += " (head + %g ms)" % (
                                            (entry.due.value -prev_due)/1000.0) 
            entry_desc.append(element_str)
            queue_desc.append("\n".join(entry_desc))
            if not prev_due:
                prev_due = entry.due.value
            ptr = entry.next
        output.append(interface.Code("\n\n".join(queue_desc)))
        return [output]

