############################################################################
# CONFIDENTIAL
#
# Copyright (c) 2015 - 2016 Qualcomm Technologies International, Ltd.
#   %%version
#
############################################################################
""" This module provides API for Oxygen Scheduler
"""
# pylint: disable=no-self-use, too-many-branches

from csr.wheels import gstrm
from csr.wheels.global_streams import iprint
from csr.dev.fw.firmware_component import FirmwareComponent
import sys
import time
from csr.wheels.bitsandbobs import timeout_clock
from csr.dev.model import interface
from csr.dev.adaptor.text_adaptor import TextAdaptor
from csr.dev.hw.address_space import AddressSpace

class SchedOxygen(FirmwareComponent):
    """ Provide access to Oxygen Scheduler
    """

    class SchedTimeout(RuntimeError):
        pass

    def __init__(self, fw_env, core):
        FirmwareComponent.__init__(self, fw_env, core)
        self.bg = SchedOxygen.SchedBG(self.env, self._core)
        self.tasks = SchedOxygen.SchedTasks(self.env, self._core)

    def _generate_report_body_elements(self):
        report = interface.Group("Sched")
        report.append(self.bg.info(report=True))
        report.append(self.tasks.info(report=True))
        report.append(self.info(report=True))
        return [report]
    
    def _on_reset(self):
        pass

    def wait_for_runlevel(self, level, timeout=2):
        """ Wait for the given runlevel of the kalimba scheduler. 
        """
        run_lvl = 0 # scheduler run level RUNLEVEL_BOOT (0)
        ts = 0.01
        tc = 0
        started_at =  timeout_clock()
        while (run_lvl != level) and (tc < timeout) :
            sched_oxygen = self.env.cus["sched_oxygen.c"]
            try:
                run_lvl = sched_oxygen.localvars["sched_flags"]["current_runlevel"].value
            except AddressSpace.ReadFailure:
                pass
            if run_lvl != level:
                time.sleep(ts)
                tc = timeout_clock() - started_at
        ended_at = timeout_clock()
        diff = ended_at - started_at
        if (run_lvl != level):
            iprint("Kalimba scheduler run level %d" % run_lvl)
            raise self.SchedTimeout("Kalimba Scheduler timed out after %1.2fs" % diff)
        else:
            iprint("Kalimba scheduler run level %d after %1.2fs" % (run_lvl, diff))

    def info(self, report=False):
        """
        Print or return a report of the general scheduler state
        """
        output = interface.Group("sched.general")
        output_table = interface.Table()
        output_table.add_row(["name", "value"])
        output_table.add_row(["background_work_pending",
                              self.env.gbl.background_work_pending.value])
        output.append(output_table)
        if report is True:
            return output
        TextAdaptor(output, gstrm.iout)

    class SchedBG(object):
        """
        Provide access to background interrupts.
        """

        def __init__(self, fw_env, core):
            """
            Saves a reference to the core object.
            """
            self.core = core
            self._fw_env = fw_env

        def state(self, pause_cpu=True):
            """
            Returns a dictionary containing information about the background
            interrupts.
            """
            ret = dict()
            core = self.core
            bg_int_ids = self._fw_env.enums["bg_int_ids"]
            bg_ints_in_priority = self._fw_env.globalvars["bg_ints_in_priority"]
            fns = self._fw_env.functions
            
            if pause_cpu and core.is_running:
                core.pause()
                unpause = True
            else:
                unpause = False
            
            for i in range(bg_ints_in_priority.num_elements):
                _bg_int = bg_ints_in_priority[i]["first"]
                while _bg_int.value:
                    bg_int = _bg_int.deref
                    id = bg_int["id"].value
                    ret[id] = dict()
                    
                    ret[id]["priority"] = i
                    ret[id]["raised"] = bg_int["raised"].value
                    try:
                        ret[id]["name"] = bg_int_ids[id]
                    except KeyError:
                        ret[id]["name"] = None
                    if bg_int["prunable"].value:
                        ret[id]["prunable"] = True
                    else:
                        ret[id]["prunable"] = False
                    ret[id]["handler"] = fns[bg_int["handler"].value]
                    
                    _bg_int = bg_int["next"]
            
            if unpause:
                core.run()
            return ret
    
        def info(self, report=False):
            """
            Print or return a report of the background interrupts state. This
            is just a text formatting wrapper around the data from 
            self.state().
            """
            state = self.state()
            output = interface.Group("sched.bg")
            output_table = interface.Table()
            output_table.add_row(["name", "id", "handler", "raised", 
                                  "priority", "prunable"])
            for id, bg_int in state.items():
                output_table.add_row([bg_int["name"], "0x%08x" % id, 
                                      bg_int["handler"], bg_int["raised"],
                                      bg_int["priority"], bg_int["prunable"]])
            output.append(output_table)
            if report is True:
                return output
            TextAdaptor(output, gstrm.iout)

    class SchedTasks(object):
        """
        Provide access to tasks and queues.
        """

        def __init__(self, fw_env, core):
            """
            Saves a reference to the core object.
            """
            self.core = core
            self._fw_env = fw_env
            self.priority_str = dict([(v,k.split("_")[0]) for k,v in 
                                  self._fw_env.enums["PRIORITY"].items()])

        def state(self, pause_cpu=True):
            """
            Returns a dictionary containing information about the tasks.
            """
            ret = dict()
            core = self.core
            tasks_in_priority = self._fw_env.globalvars["tasks_in_priority"]
            fns = self._fw_env.functions

            if pause_cpu and core.is_running:
                core.pause()
                unpause = True
            else:
                unpause = False

            for i in range(tasks_in_priority.num_elements):
                _task = tasks_in_priority[i]["first"]
                while _task.value:
                    task = _task.deref
                    id = task["id"].value
                    ret[id] = dict()
                    ret[id]["run_level"] = task.runlevel.value
                    ret[id]["priority"] = i
                    ret[id]["nqueues"] = task.nqueues
                    ret[id]["mqueues_pending"] = task.mqueues.deref.first.value
                    if task["prunable"].value:
                        ret[id]["prunable"] = True
                    else:
                        ret[id]["prunable"] = False
                    ret[id]["handler"] = fns[task["handler"].value]

                    _task = task["next"]

            if unpause:
                core.run()
            return ret

        def info(self, report=False):
            """
            Print or return a report of the sched tasks state. This
            is just a text formatting wrapper around the data from
            self.state().
            """
            state = self.state()
            output = interface.Group("sched.tasks")
            output_table = interface.Table()
            output_table.add_row(["id", "handler", "mqueues_pending",
                                  "run_level", "priority", "prunable"])
            for id, task in state.items():
                if task["mqueues_pending"]:
                    pending_flag = True
                else:
                    pending_flag = False
                output_table.add_row(["0x%08x" % id,
                                      task["handler"], pending_flag,
                                      task["run_level"],
                                      self.priority_str.get(task["priority"],
                                                            task["priority"]),
                                      task["prunable"]])
            output.append(output_table)
            if report is True:
                return output
            TextAdaptor(output, gstrm.iout)

