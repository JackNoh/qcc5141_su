############################################################################
# CONFIDENTIAL
#
# Copyright (c) 2020 Qualcomm Technologies International, Ltd.
#   %%version
#
############################################################################

from csr.dev.fw.firmware_component import FirmwareComponent

from .led import LEDManager

class CAAApp(FirmwareComponent):
    """
    Container for analysis classes representing generic parts of CAA applications
    """
    def __init__(self, env, core, parent=None):
        FirmwareComponent.__init__(self, env, core, parent=parent)
    
        # Check if we're looking at a CAA app at all.
        try:
            # Do we have variables called device_list and profile_manager present
            env.vars["device_list"]
            env.vars["profile_manager"]
        except KeyError:
            raise self.NotDetected
            
    
    @property
    def subcomponents(self):
        return {"led" : "_led"}
    
    
    @property
    def led(self):
        try:
            self._led
        except AttributeError:
            self._led = LEDManager(self.env, self._core, self)
        return self._led
