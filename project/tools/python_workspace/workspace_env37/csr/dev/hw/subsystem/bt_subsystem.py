############################################################################
# CONFIDENTIAL
#
# Copyright (c) 2012 - 2019 Qualcomm Technologies International, Ltd.
#   %%version
#
############################################################################
"""
Provides class BTSubsystem to represent the BT subsystem hardware.
"""
from .hydra_subsystem import SimpleHydraSubsystem
from csr.dev.hw.core.base_core import SimpleHydraCore
from csr.dev.fw.bt_firmware import BTDefaultZeagleFirmware
from csr.dev.fw.meta.i_firmware_build_info import IFirmwareBuildInfo
from csr.dev.hw.core.meta.i_layout_info import ArmCortexMDataInfo

class SimpleBTSubsystem(SimpleHydraSubsystem):
    
    @property
    def name(self):
        return "Bluetooth"
    
    @property
    def cores(self):
        try:
            self._cores
        except AttributeError:
            self._cores = [SimpleHydraCore("bt",
                                 data_space_size=1<<32,
                                 layout_info=ArmCortexMDataInfo(),
                                 subsystem=self)]
        return self._cores
    
    @property
    def core(self):
        return self.cores[0]
    
    default_firmware_type = BTDefaultZeagleFirmware
    firmware_type = BTDefaultZeagleFirmware
    
    def firmware_build_info_type(self, *args, **kwargs):
        raise IFirmwareBuildInfo.FirmwareSetupException
