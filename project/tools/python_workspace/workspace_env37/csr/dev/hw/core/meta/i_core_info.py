############################################################################
# CONFIDENTIAL
#
# Copyright (c) 2012 - 2016 Qualcomm Technologies International, Ltd.
#   %%version
#
############################################################################
from csr.wheels import PureVirtualError
from .i_layout_info import XapDataInfo, Kalimba24DataInfo, Kalimba32DataInfo, \
ArmCortexMDataInfo, ChipmateDataInfo, X86DataInfo, X86_64DataInfo

class ICoreInfo (object):
    """\
    Core-centric meta-data interface.
    
    Represent hardware facts, such as register map, common to all Core
    instances of a specific type.
    """

    @property
    def io_map_info(self):
        """\
        io map interface (IIOMapInfo)
        """
        raise PureVirtualError()
        
    @property
    def layout_info(self):
        """
        Info about endianness, word width, etc
        """
        raise PureVirtualError

    @property
    def custom_digits_path(self):
        
        raise PureVirtualError

class XapCoreInfo(ICoreInfo):
    
    @property
    def layout_info(self):
        try:
            self._layout_info
        except AttributeError:
            self._layout_info = XapDataInfo()
        return self._layout_info

class Kalimba24CoreInfo(ICoreInfo):
    
    @property
    def layout_info(self):
        try:
            self._layout_info
        except AttributeError:
            self._layout_info = Kalimba24DataInfo()
        return self._layout_info


class Kalimba32CoreInfo(ICoreInfo):
    
    @property
    def layout_info(self):
        try:
            self._layout_info
        except AttributeError:
            self._layout_info = Kalimba32DataInfo()
        return self._layout_info
        
class ArmCortexMCoreInfo(ICoreInfo):
    
    @property
    def layout_info(self):
        try:
            self._layout_info
        except AttributeError:
            self._layout_info = ArmCortexMDataInfo()
        return self._layout_info
    
class X86CoreInfo(ICoreInfo):

    @property
    def layout_info(self):
        try:
            self._layout_info
        except AttributeError:
            self._layout_info = X86DataInfo()
        return self._layout_info

class X86_64CoreInfo(ICoreInfo):

    @property
    def layout_info(self):
        try:
            self._layout_info
        except AttributeError:
            self._layout_info = X86DataInfo()
        return self._layout_info

class ChipmateCoreInfo(ICoreInfo):

    @property
    def layout_info(self):
        try:
            self._layout_info
        except AttributeError:
            self._layout_info = ChipmateDataInfo()
        return self._layout_info
