############################################################################
# CONFIDENTIAL
#
# Copyright (c) 2017 Qualcomm Technologies International, Ltd.
#   %%version
#
############################################################################
from ...debug_bus_mux import ARMDAP, GdbserverMux

class HasJtagSwdDebugTrans(object):
    """
    Mixin for Chip classes that support JTAG/SWD debugging, via either core-by-core
    gdbserver connections or via whole-chip DP/AP-based access.  
    To support the (possible) auto-instantiation of gdbserver instances for
    different cores, classes inheriting
    this mixin should have a class attribute GDBSERVER_PARAMS which looks like
    
    {<jtag_dongle1_name> : 
          { "core1_name" : <dict of args for starting dongle gdbserver for core1>,
            "core2_name" : <dict of args for starting dongle gdbserver for core2>},
     <jtag_dongle2_name> :...
    }            
    
    E.g. see QuartzChip.GDBSERVER_PARAMS.  Currently only JLinkGdbserver is supported.
    """

    @property
    def dap(self):
        try:
            self._dap
        except AttributeError:
            properties = getattr(self, "DAP_PROPERTIES", {})
            self._dap = ARMDAP(self.data_space_owners,
                               self.AP_MAPPING, properties=properties)
        return self._dap.port

    @property
    def gdbserver_mux(self):
        try:
            self._gdbserver_mux
        except AttributeError:
            self._gdbserver_mux = GdbserverMux(self.cores)
        return self._gdbserver_mux.port
    
    @classmethod
    def get_gdbserver_params(cls, dongle_type="jlink", core_name=False):
        """
        Get details of how a particular core on the chip appears over different 
        debug dongle types (currently just the jlink is supported
        """
        try:
            gdbserver_params = cls.GDBSERVER_PARAMS[dongle_type]
        except KeyError:
            raise ValueError("dongle type '%s' not recognised.  Available "
                             "types are '%s'" % (dongle_type, 
                                                 ", ".join(d for d in cls.GDBSERVER_PARAMS)))
        if core_name is False:
            return gdbserver_params
        else:
            try:
                return gdbserver_params[core_name]
            except KeyError:
                raise ValueError("Core '%s' not recognised. Available cores are "
                                 "'%s'" %  ", ".join(c for c in gdbserver_params["cores"]))
