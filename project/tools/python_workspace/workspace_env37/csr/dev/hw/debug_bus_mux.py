############################################################################
# CONFIDENTIAL
#
# Copyright (c) 2017 Qualcomm Technologies International, Ltd.
#   %%version
#
############################################################################

from collections import namedtuple

from .port_connection import SlavePort, MasterPort, PortConnection
from .address_space import AccessPath as AddressAccessPath, AddressRange

class MuxedAccessRequest(object):
    """
    Generic access request wrapper that includes muxing information of some sort
    of other.  Typically used via a subclass that returns self.mux_select under
    a context-relevant name.
    """
    def __init__(self, basic_request, mux_select):
        
        self.basic_request = basic_request
        self.mux_select = mux_select
        
    def __getattr__(self, attr):
        return getattr(self.basic_request, attr)
        

class DebugBusMux(object):
    """
    Generic logic for routing access requests through a generic debug bus mux,
    for example a Hydra chip's TRB interface, which has a separate debug bus
    for each subsystem.  This base class is parametrised with three quantities:
     DEBUG_MUX_ID_NAME - name of the muxed component attribute that contains 
                         the component's mux ID (e.g. for a HydraSubsystem, 
                         this is "id")
     DEBUG_PORT_NAME - name of the muxed component attribute that contains the
                       component's debug slave port (e.g. for a HydraSubsystem,
                       this is "trb_in")
     ALT_DEBUG_PORT_NAME - alternative name of the muxed component attribute that
                           contains the component's debug slave port, for cases
                           where there are two different kinds of component
                           to be muxed together 
     MUXED_ACCESS_REQUEST - subclass of MuxedAccessRequest to instantiate when
                            pass access requests outward through the mux.
                            
    """
    
    class ChipDebugPort(SlavePort):
        """
        The multiplexed debug bus slave: processes multiplexed access requests
        which combine a normal bus-orientated access request (address range[, data])
        with a mux selection (e.g. subsystem bus ID)
        """
        def __init__(self, mux):
            SlavePort.__init__(self)
            self.mux = mux
    
        def _extend_access_path(self, access_path):
            for m in self.mux.masters.values():
                m.extend_access_path(access_path)
                
        def resolve_access_request(self, muxed_request):
            basic_request = muxed_request.basic_request
            master = self.mux.masters[muxed_request.subsys]
            master.resolve_access_request(basic_request)

    class DebugBusMaster(MasterPort):
        """
        Bus master for a specific mux component's bus: processes de-/pre-multiplexed
        access request destined for/originating from this component's debug bus
        """
        def __init__(self, mux, component):
            
            MasterPort.__init__(self)
            self.mux = mux
            self._id = getattr(component, mux.DEBUG_MUX_ID_NAME)
            if mux.DEBUG_PORT_NAME is not None:
                try:
                    port = getattr(component, mux.DEBUG_PORT_NAME)
                except AttributeError:
                    # try the alternative port name
                    port = getattr(component, mux.ALT_DEBUG_PORT_NAME)
            else:
                port = component
            self._auto_connection = PortConnection(self, port)
            self._cmpt = component
        
        def execute_outwards(self, access_request):
            """
            Replace the access request with a wrapper that indicates the
            subsystem ID
            """
            rq = self.mux.MUXED_ACCESS_REQUEST(access_request, self._id)
            self.mux.port.execute_outwards(rq)

        def extend_access_path(self, access_path):
            access_path.create_simple_fork(self)
            self._cmpt.has_data_source = True

        
    @property
    def port(self):
        return self._port

    @property 
    def masters(self):
        return self._cmpt_masters

class SimpleDebugBusMux(DebugBusMux):

    def __init__(self, components):
        
        self._port = self.ChipDebugPort(self)
        
        self._cmpt_masters = {getattr(cmpt,self.DEBUG_MUX_ID_NAME):
                                          self.DebugBusMaster(self, cmpt) 
                                      for cmpt in components}
        

    
class TrbAccessRequest(MuxedAccessRequest):
    """
    Tweaks the interface of a generic MuxedAccessRequest to use the language of
    TRB
    """
    @property
    def subsys(self):
        return self.mux_select

class TrbSubsystemMux(SimpleDebugBusMux):
    """
    Concrete mux for per-subsystem transaction bridge access
    """
    DEBUG_MUX_ID_NAME = "id"
    DEBUG_PORT_NAME = "trb_in"
    MUXED_ACCESS_REQUEST = TrbAccessRequest
    
    class DebugBusMaster(DebugBusMux.DebugBusMaster):
        def extend_access_path(self, access_path):
            fork = AddressAccessPath(self._id,
                                     access_path.rank+1, self,
                                     AddressRange(0,1<<32))
            access_path.add_fork(fork)
            self._cmpt.has_data_source = True

    
class GdbserverAccessRequest(MuxedAccessRequest):
    """
    """
    
class GdbserverMux(SimpleDebugBusMux):
    """
    Passes high-level gdbserver-orientated requests on to the transport with
    an attached ID
    """
    DEBUG_MUX_ID_NAME = "name"
    DEBUG_PORT_NAME = "debug_controller"
    MUXED_ACCESS_REQUEST = GdbserverAccessRequest
    
    
class ARMDAPAccessRequest(MuxedAccessRequest):
    """
    Tweaks the interface of a generic MuxedAccessRequest to use the language of
    Jtag
    """    
    @property
    def ap(self):
        return self.mux_select

class ARMDAP(DebugBusMux):
    """
    Concrete mux for chips accessed via an ARM-style Debug Access Port
    """
    DEBUG_MUX_ID_NAME = "name"
    DEBUG_PORT_NAME = "mem_ap"
    MUXED_ACCESS_REQUEST = ARMDAPAccessRequest
    
    APIndex = namedtuple("APIndex", "ap_number ap_type")

    def __init__(self, components, ap_mapping, properties=None):
        """
        :param components: Iterable of objects that this mux muxes together
         (e.g. subsystems/cores)
        :param ap_mapping: Mapping from the name of a component to the details of 
         its associated Access Port, in the form of an ARMDAP.ApIndex
        :param properties: Optional dictionary of properties of the DAP that
         might be useful for the hardware connecting to it.  Supported properties
         are "speed_khz" and "interface", the latter being either "jtag" or "swd".
        """
        
        self._port = self.ChipDebugPort(self, properties)
        
        self._cmpt_masters = {getattr(cmpt,self.DEBUG_MUX_ID_NAME):
                                          self.DebugBusMaster(self, cmpt,
                                                              ap_mapping[getattr(cmpt, self.DEBUG_MUX_ID_NAME)]) 
                                      for cmpt in components}
        
    class DebugBusMaster(DebugBusMux.DebugBusMaster):

        def __init__(self, mux, component, target_ap):
            DebugBusMux.DebugBusMaster.__init__(self, mux, component)
            self._target_ap = target_ap

        def extend_access_path(self, access_path):
            fork = AddressAccessPath(self._id,
                                     access_path.rank+1, self,
                                     AddressRange(0,1<<32))
            access_path.add_fork(fork)
            self._cmpt.has_data_source = True

        def execute_outwards(self, access_request):
            """
            Multiplex the given access request into the DAP by adding a wrapper
            specifying the AP index that it should be delivered to, plus the
            type of AP that it is.
            """
            rq = self.mux.MUXED_ACCESS_REQUEST(access_request, self._target_ap)
            self.mux.port.execute_outwards(rq)

    class ChipDebugPort(DebugBusMux.ChipDebugPort):
        
        def __init__(self, mux, properties):
            
            DebugBusMux.ChipDebugPort.__init__(self, mux)
            self._properties = properties or {}
            
        def get_properties(self):
            return self._properties

        @property
        def multidrop_id(self):
            """
            Retrieve this DAP's ID within the multidrop set-up.  This needs to
            be set explicitly for the context (typically it is set at the device
            level to distinguish the DAPs of multiple chips on the device)
            """
            try:
                return self._multidrop_id
            except AttributeError:
                raise TypeError("Attempting to use DAP in multidrop context but "
                                "no multidrop_id attribute has been assigned")

        def assign_multidrop_id(self, id):
            """
            Set the multidrop ID for this DAP
            """
            self._multidrop_id = id
            
    
class MultidropAccessRequest(MuxedAccessRequest):
    """
    Request for a multidrop access: needs to provide the Multidrop ID
    """
    @property
    def multidrop_id(self):
        return self.mux_select
    
    
class SWDMultidrop(SimpleDebugBusMux):
    
    DEBUG_MUX_ID_NAME = "multidrop_id"
    DEBUG_PORT_NAME = None
    MUXED_ACCESS_REQUEST = MultidropAccessRequest

    class ChipDebugPort(DebugBusMux.ChipDebugPort):
        
        def get_properties(self):
            # We expect the properties to be identical in all the underlying
            # logical SWD ports so just return one of them
            try:
                return self.mux.masters[0].slave.get_properties()
            except KeyError:
                return {}


class ProcessMux(SimpleDebugBusMux):
    
    DEBUG_MUX_ID_NAME = "pid"
    DEBUG_PORT_NAME = "debug_controller"
    MUXED_ACCESS_REQUEST = ARMDAPAccessRequest # because that's what gdbserver needs at the moment


class TcMemWindowedRequest(MuxedAccessRequest):
    """
    Request for a windowed memory access.  Just needs to indicate the subsystem.
    """
    @property
    def subsys(self):
        return self.mux_select


class TcMemWindowed(SimpleDebugBusMux):
    """
    Requests for windowed toolcmd memory access go through this mux and are
    presented to the toolcmd-based transport (e.g. low-cost debug). 
    """
    DEBUG_MUX_ID_NAME = "id"
    DEBUG_PORT_NAME = "tc_in"
    MUXED_ACCESS_REQUEST = TcMemWindowedRequest
    
    class DebugBusMaster(DebugBusMux.DebugBusMaster):
        def extend_access_path(self, access_path):
            for name, m in self.mux.masters.items():
                fork = AddressAccessPath(name,
                                         access_path.rank+1, self,
                                         AddressRange(0,1<<24))
                access_path.add_fork(fork)
                self._cmpt.has_data_source = True

    
class TcMemRegBasedRequest(MuxedAccessRequest):
    """
    Request for a reg-based memory access.  Just needs to indicate the subsystem.
    """
    @property
    def subsys(self):
        return self.mux_select
    
class TcMemRegBased(SimpleDebugBusMux):
    """
    Requests for register-based toolcmd memory access go through this mux and 
    are presetnted to the toolcmd-based transport (i.e. low-cost debug)
    """
    DEBUG_MUX_ID_NAME = "id"
    DEBUG_PORT_NAME = "tc_in" # 
    MUXED_ACCESS_REQUEST = TcMemRegBasedRequest

    class DebugBusMaster(DebugBusMux.DebugBusMaster):
        def extend_access_path(self, access_path):
            for name, m in self.mux.masters.items():
                fork = AddressAccessPath(name,
                                         access_path.rank+1, self,
                                         AddressRange(0,1<<32))
                access_path.add_fork(fork)
                self._cmpt.has_data_source = True
