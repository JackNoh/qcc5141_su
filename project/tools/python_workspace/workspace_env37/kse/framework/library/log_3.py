#
# Copyright (c) 2017 Qualcomm Technologies International, Ltd.
# All rights reserved.
# Qualcomm Technologies International, Ltd. Confidential and Proprietary.
#
"""Logging extensions python 3"""

import logging

import wrapt

from ._log import format_arguments, format_instance_params, format_value
from .config import DEBUG_SESSION


@wrapt.decorator
def log_exception(wrapped, instance, args, kwargs):  # @DontTrace
    """Logs an exception if any exception is raised in the decorated function

    Args:
        wrapped (function): Wrapped function which needs to be called by your wrapper function.
        instance (any): Object to which the wrapped function was bound when it was called.
        args (tuple[any]): Positional arguments supplied when the decorated function was called.
        kwargs (dict): Keyword arguments supplied when the decorated function was called.
    """
    try:
        return wrapped(*args, **kwargs)
    except Exception as exc:
        # pylint: disable=protected-access
        instance._log.error('error executing %s (%s)', wrapped.__name__, str(exc))
        raise


def log_input(level=logging.DEBUG, formatters=None, ignore=None):  # @DontTrace
    """Decorator to emit a log message in the logger self._log when the function is invoked

    Args:
        level (int): Log level of the message
        formatters (dict): Specific log format for the given parameter.
            The parameter name is the key and its format specifier should be its value.
        ignore (list): Arguments and Keyword Arguments which should be ignored in the log.
    """
    formatters = {} if not formatters else formatters
    ignore = [] if not ignore else ignore

    @wrapt.decorator(enabled=not DEBUG_SESSION)  # @DontTrace
    def wrapper(wrapped, instance, args, kwargs):  # @DontTrace
        """
        Args:
            wrapped (function): Wrapped function which needs to be called by your wrapper function.
            instance (any): Object to which the wrapped function was bound when it was called.
            args (tuple[any]): Positional arguments supplied when the decorated function was called.
            kwargs (dict): Keyword arguments supplied when the decorated function was called.
        """
        arguments = format_arguments(wrapped, args, kwargs, formatters=formatters, ignore=ignore)
        param_str = format_instance_params(instance)

        # pylint: disable=protected-access
        instance._log.log(level, '%s%s %s', param_str, wrapped.__name__, arguments)
        return wrapped(*args, **kwargs)

    return wrapper


def log_output(level=logging.DEBUG, formatters=None, ignore=None):  # @DontTrace
    """Decorator to emit a log message in the logger self._log when the function returns

    Args:
        level (int): Log level of the message
        formatters (dict): Specific log format for the given parameter. The parameter name is the
            key and its format specifier should be its value. To format the return
            value, use `return` as the key.
        ignore (list): Arguments and Keyword Arguments which should be ignored in the log.
    """
    formatters = {} if not formatters else formatters
    ignore = [] if not ignore else ignore

    @wrapt.decorator(enabled=not DEBUG_SESSION)  # @DontTrace
    def wrapper(wrapped, instance, args, kwargs):  # @DontTrace
        """
        Args:
            wrapped (function): Wrapped function which needs to be called by your wrapper function.
            instance (any): Object to which the wrapped function was bound when it was called.
            args (tuple[any]): Positional arguments supplied when the decorated function was called.
            kwargs (dict): Keyword arguments supplied when the decorated function was called.
        """
        ret = wrapped(*args, **kwargs)

        param_str = format_instance_params(instance)
        arg_str = format_arguments(wrapped, args, kwargs, formatters=formatters, ignore=ignore)

        # pylint: disable=protected-access
        instance._log.log(level, '%s%s %s ret:%s', param_str, wrapped.__name__, arg_str,
                          '%s' % (format_value(ret, formatter=formatters.get('return', '%s')))
                          if ret is not None else '')
        return ret

    return wrapper


def log_input_output(in_level=logging.DEBUG, out_level=logging.DEBUG,
                     formatters=None, ignore=None):  # @DontTrace
    """Decorator to emit a log message in the logger self._log when the function is invoked
    and another one when the function returns

    Args:
        in_level (int): Log level of the message when invoked
        out_level (int): Log level of the message when returns
        formatters (dict): Specific log format for the given parameter. The parameter name is the
            key and its format specifier should be its value. To format the return
            value, use `return` as the key.
        ignore (list): Arguments and Keyword Arguments which should be ignored in the log.
    """
    formatters = {} if not formatters else formatters
    ignore = [] if not ignore else ignore

    @wrapt.decorator(enabled=not DEBUG_SESSION)  # @DontTrace
    def wrapper(wrapped, instance, args, kwargs):  # @DontTrace
        """
        Args:
            wrapped (function): Wrapped function which needs to be called by your wrapper function.
            instance (any): Object to which the wrapped function was bound when it was called.
            args (tuple[any]): Positional arguments supplied when the decorated function was called.
            kwargs (dict): Keyword arguments supplied when the decorated function was called.
        """
        param_str = format_instance_params(instance)
        arg_str = format_arguments(wrapped, args, kwargs, formatters=formatters, ignore=ignore)

        # pylint: disable=protected-access
        instance._log.log(in_level, '%s%s %s', param_str, wrapped.__name__, arg_str)

        ret = wrapped(*args, **kwargs)

        param_str = format_instance_params(instance)  # may have changed
        instance._log.log(out_level, '%s%s %s ret:%s', param_str, wrapped.__name__, arg_str,
                          '%s' % (format_value(ret, formatter=formatters.get('return', '%s')))
                          if ret is not None else '')
        return ret

    return wrapper
