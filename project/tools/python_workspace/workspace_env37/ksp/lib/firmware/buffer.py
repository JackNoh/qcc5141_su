#
# Copyright (c) 2020 Qualcomm Technologies, Inc. and/or its
# subsidiaries.  All rights reserved.
# Qualcomm Technologies International, Ltd. Confidential and Proprietary.
#
"""Buffer address handler."""


# An internal memory handler. It's possible refactor with Kymera object.
# pylint: disable=too-few-public-methods
class BufferAddress(object):
    """A buffer address management for a ``unint16`` memory location in memory.

    It is a context manager class. During the instantiation it allocates
    buffers and sets values of the words in place. Once the user exits
    from the context, the buffer will be released.

    Args:
        firmware (Kymera obj): An instance of Kymera class.
        address (Integral): The address in the apps1 memory.
        words (list): A list of words.
        size (int): Size of the buffer.
        owner (int): The owner identifier.
    """
    def __init__(self, firmware, words, size, owner=0):
        self._firmware = firmware
        self._words = words
        self._size = size
        self._owner = owner

        self._address = None

    def _allocate(self):
        self._address = self._firmware.allocate(self._size, self._owner)

    def _set_words(self):
        for offset, word in enumerate(self._words):
            start = self._address + 2 * offset
            end = self._address + 2 * offset + 2
            self._firmware.set_dm(start, end, [word & 0xFF, word >> 8])

    def _release(self):
        self._firmware.free(self._address)

    def __enter__(self):
        self._allocate()
        self._set_words()

        return self._address

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._release()
