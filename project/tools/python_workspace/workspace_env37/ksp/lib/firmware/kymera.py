#
# Copyright (c) 2020 Qualcomm Technologies, Inc. and/or its
# subsidiaries.  All rights reserved.
# Qualcomm Technologies International, Ltd. Confidential and Proprietary.
#
"""Encapsulate PyDBG app's call methods."""
import logging

from ksp.lib.exceptions import FirmwareError
from ksp.lib.firmware.buffer import BufferAddress
from ksp.lib.logger import method_logger

logger = logging.getLogger(__name__)


class Kymera(object):
    """Wraps around pydbg apps1 method to call the functions in the firmware.

    Args:
        apps1 (apps1 pydbg obj): apps1 pydbg object.
    """
    def __init__(self, apps1):
        self._apps = apps1

    def set_timeout(self, seconds):
        """Call the set_timeout built-in method.

        Help on method set_timeout in module csr.dev.fw.call:
        set_timeout(self, timeout) method of csr.wheels.bitsandbobs.Call_sub_0
            instance
        Set the timeout for all function calls through this object
        Args:
            seconds(int): Maximum time to wait for completion of calls.
        """
        try:
            self._apps.fw.call.set_timeout(seconds)
        except Exception as error:  # pylint: disable=broad-except
            raise FirmwareError(error)

    def _string_to_charptr(self, str_txt):
        """Creates a new variable for the given string.

        Create a Pydbg _variable object in target memory, containing
        the string as a C string (terminated with a 0 character).
        """
        try:
            chars = self._apps.fw.call.pnew("char", len(str_txt) + 1)
            for i in list(range(len(str_txt))):
                chars[i].value = ord(str_txt[i])
            chars[len(str_txt)].value = ord('\0')

        except Exception as error:  # pylint: disable=broad-except
            raise FirmwareError(error)

        return chars

    @method_logger(logger)
    def free(self, mem_object):
        """Call the 'free' firmware function.

        Args:
            mem_object: int memory address or _Variable object such as
                obtained from pnew.
        """
        try:
            self._apps.fw.call.free(mem_object)
        except Exception as error:  # pylint: disable=broad-except
            raise FirmwareError(error)

    def set_dm(self, start, end, words):
        """Set Data Memory.

        Args:
            start (int): Starting address.
            end (int): Ending address.
            words (list): List of integers.
        """
        try:
            self._apps.dm[start:end] = words
        except Exception as error:  # pylint: disable=broad-except
            raise FirmwareError(error)

    def allocate(self, size, owner):
        """Allocate program memory.

        Args:
            size (int)
            owner (int)

        Returns:
            int: Memory address.

        Raises:
            FirmwareError: Some unexpected happen in the firmware.
        """
        try:
            return self._apps.fw.call.pmalloc_trace(
                size,
                owner
            )
        except Exception as error:  # pylint: disable=broad-except
            raise FirmwareError(error)

    @method_logger(logger)
    def find_file(self, start, name):
        """Find the file in the firmware.

        Args:
            start: int, starting file index, normally 1.
            name: str, file name.
        Returns:
            int: File index if found, 0 otherwise.

        Raises:
            FirmwareError: Some unexpected happen in the firmware.
        """
        name_str = self._string_to_charptr(name)
        try:
            file_index = self._apps.fw.call.FileFind(
                start, name_str, len(name_str) - 1)

        except Exception as error:  # pylint: disable=broad-except
            raise FirmwareError(error)

        finally:
            self.free(name_str)

        return file_index

    @method_logger(logger)
    def load_downloadable(self, index, processor):
        """Loads the downloadable in the firmware.

        Args:
            index (int): The file index which is found using find_file
                method.
            processor (int): Number of the audio processor core in whose
                PM RAM the bundle should be loaded.

        Return:
            int: handle for the loaded downloadable.

        Raises:
            FirmwareError: Some unexpected happen in the firmware.
        """
        try:
            bdl = self._apps.fw.call.OperatorBundleLoad(index, processor)
        except Exception as error:  # pylint: disable=broad-except
            raise FirmwareError(error)

        return bdl

    @method_logger(logger)
    def unload_downloadable(self, handle, timeout=10):
        """Unload the loaded downloadable.

        Args:
            handle: Handle which comes from load_downloadable.
            timeout (int): Time out in seconds.
        Return:
            bool: True if successful.

        Raises:
            FirmwareError: Some unexpected happen in the firmware.
        """
        try:
            self.set_timeout(timeout)
            success = self._apps.fw.call.OperatorBundleUnload(handle)

        except Exception as error:  # pylint: disable=broad-except
            raise FirmwareError(error)

        return success

    @method_logger(logger)
    def create_operator(self, cap_id, proc_num=None):
        """Creates the operator from the given capability ID.

        Args:
            cap_id (int): Capability ID.
            proc_num (int): optional, audio processor core number, 0..1,
                default None means processor 0.

        Return:
            int: Kymera Operator ID.

        Raises:
            FirmwareError: Some unexpected happen in the firmware.
        """
        if proc_num:
            # Key 2, proc 1
            # typedef struct
            # {
            #     uint16 key;         /*!< Key for OperatorCreate. */
            #     uint32 value;       /*!< Value for the key. */
            # } OperatorCreateKeys;
            #
            with BufferAddress(self, [2, 0, proc_num, 0], 8) as params:
                try:
                    op_id = self._apps.fw.call.OperatorCreate(
                        cap_id, 1, params)
                except Exception as error:  # pylint: disable=broad-except
                    raise FirmwareError(error)
        else:
            try:
                op_id = self._apps.fw.call.OperatorCreate(cap_id, 0, 0)
            except Exception as error:  # pylint: disable=broad-except
                raise FirmwareError(error)

        return op_id

    @method_logger(logger)
    def destroy_operator(self, op_id):
        """Destroy an operator.

        Call the OperatorDestroyMultiple trap with converted arguments:
        bool OperatorDestroyMultiple(uint16 n_ops, Operator * oplist,
                                     uint16 * success_ops)
        Args:
            op_id (int): Kymera operator ID.

        Return:
            bool: True if successful.

        Raises:
            FirmwareError: Some unexpected happen in the firmware.
        """
        with BufferAddress(self, [op_id], 2*len([op_id])) as op_ad:
            destroyed_ops = BufferAddress(self, [], 2)
            with destroyed_ops:
                try:
                    success = self._apps.fw.call.OperatorDestroyMultiple(
                        1,
                        op_ad,
                        destroyed_ops.__enter__()
                    )
                except Exception as error:  # pylint: disable=broad-except
                    raise FirmwareError(error)

        return success

    @method_logger(logger)
    def start_operator(self, op_id):
        """Start an operator.

        Call the OperatorStartMultiple trap:
        bool OperatorStartMultiple(uint16 n_ops, Operator * oplist,
                                   uint16 * success_ops)
        The arguments are converted.
        Args:
            op_id (int): Kymera Operator ID.

        Returns:
            bool: True if successful.

        Raises:
            FirmwareError: Some unexpected happen in the firmware.
        """
        with BufferAddress(self, [op_id], 2*len([op_id])) as op_addr:
            try:
                ret = self._apps.fw.call.OperatorStartMultiple(
                    1,
                    op_addr,
                    0
                )
            except Exception as error:  # pylint: disable=broad-except
                raise FirmwareError(error)

        return ret

    @method_logger(logger)
    def stop_operator(self, op_id):
        """Stop the started operator.

        Args:
            op_id: Kymera operator ID.

        Returns:
            bool: True if successful, False otherwise.

        Raises:
            FirmwareError: Some unexpected happen in the firmware.
        """
        with BufferAddress(self, [op_id], 2*len([op_id])) as op_addr:
            try:
                ret = self._apps.fw.call.OperatorStopMultiple(
                    1,
                    op_addr,
                    0
                )
            except Exception as error:  # pylint: disable=broad-except
                raise FirmwareError(error)
        return ret

    @method_logger(logger)
    def send_operator_message(self, op_id, msg):
        """Send the message to an operator.

        Args:
            op_id (int): Kymera operator ID.
            msg (list): list of at least 1 integer. These will be sent to
                the operator as 16-bit words.
        Return:
            bool: True if successful, False otherwise.

        Raises:
            FirmwareError: Some unexpected happen in the firmware.
        """
        size_bytes = 2*len(msg)
        with BufferAddress(self, [], size_bytes) as receive_address:
            with BufferAddress(self, msg, size_bytes) as send_address:
                try:
                    return_val = self._apps.fw.call.OperatorMessage(
                        op_id,
                        send_address,
                        len(msg),
                        receive_address,
                        len(msg)
                    )

                except Exception as error:  # pylint: disable=broad-except
                    raise FirmwareError(error)

        return return_val
