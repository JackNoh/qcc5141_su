#
# Copyright (c) 2020 Qualcomm Technologies, Inc. and/or its
# subsidiaries.  All rights reserved.
# Qualcomm Technologies International, Ltd. Confidential and Proprietary.
#
"""Interact with the KSP operator in the firmware."""
import logging

from ksp.lib.data_types import KspDataType
from ksp.lib.exceptions import CommandError, OperatorError, FirmwareError
from ksp.lib.firmware.downloadable.ksp_downloadable import KSPDownloadable
from ksp.lib.firmware.operator.convertors import stream_to_words
from ksp.lib.logger import method_logger
from ksp.lib.types import TransformIDList

logger = logging.getLogger(__name__)


class KSPOperator(object):
    """Manage the KSP operator.

    Also, this class takes the responsibility of loading and unloading of
    the KSP downloadable.

    Args:
        firmware (Firmware obj): Firmware instance.
        edkcs (bool): If True will load a signed downloadable.
        builtin_cap (bool): If True will use the built-in KSP operator.

    Raises:
        DownloadableError: If something is wrong with loading the
            downloadable.
    """

    # KSP capability IDs.
    DOWNLOADABLE_CAP_ID = 0x4093
    BUILTIN_CAP_ID = 0x00BF

    @method_logger(logger)
    def __init__(self, firmware, edkcs=False, builtin_cap=False):

        self._firmware = firmware
        self._op_id = 0
        self._running = False
        self._number_of_active_streams = 0
        self._streams = {}
        self._downloadable = None

        if builtin_cap:
            self._cap_id = self.BUILTIN_CAP_ID
        else:
            self._cap_id = self.DOWNLOADABLE_CAP_ID
            self._downloadable = KSPDownloadable(self._firmware, edkcs=edkcs)
            self._downloadable.load()

    def is_running(self):
        """Returns the status of the operator.

        Returns:
            bool: True if it's running, False otherwise.
        """
        return self._running

    # pylint: disable=too-many-arguments
    # BUG: B-299459: Unify the concept of streams
    @method_logger(logger)
    def config_stream(self, stream_number, data_type, tr_channels,
                      nr_samples=0, channel_info=0, metadata=False,
                      timed_data=""):
        """Configures a stream.

        Args:
            stream_number (int): stream id, 0 or 1.
            data_type (KspDataType): stream data type, acceptable values are:
                'DATA16', 'PCM16', 'PCM24','PCM32', 'DATA32' and 'TTR'.
                list KspDataType for latest possible values.
            nr_samples (int): number of samples in each stream packet, up
                to 256, leave it to 0 so audio FW will decide.
            channel_info (int): channel info  type, no use at the moment set it
                to 0 always.
            tr_channels (list): a TransformIDList.
            metadata (bool): enable tracing metadata tags.
        """

        # BUG: B-299459: The sanity check should happen within the stream
        #      object. This method is too long.
        # Sanity check the inputs.
        if stream_number not in (0, 1):
            error = (
                "KSP Config failed. Stream number "
                "%s is invalid." % stream_number
            )
            logger.error(error)
            raise CommandError(error)
        try:
            data_type = KspDataType[data_type]
        except KeyError:
            error = "KSP Config failed. Data type %s is invalid." % data_type
            logger.error(error)
            raise CommandError(error)
        if nr_samples > 256:
            error = (
                "KSP Config failed. nr_samples is "
                "%s and is bigger than 256." % nr_samples
            )
            logger.error(error)
            raise CommandError(error)
        if channel_info != 0:
            error = (
                "KSP Config failed. channel_info=%s "
                "and not zero" % channel_info
            )
            logger.error(error)
            raise CommandError(error)
        if metadata and data_type != KspDataType.TTR:
            error = (
                "KSP Config failed. Data type "
                "%d and metadata enabled" % data_type
            )
            logger.error(error)
            raise CommandError(error)

        nr_channels = TransformIDList.len(tr_channels)

        if data_type == KspDataType.TTR:
            min_channels = 0
            max_channels = TransformIDList.ALL_LEN
        else:
            min_channels = 1
            if data_type.is_audio_type():
                max_channels = 4
            else:
                max_channels = 1

        if nr_channels < min_channels:
            error = 'Configure at least {} transform ID for stream type {}' \
                    .format(min_channels, str(data_type))
            logger.error(error)
            raise CommandError(error)
        if nr_channels > max_channels:
            error = (
                "KSP Config failed. {} channels requested, "
                "max {} channels supported for stream "
                "type {}" .format(nr_channels, max_channels, str(data_type))
            )
            logger.error(error)
            raise CommandError(error)

        # Clear stream if already configured.
        if stream_number in self._streams:
            del self._streams[stream_number]

        # Add stream config.
        # BUG: B-299459: Unify the concept of streams.
        self._streams[stream_number] = [
            stream_number,
            nr_samples,
            channel_info,
            data_type,
            tr_channels,
            metadata,
            timed_data
        ]

    @method_logger(logger)
    def start(self):
        """Starting KSP."""
        if self._running:
            raise OperatorError("KSP operator is already running.")
        if not self._streams:
            raise OperatorError("No stream is configured by the user.")

        self._create()
        self._configure()
        self._start_operator()

        return self._op_id

    @method_logger(logger)
    def stop(self):
        """Stopping KSP."""
        if self._running:
            self._stop_operator()
            self._destroy()

    @method_logger(logger)
    def __del__(self):
        if self._running:
            self._stop_operator()
        if self._op_id != 0:
            self._destroy()

        # Check if the downloadable object is available, unload it.
        if self._downloadable:
            self._downloadable.unload()

    @method_logger(logger)
    def _create(self):
        if self._op_id != 0:
            logger.warning("Can't create the operator. It already exists.")
            return

        try:
            self._op_id = self._firmware.create_operator(self._cap_id)

        except FirmwareError as error:
            self._op_id = 0
            raise OperatorError(
                "Creating the KSP operator is failed with '%s'." % error
            )

        if self._op_id == 0:
            raise OperatorError("Creating the KSP operator is failed.")

    @method_logger(logger)
    def _destroy(self):
        if self._op_id == 0:
            # Quietly return.
            assert self._number_of_active_streams == 0
            assert not self._running
            return

        if self._running:
            raise OperatorError("Cannot destroy the running operator.")

        ret = self._firmware.destroy_operator(self._op_id)
        if ret == 0:
            raise OperatorError("The operator is not destroyed.")

        # Destroyed!
        self._op_id = 0
        self._number_of_active_streams = 0

    @method_logger(logger)
    def _configure(self):
        """Configure the KSP operator."""
        self._number_of_active_streams = 0
        for stream_number, stream in self._streams.items():
            # BUG: B-299459: Unify the concept of streams.
            words_to_send = stream_to_words(*stream)
            ret_val = self._firmware.send_operator_message(
                self._op_id,
                words_to_send
            )

            if ret_val == 0:
                raise OperatorError(
                    "Configuring stream %s has failed." % stream_number
                )

            self._number_of_active_streams += 1

    @method_logger(logger)
    def _start_operator(self):
        """Start the KSP operator."""
        ret = self._firmware.start_operator(self._op_id)
        if ret == 0:
            raise OperatorError(
                "Starting KSP operator failed with %s return id." % ret
            )

        self._running = True

    @method_logger(logger)
    def _stop_operator(self):
        """Stop the KSP operator."""
        ret = self._firmware.stop_operator(self._op_id)
        if ret == 0:
            raise OperatorError(
                "Stopping KSP operator failed with %s return id." % ret
            )

        self._running = False
