# The following script will be run when deploying the project into the simulator environment (KSE)
# When the DOS shell is presented, it automatically performs a download of the download_ASHER.dkcs. 
# You can add additional setup commands to this file if desired.
# Please refer to the Kymera Simulator Environment User Guide for instructions on how to use it.
# Please note: This file is ONLY used for KSE (Kymera Simulator Environment)

if __name__ == '__main__':

    print('Downloading download_ASHER.dkcs')

    id = hydra_cap_download.download('script/kalsim/download_ASHER/download_ASHER.dkcs')
    print('Bundle downloaded. ID is %s' % (id))
