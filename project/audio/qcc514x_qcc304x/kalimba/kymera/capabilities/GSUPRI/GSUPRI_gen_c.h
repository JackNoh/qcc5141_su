// -----------------------------------------------------------------------------
// Copyright (c) 2019                  Qualcomm Technologies International, Ltd.
//
#ifndef __GSUPRI_GEN_C_H__
#define __GSUPRI_GEN_C_H__

#ifndef ParamType
typedef unsigned ParamType;
#endif

// CodeBase IDs
#define GSUPRI_BPT_CAP_ID	0x0001
#define GSUPRI_BPT_ALT_CAP_ID_0	0x4002
#define GSUPRI_BPT_SAMPLE_RATE	0
#define GSUPRI_BPT_VERSION_MAJOR	1

#define GSUPRI_TTP_CAP_ID	0x003C
#define GSUPRI_TTP_ALT_CAP_ID_0	0x4067
#define GSUPRI_TTP_SAMPLE_RATE	0
#define GSUPRI_TTP_VERSION_MAJOR	1

// Constant Definitions


// Runtime Config Parameter Bitfields
#define GSUPRI_CONFIG_PM_BYPASS		0x00000001


// System Mode
#define GSUPRI_SYSMODE_STATIC 		0
#define GSUPRI_SYSMODE_STANDBY		1
#define GSUPRI_SYSMODE_MONITOR		2
#define GSUPRI_SYSMODE_MAX_MODES		3

// System Control
#define GSUPRI_CONTROL_MODE_OVERRIDE		0x2000

// Statistics Block
typedef struct _tag_GSUPRI_STATISTICS
{
        ParamType OFFSET_CUR_MODE_OFFSET;
        ParamType OFFSET_PEAK_LEVEL_1;
        ParamType OFFSET_PEAK_LEVEL_2;
        ParamType OFFSET_PEAK_LEVEL_3;
        ParamType OFFSET_PEAK_LEVEL_4;
        ParamType OFFSET_PEAK_LEVEL_5;
        ParamType OFFSET_PEAK_LEVEL_6;
        ParamType OFFSET_PEAK_LEVEL_7;
        ParamType OFFSET_PEAK_LEVEL_8;
        ParamType OFFSET_OVR_CONTROL;
}GSUPRI_STATISTICS;

typedef GSUPRI_STATISTICS* LP_GSUPRI_STATISTICS;

// Parameters Block
typedef struct _tag_GSUPRI_PARAMETERS
{
        ParamType OFFSET_CONFIG;
        ParamType OFFSET_GAIN;
}GSUPRI_PARAMETERS;

typedef GSUPRI_PARAMETERS* LP_GSUPRI_PARAMETERS;

unsigned *GSUPRI_GetDefaults(unsigned capid);

#endif // __GSUPRI_GEN_C_H__
