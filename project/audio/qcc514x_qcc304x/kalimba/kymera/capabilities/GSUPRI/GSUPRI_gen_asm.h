// -----------------------------------------------------------------------------
// Copyright (c) 2021                  Qualcomm Technologies International, Ltd.
//
#ifndef __GSUPRI_GEN_ASM_H__
#define __GSUPRI_GEN_ASM_H__

// CodeBase IDs
.CONST $M.GSUPRI_BPT_CAP_ID       	0x0001;
.CONST $M.GSUPRI_BPT_ALT_CAP_ID_0       	0x4002;
.CONST $M.GSUPRI_BPT_SAMPLE_RATE       	0;
.CONST $M.GSUPRI_BPT_VERSION_MAJOR       	1;

.CONST $M.GSUPRI_TTP_CAP_ID       	0x003C;
.CONST $M.GSUPRI_TTP_ALT_CAP_ID_0       	0x4067;
.CONST $M.GSUPRI_TTP_SAMPLE_RATE       	0;
.CONST $M.GSUPRI_TTP_VERSION_MAJOR       	1;

// Constant Values


// Piecewise Disables
.CONST $M.GSUPRI.CONFIG.PM_BYPASS		0x00000001;


// Statistic Block
.CONST $M.GSUPRI.STATUS.CUR_MODE_OFFSET		0*ADDR_PER_WORD;
.CONST $M.GSUPRI.STATUS.PEAK_LEVEL_1   		1*ADDR_PER_WORD;
.CONST $M.GSUPRI.STATUS.PEAK_LEVEL_2   		2*ADDR_PER_WORD;
.CONST $M.GSUPRI.STATUS.PEAK_LEVEL_3   		3*ADDR_PER_WORD;
.CONST $M.GSUPRI.STATUS.PEAK_LEVEL_4   		4*ADDR_PER_WORD;
.CONST $M.GSUPRI.STATUS.PEAK_LEVEL_5   		5*ADDR_PER_WORD;
.CONST $M.GSUPRI.STATUS.PEAK_LEVEL_6   		6*ADDR_PER_WORD;
.CONST $M.GSUPRI.STATUS.PEAK_LEVEL_7   		7*ADDR_PER_WORD;
.CONST $M.GSUPRI.STATUS.PEAK_LEVEL_8   		8*ADDR_PER_WORD;
.CONST $M.GSUPRI.STATUS.OVR_CONTROL    		9*ADDR_PER_WORD;
.CONST $M.GSUPRI.STATUS.BLOCK_SIZE          	10;

// System Mode
.CONST $M.GSUPRI.SYSMODE.STATIC 		0;
.CONST $M.GSUPRI.SYSMODE.STANDBY		1;
.CONST $M.GSUPRI.SYSMODE.MONITOR		2;
.CONST $M.GSUPRI.SYSMODE.MAX_MODES		3;

// System Control
.CONST $M.GSUPRI.CONTROL.MODE_OVERRIDE		0x2000;

// Parameter Block
.CONST $M.GSUPRI.PARAMETERS.OFFSET_CONFIG		0*ADDR_PER_WORD;
.CONST $M.GSUPRI.PARAMETERS.OFFSET_GAIN  		1*ADDR_PER_WORD;
.CONST $M.GSUPRI.PARAMETERS.STRUCT_SIZE 		2;


#endif // __GSUPRI_GEN_ASM_H__
