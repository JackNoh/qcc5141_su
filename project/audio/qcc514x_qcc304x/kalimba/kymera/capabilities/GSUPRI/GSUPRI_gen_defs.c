// -----------------------------------------------------------------------------
// Copyright (c) 2019                  Qualcomm Technologies International, Ltd.
//
#include "GSUPRI_gen_c.h"

#ifndef __GNUC__ 
_Pragma("datasection CONST")
#endif /* __GNUC__ */

static unsigned defaults_GSUPRITTP[] = {
   0x00002080u,			// CONFIG
   0x00000000u			// GAIN
};

unsigned *GSUPRI_GetDefaults(unsigned capid){
	switch(capid){
                case 0x0001: return defaults_GSUPRITTP;
                case 0x4002: return defaults_GSUPRITTP;
                case 0x003C: return defaults_GSUPRITTP;
                case 0x4067: return defaults_GSUPRITTP;
	}
	return((unsigned *)0);
}
