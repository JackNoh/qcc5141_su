/****************************************************************************
 * Copyright (c) 2013 - 2017 Qualcomm Technologies International, Ltd.
****************************************************************************/
/**
 * \defgroup GSUPRI
 * \file  GSUPRI.h
 * \ingroup capabilities
 *
 * GSUPRI operator public header file. <br>
 *
 */

#ifndef _GSUPRI_OP_H_
#define _GSUPRI_OP_H_

#include "opmgr/opmgr_for_ops.h"

/****************************************************************************
Public Variable Definitions
*/
/** The capability data structure for GSUPRI */
extern const CAPABILITY_DATA GSUPRI_cap_data;

/** The capability data structure for passthrough with time-to-play */
extern const CAPABILITY_DATA ttp_passthrough_cap_data;


// GSUPRI_ADD_START
extern bool GSUPRI_opmsg_set_mode(OPERATOR_DATA *op_data, void *message_data, unsigned *resp_length, OP_OPMSG_RSP_PAYLOAD **resp_data);
extern bool GSUPRI_opmsg_set_volume(OPERATOR_DATA *op_data, void *message_data, unsigned *resp_length, OP_OPMSG_RSP_PAYLOAD **resp_data);
// GSUPRI_ADD_END




#endif /* _GSUPRI_OP_H_ */
