// *****************************************************************************
// Copyright (c) 2005 - 2017 Qualcomm Technologies International, Ltd.
// %%version
//
// $Change$  $DateTime$
// *****************************************************************************

// ASM function for basic mono operator data processing
// The function(s) obey the C compiler calling convention (see documentation, CS-124812-UG)

#include "cbuffer_asm.h"
#include "GSU_TRIAL_BASIC_shared_const.h"
#include "GSU_TRIAL_BASIC_struct_asm_defs.h" // data structure field offsets
#include "GSU_TRIAL_BASIC_gen_asm.h"
#include "portability_macros.h"
.MODULE $M.GSU_TRIAL_BASIC_proc;
    .CODESEGMENT PM;
    .DATASEGMENT DM;

// Reverb Memory
.CONST $Gaudio_Delay1_L 229;
.CONST $Gaudio_Delay2_L 191;
.CONST $Gaudio_Delay3_L 251;
.CONST $Gaudio_Delay4_L 163;
.CONST $Gaudio_Delay1_R 241;
.CONST $Gaudio_Delay2_R 173;
.CONST $Gaudio_Delay3_R 223;
.CONST $Gaudio_Delay4_R 193;
.CONST $Gaudio_Allpass1_L 37;
.CONST $Gaudio_Allpass2_L 463;
.CONST $Gaudio_Allpass1_R 29;
.CONST $Gaudio_Allpass2_R 491;

.CONST $Gaudio_Delay_kGain 1964947538;
.CONST $Gaudio_Allpass1_kGain 1717986918;
.CONST $Gaudio_Allpass2_kGain 214748365;
.CONST $Gaudio_Reverb_kGain 279172874;
.CONST $Gaudio_kSigDelay 100;

.VAR/DM1 $Gaudio_iirC48[5] = -32283249, 185289004, 190941298, 15191475, -478283288;
.VAR/DM1 $Gaudio_reverb_iir_delayL[4] = 0, 0, 0, 0;
.VAR/DM1 $Gaudio_reverb_iir_delayR[4] = 0, 0, 0, 0;

.VAR/DM1 $Gaudio_Delay1_Idx_L = 0;
.VAR/DM1 $Gaudio_Delay2_Idx_L = 0;
.VAR/DM1 $Gaudio_Delay3_Idx_L = 0;
.VAR/DM1 $Gaudio_Delay4_Idx_L = 0;
.VAR/DM1 $Gaudio_Delay1_Idx_R = 0;
.VAR/DM1 $Gaudio_Delay2_Idx_R = 0;
.VAR/DM1 $Gaudio_Delay3_Idx_R = 0;
.VAR/DM1 $Gaudio_Delay4_Idx_R = 0;
.VAR/DM1 $Gaudio_Allpass1_Idx_L = 0;
.VAR/DM1 $Gaudio_Allpass2_Idx_L = 0;
.VAR/DM1 $Gaudio_Allpass1_Idx_R = 0;
.VAR/DM1 $Gaudio_Allpass2_Idx_R = 0;
.VAR/DM1 $Gaudio_System_Idx = 0;

//Cross Filter Memory
.CONST $Gaudio_Cross_Delay 35;
.CONST $Gaudio_Cross_Gain 644245094;
.VAR/DM1 $Gaudio_cross_iir0[5] = 679334315, -1360957528, 1118950101, 724542593, -1360957528;
//.VAR/DM1 $Gaudio_cross_iir0[5] = 0, 0, 1073741824, 0, 0;
.VAR/DM1 $Gaudio_cross_iir_delayL0[4] = 0, 0, 0, 0;
.VAR/DM1 $Gaudio_cross_iir_delayL1[4] = 0, 0, 0, 0;

.VAR/DM1 $Gaudio_cross_iir1[5] = 945411253, -441528845, 1102779649, 974449079, -441528845;
//.VAR/DM1 $Gaudio_cross_iir1[5] = 0, 0, 1073741824, 0, 0;
.VAR/DM1 $Gaudio_cross_iir_delayR0[4] = 0, 0, 0, 0;
.VAR/DM1 $Gaudio_cross_iir_delayR1[4] = 0, 0, 0, 0;


// Limiter Filter Memory
.CONST $Gaudio_Limiter_Lookahead 31;
.CONST $Gaudio_Limiter_duration 32;
.CONST $Gaudio_kOne 33554432;//33554432;//134217727;
.CONST $Gaudio_kOneMax 2147483647;//134217727;
.CONST $Gaudio_kThreshold 33386659;//2144262422;//2140182203;//134217727;
.CONST $Gaudio_kSmoothing 1193046471;
.CONST $Gaudio_kSmoothing_Inv 119304647;
.CONST $Gaudio_kAttack 1998388241;
.CONST $Gaudio_kAttack_Inv 149095407;
.CONST $Gaudio_kRelease 2141326949;
.CONST $Gaudio_kRelease_Inv 6156699;

.VAR/DM1 $Gaudio_Limiter_GainPrev = 0;
.VAR/DM1 $Gaudio_Limiter_SmoothGainPrev = 0;
.VAR/DM1 $Gaudio_Limiter_Idx = 0;

.CONST $Gaudio_No_Silent 22302;
.CONST $Gaudio_Silent_Cycle 25293;
.VAR/DM1 $Gaudio_Silent = 0;

// output gain
//.CONST $Gaudio_Output_Gain 1207618800; //-5
//.CONST $Gaudio_Output_Gain 1354970579;   //-4
//.CONST $Gaudio_Output_Gain 1520301995;   //-3
//.CONST $Gaudio_Output_Gain 1705806895;   //-2
//.CONST $Gaudio_Output_Gain 1913946815;   //-1

// *****************************************************************************
// MODULE:
//    $_GSU_TRIAL_BASIC_processing
//
// DESCRIPTION:
//    Data processing function. Processes each active channel of the operator
//
// INPUTS:
//    - r0 = OPERATOR_EXTRA_DATA struct address (the cap specific part of the
//    data structure)
//    - r1 = number of samples to process
//    - M2 = -ADDR_PER_WORD (This is implict in that it is called from C
//
// OUTPUTS:
//    - r0 = return code (0 for success)
//
// TRASHED REGISTERS:
//    rMAC, r2, r3, r4, r10, I7, I6
//
// NOTES: REMOTE Buffers might work (for hydra) assuming they are accessed through a window,
//        but have not been tested.
//        It relies for now on imagined $cbuffer and $common modules' constants
//
// *****************************************************************************


$_GSU_TRIAL_BASIC_processing:

   // for now, we assume we get to the buffer parameters directly with some offset constants
   push rLink;
   pushm <I0, I4, L0, L4>;
   pushm <I1, I5, L1, L5>;
   pushm <B0, B1, B4, B5>;
   pushm <r5, r6, r9>;

   push r1;
   r10 = M[SP - ADDR_PER_WORD]; // load number of samples to process into r10;

   I7 = r0 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.CHANNEL_FIELD;
   r6 = M[r0 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GSU_TRIAL_BASIC_MODE_FIELD];
   r9 = r0;


   // Initialise r3 to the next channel
   r3 = M[I7, ADDR_PER_WORD];
   // get read pointer, size and start addresses of input buffer if the buffer
   // doesn't exist this and any more channels in the list are inactive
   r0 = M[r3 + $GSU_TRIAL_BASIC_struct.PASSTHROUGH_CHANNEL_struct.IP_BUFFER_FIELD];
   call $cbuffer.get_read_address_and_size_and_start_address;
   push r2;
   pop B0;
   I0 = r0;
   L0 = r1;

   // now get the output buffer info if its null then its inactive
   r0 = M[r3 + $GSU_TRIAL_BASIC_struct.PASSTHROUGH_CHANNEL_struct.OP_BUFFER_FIELD];
   call $cbuffer.get_write_address_and_size_and_start_address;
   push r2;
   pop B4;
   I4 = r0;
   L4 = r1;

   r5 = M[I7, ADDR_PER_WORD];
   // get read pointer, size and start addresses of input buffer if the buffer
   // doesn't exist this and any more channels in the list are inactive
   r0 = M[r5 + $GSU_TRIAL_BASIC_struct.PASSTHROUGH_CHANNEL_struct.IP_BUFFER_FIELD];
   call $cbuffer.get_read_address_and_size_and_start_address;
   push r2;
   pop B1;
   I1 = r0;
   L1 = r1;

   // now get the output buffer info if its null then its inactive
   r0 = M[r5 + $GSU_TRIAL_BASIC_struct.PASSTHROUGH_CHANNEL_struct.OP_BUFFER_FIELD];
   call $cbuffer.get_write_address_and_size_and_start_address;
   push r2;
   pop B5;
   I5 = r0;
   L5 = r1;


/*
      r2 = M[I0, ADDR_PER_WORD];
      r4 = M[I1, ADDR_PER_WORD];
   do GSU_TRIAL_BASIC_copy_loop;
      M[I4, ADDR_PER_WORD] = r2, r2 = M[I0, ADDR_PER_WORD];
      M[I5, ADDR_PER_WORD] = r4, r4 = M[I1, ADDR_PER_WORD];
   GSU_TRIAL_BASIC_copy_loop:
*/




// =============================================================================
// =============================================================================
// =============================================================================
// =============================================================================
// =============================================================================

// buffer pointers
//---------------------------------------
// r3,    left  in  = I0/L0,     left  out = I4/L4
// r5,    right in  = I1/L1,     right out = I5/L5

pushm <r2, r3, r4, r5, r6, r7, r8, r9>; // $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.CHANNEL_FIELD
pushm <I2, I3, I6, I7>;
pushm <M0, M1>;


   NULL = r6;
   if z jump GSU_TRIAL_BASIC_MODE0_BYPASS;
   r6 = r6 - 1;
   if z jump GSU_TRIAL_BASIC_MODE1_SpatialUpmix;
   jump GSU_TRIAL_BASIC_MODE0_BYPASS;


// =============================================================================
GSU_TRIAL_BASIC_MODE0_BYPASS:
      r2 = M[I0, ADDR_PER_WORD];
      r4 = M[I1, ADDR_PER_WORD];
   do LOOP_GSU_TRIAL_BASIC_MODE0;
      M[I4, ADDR_PER_WORD] = r2, r2 = M[I0, ADDR_PER_WORD];
      M[I5, ADDR_PER_WORD] = r4, r4 = M[I1, ADDR_PER_WORD];
   LOOP_GSU_TRIAL_BASIC_MODE0:
jump GSU_TRIAL_BASIC_GO_PROCESS_OUT;
// =============================================================================

// =============================================================================
GSU_TRIAL_BASIC_MODE1_SpatialUpmix:

    r8 = r10;
    r2 = M[I0, ADDR_PER_WORD];	// Left  Input
    r3 = M[I1, ADDR_PER_WORD];	// Right Input
     I2 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_INBUFL_FIELD;
     I6 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_INBUFR_FIELD;
     I3 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_PROC1_FIELD;
     I7 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_PROC2_FIELD;
     r10 = r8;// ASHIFT -3;
     do gaudio_headroom_setting;
      r2 = r2 ASHIFT -6;
      r3 = r3 ASHIFT -6;
      M[I2, ADDR_PER_WORD] = r2, M[I6, ADDR_PER_WORD] = r3; //Q5.27
      r2 = 0;
      M[I3, ADDR_PER_WORD] = r2;
      M[I7, ADDR_PER_WORD] = r2;
      r2 = M[I0, ADDR_PER_WORD]; //Q1.31
      r3 = M[I1, ADDR_PER_WORD]; //Q1.31
     gaudio_headroom_setting:

    // TEST Ver.
    r0 = M[r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_FRAME_COUNT_FIELD];
    r0 = r0 + 1;
    r2 = 0;
    r1 = r0 - $Gaudio_No_Silent;
    if GT r2 = 1;
    M[$Gaudio_Silent] = r2;
    r1 = r0 - $Gaudio_Silent_Cycle;
    if Z r0 = 0;
    M[r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_FRAME_COUNT_FIELD] = r0;
    r0 = M[$Gaudio_Silent];
    if Z jump gaudio_no_silent;
    I2 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_INBUFL_FIELD;
    I6 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_INBUFR_FIELD;
    r10 = r8;
    do gaudio_silent_loop;
        r2 = M[I2, 0], r3 = M[I6, 0];
        r2 = r2 ASHIFT -6;
        r3 = r3 ASHIFT -6;
        M[I2, ADDR_PER_WORD] = r2, M[I6, ADDR_PER_WORD] = r3;
    gaudio_silent_loop:

gaudio_no_silent:


    r5 = M[$Gaudio_System_Idx]; //r8 ASHIFT -1;
    r5 = r5 ASHIFT 2;
    M0 = r5;
    I2 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_OUTBUFL_FIELD;
    I3 = I2 + M0;
    I6 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_OUTBUFR_FIELD;
    I7 = I6 + M0; //Q5.27
    r10 = $Gaudio_kSigDelay;
     do gaudio_output_buffer_delay;
     r2 = M[I3,ADDR_PER_WORD], r3 = M[I7,ADDR_PER_WORD];
     M[I2, ADDR_PER_WORD] = r2, M[I6, ADDR_PER_WORD] = r3;
    gaudio_output_buffer_delay:

    r10 = r8;// ASHIFT -3;
    do gaudio_output_buffer_init;
      r2 = 0;
      M[I2, ADDR_PER_WORD] = r2, M[I6, ADDR_PER_WORD] = r2;
    gaudio_output_buffer_init:

    M[$Gaudio_System_Idx] = r8;
   /***************************************************
    *                  Reverberator                   *
    ***************************************************/
   // Reverb - Delay Filter L1
   r5 = M[$Gaudio_Delay1_Idx_L];
   r2 = r5 ASHIFT 2;
   M0 = r2;
   I3 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_DELAY1_OUT_L_FIELD;
   I2 = I3 + M0;
   I6 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_INBUFL_FIELD;
   I7 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_PROC1_FIELD;
   r10 = r8;// ASHIFT -3;
   do gaudio_left_delay1_proc;
     r3 = M[I7,0], r2 = M[I2,0];
     r3 = r3 + r2;
     M[I7,ADDR_PER_WORD] = r2;
     r4 = M[I6,ADDR_PER_WORD];
     rMAC = r2 * $Gaudio_Delay_kGain;
     r3 = rMAC;
     r4 = r4 + r3;
     M[I2,ADDR_PER_WORD] = r4;
     r5 = r5 + 1;
     r2 = $Gaudio_Delay1_L-r5;
     if NZ jump gaudio_rever_delay1_L;
        I2 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_DELAY1_OUT_L_FIELD;
        r5 = 0;
     gaudio_rever_delay1_L:
     nop;
     nop;
   gaudio_left_delay1_proc:
   M[$Gaudio_Delay1_Idx_L] = r5;

   // Reverb - Delay Filter L2
   r5 = M[$Gaudio_Delay2_Idx_L];
   r2 = r5 ASHIFT 2;
   M0 = r2;
   I3 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_DELAY2_OUT_L_FIELD;
   I2 = I3 + M0;
   I6 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_INBUFL_FIELD;
   I7 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_PROC1_FIELD;
   r10 = r8;// ASHIFT -3;
   do gaudio_left_delay2_proc;
     r3 = M[I7,0], r2 = M[I2,0];
     r3 = r3 + r2;
     M[I7,ADDR_PER_WORD] = r3;
     r4 = M[I6,ADDR_PER_WORD];
     rMAC = r2 * $Gaudio_Delay_kGain;
     r3 = rMAC;
     r4 = r4 + r3;
     M[I2,ADDR_PER_WORD] = r4;
     r5 = r5 + 1;
     r2 = $Gaudio_Delay2_L-r5;
     if NZ jump gaudio_rever_delay2_L;
        I2 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_DELAY2_OUT_L_FIELD;
        r5 = 0;
     gaudio_rever_delay2_L:
     nop;
     nop;
   gaudio_left_delay2_proc:
   M[$Gaudio_Delay2_Idx_L] = r5;

   // Reverb - Delay Filter L3
   r5 = M[$Gaudio_Delay3_Idx_L];
   r2 = r5 ASHIFT 2;
   M0 = r2;
   I3 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_DELAY3_OUT_L_FIELD;
   I2 = I3 + M0;
   I6 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_INBUFL_FIELD;
   I7 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_PROC1_FIELD;
   r10 = r8;// ASHIFT -3;
   do gaudio_left_delay3_proc;
     r3 = M[I7,0], r2 = M[I2,0];
     r3 = r3 + r2;
     M[I7,ADDR_PER_WORD] = r3;
     r4 = M[I6,ADDR_PER_WORD];
     rMAC = r2 * $Gaudio_Delay_kGain;
     r3 = rMAC;
     r4 = r4 + r3;
     M[I2,ADDR_PER_WORD] = r4;
     r5 = r5 + 1;
     r2 = $Gaudio_Delay3_L-r5;
     if NZ jump gaudio_rever_delay3_L;
        I2 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_DELAY3_OUT_L_FIELD;
        r5 = 0;
     gaudio_rever_delay3_L:
     nop;
     nop;
   gaudio_left_delay3_proc:
   M[$Gaudio_Delay3_Idx_L] = r5;

   // Reverb - Delay Filter L4
   r5 = M[$Gaudio_Delay4_Idx_L];
   r2 = r5 ASHIFT 2;
   M0 = r2;
   I3 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_DELAY4_OUT_L_FIELD;
   I2 = I3 + M0;
   I6 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_INBUFL_FIELD;
   I7 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_PROC1_FIELD;
   r10 = r8;// ASHIFT -3;
   do gaudio_left_delay4_proc;
     r3 = M[I7,0], r2 = M[I2,0];
     r3 = r3 + r2;
     M[I7,ADDR_PER_WORD] = r3;
     r4 = M[I6,ADDR_PER_WORD];
     rMAC = r2 * $Gaudio_Delay_kGain;
     r3 = rMAC;
     r4 = r4 + r3;
     M[I2,ADDR_PER_WORD] = r4;
     r5 = r5 + 1;
     r2 = $Gaudio_Delay4_L-r5;
     if NZ jump gaudio_rever_delay4_L;
        I2 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_DELAY4_OUT_L_FIELD;
        r5 = 0;
     gaudio_rever_delay4_L:
     nop;
     nop;
   gaudio_left_delay4_proc:
   M[$Gaudio_Delay4_Idx_L] = r5;

   // Reverb - Allpass Filter L1
   r5 = M[$Gaudio_Allpass1_Idx_L];
   r2 = r5 ASHIFT 2;
   M0 = r2;
   I7 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_ALLPASS1_OUT_L_FIELD;
   I2 = I7 + M0;
   I7 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_ALLPASS1_IN_L_FIELD;
   I3 = I7 + M0;
   I6 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_PROC1_FIELD;
   r10 = r8;// ASHIFT -3;
   do gaudio_left_allpass1_proc;
     r2 = M[I6,0], r3 = M[I2,0];
     r3 = r2 - r3;
     rMAC = r3 * $Gaudio_Allpass1_kGain;
     r3 = rMAC;
     r4 = M[I3,0];
     r4 = r4 + r3;
     M[I6,ADDR_PER_WORD] = r4, M[I2,ADDR_PER_WORD] = r4;
     M[I3,ADDR_PER_WORD] = r2;
     r5 = r5 + 1;
     r2 = $Gaudio_Allpass1_L-r5;
     if NZ jump gaudio_rever_allpass1_L;
        I2 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_ALLPASS1_OUT_L_FIELD;
        I3 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_ALLPASS1_IN_L_FIELD;
        r5 = 0;
     gaudio_rever_allpass1_L:
     nop;
     nop;
   gaudio_left_allpass1_proc:
   M[$Gaudio_Allpass1_Idx_L] = r5;

   // Reverb - Allpass Filter L2
   r5 = M[$Gaudio_Allpass2_Idx_L];
   r2 = r5 ASHIFT 2;
   M0 = r2;
   I7 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_ALLPASS2_OUT_L_FIELD;
   I2 = I7 + M0;
   I7 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_ALLPASS2_IN_L_FIELD;
   I3 = I7 + M0;
   I6 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_PROC1_FIELD;
   r10 = r8;// ASHIFT -3;
   do gaudio_left_allpass2_proc;
     r2 = M[I6,0], r3 = M[I2,0];
     r3 = r2 - r3;
     rMAC = r3 * $Gaudio_Allpass2_kGain;
     r3 = rMAC;
     r4 = M[I3,0];
     r4 = r4 + r3;
     M[I6,ADDR_PER_WORD] = r4, M[I2,ADDR_PER_WORD] = r4;
     M[I3,ADDR_PER_WORD] = r2;
     r5 = r5 + 1;
     r2 = $Gaudio_Allpass2_L-r5;
     if NZ jump gaudio_rever_allpass2_L;
        I2 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_ALLPASS2_OUT_L_FIELD;
        I3 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_ALLPASS2_IN_L_FIELD;
        r5 = 0;
     gaudio_rever_allpass2_L:
     nop;
     nop;
   gaudio_left_allpass2_proc:
   M[$Gaudio_Allpass2_Idx_L] = r5;


   r10 = r8;// ASHIFT -3;
   I2 = $Gaudio_reverb_iir_delayL;
   I3 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_PROC1_FIELD;
   I6 = $Gaudio_iirC48;
   M0 = -3 * ADDR_PER_WORD;
   M1 = -4 * ADDR_PER_WORD;
   do gaudio_reverb_biquad_loop_L;
     r2 = M[I2, ADDR_PER_WORD], r3 = M[I6, ADDR_PER_WORD];
     rMAC = r2 * r3, r4 = M[I2, ADDR_PER_WORD], r3 = M[I6, ADDR_PER_WORD];
     rMAC = rMAC + r4 * r3;
     r5 = M[I3, 0], r3 = M[I6, ADDR_PER_WORD];
     rMAC = rMAC + r5 * r3, r2 = M[I2, ADDR_PER_WORD], r3 = M[I6, ADDR_PER_WORD];
     rMAC = rMAC - r2 * r3, r2 = M[I2, M0], r3 = M[I6, M1];
     rMAC = rMAC - r2 * r3, M[I2,ADDR_PER_WORD] = r4;
     r3 = rMAC ASHIFT 1;
     M[I3,ADDR_PER_WORD] = r3;
     M[I2,ADDR_PER_WORD] = r5;
     M[I2,ADDR_PER_WORD] = r2;
     M[I2,M0] = r3;
   gaudio_reverb_biquad_loop_L:


   r5 = M[$Gaudio_Delay1_Idx_R];
   r2 = r5 ASHIFT 2;
   M0 = r2;
   I3 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_DELAY1_OUT_R_FIELD;
   I2 = I3 + M0;
   I6 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_INBUFR_FIELD;
   I7 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_PROC2_FIELD;
   r10 = r8;// ASHIFT -3;
   do gaudio_right_delay1_proc;
     r3 = M[I7,0], r2 = M[I2,0];
     r3 = r3 + r2;
     M[I7,ADDR_PER_WORD] = r3;
     r4 = M[I6,ADDR_PER_WORD];
     rMAC = r2 * $Gaudio_Delay_kGain;
     r3 = rMAC;
     r4 = r4 + r3;
     M[I2,ADDR_PER_WORD] = r4;
     r5 = r5 + 1;
     r2 = $Gaudio_Delay1_R-r5;
     if NZ jump gaudio_rever_delay1_R;
        I2 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_DELAY1_OUT_R_FIELD;
        r5 = 0;
     gaudio_rever_delay1_R:
     nop;
     nop;
   gaudio_right_delay1_proc:
   M[$Gaudio_Delay1_Idx_R] = r5;

   r5 = M[$Gaudio_Delay2_Idx_R];
   r2 = r5 ASHIFT 2;
   M0 = r2;
   I3 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_DELAY2_OUT_R_FIELD;
   I2 = I3 + M0;
   I6 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_INBUFR_FIELD;
   I7 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_PROC2_FIELD;
   r10 = r8;// ASHIFT -3;
   do gaudio_right_delay2_proc;
     r3 = M[I7,0], r2 = M[I2,0];
     r3 = r3 + r2;
     M[I7,ADDR_PER_WORD] = r3;
     r4 = M[I6,ADDR_PER_WORD];
     rMAC = r2 * $Gaudio_Delay_kGain;
     r3 = rMAC;
     r4 = r4 + r3;
     M[I2,ADDR_PER_WORD] = r4;
     r5 = r5 + 1;
     r2 = $Gaudio_Delay2_R-r5;
     if NZ jump gaudio_rever_delay2_R;
        I2 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_DELAY2_OUT_R_FIELD;
        r5 = 0;
     gaudio_rever_delay2_R:
     nop;
     nop;
   gaudio_right_delay2_proc:
   M[$Gaudio_Delay2_Idx_R] = r5;

   r5 = M[$Gaudio_Delay3_Idx_R];
   r2 = r5 ASHIFT 2;
   M0 = r2;
   I3 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_DELAY3_OUT_R_FIELD;
   I2 = I3 + M0;
   I6 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_INBUFR_FIELD;
   I7 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_PROC2_FIELD;
   r10 = r8;// ASHIFT -3;
   do gaudio_right_delay3_proc;
     r3 = M[I7,0], r2 = M[I2,0];
     r3 = r3 + r2;
     M[I7,ADDR_PER_WORD] = r3;
     r4 = M[I6,ADDR_PER_WORD];
     rMAC = r2 * $Gaudio_Delay_kGain;
     r3 = rMAC;
     r4 = r4 + r3;
     M[I2,ADDR_PER_WORD] = r4;
     r5 = r5 + 1;
     r2 = $Gaudio_Delay3_R-r5;
     if NZ jump gaudio_rever_delay3_R;
        I2 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_DELAY3_OUT_R_FIELD;
        r5 = 0;
     gaudio_rever_delay3_R:
     nop;
     nop;
   gaudio_right_delay3_proc:
   M[$Gaudio_Delay3_Idx_R] = r5;


   r5 = M[$Gaudio_Delay4_Idx_R];
   r2 = r5 ASHIFT 2;
   M0 = r2;
   I3 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_DELAY4_OUT_R_FIELD;
   I2 = I3 + M0;
   I6 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_INBUFR_FIELD;
   I7 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_PROC2_FIELD;
   r10 = r8;// ASHIFT -3;
   do gaudio_right_delay4_proc;
     r3 = M[I7,0], r2 = M[I2,0];
     r3 = r3 + r2;
     M[I7,ADDR_PER_WORD] = r3;
     r4 = M[I6,ADDR_PER_WORD];
     rMAC = r2 * $Gaudio_Delay_kGain;
     r3 = rMAC;
     r4 = r4 + r3;
     M[I2,ADDR_PER_WORD] = r4;
     r5 = r5 + 1;
     r2 = $Gaudio_Delay4_R-r5;
     if NZ jump gaudio_rever_delay4_R;
        I2 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_DELAY4_OUT_R_FIELD;
        r5 = 0;
     gaudio_rever_delay4_R:
     nop;
     nop;
   gaudio_right_delay4_proc:
   M[$Gaudio_Delay4_Idx_R] = r5;


   // Reverb - Allpass Filter R1
   r5 = M[$Gaudio_Allpass1_Idx_R];
   r2 = r5 ASHIFT 2;
   M0 = r2;
   I7 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_ALLPASS1_OUT_R_FIELD;
   I2 = I7 + M0;
   I7 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_ALLPASS1_IN_R_FIELD;
   I3 = I7 + M0;
   I6 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_PROC2_FIELD;
   r10 = r8;// ASHIFT -3;
   do gaudio_right_allpass1_proc;
     r2 = M[I6,0], r3 = M[I2,0];
     r3 = r2 - r3;
     rMAC = r3 * $Gaudio_Allpass1_kGain;
     r3 = rMAC;
     r4 = M[I3,0];
     r4 = r4 + r3;
     M[I6,ADDR_PER_WORD] = r4, M[I2,ADDR_PER_WORD] = r4;
     M[I3,ADDR_PER_WORD] = r2;
     r5 = r5 + 1;
     r2 = $Gaudio_Allpass1_R-r5;
     if NZ jump gaudio_rever_allpass1_R;
        I2 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_ALLPASS1_OUT_R_FIELD;
        I3 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_ALLPASS1_IN_R_FIELD;
        r5 = 0;
     gaudio_rever_allpass1_R:
     nop;
     nop;
   gaudio_right_allpass1_proc:
   M[$Gaudio_Allpass1_Idx_R] = r5;

   // Reverb - Allpass Filter R2
   r5 = M[$Gaudio_Allpass2_Idx_R];
   r2 = r5 ASHIFT 2;
   M0 = r2;
   I7 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_ALLPASS2_OUT_R_FIELD;
   I2 = I7 + M0;
   I7 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_ALLPASS2_IN_R_FIELD;
   I3 = I7 + M0;
   I6 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_PROC2_FIELD;
   r10 = r8;// ASHIFT -3;
   do gaudio_right_allpass2_proc;
     r2 = M[I6,0], r3 = M[I2,0];
     r3 = r2 - r3;
     rMAC = r3 * $Gaudio_Allpass2_kGain;
     r3 = rMAC;
     r4 = M[I3,0];
     r4 = r4 + r3;
     M[I6,ADDR_PER_WORD] = r4, M[I2,ADDR_PER_WORD] = r4;
     M[I3,ADDR_PER_WORD] = r2;
     r5 = r5 + 1;
     r2 = $Gaudio_Allpass2_R-r5;
     if NZ jump gaudio_rever_allpass2_R;
        I2 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_ALLPASS2_OUT_R_FIELD;
        I3 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_ALLPASS2_IN_R_FIELD;
        r5 = 0;
     gaudio_rever_allpass2_R:
     nop;
     nop;
   gaudio_right_allpass2_proc:
   M[$Gaudio_Allpass2_Idx_R] = r5;

   r10 = r8;// ASHIFT -3;
   I2 = $Gaudio_reverb_iir_delayR;
   I3 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_PROC2_FIELD;
   I6 = $Gaudio_iirC48;
   M0 = -3 * ADDR_PER_WORD;
   M1 = -4 * ADDR_PER_WORD;
   do gaudio_reverb_biquad_loop_R;
     r2 = M[I2, ADDR_PER_WORD], r3 = M[I6, ADDR_PER_WORD];
     rMAC = r2 * r3, r4 = M[I2, ADDR_PER_WORD], r3 = M[I6, ADDR_PER_WORD];
     rMAC = rMAC + r4 * r3;
     r5 = M[I3, 0], r3 = M[I6, ADDR_PER_WORD];
     rMAC = rMAC + r5 * r3, r2 = M[I2, ADDR_PER_WORD], r3 = M[I6, ADDR_PER_WORD];
     rMAC = rMAC - r2 * r3, r2 = M[I2, M0], r3 = M[I6, M1];
     rMAC = rMAC - r2 * r3, M[I2,ADDR_PER_WORD] = r4;
     r3 = rMAC ASHIFT 1;
     M[I3,ADDR_PER_WORD] = r3;
     M[I2,ADDR_PER_WORD] = r5;
     M[I2,ADDR_PER_WORD] = r2;
     M[I2,M0] = r3;
   gaudio_reverb_biquad_loop_R:


   I2 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_OUTBUFL_FIELD;
   I3 = I2 + $Gaudio_kSigDelay * ADDR_PER_WORD;
   I2 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_OUTBUFR_FIELD;
   I7 = I2 + $Gaudio_kSigDelay * ADDR_PER_WORD;
   I2 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_PROC1_FIELD;
   I6 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_PROC2_FIELD;
   r10 = r8;// ASHIFT -3;

   do gaudio_reverb_out;
      r2 = M[I2, ADDR_PER_WORD], r3 = M[I6, ADDR_PER_WORD];
//      r2 = r2 ASHIFT -3;
//      r3 = r3 ASHIFT -3;
      rMAC = r2 * $Gaudio_Reverb_kGain;
      r2 = rMAC;
      rMAC = r3 * $Gaudio_Reverb_kGain;
      r3 = rMAC;
      r4 = M[I3, 0], r5 = M[I7, 0];
      r4 = r4 + r2;
      r5 = r5 + r3;
      M[I3, ADDR_PER_WORD] = r4;
      M[I7, ADDR_PER_WORD] = r5;
   gaudio_reverb_out:

    /***************************************************
    *                Cross Rendering                  *
    ***************************************************/

    r10 = r8;// ASHIFT -3;
    I2 = $Gaudio_cross_iir_delayL0;
    I3 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_INBUFL_FIELD;
    I6 = $Gaudio_cross_iir0;
    I7 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_PROC1_FIELD;
    M0 = -3 * ADDR_PER_WORD;
    M1 = -4 * ADDR_PER_WORD;
    do gaudio_cross_biquad_L0;
     r2 = M[I2, ADDR_PER_WORD], r3 = M[I6, ADDR_PER_WORD];
     rMAC = r2 * r3, r4 = M[I2, ADDR_PER_WORD], r3 = M[I6, ADDR_PER_WORD];
     rMAC = rMAC + r4 * r3;
     r5 = M[I3, ADDR_PER_WORD], r3 = M[I6, ADDR_PER_WORD];
     rMAC = rMAC + r5 * r3, r2 = M[I2, ADDR_PER_WORD], r3 = M[I6, ADDR_PER_WORD];
     rMAC = rMAC - r2 * r3, r2 = M[I2, M0], r3 = M[I6, M1];
     rMAC = rMAC - r2 * r3, M[I2,ADDR_PER_WORD] = r4;
     r3 = rMAC ASHIFT 1;
     M[I7,ADDR_PER_WORD] = r3;
     M[I2,ADDR_PER_WORD] = r5;
     M[I2,ADDR_PER_WORD] = r2;
     M[I2,M0] = r3;
   gaudio_cross_biquad_L0:

    r10 = r8;// ASHIFT -3;
    I2 = $Gaudio_cross_iir_delayL1;
    I3 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_PROC1_FIELD;
    I6 = $Gaudio_cross_iir1;
    I7 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_OUTBUFL_FIELD;
    M0 = -3 * ADDR_PER_WORD;
    M1 = -4 * ADDR_PER_WORD;
    do gaudio_cross_biquad_L1;
     r2 = M[I2, ADDR_PER_WORD], r3 = M[I6, ADDR_PER_WORD];
     rMAC = r2 * r3, r4 = M[I2, ADDR_PER_WORD], r3 = M[I6, ADDR_PER_WORD];
     rMAC = rMAC + r4 * r3;
     r5 = M[I3, 0], r3 = M[I6, ADDR_PER_WORD];
     rMAC = rMAC + r5 * r3, r2 = M[I2, ADDR_PER_WORD], r3 = M[I6, ADDR_PER_WORD];
     rMAC = rMAC - r2 * r3, r2 = M[I2, M0], r3 = M[I6, M1];
     rMAC = rMAC - r2 * r3, M[I2,ADDR_PER_WORD] = r4;
     r3 = rMAC ASHIFT 1;
     r4 = M[I7,0];
     r4 = r4 + r3;
     M[I3,ADDR_PER_WORD] = r3;
     M[I7,ADDR_PER_WORD] = r4;
     M[I2,ADDR_PER_WORD] = r5;
     M[I2,ADDR_PER_WORD] = r2;
     M[I2,M0] = r3;
    gaudio_cross_biquad_L1:

   r10 = r8;// ASHIFT -3;
   I2 = $Gaudio_cross_iir_delayR0;
   I3 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_INBUFR_FIELD;
   I6 = $Gaudio_cross_iir0;
   I7 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_PROC2_FIELD;
   M0 = -3 * ADDR_PER_WORD;
   M1 = -4 * ADDR_PER_WORD;
   do gaudio_cross_biquad_R0;
     r2 = M[I2, ADDR_PER_WORD], r3 = M[I6, ADDR_PER_WORD];
     rMAC = r2 * r3, r4 = M[I2, ADDR_PER_WORD], r3 = M[I6, ADDR_PER_WORD];
     rMAC = rMAC + r4 * r3;
     r5 = M[I3, ADDR_PER_WORD], r3 = M[I6, ADDR_PER_WORD];
     rMAC = rMAC + r5 * r3, r2 = M[I2, ADDR_PER_WORD], r3 = M[I6, ADDR_PER_WORD];
     rMAC = rMAC - r2 * r3, r2 = M[I2, M0], r3 = M[I6, M1];
     rMAC = rMAC - r2 * r3, M[I2,ADDR_PER_WORD] = r4;
     r3 = rMAC ASHIFT 1;
     M[I7,ADDR_PER_WORD] = r3;
     M[I2,ADDR_PER_WORD] = r5;
     M[I2,ADDR_PER_WORD] = r2;
     M[I2,M0] = r3;
   gaudio_cross_biquad_R0:

   r10 = r8;// ASHIFT -3;
   I2 = $Gaudio_cross_iir_delayR1;
   I3 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_PROC2_FIELD;
   I6 = $Gaudio_cross_iir1;
   I7 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_OUTBUFR_FIELD;
   M0 = -3 * ADDR_PER_WORD;
   M1 = -4 * ADDR_PER_WORD;
   do gaudio_cross_biquad_R1;
     r2 = M[I2, ADDR_PER_WORD], r3 = M[I6, ADDR_PER_WORD];
     rMAC = r2 * r3, r4 = M[I2, ADDR_PER_WORD], r3 = M[I6, ADDR_PER_WORD];
     rMAC = rMAC + r4 * r3;
     r5 = M[I3, 0], r3 = M[I6, ADDR_PER_WORD];
     rMAC = rMAC + r5 * r3, r2 = M[I2, ADDR_PER_WORD], r3 = M[I6, ADDR_PER_WORD];
     rMAC = rMAC - r2 * r3, r2 = M[I2, M0], r3 = M[I6, M1];
     rMAC = rMAC - r2 * r3, M[I2,ADDR_PER_WORD] = r4;
     r3 = rMAC ASHIFT 1;
     r4 = M[I7,0];
     r4 = r4 + r3;
     M[I3,ADDR_PER_WORD] = r3;
     M[I7,ADDR_PER_WORD] = r4;
     M[I2,ADDR_PER_WORD] = r5;
     M[I2,ADDR_PER_WORD] = r2;
     M[I2,M0] = r3;
   gaudio_cross_biquad_R1:

   r10 = r8;// ASHIFT -3;
   I2 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_OUTBUFL_FIELD;
   I3 = I2 + $Gaudio_Cross_Delay*ADDR_PER_WORD;
   I2 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_OUTBUFR_FIELD;
   I7 = I2 + $Gaudio_Cross_Delay*ADDR_PER_WORD;
   I2 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_PROC1_FIELD;
   I6 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_PROC2_FIELD;
   do gaudio_cross_synthesis;
      r2 = M[I2, ADDR_PER_WORD], r3 = M[I6, ADDR_PER_WORD];
      rMAC = r2 * $Gaudio_Cross_Gain;
      r2 = rMAC;
      rMAC = r3 * $Gaudio_Cross_Gain;
      r3 = rMAC;
      r4 = M[I3, 0], r5 = M[I7, 0];
      r4 = r4 + r3;
      r5 = r5 + r2;
      M[I3, ADDR_PER_WORD] = r4, M[I7, ADDR_PER_WORD] = r5;
   gaudio_cross_synthesis:

   /***************************************************
    *                     Limiter                     *
    ***************************************************/

   I2 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_LIMITER_PROCL_FIELD;
   I3 = I2 + $Gaudio_Limiter_Lookahead * ADDR_PER_WORD;
   I2 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_LIMITER_PROCR_FIELD;
   I7 = I2 + $Gaudio_Limiter_Lookahead * ADDR_PER_WORD;
   I2 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_OUTBUFL_FIELD;
   I6 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_OUTBUFR_FIELD;
   r10 = r8;// ASHIFT -3;
   do gaudio_limiter_input_loop;
     r2 = M[I2, ADDR_PER_WORD], r3 = M[I6, ADDR_PER_WORD];
     M[I3, ADDR_PER_WORD] = r2, M[I7, ADDR_PER_WORD] = r3;
   gaudio_limiter_input_loop:

   I2 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_LIMITER_PROCL_FIELD;
   I6 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_LIMITER_PROCR_FIELD;
   I3 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_PROC1_FIELD;
   r7 = r8;// ASHIFT -3;
   r10 = r7 + $Gaudio_Limiter_Lookahead;
   do gaudio_limiter_abs_loop;
     r2 = M[I2, ADDR_PER_WORD], r3 = M[I6, ADDR_PER_WORD];
     r2 = ABS r2;
     r3 = ABS r3;
     r2 = MAX r3;
     M[I3, ADDR_PER_WORD] = r3;
   gaudio_limiter_abs_loop:

   I2 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_LIMITER_PROCL_FIELD;
   I6 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_LIMITER_PROCR_FIELD;
   I3 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_PROC1_FIELD;
   r10 = r8;// ASHIFT -3;
   do gaudio_limiter_gain_loop;
     push r10;
     push DoLoopStart;
     push DoLoopEnd;
     r10 = $Gaudio_Limiter_duration;
     r2 = 0;
     do gaudio_get_max;
       r3 = M[I3, ADDR_PER_WORD];
       r2 = MAX r3;
     gaudio_get_max:
     I3 = I3 - (($Gaudio_Limiter_Lookahead - 1) * ADDR_PER_WORD);
     pop DoLoopEnd;
     pop DoLoopStart;
     pop r10;

     r1 = $Gaudio_kThreshold;
     r0 = r1 - r2;
     if GT jump gt_one;
         rMAC = r1 * $Gaudio_kOneMax;
         rMAC = rMAC ASHIFT -1;
         r3 = r2 + 1;
         Div = rMAC/r3;
         r1 = DivResult;
         jump gt_limit;
     gt_one:
        r1 = $Gaudio_kOneMax;
     gt_limit:
     r3 = M[$Gaudio_Limiter_GainPrev];
     r4 = M[$Gaudio_Limiter_SmoothGainPrev];

     r0 = r1 - r4;
     if GT jump gt_smoothing;
        rMAC = r2 * $Gaudio_kSmoothing;
        rMAC = rMAC - r4 * $Gaudio_kSmoothing_Inv;
        r5 = rMAC ASHIFT 1;
        r1 = MIN r5;
     gt_smoothing:

     r0 = r1 - r4;
     if GT jump gt_at;
        rMAC = r4 * $Gaudio_kAttack;
        rMAC = rMAC + r1 * $Gaudio_kAttack_Inv;
        r5 = rMAC;
        jump gt_rt;
     gt_at:
        rMAC = r4 * $Gaudio_kRelease;
        rMAC = rMAC + r1 * $Gaudio_kRelease_Inv;
        r5 = rMAC;
     gt_rt:

     M[$Gaudio_Limiter_GainPrev] = r1;
     M[$Gaudio_Limiter_SmoothGainPrev] = r5;

     r2 = M[I2, ADDR_PER_WORD], r3 = M[I6, ADDR_PER_WORD];
     rMAC = r2 * r5;
     r2 = rMAC ASHIFT 6;
     M[I4, ADDR_PER_WORD] = r2;
     rMAC = r3 * r5;
     r3 = rMAC ASHIFT 6;
     M[I5, ADDR_PER_WORD] = r3;
   gaudio_limiter_gain_loop:


   r5 = r8 ASHIFT 2;
   M0 = r5;
   I3 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_LIMITER_PROCL_FIELD;
   I2 = I3 + M0;
   I7 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_LIMITER_PROCR_FIELD;
   I6 = I7 + M0;
   r10 = $Gaudio_Limiter_Lookahead;
   do gaudio_limiter_delay_loop;
     r2 = M[I2, ADDR_PER_WORD], r3 = M[I6, ADDR_PER_WORD];
     M[I3, ADDR_PER_WORD] = r2, M[I7, ADDR_PER_WORD] = r3;
   gaudio_limiter_delay_loop:

/*
   I2 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_OUTBUFL_FIELD; //Q5.27
   I6 = r9 + $GSU_TRIAL_BASIC_struct.GSU_TRIAL_BASIC_OP_DATA_struct.GAUDIO_OUTBUFR_FIELD; //Q5.27
   r10 = r8;// ASHIFT -3;
   do gaudio_headroom_setting_out;
      r2 = M[I2, ADDR_PER_WORD]; //Q1.31
      r3 = M[I6, ADDR_PER_WORD]; //Q1.31
      r2 = r2 ASHIFT 6;
      r3 = r3 ASHIFT 6;
      M[I4, ADDR_PER_WORD] = r2; //Q5.27
      M[I5, ADDR_PER_WORD] = r3; //Q5.27
   gaudio_headroom_setting_out:
     nop;
     nop;
*/
//     popm <r2, r3>;
/*     pop DoLoopEnd;
     pop DoLoopStart;
     pop r10;
     nop;
     nop;
   gaudio_iteration_loop:
*/

// =============================================================================

GSU_TRIAL_BASIC_GO_PROCESS_OUT:

popm <M0, M1>;
popm <I2, I3, I6, I7>;
popm <r2, r3, r4, r5, r6, r7, r8, r9>;

// =============================================================================
// =============================================================================
// =============================================================================
// =============================================================================
// =============================================================================






   // update read address in input buffer
   r0 = M[r3 + $GSU_TRIAL_BASIC_struct.PASSTHROUGH_CHANNEL_struct.IP_BUFFER_FIELD], r2 = M[I0, M2];
   r1 = I0;
   call $cbuffer.set_read_address;

   // update write address in output buffer
   r0 = M[r3 + $GSU_TRIAL_BASIC_struct.PASSTHROUGH_CHANNEL_struct.OP_BUFFER_FIELD];
   r1 = I4;
   call $cbuffer.set_write_address;

   // update read address in input buffer
   r0 = M[r5 + $GSU_TRIAL_BASIC_struct.PASSTHROUGH_CHANNEL_struct.IP_BUFFER_FIELD], r4 = M[I1, M2];
   r1 = I1;
   call $cbuffer.set_read_address;

   // update write address in output buffer
   r0 = M[r5 + $GSU_TRIAL_BASIC_struct.PASSTHROUGH_CHANNEL_struct.OP_BUFFER_FIELD];
   r1 = I5;
   call $cbuffer.set_write_address;


   pop r1;  // take samples to process back off the stack;

   popm <r5, r6, r9>;
   popm <B0, B1, B4, B5>;
   popm <I1, I5, L1, L5>;
   popm <I0, I4, L0, L4>;
   pop rLink;

   rts;

.ENDMODULE;


