/****************************************************************************
 * Copyright (c) 2013 - 2017 Qualcomm Technologies International, Ltd.
****************************************************************************/
/**
 * \defgroup GSU_TRIAL_BASIC
 * \file  GSU_TRIAL_BASIC.h
 * \ingroup capabilities
 *
 * Basic passthrough operator public header file. <br>
 *
 */

#ifndef _GSU_TRIAL_BASIC_OP_H_
#define _GSU_TRIAL_BASIC_OP_H_

#include "opmgr/opmgr_for_ops.h"

/****************************************************************************
Public Variable Definitions
*/
/** The capability data structure for basic passthrough */
extern const CAPABILITY_DATA GSU_TRIAL_BASIC_cap_data;

/** The capability data structure for passthrough with time-to-play */
extern const CAPABILITY_DATA ttp_passthrough_cap_data;

#endif /* _GSU_TRIAL_BASIC_OP_H_ */
