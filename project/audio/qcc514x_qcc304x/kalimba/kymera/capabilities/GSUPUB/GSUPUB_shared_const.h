/****************************************************************************
 * Copyright (c) 2015 - 2017 Qualcomm Technologies International, Ltd.
****************************************************************************/
/**
 * \defgroup GSUPUB
 * \file  GSUPUB_shared_const.h
 * \ingroup capabilities
 *
 * GSUPUB operator header file of constant values used by asm and C <br>
 *
 */

#ifndef _GSUPUB_SHARED_CONST_H_
#define _GSUPUB_SHARED_CONST_H_

/** The maximum number of channels supported by the capability */
#define MAX_CHANS 8

#endif /* _GSUPUB_SHARED_CONST_H_ */
