/****************************************************************************
 * Copyright (c) 2013 - 2017 Qualcomm Technologies International, Ltd.
****************************************************************************/
/**
 * \defgroup GSUPUB
 * \file  GSUPUB.h
 * \ingroup capabilities
 *
 * GSUPUB operator public header file. <br>
 *
 */

#ifndef _GSUPUB_OP_H_
#define _GSUPUB_OP_H_

#include "opmgr/opmgr_for_ops.h"

/****************************************************************************
Public Variable Definitions
*/
/** The capability data structure for GSUPUB */
extern const CAPABILITY_DATA GSUPUB_cap_data;

/** The capability data structure for passthrough with time-to-play */
extern const CAPABILITY_DATA ttp_passthrough_cap_data;


// GSUPUB_ADD_START
extern bool GSUPUB_opmsg_set_mode(OPERATOR_DATA *op_data, void *message_data, unsigned *resp_length, OP_OPMSG_RSP_PAYLOAD **resp_data);
extern bool GSUPUB_opmsg_set_volume(OPERATOR_DATA *op_data, void *message_data, unsigned *resp_length, OP_OPMSG_RSP_PAYLOAD **resp_data);
// GSUPUB_ADD_END




#endif /* _GSUPUB_OP_H_ */
