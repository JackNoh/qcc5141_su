// -----------------------------------------------------------------------------
// Copyright (c) 2019                  Qualcomm Technologies International, Ltd.
//
#ifndef __GSUPUB_GEN_C_H__
#define __GSUPUB_GEN_C_H__

#ifndef ParamType
typedef unsigned ParamType;
#endif

// CodeBase IDs
#define GSUPUB_BPT_CAP_ID	0x0001
#define GSUPUB_BPT_ALT_CAP_ID_0	0x4002
#define GSUPUB_BPT_SAMPLE_RATE	0
#define GSUPUB_BPT_VERSION_MAJOR	1

#define GSUPUB_TTP_CAP_ID	0x003C
#define GSUPUB_TTP_ALT_CAP_ID_0	0x4067
#define GSUPUB_TTP_SAMPLE_RATE	0
#define GSUPUB_TTP_VERSION_MAJOR	1

// Constant Definitions


// Runtime Config Parameter Bitfields
#define GSUPUB_CONFIG_PM_BYPASS		0x00000001


// System Mode
#define GSUPUB_SYSMODE_STATIC 		0
#define GSUPUB_SYSMODE_STANDBY		1
#define GSUPUB_SYSMODE_MONITOR		2
#define GSUPUB_SYSMODE_MAX_MODES		3

// System Control
#define GSUPUB_CONTROL_MODE_OVERRIDE		0x2000

// Statistics Block
typedef struct _tag_GSUPUB_STATISTICS
{
	ParamType OFFSET_CUR_MODE_OFFSET;
	ParamType OFFSET_PEAK_LEVEL_1;
	ParamType OFFSET_PEAK_LEVEL_2;
	ParamType OFFSET_PEAK_LEVEL_3;
	ParamType OFFSET_PEAK_LEVEL_4;
	ParamType OFFSET_PEAK_LEVEL_5;
	ParamType OFFSET_PEAK_LEVEL_6;
	ParamType OFFSET_PEAK_LEVEL_7;
	ParamType OFFSET_PEAK_LEVEL_8;
	ParamType OFFSET_OVR_CONTROL;
}GSUPUB_STATISTICS;

typedef GSUPUB_STATISTICS* LP_GSUPUB_STATISTICS;

// Parameters Block
typedef struct _tag_GSUPUB_PARAMETERS
{
	ParamType OFFSET_CONFIG;
	ParamType OFFSET_GAIN;
}GSUPUB_PARAMETERS;

typedef GSUPUB_PARAMETERS* LP_GSUPUB_PARAMETERS;

unsigned *GSUPUB_GetDefaults(unsigned capid);

#endif // __GSUPUB_GEN_C_H__
