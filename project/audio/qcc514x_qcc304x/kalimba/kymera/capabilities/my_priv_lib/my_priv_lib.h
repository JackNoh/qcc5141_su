/****************************************************************************
 * Copyright (c) 2014 - 2016 Qualcomm Technologies International, Ltd 
****************************************************************************/
/**
 * \file  my_priv_lib.h
 *
 * Private library ASM header file. <br>
 *
 */

#ifndef MY_PRIV_LIB_H
#define MY_PRIV_LIB_H

.CONST $MODE 2;

#endif /* MY_PRIV_LIB_H */
