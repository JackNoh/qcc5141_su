#include "portability_macros.h"
#include "stack.h"

#include "my_priv_lib.h"

// *****************************************************************************
// MODULE:
//    $M.my_priv_lib_entry
//
// DESCRIPTION:
//    Example of an entry point function. This could be part of your private library API.
//    Public entry symbols are defined in my_priv_lib.symbols
//
// INPUTS:
//   - Your input registers
//
// OUTPUTS:
//   - Your output registers
//
// TRASHED REGISTERS:
//    C calling convention respected.
//
// NOTES:
//
// *****************************************************************************
.MODULE $M.my_priv_lib_entry;
   .CODESEGMENT PM;
   .MAXIM;

$_my_priv_lib_entry:
    PUSH_ALL_C
    /*
     * TODO Assembly processing code goes here ...
     */
    /* call into your private library code */
    r1 = $MODE;
    call $my_priv_lib_secret;

    POP_ALL_C
    rts;

.ENDMODULE;


// *****************************************************************************
// MODULE:
//    $M.my_priv_lib_secret
//
// DESCRIPTION:
//    Example of a module that gets scrambled. (This is the default behaviour when
//    the name is not included in my_priv_lib.symbols)
//
// INPUTS:
//   - Your input registers
//
// OUTPUTS:
//   - Your output registers
//
// TRASHED REGISTERS:
//    C calling convention respected.
//
// NOTES:
//
// *****************************************************************************
.MODULE $M.my_priv_lib_secret;
   .CODESEGMENT PM;
   .MAXIM;
   .CONST $SECRET_KEY 0xAD54C770;
   .CONST $SECRET_VALUE 0x1A58986F;

$my_priv_lib_secret:

    /*
     * TODO Assembly processing code goes here ...
     */
    Null = r1 - $MODE;
    if NZ jump ret_false;
    r0 = r0 XOR $SECRET_KEY;
    Null = r0 - $SECRET_VALUE;
    if NZ jump ret_false;
    ret_true:
        r0 = 1;
        jump just_ret;
    ret_false:
        r0 = 0;
    just_ret:
        rts;

.ENDMODULE;