/****************************************************************************
 * Copyright (c) 2013 - 2017 Qualcomm Technologies International, Ltd.
****************************************************************************/
/**
 * \defgroup GSU_TRIAL
 * \file  GSU_TRIAL.h
 * \ingroup capabilities
 *
 * GSU_TRIAL operator public header file. <br>
 *
 */

#ifndef _GSU_TRIAL_OP_H_
#define _GSU_TRIAL_OP_H_

#include "opmgr/opmgr_for_ops.h"

/****************************************************************************
Public Variable Definitions
*/
/** The capability data structure for GSU_TRIAL */
extern const CAPABILITY_DATA GSU_TRIAL_cap_data;

/** The capability data structure for passthrough with time-to-play */
extern const CAPABILITY_DATA ttp_passthrough_cap_data;


// GSU_TRIAL_ADD_START
extern bool GSU_TRIAL_opmsg_set_mode(OPERATOR_DATA *op_data, void *message_data, unsigned *resp_length, OP_OPMSG_RSP_PAYLOAD **resp_data);
extern bool GSU_TRIAL_opmsg_set_volume(OPERATOR_DATA *op_data, void *message_data, unsigned *resp_length, OP_OPMSG_RSP_PAYLOAD **resp_data);
// GSU_TRIAL_ADD_END




#endif /* _GSU_TRIAL_OP_H_ */
