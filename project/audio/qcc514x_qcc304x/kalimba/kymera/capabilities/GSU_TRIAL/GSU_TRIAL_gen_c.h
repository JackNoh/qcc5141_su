// -----------------------------------------------------------------------------
// Copyright (c) 2019                  Qualcomm Technologies International, Ltd.
//
#ifndef __GSU_TRIAL_GEN_C_H__
#define __GSU_TRIAL_GEN_C_H__

#ifndef ParamType
typedef unsigned ParamType;
#endif

// CodeBase IDs
#define GSU_TRIAL_BPT_CAP_ID	0x0001
#define GSU_TRIAL_BPT_ALT_CAP_ID_0	0x4002
#define GSU_TRIAL_BPT_SAMPLE_RATE	0
#define GSU_TRIAL_BPT_VERSION_MAJOR	1

#define GSU_TRIAL_TTP_CAP_ID	0x003C
#define GSU_TRIAL_TTP_ALT_CAP_ID_0	0x4067
#define GSU_TRIAL_TTP_SAMPLE_RATE	0
#define GSU_TRIAL_TTP_VERSION_MAJOR	1

// Constant Definitions


// Runtime Config Parameter Bitfields
#define GSU_TRIAL_CONFIG_PM_BYPASS		0x00000001


// System Mode
#define GSU_TRIAL_SYSMODE_STATIC 		0
#define GSU_TRIAL_SYSMODE_STANDBY		1
#define GSU_TRIAL_SYSMODE_MONITOR		2
#define GSU_TRIAL_SYSMODE_MAX_MODES		3

// System Control
#define GSU_TRIAL_CONTROL_MODE_OVERRIDE		0x2000

// Statistics Block
typedef struct _tag_GSU_TRIAL_STATISTICS
{
	ParamType OFFSET_CUR_MODE_OFFSET;
	ParamType OFFSET_PEAK_LEVEL_1;
	ParamType OFFSET_PEAK_LEVEL_2;
	ParamType OFFSET_PEAK_LEVEL_3;
	ParamType OFFSET_PEAK_LEVEL_4;
	ParamType OFFSET_PEAK_LEVEL_5;
	ParamType OFFSET_PEAK_LEVEL_6;
	ParamType OFFSET_PEAK_LEVEL_7;
	ParamType OFFSET_PEAK_LEVEL_8;
	ParamType OFFSET_OVR_CONTROL;
}GSU_TRIAL_STATISTICS;

typedef GSU_TRIAL_STATISTICS* LP_GSU_TRIAL_STATISTICS;

// Parameters Block
typedef struct _tag_GSU_TRIAL_PARAMETERS
{
	ParamType OFFSET_CONFIG;
	ParamType OFFSET_GAIN;
}GSU_TRIAL_PARAMETERS;

typedef GSU_TRIAL_PARAMETERS* LP_GSU_TRIAL_PARAMETERS;

unsigned *GSU_TRIAL_GetDefaults(unsigned capid);

#endif // __GSU_TRIAL_GEN_C_H__
