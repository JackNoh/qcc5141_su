/****************************************************************************
 * Copyright (c) 2015 - 2017 Qualcomm Technologies International, Ltd.
****************************************************************************/
/**
 * \defgroup GSU_TRIAL
 * \file  GSU_TRIAL_shared_const.h
 * \ingroup capabilities
 *
 * GSU_TRIAL operator header file of constant values used by asm and C <br>
 *
 */

#ifndef _GSU_TRIAL_SHARED_CONST_H_
#define _GSU_TRIAL_SHARED_CONST_H_

/** The maximum number of channels supported by the capability */
#define MAX_CHANS 8

#endif /* _GSU_TRIAL_SHARED_CONST_H_ */
