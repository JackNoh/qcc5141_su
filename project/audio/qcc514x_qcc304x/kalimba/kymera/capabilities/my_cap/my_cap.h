/****************************************************************************
 * Copyright (c) 2014 - 2016 Qualcomm Technologies International, Ltd 
****************************************************************************/
/**
 * \file  my_cap.h
 * \ingroup capabilities
 *
 * Stub Capability public header file. <br>
 *
 */

#ifndef MY_CAP_H
#define MY_CAP_H

#include "my_cap_struct.h"

/** The capability data structure for stub capability */
extern const CAPABILITY_DATA my_cap_cap_data;

/** Assembly processing function */
extern void my_cap_proc_func(MY_CAP_OP_DATA *op_data);

/** Assembly private library entry-point function (API) */
extern int my_priv_lib_entry(int num);

#endif /* MY_CAP_H */
