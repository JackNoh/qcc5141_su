/**
 * \file  my_cap_struct.h
 * \ingroup capabilities
 *
 *
 */

#ifndef MY_CAP_STRUCT_H
#define MY_CAP_STRUCT_H

#include "capabilities.h"


/****************************************************************************
Public Type Definitions
*/

typedef struct MY_CAP_OP_DATA{
    /* Define here your capability specific data */

    unsigned my_data;
} MY_CAP_OP_DATA;


#endif /* MY_CAP_H */
