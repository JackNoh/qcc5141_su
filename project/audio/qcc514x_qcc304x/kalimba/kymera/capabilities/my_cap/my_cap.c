/****************************************************************************
 * Copyright (c) 2014 - 2018 Qualcomm Technologies International, Ltd 
****************************************************************************/
/**
 * \file  my_cap.c
 * \ingroup  capabilities
 *
 *  A Stub implementation of a Capability that can be built and communicated
 *  with. This is provided to accelerate the development of new capabilities.
 *
 */

#include "capabilities.h"
#include "my_cap.h"

/****************************************************************************
Private Function Definitions
*/
static void my_cap_process_data(OPERATOR_DATA*, TOUCHED_TERMINALS*);

/****************************************************************************
Private Constant Declarations
*/
#define MY_CAP_ID  0xC009 /* CHANGE THIS VALUE TO THAT SELECTED */

/** The stub capability function handler table */
const handler_lookup_struct my_cap_handler_table =
{
    base_op_create,           /* OPCMD_CREATE */
    base_op_destroy,          /* OPCMD_DESTROY */
    base_op_start,            /* OPCMD_START */
    base_op_stop,             /* OPCMD_STOP */
    base_op_reset,            /* OPCMD_RESET */
    base_op_connect,          /* OPCMD_CONNECT */
    base_op_disconnect,       /* OPCMD_DISCONNECT */
    base_op_buffer_details,   /* OPCMD_BUFFER_DETAILS */
    base_op_get_data_format,  /* OPCMD_DATA_FORMAT */
    base_op_get_sched_info    /* OPCMD_GET_SCHED_INFO */
};

/* Null terminated operator message handler table - this is the set of operator
 * messages that the capability understands and will attempt to service. */
const opmsg_handler_lookup_table_entry my_cap_opmsg_handler_table[] =
{
    {OPMSG_COMMON_ID_GET_CAPABILITY_VERSION, base_op_opmsg_get_capability_version},
    {0, NULL}
};


/* Capability data - This is the definition of the capability that Opmgr uses to
 * create the capability from. */
const CAPABILITY_DATA my_cap_cap_data =
{
    MY_CAP_ID,             /* Capability ID */
    0, 1,                           /* Version information - hi and lo parts */
    1, 1,                           /* Max number of sinks/inputs and sources/outputs */
    &my_cap_handler_table, /* Pointer to message handler function table */
    my_cap_opmsg_handler_table,    /* Pointer to operator message handler function table */
    my_cap_process_data,           /* Pointer to data processing function */
    0,                              /* TODO: this would hold processing time information */
    sizeof(MY_CAP_OP_DATA)       /* Size of capability-specific per-instance data */
};
MAP_INSTANCE_DATA(MY_CAP_ID, MY_CAP_OP_DATA)

/* Accessing the capability-specific per-instance data function */
static inline MY_CAP_OP_DATA *get_instance_data(OPERATOR_DATA *op_data)
{
    return (MY_CAP_OP_DATA *) base_op_get_instance_data(op_data);
}

/* Data processing function */
static void my_cap_process_data(OPERATOR_DATA *op_data, TOUCHED_TERMINALS *touched)
{
    /*
     * TODO Capability data processing code goes here...
     * This code is provided for demonstration purposes only. It shows how you can call into your private Library
     * This example library simulates a key verification algorithm although all it does
     * is an XOR operation with an internal "secret" key so do not use this for any serious purpose
     */
    int my_key = 0xB70C5F1FUL;
    MY_CAP_OP_DATA *my_cap_data = get_instance_data(op_data);
    /* Call into your private library */
    if (my_priv_lib_entry(my_key))
    {
        /*
         * Assembly code can be called for faster processing ... (see my_cap_proc.asm)
         */ 
        my_cap_proc_func(my_cap_data);
    }
}
