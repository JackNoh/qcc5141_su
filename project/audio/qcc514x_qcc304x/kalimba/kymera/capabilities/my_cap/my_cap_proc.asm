#include "my_cap_struct_asm_defs.h"

// *****************************************************************************
// MODULE:
//    $M.my_cap_proc_func
//
// DESCRIPTION:
//    Define here your processing function
//
// INPUTS:
//   - Your input registers
//
// OUTPUTS:
//   - Your output registers
//
// TRASHED REGISTERS:
//    C calling convention respected.
//
// NOTES:
//
// *****************************************************************************
.MODULE $M.my_cap_proc_func;
   .CODESEGMENT PM;
   .MAXIM;

$_my_cap_proc_func:

    /*
     * TODO Assembly processing code goes here ...
     */

    r0 = M[r0 + $my_cap_struct.MY_CAP_OP_DATA_struct.MY_DATA_FIELD];
    rts;

.ENDMODULE;
