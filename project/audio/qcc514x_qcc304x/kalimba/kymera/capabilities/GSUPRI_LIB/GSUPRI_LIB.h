/****************************************************************************
 * Copyright (c) 2014 - 2016 Qualcomm Technologies International, Ltd 
****************************************************************************/
/**
 * \file  GSUPRI_LIB.h
 *
 * Private library ASM header file. <br>
 *
 */

#ifndef GSUPRI_LIB_H
#define GSUPRI_LIB_H

.CONST $MODE 2;


#endif /* GSUPRI_LIB_H */
