// -----------------------------------------------------------------------------
// Copyright (c) 2021                  Qualcomm Technologies International, Ltd.
//
#ifndef __MALPB_GEN_C_H__
#define __MALPB_GEN_C_H__

#ifndef ParamType
typedef unsigned ParamType;
#endif

// CodeBase IDs
#define MALPB_BPT_CAP_ID	0x0001
#define MALPB_BPT_ALT_CAP_ID_0	0x4002
#define MALPB_BPT_SAMPLE_RATE	0
#define MALPB_BPT_VERSION_MAJOR	1

#define MALPB_TTP_CAP_ID	0x003C
#define MALPB_TTP_ALT_CAP_ID_0	0x4067
#define MALPB_TTP_SAMPLE_RATE	0
#define MALPB_TTP_VERSION_MAJOR	1

// Constant Definitions


// Runtime Config Parameter Bitfields
#define MALPB_CONFIG_PM_BYPASS		0x00000001


// System Mode
#define MALPB_SYSMODE_STATIC 		0
#define MALPB_SYSMODE_STANDBY		1
#define MALPB_SYSMODE_MONITOR		2
#define MALPB_SYSMODE_MAX_MODES		3

// System Control
#define MALPB_CONTROL_MODE_OVERRIDE		0x2000

// Statistics Block
typedef struct _tag_MALPB_STATISTICS
{
	ParamType OFFSET_CUR_MODE_OFFSET;
	ParamType OFFSET_PEAK_LEVEL_1;
	ParamType OFFSET_PEAK_LEVEL_2;
	ParamType OFFSET_PEAK_LEVEL_3;
	ParamType OFFSET_PEAK_LEVEL_4;
	ParamType OFFSET_PEAK_LEVEL_5;
	ParamType OFFSET_PEAK_LEVEL_6;
	ParamType OFFSET_PEAK_LEVEL_7;
	ParamType OFFSET_PEAK_LEVEL_8;
	ParamType OFFSET_OVR_CONTROL;
}MALPB_STATISTICS;

typedef MALPB_STATISTICS* LP_MALPB_STATISTICS;

// Parameters Block
typedef struct _tag_MALPB_PARAMETERS
{
	ParamType OFFSET_CONFIG;
	ParamType OFFSET_GAIN;
}MALPB_PARAMETERS;

typedef MALPB_PARAMETERS* LP_MALPB_PARAMETERS;

unsigned *MALPB_GetDefaults(unsigned capid);

#endif // __MALPB_GEN_C_H__
