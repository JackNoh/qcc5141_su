/****************************************************************************
 * Copyright (c) 2013 - 2017 Qualcomm Technologies International, Ltd.
****************************************************************************/
/**
 * \defgroup MALPB
 * \file  MALPB.h
 * \ingroup capabilities
 *
 * MALPB operator public header file. <br>
 *
 */

#ifndef _MALPB_OP_H_
#define _MALPB_OP_H_

#include "opmgr/opmgr_for_ops.h"

/****************************************************************************
Public Variable Definitions
*/
/** The capability data structure for MALPB */
extern const CAPABILITY_DATA MALPB_cap_data;

/** The capability data structure for passthrough with time-to-play */
extern const CAPABILITY_DATA ttp_passthrough_cap_data;


// MALPB_ADD_START
extern bool MALPB_opmsg_set_mode(OPERATOR_DATA *op_data, void *message_data, unsigned *resp_length, OP_OPMSG_RSP_PAYLOAD **resp_data);
extern bool MALPB_opmsg_set_volume(OPERATOR_DATA *op_data, void *message_data, unsigned *resp_length, OP_OPMSG_RSP_PAYLOAD **resp_data);
// MALPB_ADD_END




#endif /* _MALPB_OP_H_ */
