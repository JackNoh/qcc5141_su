// *****************************************************************************
// Copyright (c) 2005 - 2017 Qualcomm Technologies International, Ltd.
// %%version
//
// $Change$  $DateTime$
// *****************************************************************************

// ASM function for basic mono operator data processing
// The function(s) obey the C compiler calling convention (see documentation, CS-124812-UG)

#include "cbuffer_asm.h"
#include "MALPB_shared_const.h"
#include "MALPB_struct_asm_defs.h" // data structure field offsets
#include "MALPB_gen_asm.h"
#include "portability_macros.h"

.MODULE $M.MALPB_proc;
    .CODESEGMENT PM;

// *****************************************************************************
// MODULE:
//    $_MALPB_processing
//
// DESCRIPTION:
//    Data processing function. Processes each active channel of the operator
//
// INPUTS:
//    - r0 = OPERATOR_EXTRA_DATA struct address (the cap specific part of the
//    data structure)
//    - r1 = number of samples to process
//    - M2 = -ADDR_PER_WORD (This is implict in that it is called from C
//
// OUTPUTS:
//    - r0 = return code (0 for success)
//
// TRASHED REGISTERS:
//    rMAC, r2, r3, r4, r10, I7, I6
//
// NOTES: REMOTE Buffers might work (for hydra) assuming they are accessed through a window,
//        but have not been tested.
//        It relies for now on imagined $cbuffer and $common modules' constants
//
// *****************************************************************************

$_MALPB_processing:

   // for now, we assume we get to the buffer parameters directly with some offset constants
   push rLink;
   pushm <I0, I4, L0, L4>;
   pushm <B0, B4>;
   pushm <r5, r6>;

   push r1; // samples to process is needed for each channel so push it on the stack
   r10 = M[SP - ADDR_PER_WORD]; // load number of samples to process into r10;

   I7 = r0 + $MALPB_struct.MALPB_OP_DATA_struct.CHANNEL_FIELD;
   r6 = M[r0 + $MALPB_struct.MALPB_OP_DATA_struct.MALPB_MODE_FIELD];
   r7 = M[r0 + $MALPB_struct.MALPB_OP_DATA_struct.MALPB_VOLUME_FIELD];

   // Initialise r3 to the next channel
   r3 = M[I7, ADDR_PER_WORD];
   // get read pointer, size and start addresses of input buffer if the buffer
   // doesn't exist this and any more channels in the list are inactive
   r0 = M[r3 + $MALPB_struct.PASSTHROUGH_CHANNEL_struct.IP_BUFFER_FIELD];
   call $cbuffer.get_read_address_and_size_and_start_address;
   push r2;
   pop B0;
   I0 = r0;
   L0 = r1;

   // now get the output buffer info if its null then its inactive
   r0 = M[r3 + $MALPB_struct.PASSTHROUGH_CHANNEL_struct.OP_BUFFER_FIELD];
   call $cbuffer.get_write_address_and_size_and_start_address;
   push r2;
   pop B4;
   I4 = r0;
   L4 = r1;



      r0 = M[I0, M1];
   do LOOP_MAL_MODE0;
      M[I4, M1] = r0, r0 = M[I0, M1];
   LOOP_MAL_MODE0:




   // update read address in input buffer
   r0 = M[r3 + $MALPB_struct.PASSTHROUGH_CHANNEL_struct.IP_BUFFER_FIELD], r2 = M[I0, M2];
   r1 = I0;
   call $cbuffer.set_read_address;

   // update write address in output buffer
   r0 = M[r3 + $MALPB_struct.PASSTHROUGH_CHANNEL_struct.OP_BUFFER_FIELD];
   r1 = I4;
   call $cbuffer.set_write_address;


   pop r1;  // take samples to process back off the stack;

   popm <r5, r6>;
   popm <B0, B4>;
   popm <I0, I4, L0, L4>;
   pop rLink;

   rts;

.ENDMODULE;
