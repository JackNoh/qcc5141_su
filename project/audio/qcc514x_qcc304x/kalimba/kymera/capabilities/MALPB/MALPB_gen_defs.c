// -----------------------------------------------------------------------------
// Copyright (c) 2021                  Qualcomm Technologies International, Ltd.
//
#include "MALPB_gen_c.h"

#ifndef __GNUC__ 
_Pragma("datasection CONST")
#endif /* __GNUC__ */

static unsigned defaults_MALPBTTP[] = {
   0x00002080u,			// CONFIG
   0x00000000u			// GAIN
};

unsigned *MALPB_GetDefaults(unsigned capid){
	switch(capid){
		case 0x0001: return defaults_MALPBTTP;
		case 0x4002: return defaults_MALPBTTP;
		case 0x003C: return defaults_MALPBTTP;
		case 0x4067: return defaults_MALPBTTP;
	}
	return((unsigned *)0);
}
