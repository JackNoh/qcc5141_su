/****************************************************************************
 * Copyright (c) 2015 - 2017 Qualcomm Technologies International, Ltd.
****************************************************************************/
/**
 * \defgroup MALPB
 * \file  MALPB_shared_const.h
 * \ingroup capabilities
 *
 * MALPB operator header file of constant values used by asm and C <br>
 *
 */

#ifndef _MALPB_SHARED_CONST_H_
#define _MALPB_SHARED_CONST_H_

/** The maximum number of channels supported by the capability */
#define MAX_CHANS 8

#endif /* _MALPB_SHARED_CONST_H_ */
