// -----------------------------------------------------------------------------
// Copyright (c) 2019                  Qualcomm Technologies International, Ltd.
//
#ifndef __LG3D_GEN_C_H__
#define __LG3D_GEN_C_H__

#ifndef ParamType
typedef unsigned ParamType;
#endif

// CodeBase IDs
#define LG3D_BPT_CAP_ID	0x0001
#define LG3D_BPT_ALT_CAP_ID_0	0x4002
#define LG3D_BPT_SAMPLE_RATE	0
#define LG3D_BPT_VERSION_MAJOR	1

#define LG3D_TTP_CAP_ID	0x003C
#define LG3D_TTP_ALT_CAP_ID_0	0x4067
#define LG3D_TTP_SAMPLE_RATE	0
#define LG3D_TTP_VERSION_MAJOR	1

// Constant Definitions


// Runtime Config Parameter Bitfields
#define LG3D_CONFIG_PM_BYPASS		0x00000001


// System Mode
#define LG3D_SYSMODE_STATIC 		0
#define LG3D_SYSMODE_STANDBY		1
#define LG3D_SYSMODE_MONITOR		2
#define LG3D_SYSMODE_MAX_MODES		3

// System Control
#define LG3D_CONTROL_MODE_OVERRIDE		0x2000

// Statistics Block
typedef struct _tag_LG3D_STATISTICS
{
	ParamType OFFSET_CUR_MODE_OFFSET;
	ParamType OFFSET_PEAK_LEVEL_1;
	ParamType OFFSET_PEAK_LEVEL_2;
	ParamType OFFSET_PEAK_LEVEL_3;
	ParamType OFFSET_PEAK_LEVEL_4;
	ParamType OFFSET_PEAK_LEVEL_5;
	ParamType OFFSET_PEAK_LEVEL_6;
	ParamType OFFSET_PEAK_LEVEL_7;
	ParamType OFFSET_PEAK_LEVEL_8;
	ParamType OFFSET_OVR_CONTROL;
}LG3D_STATISTICS;

typedef LG3D_STATISTICS* LP_LG3D_STATISTICS;

// Parameters Block
typedef struct _tag_LG3D_PARAMETERS
{
	ParamType OFFSET_CONFIG;
	ParamType OFFSET_GAIN;
}LG3D_PARAMETERS;

typedef LG3D_PARAMETERS* LP_LG3D_PARAMETERS;

unsigned *LG3D_GetDefaults(unsigned capid);

#endif // __LG3D_GEN_C_H__
