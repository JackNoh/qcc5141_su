// -----------------------------------------------------------------------------
// Copyright (c) 2019                  Qualcomm Technologies International, Ltd.
//
#include "LG3D_gen_c.h"

#ifndef __GNUC__ 
_Pragma("datasection CONST")
#endif /* __GNUC__ */

static unsigned defaults_LG3DTTP[] = {
   0x00002080u,			// CONFIG
   0x00000000u			// GAIN
};

unsigned *LG3D_GetDefaults(unsigned capid){
	switch(capid){
		case 0x0001: return defaults_LG3DTTP;
		case 0x4002: return defaults_LG3DTTP;
		case 0x003C: return defaults_LG3DTTP;
		case 0x4067: return defaults_LG3DTTP;
	}
	return((unsigned *)0);
}
