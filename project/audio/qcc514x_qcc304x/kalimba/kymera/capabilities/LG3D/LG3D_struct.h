/****************************************************************************
 * Copyright (c) 2015 - 2017 Qualcomm Technologies International, Ltd.
****************************************************************************/
/**
 * \defgroup LG3D
 * \file  LG3D_struct.h
 * \ingroup capabilities
 *
 * LG3D operator header file containing type definitions shared
 * between C and asm. <br>
 *
 */

#ifndef _LG3D_STRUCT_H_
#define _LG3D_STRUCT_H_
/*****************************************************************************
Include Files
*/
#include "buffer/cbuffer_c.h"
#include "audio_fadeout.h"
#include "stream/stream_audio_data_format.h"
#include "LG3D_shared_const.h"
#include "LG3D_gen_c.h"
#include "ttp/ttp.h"
#include "op_msg_utilities.h"

/****************************************************************************
Public Constant Definitions
*/


/****************************************************************************
Public Type Declarations
*/

/** LG3D per channel capability data */
typedef struct PASSTHROUGH_CHANNEL
{
    /** The buffer at the input terminal for this channel */
    tCbuffer *ip_buffer;

    /** The buffer at the output terminal for this channel */
    tCbuffer *op_buffer;

    /** Fade-out parameters for this channel */
    FADEOUT_PARAMS fadeout_parameters;

    /** The number channel number of this channel as viewed externally to the
     * operator.
     */
    unsigned channel_num;

} PASSTHROUGH_CHANNEL;

/** LG3D capability specific extra operator data */
typedef struct LG3D_OP_DATA
{
    /** operator copy function, this varies depending on the type the operator
     * is set to. e.g. Data, Audio, Garg, XOR */
    void *copy_function;

    /** The audio data format configurations of the input terminal */
    AUDIO_DATA_FORMAT ip_format;

    /** The audio data format configurations of the output terminal */
    AUDIO_DATA_FORMAT op_format;

    /** Flag indicating whether only a single channel needs to be considered to
     * determine amount of data and space.
     */
    bool simple_data_test_safe;

#ifdef INSTALL_TTP
    /** Time-to-play context */
    ttp_context *time_to_play;
#endif

    /** Latency buffer size */
    unsigned latency_buffer_size;

    /** Configured sample rate */
    unsigned sample_rate;

    /** The number of channels currently active */
    unsigned num_active_chans;

    /** Bitfield indicates which channels are currently active. */
    unsigned active_chans;

    /** List of connected channels. This list is ordered so those with 
     * both ip and op connected are at the top */
    PASSTHROUGH_CHANNEL *channel[MAX_CHANS];

    /** TRUE if the operator should kick forwards if limited by output space */
    bool kick_on_full_output;

#ifdef INSTALL_METADATA
    /** The input buffer with metadata to transport from */
    tCbuffer *metadata_ip_buffer;
    /** The output buffer with metadata to transport to */
    tCbuffer *metadata_op_buffer;
#endif /* INSTALL_METADATA */

    LG3D_PARAMETERS params;

    unsigned ReInitFlag;
    unsigned Cur_mode;
    unsigned Host_mode;
    unsigned Obpm_mode;
    unsigned Ovr_Control;
    unsigned gain_linear;
    unsigned peak_level_1;
    unsigned peak_level_2;
    unsigned peak_level_3;
    unsigned peak_level_4;
    unsigned peak_level_5;
    unsigned peak_level_6;
    unsigned peak_level_7;
    unsigned peak_level_8;
    CPS_PARAM_DEF parms_def;


    // LG3D_ADD_START
    unsigned LG3D_mode;
    // LG3D_ADD_END

    // Gaudio Memory
    unsigned Gaudio_inBufL[1024];
    unsigned Gaudio_inBufR[1024];
    unsigned Gaudio_outBufL[1124];
    unsigned Gaudio_outBufR[1124];
    unsigned Gaudio_Proc1[1056];
    unsigned Gaudio_Proc2[1024];

    unsigned Gaudio_Delay1_Out_L[229];
    unsigned Gaudio_Delay2_Out_L[191];
    unsigned Gaudio_Delay3_Out_L[251];
    unsigned Gaudio_Delay4_Out_L[163];
    unsigned Gaudio_Allpass1_In_L[37];
    unsigned Gaudio_Allpass1_Out_L[37];
    unsigned Gaudio_Allpass2_In_L[463];
    unsigned Gaudio_Allpass2_Out_L[463];

    unsigned Gaudio_Delay1_Out_R[241];
    unsigned Gaudio_Delay2_Out_R[173];
    unsigned Gaudio_Delay3_Out_R[223];
    unsigned Gaudio_Delay4_Out_R[193];
    unsigned Gaudio_Allpass1_In_R[29];
    unsigned Gaudio_Allpass1_Out_R[29];
    unsigned Gaudio_Allpass2_In_R[491];
    unsigned Gaudio_Allpass2_Out_R[491];

    unsigned Gaudio_Limiter_procL[1055];
    unsigned Gaudio_Limiter_procR[1055];

} LG3D_OP_DATA;

#endif /* _LG3D_STRUCT_H_ */
