/****************************************************************************
 * Copyright (c) 2013 - 2017 Qualcomm Technologies International, Ltd.
****************************************************************************/
/**
 * \defgroup LG3D
 * \file  LG3D.h
 * \ingroup capabilities
 *
 * LG3D operator public header file. <br>
 *
 */

#ifndef _LG3D_OP_H_
#define _LG3D_OP_H_

#include "opmgr/opmgr_for_ops.h"

/****************************************************************************
Public Variable Definitions
*/
/** The capability data structure for LG3D */
extern const CAPABILITY_DATA LG3D_cap_data;

/** The capability data structure for passthrough with time-to-play */
extern const CAPABILITY_DATA ttp_passthrough_cap_data;


// LG3D_ADD_START
extern bool LG3D_opmsg_set_mode(OPERATOR_DATA *op_data, void *message_data, unsigned *resp_length, OP_OPMSG_RSP_PAYLOAD **resp_data);
extern bool LG3D_opmsg_set_volume(OPERATOR_DATA *op_data, void *message_data, unsigned *resp_length, OP_OPMSG_RSP_PAYLOAD **resp_data);
// LG3D_ADD_END




#endif /* _LG3D_OP_H_ */
