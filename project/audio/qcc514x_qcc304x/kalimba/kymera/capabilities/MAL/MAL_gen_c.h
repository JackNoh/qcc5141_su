// -----------------------------------------------------------------------------
// Copyright (c) 2019                  Qualcomm Technologies International, Ltd.
//
#ifndef __MAL_GEN_C_H__
#define __MAL_GEN_C_H__

#ifndef ParamType
typedef unsigned ParamType;
#endif

// CodeBase IDs
#define MAL_BPT_CAP_ID	0x0001
#define MAL_BPT_ALT_CAP_ID_0	0x4002
#define MAL_BPT_SAMPLE_RATE	0
#define MAL_BPT_VERSION_MAJOR	1

#define MAL_TTP_CAP_ID	0x003C
#define MAL_TTP_ALT_CAP_ID_0	0x4067
#define MAL_TTP_SAMPLE_RATE	0
#define MAL_TTP_VERSION_MAJOR	1

// Constant Definitions


// Runtime Config Parameter Bitfields
#define MAL_CONFIG_PM_BYPASS		0x00000001


// System Mode
#define MAL_SYSMODE_STATIC 		0
#define MAL_SYSMODE_STANDBY		1
#define MAL_SYSMODE_MONITOR		2
#define MAL_SYSMODE_MAX_MODES		3

// System Control
#define MAL_CONTROL_MODE_OVERRIDE		0x2000

// Statistics Block
typedef struct _tag_MAL_STATISTICS
{
	ParamType OFFSET_CUR_MODE_OFFSET;
	ParamType OFFSET_PEAK_LEVEL_1;
	ParamType OFFSET_PEAK_LEVEL_2;
	ParamType OFFSET_PEAK_LEVEL_3;
	ParamType OFFSET_PEAK_LEVEL_4;
	ParamType OFFSET_PEAK_LEVEL_5;
	ParamType OFFSET_PEAK_LEVEL_6;
	ParamType OFFSET_PEAK_LEVEL_7;
	ParamType OFFSET_PEAK_LEVEL_8;
	ParamType OFFSET_OVR_CONTROL;
}MAL_STATISTICS;

typedef MAL_STATISTICS* LP_MAL_STATISTICS;

// Parameters Block
typedef struct _tag_MAL_PARAMETERS
{
	ParamType OFFSET_CONFIG;
	ParamType OFFSET_GAIN;
}MAL_PARAMETERS;

typedef MAL_PARAMETERS* LP_MAL_PARAMETERS;

unsigned *MAL_GetDefaults(unsigned capid);

#endif // __MAL_GEN_C_H__
