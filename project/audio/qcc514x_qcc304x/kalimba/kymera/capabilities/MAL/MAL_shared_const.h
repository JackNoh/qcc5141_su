/****************************************************************************
 * Copyright (c) 2015 - 2017 Qualcomm Technologies International, Ltd.
****************************************************************************/
/**
 * \defgroup MAL
 * \file  MAL_shared_const.h
 * \ingroup capabilities
 *
 * MAL operator header file of constant values used by asm and C <br>
 *
 */

#ifndef _MAL_SHARED_CONST_H_
#define _MAL_SHARED_CONST_H_

/** The maximum number of channels supported by the capability */
#define MAX_CHANS 8

#endif /* _MAL_SHARED_CONST_H_ */
