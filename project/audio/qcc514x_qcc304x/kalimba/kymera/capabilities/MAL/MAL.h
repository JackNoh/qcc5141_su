/****************************************************************************
 * Copyright (c) 2013 - 2017 Qualcomm Technologies International, Ltd.
****************************************************************************/
/**
 * \defgroup MAL
 * \file  MAL.h
 * \ingroup capabilities
 *
 * MAL operator public header file. <br>
 *
 */

#ifndef _MAL_OP_H_
#define _MAL_OP_H_

#include "opmgr/opmgr_for_ops.h"

/****************************************************************************
Public Variable Definitions
*/
/** The capability data structure for MAL */
extern const CAPABILITY_DATA MAL_cap_data;

/** The capability data structure for passthrough with time-to-play */
extern const CAPABILITY_DATA ttp_passthrough_cap_data;


// MAL_ADD_START
extern bool MAL_opmsg_set_mode(OPERATOR_DATA *op_data, void *message_data, unsigned *resp_length, OP_OPMSG_RSP_PAYLOAD **resp_data);
// MAL_ADD_END




#endif /* _MAL_OP_H_ */
