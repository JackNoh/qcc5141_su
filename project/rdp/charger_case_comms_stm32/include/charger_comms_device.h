/*!
\copyright  Copyright (c) 2020 Qualcomm Technologies International, Ltd.\n
            All Rights Reserved.\n
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\file
\brief      Charger
*/

#ifndef CHARGER_COMMS_DEVICE_H_
#define CHARGER_COMMS_DEVICE_H_

/*-----------------------------------------------------------------------------
------------------ INCLUDES ---------------------------------------------------
-----------------------------------------------------------------------------*/

#include "cli_parse.h"

/*-----------------------------------------------------------------------------
------------------ PREPROCESSOR DEFINITIONS -----------------------------------
-----------------------------------------------------------------------------*/

#define CHARGER_COMMS_SENSE_AMP_COMMAND 0x01
#define CHARGER_COMMS_SENSE_AMP_COMMS   0x02
#define CHARGER_COMMS_SENSE_AMP_BATTERY 0x04

/*-----------------------------------------------------------------------------
------------------ PROTOTYPES -------------------------------------------------
-----------------------------------------------------------------------------*/

void charger_comms_device_init(void);
void charger_comms_periodic(void);
CLI_RESULT ats_regulator(uint8_t cmd_source);
CLI_RESULT atq_sense(uint8_t cmd_source);
void charger_comms_set_sense_amp(uint8_t reason);
void charger_comms_clear_sense_amp(uint8_t reason);

#endif /* CHARGER_COMMS_DEVICE_H_ */
