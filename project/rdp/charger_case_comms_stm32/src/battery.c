/*!
\copyright  Copyright (c) 2020 Qualcomm Technologies International, Ltd.\n
            All Rights Reserved.\n
            Qualcomm Technologies International, Ltd. Confidential and Proprietary.
\file
\brief      Battery
*/

/*-----------------------------------------------------------------------------
------------------ INCLUDES ---------------------------------------------------
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include "main.h"
#include "adc.h"
#include "gpio.h"
#include "led.h"
#include "timer.h"
#include "case_charger.h"
#include "config.h"
#include "power.h"
#include "charger_comms_device.h"
#include "battery.h"

/*-----------------------------------------------------------------------------
------------------ PREPROCESSOR DEFINITIONS -----------------------------------
-----------------------------------------------------------------------------*/

/**
 * Time in ticks that we must wait for the VBAT monitor to settle before taking
 * a measurement from it.
 */
#define BATTERY_READ_DELAY_TIME 20

/**
 * Time in ticks to wait for the ADC reading to take place.
 */
#define BATTERY_ADC_DELAY_TIME 4

/*
* BATTERY_NO_OF_CUTOFF_READS: Number of reads we will make if the voltage is
* below the cutoff voltage.
*/
#define BATTERY_NO_OF_CUTOFF_READS 3

/*-----------------------------------------------------------------------------
------------------ TYPE DEFINITIONS -------------------------------------------
-----------------------------------------------------------------------------*/

typedef enum
{
    BATTERY_IDLE,
    BATTERY_START_READING,
    BATTERY_READING,
    BATTERY_STOP_READING,
    BATTERY_DONE
}
BATTERY_STATE;

typedef struct
{
    BATTERY_STATE state;
    uint8_t current_battery_percent;
    uint16_t current_battery_mv;
    uint16_t delay_ticks;
    bool led;
    uint8_t cmd_source;
    uint32_t cutoff_mv;
    uint8_t read_ctr;
}
BATTERY_STATUS;

typedef struct
{
    /* The voltage of the battery at this level in millivolts. */
    uint16_t voltage_mv;

    /* The percentage of charge this level represents from 0% to 100%. */
    uint8_t percentage;
}
BATTERY_LEVEL;

typedef struct
{
    /* The load on VBUS supplying the earbuds. This is assumed to be total load
     * on the case battery so regulator efficiency and other peripheral power
     * consumption is negligible. */
    uint16_t load_mA;

    /* The expected drop in battery voltage under the load. Measured in millivolts. */
    uint16_t battery_drop_mv;
}
BATTERY_DROP;

/*-----------------------------------------------------------------------------
------------------ PROTOTYPES -------------------------------------------------
-----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
------------------ VARIABLES --------------------------------------------------
-----------------------------------------------------------------------------*/

/*
 * Measured battery capacity levels of the VDL 602045 545mA
 * 3.7V lithium battery.
 * These were determined using the B2281S-20-6 Battery Simulator in mode 9.
 */
static const BATTERY_LEVEL battery_levels[] =
{   
    {3500, 0},
    {3627, 1},
    {3784, 5},
    {3803, 10},
    {3834, 15},
    {3860, 20},
    {3884, 25},
    {3897, 30},
    {3920, 40},
    {3949, 50},
    {3981, 60},
    {4018, 70},
    {4063, 80},
    {4115, 90},
    {4139, 95},
    {4160, 100},
};
static const size_t battery_levels_num = sizeof(battery_levels) / sizeof(BATTERY_LEVEL);

#ifdef EARBUD_CURRENT_SENSES
/*
 * Measured battery voltage drops of the VDL 602045 545mA
 * 3.7V lithium battery.
 * These were determined using the B2281S-20-6 Battery Simulator in mode 9.
 */
static const BATTERY_DROP battery_drops[] =
{   
    {0,   0},
    {50,  32},
    {100, 63},
    {200, 124},
    {280, 174}
};
static const size_t battery_drops_num = sizeof(battery_drops) / sizeof(BATTERY_DROP);
#endif

static BATTERY_STATUS battery_status =
{
    .state = BATTERY_IDLE,
    .cmd_source = CLI_SOURCE_NONE
};

/*-----------------------------------------------------------------------------
------------------ FUNCTIONS --------------------------------------------------
-----------------------------------------------------------------------------*/

static uint8_t battery_percentage(void)
{
    size_t i;
    uint16_t mv = battery_status.current_battery_mv;
    uint8_t ret = 0;

    /* Any voltage less than the minimum level is considered 0% */
    if (mv > battery_levels[0].voltage_mv)
    {
        /* Any voltage larger than the maximum is considered 100% */
        ret = 100;

        /* Find the battery levels which the measured voltage sits between and
         * linearly interpolate the resulting percentage */
        for (i = 1; i < battery_levels_num; i++)
        {
            if (mv < battery_levels[i].voltage_mv)
            {
                /* Linearly interpolate a percentage based on this level and the previous */
                uint16_t range_mv = battery_levels[i].voltage_mv - battery_levels[i - 1].voltage_mv;
                uint8_t  range_pc = battery_levels[i].percentage - battery_levels[i - 1].percentage;
                uint8_t  d_mv = mv - battery_levels[i - 1].voltage_mv;

                ret = battery_levels[i - 1].percentage +
                    ((10 * range_pc * d_mv) / range_mv + 5) / 10;
                break;
            }
        }
    }

    return ret;
}

uint8_t battery_percentage_current(void)
{
   return battery_status.current_battery_percent;
}

#ifdef EARBUD_CURRENT_SENSES
static uint32_t battery_compensated_voltage(uint16_t reading, uint16_t left_sense, uint16_t right_sense)
{
    size_t i;
    /* The current senses are biased at ~190-200mV. Subtract 180mV for margin. */
    uint32_t total_cur_mv = (left_sense - 180) + (right_sense - 180);
    /* The current sense resistor and amplifier is set up so 1mA = 20mV. */
    uint32_t total_ma = total_cur_mv/20;
    uint32_t drop_mv = 0;

    /* Only compensate if there is a load. */
    if (total_ma > battery_drops[0].load_mA)
    {
        /* Any load larger than the maximum is considered to impose a 200mV drop. */
        drop_mv = 200;

        /* Find the battery load which the measured load sits between and
         * linearly interpolate the resulting battery drop*/
        for (i = 1; i < battery_drops_num; i++)
        {
            if (total_ma < battery_drops[i].load_mA)
            {
                /* Linearly interpolate a battery_drop_mv based on this level and the previous */
                uint16_t range_ma = battery_drops[i].load_mA - battery_drops[i - 1].load_mA;
                uint8_t  range_mv = battery_drops[i].battery_drop_mv - battery_drops[i - 1].battery_drop_mv;
                uint8_t  d_ma = total_ma - battery_drops[i - 1].load_mA;

                drop_mv = battery_drops[i - 1].battery_drop_mv +
                    ((10 * range_mv * d_ma) / range_ma + 5) / 10;
                break;
            }
        }
    }

    /* If the battery voltage is low, we expect the battery voltage to drop by
     * a further 20%. */
    if (reading < 3500)
    {
        drop_mv = (drop_mv * 12)/10;
    }

    return reading + drop_mv; 
}
#endif

static uint16_t battery_mv(void)
{
    uint16_t raw_mv = adc_read_mv(ADC_VBAT, 6600);

#ifdef EARBUD_CURRENT_SENSES
    uint16_t left_sense = adc_read_mv(ADC_CURRENT_SENSE_L,3300);
    uint16_t right_sense = adc_read_mv(ADC_CURRENT_SENSE_R,3300);

    return battery_compensated_voltage(raw_mv, left_sense, right_sense);
#else
    return raw_mv;
#endif
}

void battery_periodic(void)
{
    /* Ensure that we read the case battery before initiating the status
     * sequence with the earbuds. */
    switch (battery_status.state)
    {
        case BATTERY_START_READING:
            /* We must enable the VBAT monitor circuit and then wait for it
             * to settle before taking a measurement. */
            gpio_enable(GPIO_VBAT_MONITOR_ON_OFF);
#ifdef EARBUD_CURRENT_SENSES
            charger_comms_set_sense_amp(CHARGER_COMMS_SENSE_AMP_BATTERY);
#endif
            battery_status.delay_ticks = BATTERY_READ_DELAY_TIME;
            battery_status.state = BATTERY_READING;
            battery_status.read_ctr = 0;
            battery_status.cutoff_mv = config_get_battery_cutoff_mv();
            charger_set_reason(CHARGER_OFF_BATTERY_READ);
            power_set_run_reason(POWER_RUN_BATTERY_READ);
            break;

        case BATTERY_READING:
            /* Wait and then instruct the ADC to take the measurement. */
            if (!battery_status.delay_ticks)
            {
                if (adc_start_measuring())
                {
                    battery_status.delay_ticks = BATTERY_ADC_DELAY_TIME;
                    battery_status.state = BATTERY_STOP_READING;
                }
            }
            else
            {
                battery_status.delay_ticks--;
            }
            break;

        case BATTERY_STOP_READING:
            /* Wait for the ADC to take the measurement. */
            if (!battery_status.delay_ticks)
            {
                battery_status.current_battery_mv = battery_mv();
                battery_status.current_battery_percent = battery_percentage();

                if (battery_status.current_battery_mv < battery_status.cutoff_mv)
                {
                    /*
                    * Reading was below the configured cutoff threshold.
                    */
                    if (++battery_status.read_ctr < BATTERY_NO_OF_CUTOFF_READS)
                    {
                        battery_status.state = BATTERY_READING;
                        break;
                    }
                    else
                    {
                        /*
                        * Readings persistently low, so go to standby.
                        */
                        power_set_standby_reason(POWER_STANDBY_LOW_BATTERY);
                    }
                }
                else
                {
                    power_clear_standby_reason(POWER_STANDBY_LOW_BATTERY);
                }

                /* We no longer need the VBAT monitor HW to be powered */
                gpio_disable(GPIO_VBAT_MONITOR_ON_OFF);
#ifdef EARBUD_CURRENT_SENSES
                charger_comms_clear_sense_amp(CHARGER_COMMS_SENSE_AMP_BATTERY);
#endif
                charger_clear_reason(CHARGER_OFF_BATTERY_READ);
                power_clear_run_reason(POWER_RUN_BATTERY_READ);
                battery_status.state = BATTERY_DONE;
                if (battery_status.led)
                {
                    led_indicate_battery(battery_status.current_battery_percent);
                }

                /*
                * If a command to read the battery is in progress, display
                * the result.
                */
                if (battery_status.cmd_source != CLI_SOURCE_NONE)
                {
                    uint8_t cmd_source = battery_status.cmd_source;

                    PRINTF("%u,%u",
                        battery_status.current_battery_mv,
                        battery_status.current_battery_percent);
                    PRINTF("OK");
                    battery_status.cmd_source = CLI_SOURCE_NONE;
                }
            }
            else
            {
                battery_status.delay_ticks--;
            }
            break;

        case BATTERY_IDLE:
        case BATTERY_DONE:
        default:
            break;
    }
}

void battery_read_request(bool led)
{
    if ((battery_status.state==BATTERY_IDLE) ||
        (battery_status.state==BATTERY_DONE))
    {
        /*
        * No battery read in progress, so start one.
        */
        battery_status.state = BATTERY_START_READING;
        battery_status.led = led;
    }
    else if (led)
    {
        /*
        * Set the led flag, so that the battery read already in progress
        * will report to the LED module at the end.
        */
        battery_status.led = true;
    }
}

bool battery_read_done(void)
{
    return (battery_status.state == BATTERY_DONE) ? true:false;
}

uint16_t battery_read_ntc(void)
{
    uint16_t ntc_mv;

    gpio_enable(GPIO_NTC_MONITOR_ON_OFF);
    adc_blocking_measure();
    ntc_mv = adc_read_mv(ADC_NTC, 3300);
    gpio_disable(GPIO_NTC_MONITOR_ON_OFF);

    return ntc_mv;
}

CLI_RESULT atq_ntc(uint8_t cmd_source)
{
    PRINTF("%u", battery_read_ntc());
    return CLI_OK;
}

CLI_RESULT atq_battery(uint8_t cmd_source)
{
    CLI_RESULT ret = CLI_ERROR;

    if (battery_status.cmd_source==CLI_SOURCE_NONE)
    {
        battery_read_request(false);
        battery_status.cmd_source = cmd_source;
        ret = CLI_WAIT;
    }

    return ret;
}
