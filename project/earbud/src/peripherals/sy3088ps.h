/*!
\copyright  jybae@bluecom
\
\version    1.0
\file       sy3088ps.h
\brief      Header file for sy3088ps Proximity Sensor
*/

#ifndef SY3088PS_PRIVATE_H
#define SY3088PS_PRIVATE_H

#ifdef HAVE_SY3088PS

#include <types.h>

/*! I2C communication addresses */
#define I2C_ADDRESS_SY3088PS		(0x1e) //(0x23)

// RTL-706PS-01
/* Address 	R / W 	Register Name 			Description 									Reset Value
	0x80 	R / W 	CLK_ON 					register Internal Clock control and SW reset 	0x00
	0x81	R / W 	PS_CONTR 				PS operation mode control 						0x40
	0x82 	R / W 	PS_LED 					PS LED setting 									0x76
	0x83 	R / W 	PS_N_PULSES 			PS number of pulses 							0x01
	0x84 	R / W 	PS_MEAS_RATE 			PS measurement rate in active mode 				0x04
	0x86 	R 		PART_ID 				Part Number ID and Revision ID 					0x05
	0x87 	R 		MANUFAC_ID 				Manufacturer ID 								0x05
	0x90 	R 		PS_STATUS 				PS new data status 								0x00
	0x91 	R 		PS_DATA_0 				PS measurement data, lower byte 				0x00
	0x92 	R 		PS_DATA_1 				PS measurement data, upper byte 				0x00
	0x93 	R / W 	INTERRUPT 				Interrupt settings 								0x00
	0x94 	R / W 	INTERTUPT_PERSIST 		PS interrupt persist setting 					0x00
	0x95 	R / W 	PS_THRES_UP_0 			PS interrupt upper threshold, lower byte 		0xFF
	0x96 	R / W 	PS_THRES_UP_1 			PS interrupt upper threshold, upper byte 		0x07
	0x97 	R / W 	PS_THRES_LOW_0 			PS interrupt lower threshold, lower byte 		0x00
	0x98 	R / W 	PS_THRES_LOW_1 			PS interrupt lower threshold, upper byte 		0x00
	0x9F 	R / W 	FAULT DETECTION 		Fault detection control and status register 	0x00
	0xA0 	R 		FD STATUS 				Fault detection status 							0x00
*/

/*! @name SY3088PS I2C registers. */
//!@{
#if 0
#define LTR706PS01_CLK_ON_REGISTER				(0x80)
#define LTR706PS01_PS_CONTR_REGISTER            (0x81)
#define LTR706PS01_PS_VCSEL              		(0x82)
#define LTR706PS01_PS_PULSE						(0x83)
#define LTR706PS01_PROXIMITY_RATE_REG           (0x84)
#define LTR706PS01_PRODCUT_ID_REVISION_REG      (0x86)
#define LTR706PS01_PS_STATUS      				(0x90)
#define LTR706PS01_PROXIMITY_RESULT_LO_REG      (0x91)
#define LTR706PS01_PROXIMITY_RESULT_HI_REG     	(0x92)
#define LTR706PS01_INTERRUPT_CONTORL_REG        (0x93)
#define LTR706PS01_HIGH_THRESHOLD_LO_REG        (0x95)
#define LTR706PS01_HIGH_THRESHOLD_HI_REG        (0x96)
#define LTR706PS01_LOW_THRESHOLD_LO_REG         (0x97)
#define LTR706PS01_LOW_THRESHOLD_HI_REG         (0x98)
#define LTR706PS01_OFFSET_LO_REG         		(0x99)
#define LTR706PS01_OFFSET_HI_REG         		(0x9A)
#endif
#define SY3088PS_PROXIMITY_PXS_DATA      (0x05)

//!@}

/*! Important note, this code assumes the compiler fills bitfields from the
    least significant bit, as KCC does, such that the following register
    bitfield definitions correctly map onto the physical bits */
#ifndef __KCC__
#warning "Not compiling with KCC. The bitfield register definitions may be incorrect."
#endif

/*! This register is for starting proximity measurements */
typedef union sy3088ps_command_register
{
    /*! The register bit fields */
    struct
    {
    	unsigned _reserved_ : 1;
    	unsigned mode : 1;
    	unsigned _reserved : 3;
    	unsigned ftn_ntf_en : 1;
    	unsigned reserved : 1;
        unsigned offset_enable : 1;
    } bits;
    /*! The entire register as a byte */
    uint8 reg;
} sy3088ps_command_register_t;

/*! This register contains information about Part Number and revision */
typedef union sy3088ps_revision_register
{
    /*! The register bit fields */
    struct
    {
		unsigned revision_id : 2;
        unsigned part_id : 6;
    } bits;
    /*! The entire register as a byte */
    uint8 reg;
} sy3088ps_revision_register_t;

/*! Proximity rate control */
enum sy3088ps_proximity_rates
{
    sy3088ps_proximity_rate_6p125_ms_repeatly = 0,
    sy3088ps_proximity_rate_12p5_ms_repeatly,
    sy3088ps_proximity_rate_25_ms_repeatly,
    sy3088ps_proximity_rate_50_ms_repeatly,
    sy3088ps_proximity_rate_100_ms_repeatly,
    sy3088ps_proximity_rate_200_ms_repeatly,
    sy3088ps_proximity_rate_400_ms_repeatly,
    sy3088ps_proximity_rate_800_ms_repeatly,
};

/*! This register controls the proximity rate */
typedef union sy3088ps_proximity_rate_register
{
    /*! The register bit fields */
    struct
    {
        /*! The measurement rate */
        enum sy3088ps_proximity_rates rate : 3;
        /*! Function of this bit is not defined */
        unsigned _unused : 5;
    } bits;
    /*! The entire register as a byte */
    uint8 reg;
} sy3088ps_proximity_rate_register_t;

/*! This register controls the IR LED current */
typedef union sy3088ps_ir_led_current_register
{
    /*! The register bit fields */
    struct
    {
        /*! The IR led current = current x 10mA (2=2mA=default) */
        unsigned current : 6;
        /*! Read only bits. Information about fuse program revision used for
            initial setup/calibration of the device. */
        unsigned fuse_prog_id : 2;
    } bits;
    /*! The entire register as a byte */
    uint8 reg;
} sy3088ps_ir_led_current_register_t;

/*! The number of times the threshold is exceeded before generating an interrupt */
enum sy3088ps_threshold_counts
{
    sy3088ps_threshold_count_1 = 0,
    sy3088ps_threshold_count_2,
    sy3088ps_threshold_count_4,
    sy3088ps_threshold_count_8,
    sy3088ps_threshold_count_16,
    sy3088ps_threshold_count_32,
    sy3088ps_threshold_count_64,
    sy3088ps_threshold_count_128,
};

/*! This register controls the interrupt */
typedef union sy3088ps_interrupt_control_register
{
    /*! The register bit fields */
    struct
    {
    	unsigned intrrupt_mode : 2;
    	unsigned interrupt_polarity : 1;
    	unsigned reserved : 3;
    	unsigned ntf : 1;
        unsigned ftn : 1;
    } bits;
    /*! The entire register as a byte */
    uint8 reg;
} sy3088ps_interrupt_control_register_t;

/*! This register controls the interrupt */
typedef union sy3088ps_THRES_register
{
    /*! The register bit fields */
    struct
    {
    	unsigned reserved : 5;
    	unsigned data: 3;
    } bits;
    /*! The entire register as a byte */
    uint8 reg;
} sy3088ps_THRES_register_t;

/*! The interrupt status register. Once an interrupt is generated the
    corresponding status bit goes to 1 and stays there unless it is cleared by
    writing a 1 in the corresponding bit. The int pad will be pulled down while
    at least one of the status bit is 1 */
typedef union sy3088ps_interrupt_status_register
{
    /*! The register bit fields */
    struct
    {
		unsigned data_status : 1;
		unsigned interrupt_status : 1;
        unsigned reserved : 6;
    } bits;
    /*! The entire register as a byte */
    uint8 reg;
} sy3088ps_interrupt_status_register_t;

/*! The 'on' PIO will not be controlled when __proximity_config.pio.on is set to
    this value */
#define SY3088PS_ON_PIO_UNUSED 255

/*! The high level configuration for taking measurement */
struct __proximity_config
{
    /*! Measurements higher than this value will result in the sensor considering
        it is in-proximity of an object */
    //uint16 threshold_high;
    /*! Measurements lower than this value will result in the sensor considering
        it is not in-proximity of an object */
    //uint16 threshold_low;
    /*! The I2C clock frequency */
    uint16 i2c_clock_khz;
    /*! The number of measurements above/below the threshold before the sensor
        generates an interrupt */
  // enum sy3088ps_threshold_counts threshold_counts;
    /*! The number of measurements per second */
  //  enum sy3088ps_proximity_rates rate;
    /*! The PIOs used to control/communicate with the sensor */
    struct
    {
        /*! PIO used to power-on the sensor, or #sy3088ps_ON_PIO_UNUSED */
        uint8 on;
        /*! Interrupt PIO driven by the sensor */
        uint8 interrupt;
        /*! I2C serial data PIO */
        uint8 i2c_sda;
        /*! I2C serial clock PIO */
        uint8 i2c_scl;
    } pios;
};

/*! Internal representation of proximity state */
enum proximity_states
{
    proximity_state_unknown,
    proximity_state_in_proximity,
    proximity_state_not_in_proximity
};

/*! Trivial state for storing in-proximity state */
struct __proximity_state
{
    /*! The sensor proximity state */
    enum proximity_states proximity;
};

extern void appProximityUpdata(void);
extern uint16 appProximityRead(void);	

#endif /* HAVE_SY3088PS */
#endif /* SY3088PS_PRIVATE_H */
