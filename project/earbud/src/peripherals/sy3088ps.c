/*!
\copyright  jybae@bluecom
\
\version    1.0
\file       sy3088ps.c
\brief      Support for the sy3088ps proximity sensor
*/

#ifdef INCLUDE_PROXIMITY
#ifdef HAVE_SY3088PS

#include <bitserial_api.h>
#include <panic.h>
#include <pio.h>
#include <pio_monitor.h>
#include <pio_common.h>
#include <stdlib.h>

#include "earbud_led.h"

#include "adk_log.h"
#include "../../domains/peripheral/led_manager/led_manager.h"
#include "proximity.h"
#include "proximity_config.h"

#include "sy3088ps.h"

#define FEATURE_PROXIMITY_SILERGY
#define DEBUG_PROXIMITY_REGISTER_READING

#define INTERRUPT_MAX_THRESHOLD     0xff
#define INTERRUPT_MIN_THRESHOLD     0x00
#define INTERRUPT_HIGH_THRESHOLD    0x82
#define INTERRUPT_LOW_THRESHOLD     0x64
#define REGISTER_HIGH_THRESHOLD     0x04
#define REGISTER_LOW_THRESHOLD      0x03
#define INTERRUPT_FLAG_CLEAR        0x21

/* Make the type used for message IDs available in debug tools */
LOGGING_PRESERVE_MESSAGE_ENUM(proximity_messages)

const struct __proximity_config proximity_config = {
   // .threshold_low = 600,
   // .threshold_high = 2047,
   // .threshold_counts = sy3088ps_threshold_count_1,
   // .rate = sy3088ps_proximity_rate_100_ms_repeatly,
    .i2c_clock_khz = 200,
    .pios = {
        /* The PROXIMITY_PIO definitions are defined in the platform x2p file */
        .on = PROXIMITY_PIO_ON,
        .i2c_scl = PROXIMITY_PIO_I2C_SCL,
        .i2c_sda = PROXIMITY_PIO_I2C_SDA,
        .interrupt = PROXIMITY_PIO_INT,
    },
};

#define M_NUMBER_OF(a) (sizeof(a) / 2)

typedef struct {
	uint8 command;
	uint8 param;
} PROX_REG_tbl;

#ifdef FEATURE_PROXIMITY_SILERGY
#define SENSOR_CONFIG_1_BASE            (0x08)  //6.25

#define SENSOR_CONFIG_1_PXS_WATT_6      (0x00)  //6.25
#define SENSOR_CONFIG_1_PXS_WATT_12     (0x10)  //12.5
#define SENSOR_CONFIG_1_PXS_WATT_25     (0x20)  //25
#define SENSOR_CONFIG_1_PXS_WATT_50     (0x30)  //50

#define SENSOR_CONFIG_1_PXS_PRST_OFF    (0x00)
#define SENSOR_CONFIG_1_PXS_PRST_1      (0x01)
#define SENSOR_CONFIG_1_PXS_PRST_2      (0x02)
#define SENSOR_CONFIG_1_PXS_PRST_4      (0x03)
#define SENSOR_CONFIG_1_PXS_PRST_8      (0x04)
#define SENSOR_CONFIG_1_PXS_PRST_16     (0x05)
#define SENSOR_CONFIG_1_PXS_PRST_32     (0x06)
#define SENSOR_CONFIG_1_PXS_PRST_ALL    (0x07)

static PROX_REG_tbl Prox_Register_tbl[] = 
{
    {0x01, 0x00},
    /*{0x02, (SENSOR_CONFIG_1_BASE | SENSOR_CONFIG_1_PXS_WATT_50 | SENSOR_CONFIG_1_PXS_PRST_4)},*/
	{0x02, 0x3B},
    {0x03, 0x64},
    {0x04, 0x82},
    {0x01, 0x40},
    {INTERRUPT_FLAG_CLEAR, 0x00 }
};
#else
static PROX_REG_tbl Prox_Register_tbl[] = 
{
    {0x80,0x00},
    {0x81,0xc2},
    /*{0x82,0x5c},*/
    {0x82,0x58}, /*DV*/
    /*{0x83,0x32},25,30,35,40,50*/
    {0x83,0x14}, /*20, DV*/	
    {0x84,0x07}, /*800ms*/
    /*{0x84,0x05}, 200ms*/
    {0x85,0x0b},
    /*{0x86,0x5
    {0x87,0x5*/
    {0x88,0x00},
    {0x89,0x00},
    {0x8a,0x00},
    {0x8b,0x00},
    {0x8c,0x00},
    {0x8d,0x00},
    {0x8e,0x00},
    {0x8f,0x00},
    /*{0x90,0x03},
    {0x91,0x00},
    {0x92,0x00},*/
    {0x93,0x01},
    {0x94,0x00},
    {0x95,0xff},
    {0x96,0x07},
    {0x97,0x2C}, /*300*/
    {0x98,0x01}, /*300*/	
    //{0x97,0x20}, /*800*/
    //{0x98,0x03}, /*800*/
    /*{0x97,0xBC}, 700*/
    /*{0x98,0x02}, 700*/
    /*{0x97,0x00}, 1024*/
    /*{0x98,0x04}, 1024*/
    /*{0x97,0x58},
    {0x98,0x02},*//*DV*/
    /*{0x99,0xf4},
    {0x9a,0x01},*//*DV*/
    {0x99,0xb6},
    {0x9a,0x03},	
    {0x9b,0xff},
    {0x9c,0xff},
    {0x9d,0x00},
    {0x9e,0x00},
    {0x9f,0x01},
    /*{0xa0,0x00},*/
    {0xa1,0x00},
    {0xa2,0x00},
    {0xa3,0x00},
    {0xa4,0x00},
    {0xa5,0x00},
    {0xa6,0x00},
    {0xa7,0x00},
    {0xa8,0x00},
    {0xa9,0x00},
    {0xaa,0x00},
    {0xab,0x00},
    {0xac,0x00},
    {0xad,0x00},
    {0xae,0x00},
    {0xaf,0x00},
    {0xb0,0x00},
    {0xb1,0x04},
};
#endif

proximityTaskData app_proximity;

/*! \brief Read a register from the proximity sensor */
static bool sy3088psReadRegister(bitserial_handle handle, uint8 reg,  uint8 *value)
{
    bitserial_result result;

    proximityTaskData *prox = ProximityGetTaskData();			
	BitserialChangeParam(prox->handle, BITSERIAL_PARAMS_I2C_DEVICE_ADDRESS, I2C_ADDRESS_SY3088PS, BITSERIAL_FLAG_BLOCK);
	
    /* First write the register address to be read */
    result = BitserialWrite(handle,
                            BITSERIAL_NO_MSG,
                            &reg, 1,
                            BITSERIAL_FLAG_BLOCK);
    if (result == BITSERIAL_RESULT_SUCCESS)
    {
        /* Now read the actual data in the register */
        result = BitserialRead(handle,
                                BITSERIAL_NO_MSG,
                                value, 1,
                                BITSERIAL_FLAG_BLOCK);
    }
    return (result == BITSERIAL_RESULT_SUCCESS);
}

/*! \brief Write to a proximity sensor register */
static bool sy3088psWriteRegister(bitserial_handle handle, uint8 reg, uint8 value)
{
    bitserial_result result;
    uint8 command[2] = {reg, value};
    
	//DEBUG_LOG_INFO("sy3088psWriteRegister reg,value : 0x%02x, 0x%02x\n",command[0],command[1]);
	BitserialChangeParam(handle, BITSERIAL_PARAMS_I2C_DEVICE_ADDRESS, I2C_ADDRESS_SY3088PS, BITSERIAL_FLAG_BLOCK);
    /* Write the write command and register */
    result = BitserialWrite(handle,
                            BITSERIAL_NO_MSG,
                            command, 2,
                            BITSERIAL_FLAG_BLOCK);
    return (result == BITSERIAL_RESULT_SUCCESS);
}

static bool sy3088psWriting(bitserial_handle handle)
{
	bool result = FALSE;
	uint32 i;
	uint32 cnt = M_NUMBER_OF(Prox_Register_tbl);
	DEBUG_LOG_INFO("=====> All Write Proxi Start cnt %d\n",cnt);
    for(i =0; i<cnt;i++)
    {
    	result = sy3088psWriteRegister(handle, Prox_Register_tbl[i].command, Prox_Register_tbl[i].param);
		DEBUG_LOG_INFO("=====> {0x%02x, 0x%02x}\n", Prox_Register_tbl[i].command, Prox_Register_tbl[i].param);
		
	    if (!result)
	    {
			DEBUG_LOG_INFO("=====> All Write Proxi error\n");
		    return result;
		}
    }
			
	return result;
}

#ifdef DEBUG_PROXIMITY_REGISTER_READING
static bool sy3088psReading(bitserial_handle handle)
{
	bool result = FALSE;
    uint8 Read;	
	uint32 i;
	uint32 cnt = M_NUMBER_OF(Prox_Register_tbl);
	DEBUG_LOG_INFO("=====> All Read Proxi Start cnt... %d\n", cnt);	
	
    for(i =0; i<cnt;i++)
    {
    	result = sy3088psReadRegister(handle, Prox_Register_tbl[i].command, &Read);
	    if (result)
	    {
			DEBUG_LOG_INFO("=====> {0x%02x, 0x%02x}\n", Prox_Register_tbl[i].command,Read);
	    }else
	    {
			DEBUG_LOG_INFO("=====> All Read Proxi error\n");
		    return result;
		}
    }
			
	return result;
}
#endif

static bool sy3088ps_WriteProxi_LowThreshold(bitserial_handle handle)
{
	return sy3088psWriteRegister(handle, REGISTER_LOW_THRESHOLD, INTERRUPT_LOW_THRESHOLD) 
        && sy3088psWriteRegister(handle, REGISTER_HIGH_THRESHOLD, INTERRUPT_MAX_THRESHOLD);
}

static bool sy3088ps_WriteProxi_HighThreshold(bitserial_handle handle)
{
	return sy3088psWriteRegister(handle, REGISTER_LOW_THRESHOLD, INTERRUPT_MIN_THRESHOLD)
        && sy3088psWriteRegister(handle, REGISTER_HIGH_THRESHOLD, INTERRUPT_HIGH_THRESHOLD);
}

/*! \brief Handle the proximity interrupt */
static void sy3088psInterruptHandler(Task task, MessageId id, Message msg)
{
    proximityTaskData *proximity = (proximityTaskData *) task;
	bool result = FALSE;

    DEBUG_LOG_INFO("=====> sy3088psInterruptHandler id[0x%04x]", id);

    switch(id)
    {
        case MESSAGE_PIO_CHANGED:	/* 0x8023 */
        {
            const MessagePioChanged *mpc = (const MessagePioChanged *)msg;
            const proximityConfig *config = proximity->config;

            bool pio_set;

            if(PioMonitorIsPioInMessage(mpc, config->pios.interrupt, &pio_set))
            {
				if(!pio_set)
				{
					uint8 Read = 0;

                    DEBUG_LOG_INFO("=====> sy3088ps Interrupt Pin LOW");
                    if(!sy3088psReadRegister(proximity->handle, SY3088PS_PROXIMITY_PXS_DATA, &Read))
				    {
						DEBUG_LOG_INFO("=====> All Read Proxi error\n");
                        break;
					}

                    DEBUG_LOG_INFO("=====> sy3088ps : {0x%02x}\n", Read);
                    
                    if(Read > INTERRUPT_HIGH_THRESHOLD)
                    {
		                DEBUG_LOG_INFO("=====> sy3088ps_WriteProxi_LowThreshold IN proximity");
		                proximity->state->proximity = proximity_state_in_proximity;
                        
		                TaskList_MessageSendId(proximity->clients, PROXIMITY_MESSAGE_IN_PROXIMITY);
						PanicFalse(sy3088ps_WriteProxi_LowThreshold(proximity->handle));
					}
					else
					{
                    	DEBUG_LOG_INFO("=====> sy3088ps_WriteProxi_HighThreshold NOT IN proximity");
	                    proximity->state->proximity = proximity_state_not_in_proximity;
                        
	                    TaskList_MessageSendId(proximity->clients, PROXIMITY_MESSAGE_NOT_IN_PROXIMITY);      
						PanicFalse(sy3088ps_WriteProxi_HighThreshold(proximity->handle));		
					}
					
			    	result = sy3088psWriteRegister(proximity->handle, INTERRUPT_FLAG_CLEAR, 0x00);
				    if (!result)
				    {
						DEBUG_LOG_INFO("=====> INT Clear Write Proxi error\n");
					}
				}
            }
        }
        break;
        default:
        break;
    }
}

/*! \brief Enable the proximity sensor */
static bitserial_handle sy3088psEnable(const proximityConfig *config)
{
    bitserial_config bsconfig;
    uint32 i;
    uint16 bank;
    uint32 mask;
    struct
    {
        uint16 pio;
        pin_function_id func;
    } i2c_pios[] = {{config->pios.i2c_scl, BITSERIAL_1_CLOCK_OUT},
                    {config->pios.i2c_scl, BITSERIAL_1_CLOCK_IN},
                    {config->pios.i2c_sda, BITSERIAL_1_DATA_OUT},
                    {config->pios.i2c_sda, BITSERIAL_1_DATA_IN}};

    DEBUG_LOG_INFO("=====> sy3088psEnable");

    if (config->pios.on != SY3088PS_ON_PIO_UNUSED)
    {
        /* Setup power PIO then power-on the sensor */
        bank = PioCommonPioBank(config->pios.on);
        mask = PioCommonPioMask(config->pios.on);
        PanicNotZero(PioSetMapPins32Bank(bank, mask, mask));
        PanicNotZero(PioSetDir32Bank(bank, mask, mask));
        PanicNotZero(PioSet32Bank(bank, mask, mask));
    }

    /* Setup Interrupt as input with weak pull up */
    bank = PioCommonPioBank(config->pios.interrupt);
    mask = PioCommonPioMask(config->pios.interrupt);
    PanicNotZero(PioSetMapPins32Bank(bank, mask, mask));
    PanicNotZero(PioSetDir32Bank(bank, mask, 0));
    PanicNotZero(PioSet32Bank(bank, mask, mask));	

    for (i = 0; i < ARRAY_DIM(i2c_pios); i++)
    {
        uint16 pio = i2c_pios[i].pio;
        bank = PioCommonPioBank(pio);
        mask = PioCommonPioMask(pio);

        /* Setup I2C PIOs with strong pull-up */
        PanicNotZero(PioSetMapPins32Bank(bank, mask, 0));
        PanicFalse(PioSetFunction(pio, i2c_pios[i].func));
        PanicNotZero(PioSetDir32Bank(bank, mask, 0));
        PanicNotZero(PioSet32Bank(bank, mask, mask));
        PanicNotZero(PioSetStrongBias32Bank(bank, mask, mask));
    }

    /* Configure Bitserial to work with sy3088ps proximity sensor */
    memset(&bsconfig, 0, sizeof(bsconfig));
    bsconfig.mode = BITSERIAL_MODE_I2C_MASTER;
    bsconfig.clock_frequency_khz = config->i2c_clock_khz;
    bsconfig.u.i2c_cfg.i2c_address = I2C_ADDRESS_SY3088PS;
    return BitserialOpen((bitserial_block_index)BITSERIAL_BLOCK_1, &bsconfig);
}

/*! \brief Disable the proximity sensor */
static void sy3088psDisable(bitserial_handle handle, const proximityConfig *config)
{
    uint16 bank;
    uint32 mask;
    DEBUG_LOG_INFO("=====> sy3088psDisable");

    /* Disable interrupt and set weak pull down */
    bank = PioCommonPioBank(config->pios.interrupt);
    mask = PioCommonPioMask(config->pios.interrupt);
    PanicNotZero(PioSet32Bank(bank, mask, 0));

    /* Release bitserial instance */
    BitserialClose(handle);
    handle = BITSERIAL_HANDLE_ERROR;

    if (config->pios.on != SY3088PS_ON_PIO_UNUSED)
    {
        /* Power off the proximity sensor */
        PanicNotZero(PioSet32Bank(PioCommonPioBank(config->pios.on),
                                  PioCommonPioMask(config->pios.on),
                                  0));
    }
}

#if 0
static bool sy3088psReadProximityResult(bitserial_handle handle, uint16 *data)
{
    uint8 value;
    bool result = FALSE;

    result = sy3088psReadRegister(handle, SY3088PS_PROXIMITY_PXS_DATA, &value);
    DEBUG_LOG_INFO("sy3088psReadProximityResult, result %d, value 0x%x", result ,value);
    if (result)
        *data = value;

    return result;
}

static uint16 sy3088ps_ReadProximity(void)
{
	uint16 Proximity_Data = 0;
	
	proximityTaskData *prox = ProximityGetTaskData();

 	sy3088psReadProximityResult(prox->handle, &Proximity_Data);

    return Proximity_Data;
}
#endif
bool appProximityClientRegister(Task task)
{    
    proximityTaskData *prox = ProximityGetTaskData();

    DEBUG_LOG_INFO("=====> appProximityClientRegister in");
    if (NULL == prox->clients)
    {
    	volatile uint32 i;
        
        const proximityConfig *config = appConfigProximity();

        prox->config = config;
        prox->state = PanicUnlessNew(proximityState);
        prox->state->proximity = proximity_state_unknown;
        prox->clients = TaskList_Create();

        prox->handle = sy3088psEnable(config);
        PanicFalse(prox->handle != BITSERIAL_HANDLE_ERROR);

		/* you have to watting over 2.5ms after supply VDD to sensor */
        for(i = 0; i < 8000; i++){ ; }
        
        /* Run Proximity. it must be using CMD. Because of this, I was debug during three days. I wanna go home now.*/
		//PanicFalse(sy3088psWriting(prox->handle));		
		sy3088psWriting(prox->handle);

#ifdef DEBUG_PROXIMITY_REGISTER_READING
        for(i = 0; i < 800000; i++){ ; }		
	    //PanicFalse(sy3088psReading(prox->handle));
	    sy3088psReading(prox->handle);
#endif

#if 0
        PanicFalse(sy3088psStartReading(prox->handle));
        
        /* Interrupt Enable and FTN/NTF Enable*/
        PanicFalse(sy3088psEnableInterruptOnThreshold(prox->handle));

        /* Threadhold */
		PanicFalse(sy3088psSetHighThreshold(prox->handle, config->threshold_high));
		PanicFalse(sy3088psSetLowThreshold(prox->handle, config->threshold_low));

		/* Offset */
		PanicFalse(sy3088psSetOffsetLow(prox->handle));
		PanicFalse(sy3088psSetOffsetHigh(prox->handle));		
#endif
		/* you have to watting over 2.5ms after supply VDD to sensor */
        for(i = 0; i < 80000; i++){ ; }
        
        /* Register for interrupt events */
        prox->task.handler = sy3088psInterruptHandler;
        PioMonitorRegisterTask(&prox->task, config->pios.interrupt);
    }

    /* Send initial message to client */
    switch (prox->state->proximity)
    {
        case proximity_state_in_proximity:
            MessageSend(task, PROXIMITY_MESSAGE_IN_PROXIMITY, NULL);
            break;
        case proximity_state_not_in_proximity:
            MessageSend(task, PROXIMITY_MESSAGE_NOT_IN_PROXIMITY, NULL);
            break;
        case proximity_state_unknown:
            {
                const uint32 bank = PROXIMITY_PIO_INT / 32;
                const uint32 bank_pio_mask = 1UL << PROXIMITY_PIO_INT;
                const uint32 bank_state = PioGet32Bank(bank);         

                if(bank_state & bank_pio_mask)
                {
                	DEBUG_LOG_INFO("=====> proximity_state_in_proximity");
                    prox->state->proximity = proximity_state_in_proximity;
                    MessageSend(task, PROXIMITY_MESSAGE_IN_PROXIMITY, NULL);
                }
                else
                {
                	DEBUG_LOG_INFO("=====> proximity_state_not_in_proximity");
                    prox->state->proximity = proximity_state_not_in_proximity;
                    MessageSend(task, PROXIMITY_MESSAGE_NOT_IN_PROXIMITY, NULL);
                }
            }
        	break;
        default:
            /* The client will be informed after the first interrupt */
            break;
    }
	
    return TaskList_AddTask(prox->clients, task);
}

void appProximityClientUnregister(Task task)
{
	DEBUG_LOG_INFO("=====> appProximityClientUnregister in");
	
    proximityTaskData *prox = ProximityGetTaskData();
    
    TaskList_RemoveTask(prox->clients, task);
    if (0 == TaskList_Size(prox->clients))
    {
        TaskList_Destroy(prox->clients);
        prox->clients = NULL;

        free(prox->state);
        prox->state = NULL;

        PanicFalse(prox->handle != BITSERIAL_HANDLE_ERROR);

        /* Unregister for interrupt events */
        PioMonitorUnregisterTask(&prox->task, prox->config->pios.interrupt);

        /* Reset into lowest power mode in case the sensor is not powered off. */
        sy3088psDisable(prox->handle, prox->config);
        prox->handle = BITSERIAL_HANDLE_ERROR;
    }
}

/* This function is to switch on/off sensor for power saving*/
void appProximityEnableSensor(Task task, bool enable)
{
    UNUSED(task);
    UNUSED(enable);
}

#endif /* HAVE_SY3088PS */
#endif /* INCLUDE_PROXIMITY */
