/*!
    \copyright Copyright (c) 2021 Qualcomm Technologies International, Ltd.
        All Rights Reserved.
        Qualcomm Technologies International, Ltd. Confidential and Proprietary.
    \file chain_aanc_splitter_sco_tx_path.h
    \brief The chain_aanc_splitter_sco_tx_path chain.

    This file is generated by C:\Users\gaudio\Documents\1_HBS-FP8_r253.1____20210517_Patch\adk\tools\packages\chaingen\chaingen_mod\__init__.pyc.
*/

#ifndef _CHAIN_AANC_SPLITTER_SCO_TX_PATH_H__
#define _CHAIN_AANC_SPLITTER_SCO_TX_PATH_H__

/*!
\page chain_aanc_splitter_sco_tx_path
@startuml
    object OPR_AANC_SPLT_SCO_TX
    OPR_AANC_SPLT_SCO_TX : id = CAP_ID_SPLITTER
    object OPR_AANC_BASIC_PASS
    OPR_AANC_BASIC_PASS : id = CAP_ID_BASIC_PASS
    OPR_AANC_SPLT_SCO_TX "IN(0)"<-- "OUT(0)" OPR_AANC_BASIC_PASS
    object EPR_AANC_BASIC_PASS_IN #lightgreen
    OPR_AANC_BASIC_PASS "IN(0)" <-- EPR_AANC_BASIC_PASS_IN
    object EPR_SPLT_OUT_CVC_SEND #lightblue
    EPR_SPLT_OUT_CVC_SEND <-- "OUT1(0)" OPR_AANC_SPLT_SCO_TX
    object EPR_SPLT_OUT_ANC_FF #lightblue
    EPR_SPLT_OUT_ANC_FF <-- "OUT2(1)" OPR_AANC_SPLT_SCO_TX
@enduml
*/

#include <chain.h>

extern const chain_config_t chain_aanc_splitter_sco_tx_path_config;

#endif /* _CHAIN_AANC_SPLITTER_SCO_TX_PATH_H__ */

