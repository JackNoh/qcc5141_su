/*!
    \copyright Copyright (c) 2021 Qualcomm Technologies International, Ltd.
        All Rights Reserved.
        Qualcomm Technologies International, Ltd. Confidential and Proprietary.
    \file chain_aanc_resampler_sco_rx_path.c
    \brief The chain_aanc_resampler_sco_rx_path chain.

    This file is generated by C:\Users\gaudio\Documents\1_HBS-FP8_r253.1____20210517_Patch\adk\tools\packages\chaingen\chaingen_mod\__init__.pyc.
*/

#include <chain_aanc_resampler_sco_rx_path.h>
#include <cap_id_prim.h>
#include <opmsg_prim.h>
#include <hydra_macros.h>
#include <../earbud_cap_ids.h>
#include <kymera_chain_roles.h>
static const operator_config_t operators[] =
{
    MAKE_OPERATOR_CONFIG(CAP_ID_IIR_RESAMPLER, OPR_AANC_UP_SAMPLE),
} ;

static const operator_endpoint_t inputs[] =
{
    {OPR_AANC_UP_SAMPLE, EPR_IIR_RX_8K_IN1, 0},
} ;

static const operator_endpoint_t outputs[] =
{
    {OPR_AANC_UP_SAMPLE, EPR_IIR_RX_16K_OUT1, 0},
} ;

const chain_config_t chain_aanc_resampler_sco_rx_path_config = {1, 0, operators, 1, inputs, 1, outputs, 1, NULL, 0};

