/*!
    \copyright Copyright (c) 2021 Qualcomm Technologies International, Ltd.
        All Rights Reserved.
        Qualcomm Technologies International, Ltd. Confidential and Proprietary.
    \file chain_va_graph_manager.c
    \brief The chain_va_graph_manager chain.

    This file is generated by C:\Users\gaudio\Documents\1_HBS-FP8_r253.1____20210517_Patch\adk\tools\packages\chaingen\chaingen_mod\__init__.pyc.
*/

#include <chain_va_graph_manager.h>
#include <cap_id_prim.h>
#include <opmsg_prim.h>
#include <hydra_macros.h>
#include <../earbud_cap_ids.h>
#include <kymera_chain_roles.h>
static const operator_config_t operators[] =
{
    MAKE_OPERATOR_CONFIG(EB_CAP_ID_VA_GRAPH_MANAGER, OPR_VA_GRAPH_MANAGER),
} ;

const chain_config_t chain_va_graph_manager_config = {0, 0, operators, 1, NULL, 0, NULL, 0, NULL, 0};

