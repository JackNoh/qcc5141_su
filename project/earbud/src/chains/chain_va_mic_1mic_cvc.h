/*!
    \copyright Copyright (c) 2021 Qualcomm Technologies International, Ltd.
        All Rights Reserved.
        Qualcomm Technologies International, Ltd. Confidential and Proprietary.
    \file chain_va_mic_1mic_cvc.h
    \brief The chain_va_mic_1mic_cvc chain.

    This file is generated by C:\Users\gaudio\Documents\1_HBS-FP8_r253.1____20210517_Patch\adk\tools\packages\chaingen\chaingen_mod\__init__.pyc.
*/

#ifndef _CHAIN_VA_MIC_1MIC_CVC_H__
#define _CHAIN_VA_MIC_1MIC_CVC_H__

/*!
\page chain_va_mic_1mic_cvc
@startuml
    object OPR_CVC_SEND
    OPR_CVC_SEND : id = EB_CAP_ID_CVC_VA_1MIC
    object EPR_VA_MIC_AEC_IN #lightgreen
    OPR_CVC_SEND "AEC_IN(0)" <-- EPR_VA_MIC_AEC_IN
    object EPR_VA_MIC_MIC1_IN #lightgreen
    OPR_CVC_SEND "MIC1_IN(1)" <-- EPR_VA_MIC_MIC1_IN
    object EPR_VA_MIC_ENCODE_OUT #lightblue
    EPR_VA_MIC_ENCODE_OUT <-- "OUT(CVC_1MIC_VA_OUTPUT_TERMINAL)" OPR_CVC_SEND
@enduml
*/

#include <chain.h>

extern const chain_config_t chain_va_mic_1mic_cvc_config;

#endif /* _CHAIN_VA_MIC_1MIC_CVC_H__ */

