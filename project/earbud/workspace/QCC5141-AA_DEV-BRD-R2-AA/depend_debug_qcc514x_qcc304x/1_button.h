#ifndef BUTTON_CONFIG_H
#define BUTTON_CONFIG_H

#include "domain_message.h"
#include "input_event_manager.h"
extern const InputEventConfig_t input_event_config;

extern const InputActionMessage_t voice_assistant_message_group[6];
extern const InputActionMessage_t media_message_group[20];

#define SYS_CTRL             (1UL <<  0)

#define MIN_INPUT_ACTION_MESSAGE_ID (LOGICAL_INPUT_MESSAGE_BASE-5)
#define MAX_INPUT_ACTION_MESSAGE_ID (LOGICAL_INPUT_MESSAGE_BASE+54)

#define APP_BUTTON_HELD_FACTORY_RESET            (LOGICAL_INPUT_MESSAGE_BASE- 0x5)
#define APP_BUTTON_FORCE_RESET                   (LOGICAL_INPUT_MESSAGE_BASE- 0x4)
#define APP_BUTTON_FACTORY_RESET                 (LOGICAL_INPUT_MESSAGE_BASE- 0x3)
#define APP_BUTTON_HELD_FORCE_RESET              (LOGICAL_INPUT_MESSAGE_BASE- 0x2)
#define CAP_SENSE_FIVE_PRESS                     (LOGICAL_INPUT_MESSAGE_BASE- 0x1)
#define CAP_SENSE_TRIPLE_PRESS                   (LOGICAL_INPUT_MESSAGE_BASE+ 0x0)
#define CAP_SENSE_SINGLE_PRESS                   (LOGICAL_INPUT_MESSAGE_BASE+ 0x1)
#define APP_MFB_BUTTON_HELD_RELEASE_6SEC         (LOGICAL_INPUT_MESSAGE_BASE+ 0x2)
#define CAP_SENSE_SLIDE_LEFT                     (LOGICAL_INPUT_MESSAGE_BASE+ 0x3)
#define APP_MFB_BUTTON_HELD_9SEC                 (LOGICAL_INPUT_MESSAGE_BASE+ 0x4)
#define APP_MFB_BUTTON_HELD_6SEC                 (LOGICAL_INPUT_MESSAGE_BASE+ 0x5)
#define APP_VA_BUTTON_RELEASE                    (LOGICAL_INPUT_MESSAGE_BASE+ 0x6)
#define APP_LEAKTHROUGH_DISABLE                  (LOGICAL_INPUT_MESSAGE_BASE+ 0x7)
#define CAP_SENSE_VERY_VERY_LONG_PRESS           (LOGICAL_INPUT_MESSAGE_BASE+ 0x8)
#define APP_MFB_BUTTON_HELD_1SEC                 (LOGICAL_INPUT_MESSAGE_BASE+ 0x9)
#define APP_MFB_BUTTON_HELD_RELEASE_8SEC         (LOGICAL_INPUT_MESSAGE_BASE+ 0xa)
#define CAP_SENSE_HAND_COVER                     (LOGICAL_INPUT_MESSAGE_BASE+ 0xb)
#define CAP_SENSE_NINE_PRESS                     (LOGICAL_INPUT_MESSAGE_BASE+ 0xc)
#define CAP_SENSE_FIVE_PRESS_HOLD                (LOGICAL_INPUT_MESSAGE_BASE+ 0xd)
#define APP_MESSAGE_IGNORE                       (LOGICAL_INPUT_MESSAGE_BASE+ 0xe)
#define APP_MFB_BUTTON_HELD_RELEASE_9SEC         (LOGICAL_INPUT_MESSAGE_BASE+ 0xf)
#define APP_MFB_BUTTON_HELD_8SEC                 (LOGICAL_INPUT_MESSAGE_BASE+0x10)
#define CAP_SENSE_HAND_COVER_RELEASE             (LOGICAL_INPUT_MESSAGE_BASE+0x11)
#define CAP_SENSE_LONG_PRESS_RELEASE             (LOGICAL_INPUT_MESSAGE_BASE+0x12)
#define CAP_SENSE_SEVEN_PRESS                    (LOGICAL_INPUT_MESSAGE_BASE+0x13)
#define CAP_SENSE_DOUBLE_PRESS                   (LOGICAL_INPUT_MESSAGE_BASE+0x14)
#define CAP_SENSE_DOUBLE_PRESS_HOLD              (LOGICAL_INPUT_MESSAGE_BASE+0x15)
#define APP_VA_BUTTON_DOWN                       (LOGICAL_INPUT_MESSAGE_BASE+0x16)
#define CAP_SENSE_TRIPLE_PRESS_HOLD              (LOGICAL_INPUT_MESSAGE_BASE+0x17)
#define APP_LEAKTHROUGH_ENABLE                   (LOGICAL_INPUT_MESSAGE_BASE+0x18)
#define CAP_SENSE_SIX_PRESS                      (LOGICAL_INPUT_MESSAGE_BASE+0x19)
#define APP_ANC_TOGGLE_ON_OFF                    (LOGICAL_INPUT_MESSAGE_BASE+0x1a)
#define CAP_SENSE_DOUBLE_PRESS_HOLD_RELEASE      (LOGICAL_INPUT_MESSAGE_BASE+0x1b)
#define APP_BUTTON_BACKWARD                      (LOGICAL_INPUT_MESSAGE_BASE+0x1c)
#define APP_ANC_ENABLE                           (LOGICAL_INPUT_MESSAGE_BASE+0x1d)
#define CAP_SENSE_SLIDE_DOWN                     (LOGICAL_INPUT_MESSAGE_BASE+0x1e)
#define CAP_SENSE_FOUR_PRESS                     (LOGICAL_INPUT_MESSAGE_BASE+0x1f)
#define CAP_SENSE_EIGHT_PRESS                    (LOGICAL_INPUT_MESSAGE_BASE+0x20)
#define CAP_SENSE_VERY_LONG_PRESS                (LOGICAL_INPUT_MESSAGE_BASE+0x21)
#define APP_MFB_BUTTON_SINGLE_CLICK              (LOGICAL_INPUT_MESSAGE_BASE+0x22)
#define APP_ANC_SET_NEXT_MODE                    (LOGICAL_INPUT_MESSAGE_BASE+0x23)
#define APP_LEAKTHROUGH_TOGGLE_ON_OFF            (LOGICAL_INPUT_MESSAGE_BASE+0x24)
#define APP_ANC_DISABLE                          (LOGICAL_INPUT_MESSAGE_BASE+0x25)
#define CAP_SENSE_FOUR_PRESS_HOLD                (LOGICAL_INPUT_MESSAGE_BASE+0x26)
#define APP_VA_BUTTON_HELD_1SEC                  (LOGICAL_INPUT_MESSAGE_BASE+0x27)
#define APP_BUTTON_DFU                           (LOGICAL_INPUT_MESSAGE_BASE+0x28)
#define CAP_SENSE_SLIDE_UP                       (LOGICAL_INPUT_MESSAGE_BASE+0x29)
#define APP_BUTTON_FORWARD                       (LOGICAL_INPUT_MESSAGE_BASE+0x2a)
#define APP_LEAKTHROUGH_SET_NEXT_MODE            (LOGICAL_INPUT_MESSAGE_BASE+0x2b)
#define CAP_SENSE_SLIDE_RIGHT                    (LOGICAL_INPUT_MESSAGE_BASE+0x2c)
#define CAP_SENSE_LONG_PRESS                     (LOGICAL_INPUT_MESSAGE_BASE+0x2d)
#define APP_MFB_BUTTON_HELD_RELEASE_3SEC         (LOGICAL_INPUT_MESSAGE_BASE+0x2e)
#define APP_VA_BUTTON_SINGLE_CLICK               (LOGICAL_INPUT_MESSAGE_BASE+0x2f)
#define APP_VA_BUTTON_DOUBLE_CLICK               (LOGICAL_INPUT_MESSAGE_BASE+0x30)
#define APP_MFB_BUTTON_HELD_RELEASE_1SEC         (LOGICAL_INPUT_MESSAGE_BASE+0x31)
#define APP_BUTTON_HELD_FACTORY_RESET_CANCEL     (LOGICAL_INPUT_MESSAGE_BASE+0x32)
#define APP_MFB_BUTTON_HELD_3SEC                 (LOGICAL_INPUT_MESSAGE_BASE+0x33)
#define APP_VA_BUTTON_HELD_RELEASE               (LOGICAL_INPUT_MESSAGE_BASE+0x34)
#define CAP_SENSE_VERY_VERY_VERY_LONG_PRESS      (LOGICAL_INPUT_MESSAGE_BASE+0x35)
#define APP_BUTTON_HELD_DFU                      (LOGICAL_INPUT_MESSAGE_BASE+0x36)

#endif

