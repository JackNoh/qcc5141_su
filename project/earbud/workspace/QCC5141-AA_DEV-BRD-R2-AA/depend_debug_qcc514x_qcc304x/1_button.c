#include "input_event_manager.h"
#include "1_button.h"

const InputEventConfig_t input_event_config = 
{
	/* Table to convert from PIO to input event ID*/
	{
		-1, -1,  0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
	},

	/* Masks for each PIO bank to configure as inputs */
	{ 0x00000004UL, 0x00000000UL, 0x00000000UL },
	/* PIO debounce settings */
	4, 5
};


const InputActionMessage_t voice_assistant_message_group[6] = 
{
	{
		0,                                      /* Input event bits */
		SYS_CTRL,                               /* Input event mask */
		ENTER,                                  /* Action */
		0,                                      /* Timeout */
		0,                                      /* Repeat */
		APP_VA_BUTTON_DOWN,                     /* Message */
	},
	{
		0,                                      /* Input event bits */
		SYS_CTRL,                               /* Input event mask */
		HELD,                                   /* Action */
		1000,                                   /* Timeout */
		0,                                      /* Repeat */
		APP_VA_BUTTON_HELD_1SEC,                /* Message */
	},
	{
		0,                                      /* Input event bits */
		SYS_CTRL,                               /* Input event mask */
		SINGLE_CLICK,                           /* Action */
		0,                                      /* Timeout */
		0,                                      /* Repeat */
		APP_VA_BUTTON_SINGLE_CLICK,             /* Message */
	},
	{
		0,                                      /* Input event bits */
		SYS_CTRL,                               /* Input event mask */
		HELD_RELEASE,                           /* Action */
		1000,                                   /* Timeout */
		0,                                      /* Repeat */
		APP_VA_BUTTON_HELD_RELEASE,             /* Message */
	},
	{
		0,                                      /* Input event bits */
		SYS_CTRL,                               /* Input event mask */
		DOUBLE_CLICK,                           /* Action */
		0,                                      /* Timeout */
		0,                                      /* Repeat */
		APP_VA_BUTTON_DOUBLE_CLICK,             /* Message */
	},
	{
		0,                                      /* Input event bits */
		SYS_CTRL,                               /* Input event mask */
		RELEASE,                                /* Action */
		0,                                      /* Timeout */
		0,                                      /* Repeat */
		APP_VA_BUTTON_RELEASE,                  /* Message */
	},
};

const InputActionMessage_t media_message_group[20] = 
{
	{
		0,                                      /* Input event bits */
		SYS_CTRL,                               /* Input event mask */
		HELD,                                   /* Action */
		9000,                                   /* Timeout */
		0,                                      /* Repeat */
		APP_MFB_BUTTON_HELD_9SEC,               /* Message */
	},
	{
		0,                                      /* Input event bits */
		SYS_CTRL,                               /* Input event mask */
		HELD,                                   /* Action */
		6000,                                   /* Timeout */
		0,                                      /* Repeat */
		APP_MFB_BUTTON_HELD_6SEC,               /* Message */
	},
	{
		0,                                      /* Input event bits */
		SYS_CTRL,                               /* Input event mask */
		HELD,                                   /* Action */
		1000,                                   /* Timeout */
		0,                                      /* Repeat */
		APP_MFB_BUTTON_HELD_1SEC,               /* Message */
	},
	{
		0,                                      /* Input event bits */
		SYS_CTRL,                               /* Input event mask */
		HELD,                                   /* Action */
		8000,                                   /* Timeout */
		0,                                      /* Repeat */
		APP_MFB_BUTTON_HELD_8SEC,               /* Message */
	},
	{
		0,                                      /* Input event bits */
		SYS_CTRL,                               /* Input event mask */
		HELD,                                   /* Action */
		15000,                                  /* Timeout */
		0,                                      /* Repeat */
		APP_BUTTON_HELD_FACTORY_RESET,          /* Message */
	},
	{
		0,                                      /* Input event bits */
		SYS_CTRL,                               /* Input event mask */
		HELD,                                   /* Action */
		3000,                                   /* Timeout */
		0,                                      /* Repeat */
		APP_MFB_BUTTON_HELD_3SEC,               /* Message */
	},
	{
		0,                                      /* Input event bits */
		SYS_CTRL,                               /* Input event mask */
		HELD,                                   /* Action */
		30000,                                  /* Timeout */
		0,                                      /* Repeat */
		APP_BUTTON_HELD_FORCE_RESET,            /* Message */
	},
	{
		0,                                      /* Input event bits */
		SYS_CTRL,                               /* Input event mask */
		HELD,                                   /* Action */
		12000,                                  /* Timeout */
		0,                                      /* Repeat */
		APP_BUTTON_HELD_DFU,                    /* Message */
	},
	{
		0,                                      /* Input event bits */
		SYS_CTRL,                               /* Input event mask */
		SINGLE_CLICK,                           /* Action */
		0,                                      /* Timeout */
		0,                                      /* Repeat */
		APP_MFB_BUTTON_SINGLE_CLICK,            /* Message */
	},
	{
		0,                                      /* Input event bits */
		SYS_CTRL,                               /* Input event mask */
		HELD_RELEASE,                           /* Action */
		6000,                                   /* Timeout */
		0,                                      /* Repeat */
		APP_MFB_BUTTON_HELD_RELEASE_6SEC,       /* Message */
	},
	{
		0,                                      /* Input event bits */
		SYS_CTRL,                               /* Input event mask */
		HELD_RELEASE,                           /* Action */
		8000,                                   /* Timeout */
		0,                                      /* Repeat */
		APP_MFB_BUTTON_HELD_RELEASE_8SEC,       /* Message */
	},
	{
		0,                                      /* Input event bits */
		SYS_CTRL,                               /* Input event mask */
		HELD_RELEASE,                           /* Action */
		15000,                                  /* Timeout */
		0,                                      /* Repeat */
		APP_BUTTON_FACTORY_RESET,               /* Message */
	},
	{
		0,                                      /* Input event bits */
		SYS_CTRL,                               /* Input event mask */
		HELD_RELEASE,                           /* Action */
		9000,                                   /* Timeout */
		0,                                      /* Repeat */
		APP_MFB_BUTTON_HELD_RELEASE_9SEC,       /* Message */
	},
	{
		0,                                      /* Input event bits */
		SYS_CTRL,                               /* Input event mask */
		HELD_RELEASE,                           /* Action */
		500,                                    /* Timeout */
		0,                                      /* Repeat */
		APP_ANC_TOGGLE_ON_OFF,                  /* Message */
	},
	{
		0,                                      /* Input event bits */
		SYS_CTRL,                               /* Input event mask */
		HELD_RELEASE,                           /* Action */
		1500,                                   /* Timeout */
		0,                                      /* Repeat */
		APP_ANC_SET_NEXT_MODE,                  /* Message */
	},
	{
		0,                                      /* Input event bits */
		SYS_CTRL,                               /* Input event mask */
		HELD_RELEASE,                           /* Action */
		12000,                                  /* Timeout */
		0,                                      /* Repeat */
		APP_BUTTON_DFU,                         /* Message */
	},
	{
		0,                                      /* Input event bits */
		SYS_CTRL,                               /* Input event mask */
		HELD_RELEASE,                           /* Action */
		3000,                                   /* Timeout */
		0,                                      /* Repeat */
		APP_MFB_BUTTON_HELD_RELEASE_3SEC,       /* Message */
	},
	{
		0,                                      /* Input event bits */
		SYS_CTRL,                               /* Input event mask */
		HELD_RELEASE,                           /* Action */
		30000,                                  /* Timeout */
		0,                                      /* Repeat */
		APP_BUTTON_FORCE_RESET,                 /* Message */
	},
	{
		0,                                      /* Input event bits */
		SYS_CTRL,                               /* Input event mask */
		HELD_RELEASE,                           /* Action */
		1000,                                   /* Timeout */
		0,                                      /* Repeat */
		APP_MFB_BUTTON_HELD_RELEASE_1SEC,       /* Message */
	},
	{
		0,                                      /* Input event bits */
		SYS_CTRL,                               /* Input event mask */
		HELD_RELEASE,                           /* Action */
		2000,                                   /* Timeout */
		0,                                      /* Repeat */
		APP_LEAKTHROUGH_TOGGLE_ON_OFF,          /* Message */
	},
};


ASSERT_MESSAGE_GROUP_NOT_OVERFLOWED(LOGICAL_INPUT,MAX_INPUT_ACTION_MESSAGE_ID)

