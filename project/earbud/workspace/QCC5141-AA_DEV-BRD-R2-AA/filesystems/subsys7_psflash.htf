file=app1

#############
# USER KEYS #
#############
# These keys are used to configure the software or to save settings.
# See the corresponding adk\src\domains\common\ps_key_map.h


#USR1 - This key is reserved. It is used to save the last volume for HFP.
#       See hfp_profile.

# USR2 Setting for earbud to have a fixed role. 
# If this key is not set then the earbud will be able to switch roles (recommended)
# If fixed roles are to be used each earbud pair must have one primary and one secondary. 
# This setting is currently only supported on ROM based QCC512x parts.
#USR2 = [ 01 00 ] #Uncomment to set the earbud to always be a primary device
#USR2 = [ 02 00 ] #Uncomment to set the earbud to always be a secondary device
#USR2 = [ 03 00 ] #Uncomment to set the earbud to use a fixed role. Right earbud will be primary.

# USR3 Setting for the earbud to enable device testing. 
# If this key is not set, or is zero, then the device test service is not available.
# Setting to a non-zero value means the earbud will enter device test mode on boot,
# and WILL NOT enter normal operation.
# Setting [ 01 00 ] enables test mode with SPP interface.
# Setting [ 02 00 ] enables test mode with no SPP interface and LE enabled.
#USR3 = [ 01 00 ] # Uncomment to enable device test mode

#USR4 = This key is reserved to store reboot action. 
#		See system_reboot.c

# USR5 - This key is used to set Fast Pair Model ID for an audio sink device. Stored in Little Endian format.
#USR5 = [ 79 02 B5 00 ]
#FEATURE_BLUECOM - HBS-TFP8 0x8E14D7
#USR5 = [ D7 14 8E 00 ]
#FEATURE_BLUECOM - HBS-FP8 0x1CEE9C
#USR5 = [ 9C EE 1C 00 ]
#FEATURE_BLUECOM - HBS-FP9 0x3361A1
USR5 = [ A1 61 33 00 ]


# USR6 - This key is used to set Scrambled ASPK for an audio sink device. Stored in Little Endian format.
#USR6 = [ DC 74 6D 04 73 93 E2 AC 9E D2 1C C3 C4 6D 4C 1A 86 C3 89 EC 16 26 B9 79 D4 DE 3F A0 CB F6 96 19 ]
#FEATURE_BLUECOM - HBS-TFP8
#USR6 = [ 0D 50 00 2B 4E 6D 86 92 D7 62 1E AE 34 BF 57 F6 25 99 53 9C F1 2D 4D EF 8E 62 23 E8 80 76 66 CF ]
#FEATURE_BLUECOM - HBS-FP8
#USR6 = [ 4B D6 98 F1 8F 26 4F CB 15 3D 9E 4D 82 36 B5 D1 D2 D1 EC C2 72 DD D0 FF 11 AA 5E AC 44 C8 1A 35 ]
#FEATURE_BLUECOM - HBS-FP9
USR6 = [ 87 3A 28 2B 72 66 BD 0F 6F 5B E0 F1 CF 76 DE B1 F8 46 BF 15 F6 B5 46 46 9A D8 78 4E 52 48 47 97 ]

#USR7 - This key is reserved. It is used to save the status for device upgrade
#       See upgrade.h

#USR8 - GAA model ID: A 32-bit unsigned number assigned by Google to identify the device to the Google Assistant Accessory (GAA).
#The bytes in each 16-bit pair are reversed, so for example, for the Qualcomm ADK earbuds GAA model ID of 0xB50279 the sequence
#is [ B5 00 79 02 ].
USR8 = [ B5 00 79 02 ]

#USR10 - GAA OTA test control: A 16-bit value used to control GAA OTA testing. At the moment if it is absent or FALSE (0) then
#no GAA OTA test control is done. If it is present and non-zero (TRUE) then silent commit is not supported and the GAA OTA will
#perform an immediate commit on apply. In future this may be bit-mapped to provide separate controls of other aspects.
#USR10 = [ 01 00 ] #Uncomment to enable GAA OTA test control

# USR50 Version of ps key layout. Can be used in future to simplify upgrade.
USR50 = [ 01 00 ]

# USR80 Setting for testing AV Codec in test mode.
# Note: Needs TEST_AV_CODEC_PSKEY defined within the application
#
#   bit0 - SBC
#   bit1 - AAC
#   bit2 - aptX
#   bit3 - aptX-Adaptive
#   bit4 - aptX-HD
#
#USR80 = [ 01 00 ] # Uncomment to enable use of the test AV Codec

# USR81 Setting for testing HFP Codec in test mode.
# Note: Needs TEST_HFP_CODEC_PSKEY defined within the application
#
#   bit0 - NB
#   bit1 - WB
#   bit2 - SWB
#USR81 = [ 03 00 ] # Uncomment to enable use of the test HFP Codec
